<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Course;
use App\Models\Audience;
use App\Models\Tag;
use Livewire\WithPagination;


class CourseFilter extends Component
{
    use WithPagination;

    #Atributos
    public $audiences = [];
    public $categories = [];
    public $levels = [];
    public $statuses = [];
    public $role;
    public $teacher_id;
    public int $page = 1;
    public $search = '';

    // Agregar propiedades para las listas de audiencias y etiquetas
    public $audiences_list = []; // Inicializa la propiedad
    public $tags_list = [];      // Inicializa la propiedad


    public function mount()
    {
        $this->audiences_list = Audience::all();
        $this->tags_list = Tag::all();
        $this->role = session()->get('role');
        $this->teacher_id = session()->get('teacher_id');
    }

    public function updatedSearch()
    {
        // Reiniciar paginación cuando cambia el término de búsqueda
        $this->resetPage();  
    }

    public function render()
    {
        $coursesQuery = Course::withCount('modules')->orderBy('updated_at', 'desc');

        if ($this->role === 'teacher' && $this->teacher_id) {
            $coursesQuery->whereHas('teachers', function ($query) {
                $query->where('teacher_id', $this->teacher_id);
            });
        }

        if (!empty($this->statuses)) {
            $coursesQuery->whereIn('state', $this->statuses);
        }

        // Filtrar por audiencias (usando IDs directamente)
        if (!empty($this->audiences)) {
            $coursesQuery->whereHas('audiences', function ($query) {
                $query->whereIn('audiences.id', $this->audiences);
            });
        }

        // Filtrar por etiquetas (usando IDs directamente)
        if (!empty($this->categories)) {
            $coursesQuery->whereHas('tags', function ($query) {
                $query->whereIn('tags.id', $this->categories);
            });
        }

        if (!empty($this->levels)) {
            $coursesQuery->whereIn('level', $this->levels);
        }

        if (!empty($this->search)) {
            $coursesQuery->whereRaw('LOWER(name) LIKE ?', ['%' . strtolower($this->search) . '%']);
        }

        // Agregamos paginación
        $courses = $coursesQuery->paginate(12);

        return view('livewire.course-filter', [
            'courses' => $courses,
        ]);
    }
}
