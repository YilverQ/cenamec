<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Course;
use App\Models\Audience;
use App\Models\Tag;
use Livewire\WithPagination;

class StudentCourse extends Component
{
    use WithPagination;

    #Atributos
    public $audiences = [];
    public $categories = [];
    public $levels = [];
    public $courseStatus = [];
    public int $page = 1;
    public $search = '';

    // Agregar propiedades para las listas de audiencias y etiquetas
    public $audiences_list = []; // Inicializa la propiedad
    public $tags_list = [];      // Inicializa la propiedad

    public function mount()
    {
        $this->audiences_list = Audience::whereHas('courses', function ($query) {
            $query->where('state', 'Publicado');
        })->get();
        $this->tags_list = Tag::whereHas('courses', function ($query) {
            $query->where('state', 'Publicado');
        })->get();
    }

    public function updatedSearch()
    {
        // Reiniciar paginación cuando cambia el término de búsqueda
        $this->resetPage();  
    }

    public function render()
    {
        // Obtener el ID del estudiante de la sesión
        $student_id = request()->session()->get('student_id');

        // Nos traemos los cursos con estado "publicado"
        $coursesQuery = Course::withCount('modules')
            ->where('state', 'Publicado');

        // Agregar un campo temporal para ordenar por inscripción
        $coursesQuery->selectRaw('courses.*, 
            CASE WHEN EXISTS (
                SELECT 1 FROM course_student 
                WHERE course_student.course_id = courses.id 
                AND course_student.student_id = ?
            ) THEN 1 ELSE 0 END AS is_enrolled', [$student_id]);

        // Ordenar primero por inscripción (1 primero, 0 después) y luego por fecha de actualización
        $coursesQuery->orderByDesc('is_enrolled')
                     ->orderByDesc('updated_at');

        // Filtrar por estado de inscripción
        if (!empty($this->courseStatus)) {
            if (in_array('Inscrito', $this->courseStatus) && in_array('No Inscrito', $this->courseStatus)) {
                // No aplicamos filtro, traemos todos los cursos
            } else {
                if (in_array('Inscrito', $this->courseStatus)) {
                    $coursesQuery->whereHas('students', function ($query) use ($student_id) {
                        $query->where('student_id', $student_id);
                    });
                }

                if (in_array('No Inscrito', $this->courseStatus)) {
                    $coursesQuery->whereDoesntHave('students', function ($query) use ($student_id) {
                        $query->where('student_id', $student_id);
                    });
                }
            }
        }

        // Filtrar por audiencias (usando IDs directamente)
        if (!empty($this->audiences)) {
            $coursesQuery->whereHas('audiences', function ($query) {
                $query->whereIn('audiences.id', $this->audiences);
            });
        }

        // Filtrar por etiquetas (usando IDs directamente)
        if (!empty($this->categories)) {
            $coursesQuery->whereHas('tags', function ($query) {
                $query->whereIn('tags.id', $this->categories);
            });
        }

        if (!empty($this->levels)) {
            $coursesQuery->whereIn('level', $this->levels);
        }

        if (!empty($this->search)) {
            $coursesQuery->whereRaw('LOWER(name) LIKE ?', ['%' . strtolower($this->search) . '%']);
        }

        // Paginar los resultados
        $courses = $coursesQuery->paginate(12);

        // Agregar la propiedad `isEnrolled` a cada curso
        foreach ($courses as $course) {
            $course->isEnrolled = $course->students->contains($student_id);
        }

        return view('livewire.student-course', [
            'courses' => $courses,
        ]);
    }
}
