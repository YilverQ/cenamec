<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\User;
use App\Models\Course;
use App\Models\Module;
use App\Models\Likert;
use App\Models\Content;
use App\Models\Certificate;
use App\Models\ViewContent;


class Student extends Model
{
    use HasFactory;

    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = [];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function user ()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function certificates ()
    {
        return $this->hasMany(Certificate::class);
    }


    /**
     * Relationship. 
     * Many to many
    **/
    public function courses ()
    {
        return $this->belongsToMany(Course::class, 
                                'course_student', 
                                'student_id', 
                                'course_id');
    }
    public function courses_pivot()
    {
        return $this->belongsToMany(Course::class, 
                                      'course_student',
                                      'student_id', 
                                      'course_id')
                    ->withPivot('dateFinished', 'dateStart', 'last_connection') 
                    ->withTimestamps(); // Si deseas que Laravel maneje automáticamente los timestamps
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class, 'module_student')
                        ->withPivot('state', 'percentage', 'start_date', 'end_date')
                        ->withTimestamps();
    }
    public function modulesWithPercentage ()
    {
        return $this->belongsToMany(Module::class)
                    ->withPivot('percentage');
    }
    public function modules_pivot()
    {
        return $this->belongsToMany(Module::class, 'module_student')
                    ->withPivot('state', 'percentage')
                    ->withTimestamps();
    }
    public function countModulesWithEndDate()
    {
        return $this->modules()->wherePivotNotNull('end_date');
    }

    public function contents()
    {
        return $this->belongsToMany(Content::class, 'content_student')
                    ->withPivot('state')
                    ->withTimestamps();
    }

    public function viewContents()
    {
        return $this->hasMany(ViewContent::class, 'student_id');
    }

    public function likerts()
    {
        return $this->belongsToMany(Likert::class, 'student_likert')
                    ->withPivot('selection')
                    ->withTimestamps(); 
    }




    /*Para conocer el progreso del estudiante en relación a los módulos y contenidos*/
    public function currentModule($courseId)
    {
        // Obtener el último módulo activo o en progreso
        $module = $this->modules()
                       ->whereHas('course', function ($query) use ($courseId) {
                           $query->where('id', $courseId); // Filtra por el ID del curso
                       })
                       ->where(function ($query) {
                           $query->where('module_student.state', 'active') // Módulos activos
                                 ->orWhere('module_student.state', 'finished'); // O módulos finalizados
                       })
                       ->orderBy('level', 'desc') // Ordena por el nivel del módulo
                       ->first();
        return $module;
    }
}
