<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Answer;


class AnswerTableItem extends Model
{
    use HasFactory;
    protected $table = 'answer_table_item';
    protected $fillable = ['column', 'description', 'is_correct'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
