<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Content;
use App\Models\Module;

class Note extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['title', 'content', 'level', 'content_id'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function contentTable ()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }


    /**
     * Acceso directo al módulo desde la nota.
     **/
    public function module()
    {
        return $this->hasOneThrough(Module::class, Content::class, 'id', 'id', 'content_id', 'modulo_id');
    }
}
