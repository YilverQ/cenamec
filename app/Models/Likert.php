<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Autoevaluation;

class Likert extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['affirmation'];

    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function autoevaluation ()
    {
        return $this->belongsTo(Autoevaluation::class);
    }

    /**
     * Relationship. 
     * Many to many
    **/
    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_likert')
                    ->withPivot('selection')
                    ->withTimestamps(); 
    }
}
