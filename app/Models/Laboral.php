<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;


class Laboral extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_intitution',
        'type',
        'area',
        'position',
        'year_entry',
        'year_exit',
        'user_id',
    ];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function user()
    {
        return $this->belongsTo(User::class, 'id_usuario');
    }
}
