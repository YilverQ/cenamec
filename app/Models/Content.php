<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Note;
use App\Models\Module;
use App\Models\Student;
use App\Models\ViewContent;
use App\Models\Autoevaluation;
use App\Models\InteractiveEvaluation;

class Content extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['orden', 'type'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function module ()
    {
        return $this->belongsTo(Module::class, 'modulo_id');
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function note ()
    {
        return $this->hasOne(Note::class);
    }

    public function autoevaluation ()
    {
        return $this->hasOne(Autoevaluation::class);
    }

    public function interactiveEvaluation ()
    {
        return $this->hasOne(InteractiveEvaluation::class);
    }

    /**
     * Relationship. 
     * Many to many
    **/ 
    public function students()
    {
        return $this->belongsToMany(Student::class, 'content_student')
                    ->withPivot('state')
                    ->withTimestamps();
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function viewContents()
    {
        return $this->hasMany(ViewContent::class, 'content_id');
    }
}
