<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Answer;


class AnswerSequence extends Model
{
    use HasFactory;
    protected $table = 'answer_sequence';
    protected $fillable = ['orden', 'is_correct'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
