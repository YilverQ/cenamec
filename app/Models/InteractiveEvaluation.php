<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Content;
use App\Models\ItemEvaluation;

class InteractiveEvaluation extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = [];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function content ()
    {
        return $this->belongsTo(Content::class);
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function itemEvaluations ()
    {
        return $this->hasMany(ItemEvaluation::class);
    }

    public function getShuffledItemEvaluations()
    {
        return $this->itemEvaluations->shuffle();
    }
}
