<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Answer;


class AnswerShortAnswer extends Model
{
    use HasFactory;
    protected $table = 'answer_short_answer';
    protected $fillable = ['selection', 'is_correct'];

    // Relación 1:1 con Answer
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
