<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Answer;


class AnswerMatching extends Model
{
    use HasFactory;
    protected $table = 'answer_matching';
    protected $fillable = ['elementA', 'elementB', 'is_correct'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
