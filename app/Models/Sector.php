<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Academic;


class Sector extends Model
{
    use HasFactory;

    protected $fillable = ['name'];


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function academics ()
    {
        return $this->hasMany(Academic::class);
    }
}
