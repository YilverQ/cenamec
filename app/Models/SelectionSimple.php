<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\ItemEvaluation;

class SelectionSimple extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['answer', 'bad1', 'bad2', 'bad3'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function itemEvaluation ()
    {
        return $this->belongsTo(ItemEvaluation::class);
    }
}
