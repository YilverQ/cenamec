<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewContent extends Model
{
    use HasFactory;

    protected $table = 'view_content';

    protected $fillable = ['start_view', 'end_view', 'time_view', 'student_id', 'content_id', 'mean'];


    /**
     * Relationship. 
     * Many to many
    **/
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function content()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
