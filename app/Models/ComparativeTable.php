<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\ItemEvaluation;
use App\Models\TableItem;

class ComparativeTable extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['columnA', 'columnB', 'columnC'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function itemEvaluation ()
    {
        return $this->belongsTo(ItemEvaluation::class);
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function tableItems ()
    {
        return $this->hasMany(TableItem::class);
    }
    public function getItemsByColumn($column)
    {
        return $this->tableItems()->where('column', $column)->get();
    }
    public function getItemsByColumnAsArray($column)
    {
        return $this->tableItems()->where('column', $column)->pluck('id')->toArray();
    }
}
