<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/*Modelos*/
use App\Models\Parishe;
use App\Models\Teacher;
use App\Models\Student;
use App\Models\Laboral;
use App\Models\Academic;
use App\Models\Profileimg;
use App\Models\Administrator;


class User extends Model
{
    use HasFactory;

    protected $fillable = [ 'firts_name', 
                            'lastname',
                            'gender',
                            'birthdate',
                            'nacionality', 
                            'identification_card', 
                            'number_phone',
                            'email', 
                            'password',
                            'disabled',
                            'last_connection'];

    protected $hidden = ['password'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function parishe ()
    {
        return $this->belongsTo(Parishe::class);
    }

    public function profileimg ()
    {
        return $this->belongsTo(Profileimg::class);
    }


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function administrator()
    {
        return $this->hasOne(Administrator::class);
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }

    public function student()
    {
        return $this->hasOne(Student::class);
    }


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function academics ()
    {
        return $this->hasMany(Academic::class);
    }

    public function laborals ()
    {
        return $this->hasMany(Laboral::class);
    }
}
