<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/*Modelos*/
use App\Models\User;
use App\Models\Area;
use App\Models\Career;
use App\Models\Sector;
use App\Models\Institution;
use App\Models\Academiclevel;


class Academic extends Model
{
    use HasFactory;

    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['year_entry', 'year_exit', 'academic_title'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function institution ()
    {
        return $this->belongsTo(Institution::class);
    }

    public function academiclevel ()
    {
        return $this->belongsTo(Academiclevel::class);
    }

    public function career ()
    {
        return $this->belongsTo(Career::class);
    }

    public function area ()
    {
        return $this->belongsTo(Area::class);
    }

    public function sector ()
    {
        return $this->belongsTo(Sector::class);
    }
}
