<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\Answer;


class AnswerTrueFalse extends Model
{
    use HasFactory;
    protected $table = 'answer_true_false';
    protected $fillable = ['selection', 'is_correct'];


    /**
     * Relationship. 
     * One to One - Inverse
    **/
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
