<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\InteractiveEvaluation;
use App\Models\TrueFalse;
use App\Models\SelectionSimple;
use App\Models\ShortAnswer;
use App\Models\ComparativeTable;
use App\Models\Sequence;
use App\Models\Matching;

class ItemEvaluation extends Model
{
    use HasFactory;
    
    /**
     * protected $fillable  = [array_fields] : Para definir los campos que se pueden cargar.
     * protected $hidden    = [array_fields] : Para definir los campos que no son visibles.
    **/
    protected $fillable = ['statement', 'feedback', 'orden', 'type'];


    /**
     * Relationship. 
     * One to Many - Inverse
    **/
    public function interactiveEvaluation ()
    {
        return $this->belongsTo(InteractiveEvaluation::class, 'interactive_evaluation_id');
    }

    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function shortAnswers()
    {
        return $this->hasMany(ShortAnswer::class);
    }
    public function shortAnswersAcceptedWords()
    {
        return $this->shortAnswers()->pluck('word')->toArray(); // Devuelve un array de palabras
    }

    public function sequences()
    {
        // Ordenar por el campo 'orden'
        return $this->hasMany(Sequence::class)->orderBy('orden'); 
    }

    public function correctSequenceId()
    {
        return $this->sequences()->orderBy('orden')->pluck('id');
    }


    public function matchings ()
    {
        return $this->hasMany(Matching::class);
    }


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function trueOrFalse ()
    {
        return $this->hasOne(TrueFalse::class);
    }

    public function selectionSimple ()
    {
        return $this->hasOne(SelectionSimple::class);
    }

    public function comparativeTable ()
    {
        return $this->hasOne(ComparativeTable::class);
    }
}
