<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/*Modelos*/
use App\Models\AnswerSequence;
use App\Models\AnswerMatching;
use App\Models\AnswerTableItem;
use App\Models\AnswerTrueFalse;
use App\Models\AnswerShortAnswer;
use App\Models\AnswerSelectionSimple;


class Answer extends Model
{
    use HasFactory;

    protected $fillable = ['is_correct', 'percentage'];


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function selectionSimple()
    {
        return $this->hasOne(AnswerSelectionSimple::class);
    }

    public function trueFalse()
    {
        return $this->hasOne(AnswerTrueFalse::class);
    }

    public function shortAnswer()
    {
        return $this->hasOne(AnswerShortAnswer::class);
    }


    /**
     * Relationship. 
     * One to Many - Many to One
    **/
    public function sequences()
    {
        return $this->hasMany(AnswerSequence::class);
    }

    public function matchings()
    {
        return $this->hasMany(AnswerMatching::class);
    }

    public function tableItems()
    {
        return $this->hasMany(AnswerTableItem::class);
    }


    /**
     * Relationship. 
     * One to One - Inverse
    **/
    public function viewContent()
    {
        return $this->belongsTo(ViewContent::class);
    }
}
