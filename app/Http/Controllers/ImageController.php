<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function upload(Request $request)
    {
        $image = $request->file('file');

        if (!$image) {
            return response()->json(['error' => 'No se ha enviado ninguna imagen'], 400);
        }

        $imageName = time() . '.' . $image->getClientOriginalExtension();
        Storage::disk('public')->put($imageName, file_get_contents($image));

        $imageUrl = url('storage/' . $imageName);

        return response()->json(['location' => $imageUrl]);
    }
}
