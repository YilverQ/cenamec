<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf as PDF;

/*Models*/
use App\Models\Certificate;

use SimpleSoftwareIO\QrCode\Facades\QrCode;


class CertificateController extends Controller
{
    // Método para formatear el número de cédula.
    private function formatIdentification($identification)
    {
        // Asegurarse de que el valor sea una cadena
        $identification = (string) $identification;

        // Longitud del número de identificación
        $length = strlen($identification);

        // Formatear según la longitud
        if ($length <= 3) {
            // Si tiene 3 dígitos o menos, no se formatea
            return $identification;
        } elseif ($length <= 6) {
            // Si tiene entre 4 y 6 dígitos, se formatea como XX.XXX
            return substr($identification, 0, 2) . '.' . substr($identification, 2);
        } else {
            // Si tiene más de 6 dígitos, se formatea como XX.XXX.XXX
            return substr($identification, 0, 2) . '.' . 
                   substr($identification, 2, 3) . '.' . 
                   substr($identification, 5);
        }
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        #$pdf = app('dompdf.wrapper');
        #$pdf->loadHTML('<h1>Hola Mundo<h1>');
        $pdf = PDF::loadView('certificate.pdf');
        return $pdf->stream();
        #return $pdf->download(); //Para descargar automaticamente el PDF.
    }

    /**
     * Display the specified resource.
     */
    public function show(Certificate $item)
    {
        $course   = $item->course;
        $student  = $item->student;

        // Formatear el número de identificación
        $formattedID = $this->formatIdentification($student->user->identification_card);

        // Link del código QR
        $linkQr = route('certificate.show', ['item' => $item]);
        $qrImage = QrCode::size(150)->generate($linkQr);

        $data = [
            "course"      => $course,
            "student"     => $student->user, // Este tiene el identification_card
            "formattedID" => $formattedID,   // Número de identificación formateado
            "qrImage"     => $qrImage,
            "certificate" => $item
        ];

         // Cargar la vista del PDF
        $pdf = PDF::loadView('certificate.pdf', $data);

        // Configurar el papel y la orientación
        $pdf->setPaper('A4', 'landscape');

        // Devolver el PDF como stream
        return $pdf->stream();
    }
}
