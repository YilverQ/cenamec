<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*Importamos los modelos*/
use App\Models\User;


class AdminStaticController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.admin -> Comprueba que el usuario tiene permiso de administrador.
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    /**
     * Estadísticas para el Administrador.  
     */
    public function adminStatics(Request $request)
    {
        return view('administrator.statics')
                    ->with("bestCourses", 10);
    }
}
