<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


/*Importamos los modelos*/
use App\Models\State;
use App\Models\Parishe;
use App\Models\Municipalitie;

class UbicationAPIController extends Controller
{
    /*Retorna una lista de Municipios*/
    public function getMunicipalities(Request $request, State $item)
    {
        // Construye una respuesta JSON con los datos y un código de estado personalizado
        $municipalities = Municipalitie::where("state_id", '=', $item->id)
                                                ->orderBy('name')
                                                ->get();
        return response()->json($municipalities, 200);
    }


    /*Retorna una lista de Parroquias*/
    public function getParishes(Request $request, Municipalitie $item)
    {
        // Construye una respuesta JSON con los datos y un código de estado personalizado
        $parishes = Parishe::where("municipalitie_id", '=', $item->id)
                                    ->orderBy('name')
                                    ->get();
        return response()->json($parishes, 200);
    }
}
