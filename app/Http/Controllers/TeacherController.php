<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;


/*Importamos los modelos*/
use App\Models\User;
use App\Models\State;
use App\Models\Parishe;
use App\Models\Teacher;
use App\Models\Profileimg;
use App\Models\Municipalitie;


class TeacherController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher');
    }


    /**
     * Retornamos la vista principal del profesor.
     * Envíamos un cursos asociado al profesor de forma aleatoria.
     */
    public function index(Request $request)
    {
        $user_id = $request->session()->get('user_id');
        $user    = User::find($user_id);
        $teacher = Teacher::find($user->teacher->id);
        $course  = $teacher->courses()->inRandomOrder()->first(); 

        return view('teacher.index')
                ->with("course", $course)
                ->with("teacher", $user);
    }
}