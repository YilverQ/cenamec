<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*Modelos*/
use App\Models\Answer;
use App\Models\Student;
use App\Models\ViewContent;
use App\Models\ItemEvaluation;
use App\Models\AnswerSequence;
use App\Models\AnswerMatching;
use App\Models\AnswerTableItem;
use App\Models\AnswerTrueFalse;
use App\Models\AnswerShortAnswer;
use App\Models\AnswerSelectionSimple;

class TestController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.student');
    }

    private function addItemEvaluationResponse(ItemEvaluation $itemEvaluation, array $itemResponse, Answer $answer)
    {
        //dd($itemResponse);
        if ($itemResponse['type'] == 'Selección Simple') {
            $item = new AnswerSelectionSimple;
            $item->selection_simple_id = $itemEvaluation->selectionSimple->id;
            $item->answer_id = $answer->id;
            $item->selection = $itemResponse['selection'];
            $item->is_correct = $itemResponse['correct'];
            $item->save();
        }
        elseif($itemResponse['type'] == 'Verdadero o Falso'){
            $item = new AnswerTrueFalse;
            $item->true_false_id = $itemEvaluation->trueOrFalse->id;
            $item->answer_id = $answer->id;
            $item->selection = $itemResponse['selection'];
            $item->is_correct = $itemResponse['correct'];
            $item->save();
        }
        elseif($itemResponse['type'] == 'Respuesta Corta'){
            $item = new AnswerShortAnswer;
            $item->answer_id = $answer->id;
            $item->selection = $itemResponse['selection'];
            $item->is_correct = $itemResponse['correct'];
            $item->save();
        }
        elseif ($itemResponse['type'] == 'Secuencia') {
            foreach ($itemResponse['selection'][0] as $index => $sequenceId) {
                $item = new AnswerSequence;
                $item->answer_id = $answer->id;
                $item->sequence_id = $sequenceId; 
                $item->is_correct = $itemResponse['selection'][1][$index]; 
                $item->orden = $index + 1; 
                $item->save();
            }
        }
        elseif ($itemResponse['type'] == 'Emparejamiento') {
            // Iterar sobre cada selección
            foreach ($itemResponse['selection'] as $selection) {
                $item = new AnswerMatching;
                $item->answer_id    = $answer->id;
                $item->matching_id  = $selection["ids"][0];
                $item->elementA     = $selection['selection'][0]; 
                $item->elementB     = $selection['selection'][1]; 
                $item->is_correct   = $selection['correct']; 
                $item->save();
            }
        }
        elseif ($itemResponse['type'] == 'Cuadro Comparativo') {
            // Iterar sobre las columnas A, B y C
            foreach (['A', 'B', 'C'] as $column) {
                // Obtener los datos de la columna actual
                $correctIds = $itemResponse["selection"]["column{$column}_correct"];
                $booleans = $itemResponse["selection"]["column{$column}_boolean"];

                // Iterar sobre las selecciones y guardarlas en la base de datos
                foreach ($correctIds as $index => $selection) {
                    // Crear una nueva instancia de AnswerTableItem
                    $item = new AnswerTableItem;
                    $item->answer_id = $answer->id; 
                    $item->column = $column; 
                    $item->is_correct = $booleans[$index];
                    $item->table_item_id = $correctIds[$index]; 
                    $item->save();
                }
            }
        }
    }

    private function updatePercentageAnswer(array $itemResponse, Answer $answer)
    {
        if ($itemResponse['type'] == 'Secuencia') {
            $totalItems = count($itemResponse['selection'][0]); 
            $count = array_sum($itemResponse['selection'][1]);
            $averageCorrect = ($totalItems > 0) ? ($count / $totalItems) * 100 : 0; // Evitar división por cero
            $isCorrect = $averageCorrect > 60 ? true : false;

            $answer->update([
                'percentage' => round($averageCorrect, 2),
                'is_correct' => $isCorrect,
            ]);
        }
        elseif ($itemResponse['type'] == 'Emparejamiento') {
            $totalItems = count($itemResponse['selection']); 
            $count = array_sum(array_column($itemResponse['selection'], 'correct'));
            $averageCorrect = ($totalItems > 0) ? ($count / $totalItems) * 100 : 0; 
            $isCorrect = $averageCorrect > 60 ? true : false;
            $answer->update([
                'percentage' => round($averageCorrect, 2),
                'is_correct' => $isCorrect,
            ]);
        }
        elseif ($itemResponse['type'] == 'Cuadro Comparativo') {
            $totalItems = 0; 
            $totalCorrect = 0; 

            foreach (['A', 'B', 'C'] as $column) {
                $booleans = $itemResponse["selection"]["column{$column}_boolean"];
                $totalItems += count($booleans);
                $totalCorrect += array_sum($booleans); 
            }

            $averageCorrect = ($totalItems > 0) ? ($totalCorrect / $totalItems) * 100 : 0;
            $isCorrect = $averageCorrect > 60 ? true : false;

            $answer->update([
                'percentage' => round($averageCorrect, 2),
                'is_correct' => $isCorrect,
            ]);
        }
    }


    private function calculateMeanPercentage(ViewContent $viewContent): float
    {
        $answers = Answer::where('view_content_id', $viewContent->id)->get();
        $totalAnswers = $answers->count();
        $totalPercentage = $answers->sum('percentage');
        $averagePercentage = ($totalAnswers > 0) ? round($totalPercentage / $totalAnswers, 2) : 0;
        return $averagePercentage;
    }


    /**
     * Obtenemos la evaluación
     */
    public function test(Request $request, ViewContent $item)
    {
        // Obtener el valor de evaluationState
        $evaluationState = $request->input('evaluationState');
        $evaluationStateArray = json_decode($evaluationState, true);

        // Recorrer el array para persistir las respuestas del estudiante.
        foreach ($evaluationStateArray as $key => $value) {
            $answer = new Answer;
            $answer->is_correct = $value["correct"];
            $answer->percentage = ($value["correct"] ? 100 : 0);
            $answer->view_content_id = $item->id;
            $answer->item_evaluation_id = $key;
            $answer->save();

            $itemEvaluation = ItemEvaluation::find($key);
            $this->addItemEvaluationResponse($itemEvaluation, $value, $answer);
            $this->updatePercentageAnswer($value, $answer);
        }

        // Actualizamos el registro correspondiente en la tabla view_content
        $meanPercentage = $this->calculateMeanPercentage($item);
        $item->update([
            'mean' => $meanPercentage,
            'end_view' => now(),
            'time_view' => now()->diff($item->start_view)->format('%H:%I:%S'),
        ]);


        // Obtener el siguiente contenido directamente desde la base de datos
        $nextContent = $item->content->module->contents()
            ->where('orden', '>', $item->content->orden) // Buscar el siguiente por el campo 'orden'
            ->orderBy('orden', 'asc') // Asegurarse de obtener el más cercano
            ->first();

        if ($meanPercentage >= 60) {
            $student_id = $request->session()->get('student_id');
            $currentContent = $item->content;
            $currentContent->students()->updateExistingPivot($student_id, ['state' => 'Completado']);
            
            // Si existe un siguiente contenido, se actualiza o se asigna
            if ($nextContent) {
                $nextContentByStudent = $nextContent->students()->where('student_id', $student_id)->first();
                if (!$nextContentByStudent) {
                    // Si el siguiente contenido no está asociado con el estudiante, lo asociamos
                    $nextContent->students()->attach($student_id, ['state' => 'Activo']);
                }
            }

        }


        /*************************************************************************/
        //Lógica para la barra de navegación
        $student_id = $request->session()->get('student_id');
        $student = Student::find($student_id);
        // Obtener el primer contenido activo de la relación content_student
        $activeContentStudent = $student->contents()
                    ->wherePivot('state', 'Activo')
                    ->where('modulo_id', $item->content->module->id)
                    ->first();

        // Obtener el estado de cada contenido para el estudiante
        $contentStates = DB::table('content_student')
            ->where('student_id', $student_id)
            ->pluck('state', 'content_id');
        /*************************************************************************/


        // Retornamos todos los datos a la vista
        return view('studentModule.test')
            ->with('activeContentStudent', $activeContentStudent)
            ->with('contentStates', $contentStates)
            ->with('content', $item->content)
            ->with('contents', $item->content->module->contents)
            ->with('nextItem', $nextContent)
            ->with('module', $item->content->module)
            ->with('viewContent', $item); 
    }
}
