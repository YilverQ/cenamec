<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


/*Importamos los modelos*/
use App\Models\User;
use App\Models\Note;
use App\Models\State;
use App\Models\Course;
use App\Models\Module;
use App\Models\Parishe;
use App\Models\Teacher;
use App\Models\Student;
use App\Models\Laboral;
use App\Models\Academic;
use App\Models\Profileimg;
use App\Models\Municipalitie;
use App\Models\Administrator;


class AdministratorController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.admin -> Comprueba que el usuario tiene permiso de administrador.
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    /**
     * Retornamos una vista. 
     */
    public function home(Request $request)
    {
        //buscamos el administrador.
        $admin_id   = $request->session()->get('admin_id');
        $admin      = Administrator::find($admin_id);
        $admin_user = User::find($admin->user->id);  
       
        return view('administrator.home')
                ->with("admin", $admin_user);
    }


    /**
     * Retornamos una lista de todos los administradores.
     */
    public function index()
    {
        // Obtenemos todos los administradores ordenados por fecha de creación descendente.
        $administrators = Administrator::join('users', 'administrators.user_id', '=', 'users.id')
                ->where('users.disabled', false)
                ->select('administrators.*')
                ->orderBy('users.updated_at', 'desc') // Luego ordena por fecha de modificación
                ->get();

        // Obtenemos todos los profesores ordenados por fecha de creación descendente.
        $teachers = Teacher::join('users', 'teachers.user_id', '=', 'users.id')
                ->where('users.disabled', false)
                ->select('teachers.*')
                ->orderBy('users.updated_at', 'desc') // Luego ordena por fecha de modificación
                ->get();

        // Obtenemos todos los estudiantes ordenados por fecha de creación descendente.
        $students = Student::join('users', 'students.user_id', '=', 'users.id')
                ->where('users.disabled', false)
                ->select('students.*')
                ->orderBy('users.updated_at', 'desc') // Luego ordena por fecha de modificación
                ->get();

        // Obtenemos todos los usuarios deshabilitados ordenados por fecha de creación descendente.
        $usersDisabled = User::where('disabled', true)
                ->orderBy('updated_at', 'desc') // Luego ordena por fecha de modificación
                ->get();




        return view('administrator.index')
                ->with("administrators", $administrators)
                ->with("teachers", $teachers)
                ->with("students", $students)
                ->with("usersDisabled", $usersDisabled);
    }


    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function create()
    {
        //Buscamos todas las ubicaciones geográficas.
        $states = State::all();

        return view('administrator.create')
                    ->with("states", $states);
    }


    /**
     * Acción para crear un elemento.
     * 
     * Para crear el elemento se debe cumplir:
     *      1. Ingresar un correo electrónico único.
     *      2. Ingresar un número de teléfono único.
     *      3. Ingresar un número de cédula único.
     *      4. Los datos ingresados no puede estar asociado a ningún otro usuario.
     * 
     * Luego de comprobar se procede a:
     *      1. Persistir los datos que ingresó el usuario.
     *      2. Se crea un relación con según el role que se agregó. 
     *      3. Se redirecciona a la vista principal del estudiante.   
     */
    public function store(Request $request)
    {
        /*Recibimos los datos sencibles*/
        $email = strtolower($request->input('email'));
        $phone = $request->input('number_phone');
        $identification_card = $request->input('identification_card');

        /*Buscamos en la DB si existe alguno de los datos*/
        $is_email_valid  = User::where('email', '=', $email)->first();
        $is_phone_valid  = User::where('number_phone', '=', $phone)->first();
        $is_card_valid  = User::where('identification_card', '=', $identification_card)->first();

        //Si se encuentra un elemento es porque el correo ingresado es incorrecto.
        if (!(empty($is_card_valid->identification_card))) {
            #Retornamos un mensaje flash de error.
            session()->flash('message-error', 'Error, el documento de identidad ya está en uso');
            return to_route('administrator.create');
        }

        //Si se encuentra un elemento es porque el correo ingresado es incorrecto.
        if (!(empty($is_email_valid->email))) {
            #Retornamos un mensaje flash de error.
            session()->flash('message-error', 'Error, el correo electrónico ya está en uso');
            return to_route('administrator.create');
        }

        //Si se encuentra un elemento es porque el número de teléfono ingresado es incorrecto.
        if (!(empty($is_phone_valid->number_phone))) {
            #Retornamos un mensaje flash de error.
            session()->flash('message-error', 'Error, el número de teléfono ya está en uso');
            return to_route('administrator.create');
        }

        //Creamos un nuevo elemento.
        $user = new User;
        $user->firts_name  = ucwords(strtolower($request->input('firts_name')));
        $user->lastname    = ucwords(strtolower($request->input('lastname')));
        $user->gender      = $request->input('gender');
        $user->birthdate   = $request->input('birthdate');
        $user->nacionality = $request->input('nacionality');
        $user->identification_card = strtoupper($request->input('identification_card'));
        $user->number_phone = $request->input('number_phone');
        $user->email        = $email;
        $user->password     = Hash::make($request->input('password'));
        //Según el genero, le asignamos una imagen de perfil.
        if ($user->gender == "Masculino"){
            $user->profileimg_id = 49;
        }
        else{
            $user->profileimg_id = 46;
        }
        $user->parishe_id = $request->input('parishe');
        $user->save();

        //Creamos un nuevo elemento ROLE.
        if (!(empty($request->input("is_admin")))) {
            $role = new Administrator;
            $role->user_id = $user->id; 
            $role->save();
        }
        if (!(empty($request->input("is_teacher")))) {
            $role = new Teacher;
            $role->user_id = $user->id; 
            $role->save();
        }
        if (!(empty($request->input("is_student")))) {
            $role = new Student;
            $role->user_id = $user->id; 
            $role->save();
        }

        #Retornamos un mensaje flash de error.
        session()->flash('message-success', '¡Has agregado un nuevo usuario!');
        return to_route('administrator.index');
    }


    /**
     * Retornamos una vista que nos muestra un elemento. 
     */
    public function show(Administrator $item)
    {
        return view('administrator.show')
                ->with("admin", $item);
    }


    /**
     * Retornamos un formulario que nos permite actualizar un elemento. 
     */
    public function edit(Request $request, User $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        //Buscamos todas las ubicaciones geográficas.
        $states = State::orderBy('name')->get();

        //Buscamos todas las imagenes para el perfil de usuario.
        $profileimgs = Profileimg::all();
        
        $user_id = $item->id;
        $user    = User::find($user_id);

        $academics = Academic::where('user_id', $user_id)
                            ->orderBy('id', 'desc')
                            ->get();

        $laborals = Laboral::where('user_id', $user_id)
                            ->orderBy('id', 'desc')
                            ->get();

        return view('administrator.edit')
                    ->with("role", $role)
                    ->with("academics", $academics)
                    ->with("laborals", $laborals)
                    ->with("states", $states)
                    ->with("profileimgs", $profileimgs)
                    ->with('user', $user);
    }


    /**
     * Acción para actualizar un elemento.
     * 
     * Para actualizar el elemento se debe cumplir:
     *      1. Ingresar un correo electrónico único.
     *      2. Ingresar un número de teléfono único.
     *      3. Ingresar un número de cédula único.
     *      4. Los datos ingresados puedes ser igual al que tenían anteriormente.
     * 
     * Luego de comprobar se procede a:
     *      1. Persistir los datos que ingresó el usuario.
     *      2. Se crea un relación con según el role que se agregó. 
     *      3. Se redirecciona a la vista principal del estudiante.   
     */
    public function update(Request $request, User $item)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;
        $item    = User::find($user_id);

        /*Recibimos los datos sencibles*/
        $email = strtolower($request->input('email'));
        $phone = $request->input('number_phone');

        /*Buscamos en la DB si existe alguno de los datos*/
        $is_email_valid  = User::where('email', '=', $email)->first();
        $is_phone_valid  = User::where('number_phone', '=', $phone)->first();


        //El correo ingresado en el formulario ya está en la bd.
        if (!(empty($is_email_valid->email))) {
            if ($item->email != $email) {
                #El correo ya lo tiene otra persona.
                #No actualiza el dato. 
                #Retorna un mensaje de error. 
                session()->flash('message-error', 'Error, el correo electrónico ya está en uso');
                return to_route('administrator.edit', $item);
            }
        }

        //Si se encuentra un elemento es porque el número de teléfono ingresado es incorrecto.
        if (!(empty($is_phone_valid->number_phone))) {
            if ($item->number_phone != $phone) {
                #El número de teléfono ya lo tiene otra persona.
                session()->flash('message-error', 'Error, el número de teléfono ya está en uso');
                return to_route('administrator.edit', $item);
            }
        }
        
        //Si no se cumple lo anterior es porque se puede actualizar los datos. 
        $item->firts_name  = ucwords(strtolower($request->input('firts_name')));
        $item->lastname    = ucwords(strtolower($request->input('lastname')));
        $item->gender      = $request->input('gender');
        $item->nacionality = $request->input('nacionality');
        $item->birthdate   = $request->input('birthdate');
        $item->identification_card = strtoupper($request->input('identification_card'));
        $item->number_phone = $request->input('number_phone');
        $item->email       = $email;
        $item->parishe_id  = $request->input('parishe');
        
        #Almacenamos los datos.
        $item->save();

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡Tus datos fueron actualizados!');
        return to_route('administrator.edit', $item);
    }


    /**
     * Comprobamos los roles del usuario 
     */
    public function checkRoles(Request $request, User $item)
    {
        // Comprobamos el rol de Administrador
        if ($request->has('is_admin')) {
            if (!$item->administrator) {
                // Solo crear si no existe el rol
                if (!Administrator::where('user_id', $item->id)->exists()) {
                    $role = new Administrator();
                    $role->user_id = $item->id;
                    $role->save();
                }
            }
        } else {
            // Si el usuario tiene el rol de administrador, lo eliminamos
            if ($item->administrator) {
                $role = Administrator::where('user_id', $item->id)->first();
                $role->delete();
            }
        }

        // Comprobamos el rol de Profesor
        if ($request->has('is_teacher')) {
            // Si el usuario no tiene el rol de profesor, lo asignamos
            if (!$item->teacher) {
                // Solo crear si no existe el rol
                if (!Teacher::where('user_id', $item->id)->exists()) {
                    $role = new Teacher();
                    $role->user_id = $item->id;
                    $role->save();
                }
            }
        } else {
            // Si el usuario tiene el rol de profesor, lo eliminamos
            if ($item->teacher) {
                $role = Teacher::where('user_id', $item->id)->first();
                $role->delete();
            }
        }

        // Comprobamos el rol de Estudiante
        if ($request->has('is_student')) {
            // Si el usuario no tiene el rol de estudiante, lo asignamos
            if (!$item->student) {
                // Solo crear si no existe el rol
                if (!Student::where('user_id', $item->id)->exists()) {
                    $role = new Student();
                    $role->user_id = $item->id;
                    $role->save();
                }
            }
        } else {
            // Si el usuario tiene el rol de estudiante, lo eliminamos
            if ($item->student) {
                $role = Student::where('user_id', $item->id)->first();
                $role->delete();
            }
        }

        // Deshabilitar usuario si no tiene roles
        $hasRoles = Administrator::where('user_id', $item->id)->exists() ||
                    Teacher::where('user_id', $item->id)->exists() ||
                    Student::where('user_id', $item->id)->exists();

        // Si no tiene roles, deshabilitar
        $item->disabled = !$hasRoles;  
        $item->save();



        // Mensaje de éxito
        session()->flash('message-success', '¡Los roles del usuario fueron actualizados!');

        // Redirigir al usuario a su perfil de administrador
        return to_route('administrator.edit', $item);
    }


    /**
     * Actualizamos la contraseña del usuario 
     */
    public function updatePassword(Request $request, User $item)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;
        $item    = User::find($user_id);

        /*Si el usuario no ingresó datos, no puede actualizar la contraseña*/
        $password = $request->input('password');
        if (!$password) {
            session()->flash('message-error', '¡Tu contraseña no puede actualizarse!');
            return to_route('administrator.edit', $item);
        }

        /*Encriptamos la contraseña y la guardamos*/
        $item->password = Hash::make($password);
        $item->save();

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡Tu contraseña fue actualizada!');
        return to_route('administrator.edit', $item);
    }


    /**
     * Actualizamos la imagen del usuario 
     */
    public function updateImg(Request $request, User $item)
    {
        //Recibimos el nombre de la imagen. 
        $value = $request->input('picture');
        
        //Comprobamos si el usuario quiso actualizar la imagen.
        if ($value){
            $item->profileimg_id = $value;
            $item->save();
        }

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡La imagen del usuario fue actualizada!');
        return to_route('administrator.edit', $item);
    }


    /**
     * Ya no se pueden eliminar usuarios, entonces debemos deshabilitarlos.  
     */
    public function disabled(Request $request, User $item)
    {
        //Obtener el administrador con la sesión activa. 
        $user_id = $request->session()->get('user_id');
        $user    = User::find($user_id);

        /*
            Si el administrador de la sesión activa es el mismo
            que el elemento que se desea eliminar, no lo elimina.
            Retorna un mensaje flash de error. 
        */
        if ($user->email == $item->email) {
            session()->flash('message-error', '¡Tu sesión está activa, no te puedes deshabilitar!');
            return to_route('administrator.index');
        }

        //Elimina el elemento y retorna un mensaje flash.
        $item->disabled = true;
        $item->save();

        session()->flash('message-success', '¡El usuario fue deshabilitado correctamente!');
        return to_route('administrator.index');
    }


    /**
     * Aquí podemos habilitar nuevamente a los usuarios.  
     */
    public function enabled(Request $request, User $item)
    {
        //Elimina el elemento y retorna un mensaje flash.
        $item->disabled = false;
        $item->save();

        session()->flash('message-success', '¡El usuario fue habilitado correctamente!');
        return to_route('administrator.index');
    }


    /**
     * Agregamos un nuevo dato acádemico. 
     */
    public function addAcademic(Request $request, User $item)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;

        session()->flash('message-error', 'Por favor completa todos los campos requeridos');
        $request->validate([
            'academicLevel' => 'required',
            'intitution' => 'required',
            'career' => 'required',
            'sector' => 'required',
            'area' => 'required'
        ]);
        session()->forget('message-error');

        /*Formateamos el datos de academic_title. Cada inicio de cada palabra es mayúscula*/
        $academic_title    = $request->input('academic_title');
        $academic_title    = ucwords($academic_title);

        /*Creamos un nuevo dato*/
        $data = new Academic;
        $data->user_id          = $user_id;
        $data->institution_id   = $request->input('intitution');
        $data->academiclevel_id = $request->input('academicLevel');
        $data->career_id        = $request->input('career');
        $data->area_id          = $request->input('area');
        $data->sector_id        = $request->input('sector');
        $data->year_entry       = $request->input('year_entry');;
        $data->year_exit        = $request->input('year_exit');
        $data->academic_title   = $academic_title;

        $data->save();

        session()->flash('message-success', '¡Se ha creado un nuevo dato acádemico!');
        return to_route('administrator.edit', $item);
    }

    /**
     * Actualiza un registro acádemico 
     */
    public function updateAcademic(Request $request, User $item){
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;

        $academic_id = $request->input('idAcademic');
        $academic = Academic::find($academic_id);

        /*Formateamos el datos de academic_title. Cada inicio de cada palabra es mayúscula*/
        $academic_title    = $request->input('academic_titleEdit');
        $academic_title    = ucwords($academic_title);

        /*Actualizamos la academia del usuario*/
        $academic->institution_id   = $request->input('intitutionEdit');
        $academic->academiclevel_id = $request->input('academicLevelEdit');
        $academic->career_id        = $request->input('careerEdit');
        $academic->area_id          = $request->input('areaEdit');
        $academic->sector_id        = $request->input('sectorEdit');
        $academic->year_entry       = $request->input('year_entryEdit');;
        $academic->year_exit        = $request->input('year_exitEdit');
        $academic->academic_title   = $academic_title;

        /*Persistimos los datos*/
        $academic->save();

        session()->flash('message-success', '¡Datos Actualizados!');
        return to_route('administrator.edit', $item);
    }

    /**
     * Elimina un registro academico 
     */
    public function deleteAcademic(Request $request, Academic $item, User $user)
    {
        //Elimina el elemento y retorna un mensaje flash.
        $item->delete();
        session()->flash('message-success', '¡El dato acádemico fue eliminado correctamente!');
        return to_route('administrator.edit', $user);
    }

    /**
     * Agregamos un nuevo dato acádemico. 
     */
    public function addLaboral(Request $request, User $item)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;

        session()->flash('message-error', 'Por favor completa todos los campos requeridos');
        $request->validate([
            'name_intitution' => 'required',
            'type_institution' => 'required',
            'area_institution' => 'required',
            'position' => 'required',
            'year_entryLaboral' => 'required'
        ]);
        session()->forget('message-error');

        /*Formateamos los datos. Cada inicio de cada palabra es mayúscula*/
        $name_intitution    = $request->input('name_intitution');
        $name_intitution    = ucwords($name_intitution);
        $position    = $request->input('position');
        $position    = ucwords($position);

        /*Creamos un nuevo registro*/
        $data = new Laboral;
        $data->user_id          = $user_id;
        $data->name_intitution  = $name_intitution;
        $data->type             = $request->input('type_institution');
        $data->area             = $request->input('area_institution');
        $data->position         = $position;
        $data->year_entry       = $request->input('year_entryLaboral');;
        $data->year_exit        = $request->input('year_exitLaboral');

        $data->save();

        session()->flash('message-success', '¡Se ha creado un nuevo dato laboral!');
        return to_route('administrator.edit', $item);
    }

    /**
     * Actualiza un registro laboral 
     */
    public function updateLaboral(Request $request, User $item){
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $item->id;

        $laboralID = $request->input('idLaboral');
        $laboral = Laboral::find($laboralID);

        /*Formateamos los datos. Cada inicio de cada palabra es mayúscula*/
        $name_intitution    = $request->input('name_institutionEdit');
        $name_intitution    = ucwords($name_intitution);
        $position    = $request->input('positionEdit');
        $position    = ucwords($position);

        /*Actualizamos la academia del usuario*/
        $laboral->name_intitution  = $name_intitution;
        $laboral->type             = $request->input('type_institutionEdit');
        $laboral->area             = $request->input('area_institutionEdit');
        $laboral->position         = $position;
        $laboral->year_entry       = $request->input('year_entryLaboralEdit');;
        $laboral->year_exit        = $request->input('year_exitLaboralEdit');

        /*Persistimos los datos*/
        $laboral->save();

        session()->flash('message-success', '¡Datos Actualizados!');
        return to_route('administrator.edit', $item);
    }

    /**
     * Elimina un registro laboral 
     */
    public function deleteLaboral(Request $request, Laboral $item, User $user)
    {
        //Elimina el elemento y retorna un mensaje flash.
        $item->delete();
        session()->flash('message-success', '¡El dato laboral fue eliminado correctamente!');
        return to_route('administrator.edit', $user);
    }
}
