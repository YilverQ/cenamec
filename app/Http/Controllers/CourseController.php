<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/*Importamos los modelos*/
use App\Models\Tag;
use App\Models\Course;
use App\Models\Module;
use App\Models\Teacher;
use App\Models\Audience;
use App\Models\Certificate;


class CourseController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }


    /**
     * Retornamos una lista de todos los cursos.
     */
    public function index(Request $request)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $tags = Tag::all();
        $audiences = Audience::all();

        return view('course.index')
                ->with("tags", $tags)
                ->with("audiences", $audiences)
                ->with("role", $role);
    }


    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function create(Request $request)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $tags = Tag::all();
        $audiences = Audience::all();

        $teachers = Teacher::all();
        return view('course.create')
                ->with("role", $role)
                ->with("tags", $tags)
                ->with("audiences", $audiences)
                ->with("teachers", $teachers);
    }


    /**
     * Acción para crear un nuevo elemento.
     * 
     * Para crear el elemento se debe cumplir: 
     *      1. El nombre del elemento debe ser único. 
     * 
     * Procesamos la imagen. 
     *      1. Comprobamos que la imagen ingresada sea valida. 
     *      2. Guardamos la imagen en Storage. 
     * 
     * Guardamos el registro. 
     */
    public function store(Request $request)
    {
        //Comprobamos si el nombre es valido.
        $name = $request->input('super_name');
        $is_name_valid  = Course::where('name', '=', $name)->first();
        //Si se encuentra un elemento es porque el nombre ingresado es incorrecto.
        if (!(empty($is_name_valid->name))) {
            session()->flash('message-error', 'Error, el nombre del curso ya está en uso');
            return to_route('teacher.course.create');
        }

        // Obtener la imagen del formulario
        $img = $request->file('img');
        // Comprobar si la imagen fue subida
        if (!$img) {
            session()->flash('message-error', 'Error, se debe subir una imagen');
            return to_route('teacher.course.create');
        }

        // Verificar que el tamaño de la imagen no exceda 2 MB
        if ($img->getSize() > 2 * 1024 * 1024) {
            session()->flash('message-error', 'Error, la imagen no debe pesar más de 2 MB');
            return to_route('teacher.course.create');
        }

        // Generar un nombre único para la imagen
        $url = "/img/public/imgCourses/";
        $nameUnique = Str::random(20) . '.' . $img->getClientOriginalExtension();
        
        // Guardar la imagen en la carpeta "uploads"
        $img->move(public_path($url), $nameUnique);
        
        // Ruta completa de la imagen.
        $urlImage = $url . $nameUnique;


        //Persistimos los datos en la bd.
        $course = new Course;
        $course->name        = $request->input('super_name');
        $course->purpose     = $request->input('purpose');
        $course->general_objetive  = $request->input('general_objetive');
        $course->specific_objetive = $request->input('specific_objetive');;
        $course->competence  = $request->input('competence');;
        $course->img         = $urlImage;
        $course->state       = $request->input('status');
        $course->level       = $request->input('level');
        $course->save();

        //Asignamos las relaciones.
        $course->teachers()->sync($request->input('teachers'));
        $course->audiences()->sync($request->input('audiences'));
        $course->tags()->sync($request->input('tags'));


        session()->flash('message-success', '¡El curso fue creado!');
        return redirect()->route('teacher.course.index');
    }


    /**
     * Retornamos una vista que nos muestra un elemento.
     * Buscamos los módulos que tiene el curso que viene por párametro.
     * retornamos todos los datos. 
     */
    public function show(Request $request, Course $item)
    {
        // Identificamos el role del usuario.
        $role = $request->session()->get('role');

        // Buscamos los datos. 
        $teachers = $item->teachers;
        $students = $item->students;
        $modules = Module::where('course_id', $item->id)
                        ->orderBy('level')
                        ->get();

        //Link del código QR. 
        $linkQr = route('shared.course', ['item' => $item]);

        //Cantidad de certificados emitidos
        $certificates = Certificate::where('course_id', $item->id)->get();


        // Retornamos todos los datos a la vista. 
        return view('course.show')
                ->with("role", $role)
                ->with("course", $item)
                ->with("linkQr", $linkQr)
                ->with("modules", $modules)
                ->with("teachers", $teachers)
                ->with("students", $students);
    }



    /**
     * Retornamos un formulario que nos permite actualizar un elemento. 
     */
    public function edit(Request $request, Course $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $tags = Tag::all();
        $audiences = Audience::all();
        $teachers = Teacher::all();

        return view('course.edit')
                ->with("tags", $tags)
                ->with("audiences", $audiences)
                ->with("role", $role)
                ->with('teachers', $teachers)
                ->with('course', $item);
    }


    /**
     * Acción para actualizar un elemento.
     * 
     * Para actualizar el elemento se debe cumplir:
     *      1. El nombre del elemento debe ser único.
     *      2. El 'nombre' no puede ser igual al que tenía asociado.
     * 
     * Comprobamos si se quiere actualizar la imágen. 
     *      1. Se comprueba el campo de la imágen. 
     *      2. Se comprueba que la imágen ingresada sea correcta.
     *      3. Se guarda la imágen.
     *    
     * Se guardan los datos en bd.
     */
    public function update(Request $request, Course $item)
    {
        //Comprobamos si el nombre es valido.
        $name = $request->input('super_name');
        $is_name_valid  = Course::where('name', '=', $name)->first();
        //Si se encuentra un elemento es porque el nombre ingresado es incorrecto.
        if (!(empty($is_name_valid->name))) {
            if ($item->name != $name) {
                #El nombre del curso ya lo tiene otra persona.
                #No actualiza el dato. 
                #Retorna un mensaje de error. 
                session()->flash('message-error', 'Error, nombre del curso ya está en uso');
                return to_route('teacher.course.edit', $item);
            }
        }

        //Comprobamos si se quiere actualizar una imágen. 
        $urlImage = $item->img;
        $imagen = $request->file('img');
        if (!(empty($imagen))){
            // Verificar que el tamaño de la imagen no exceda 2 MB
            if ($imagen->getSize() > 2 * 1024 * 1024) {
                session()->flash('message-error', 'Error, la imagen no debe pesar más de 2 MB');
                return to_route('teacher.course.create');
            }

            // Verificar si la imagen existe antes de eliminarla
            if (file_exists(public_path($item->img))) {
                // Eliminar la imagen
                unlink(public_path($item->img));
            }

            // Obtener la imagen del formulario
            $url =  "/img/public/imgCourses/";
            $img = $request->file('img');
            // Generar un nombre único para la imagen
            $nameUnique = Str::random(20) . '.' . $img->getClientOriginalExtension();
            // Guardar la imagen en la carpeta "uploads"
            $img->move(public_path($url), $nameUnique);
            // Ruta completa de la imagen.
            $urlImage = $url . $nameUnique;

            $item->img = $urlImage;
        }

        //Persistimos los datos en la bd.
        $item->name        = $request->input('super_name');
        $item->purpose     = $request->input('purpose');
        $item->general_objetive  = $request->input('general_objetive');
        $item->specific_objetive = $request->input('specific_objetive');;
        $item->competence  = $request->input('competence');;
        $item->img         = $urlImage;
        $item->state       = $request->input('status');
        $item->level       = $request->input('level');
        $item->save();

        //Actualizamos las relaciones.
        $item->teachers()->sync($request->input('teachers'));
        $item->audiences()->sync($request->input('audiences'));
        $item->tags()->sync($request->input('tags'));

        //Actualizamos la fecha de actualización.
        $item->touch();

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡El curso fue actualizado!');
        return to_route('teacher.course.index');
    }


    /**
     * Eliminamos la imagen del curso que está en 
     * carpeta "Storage" en nuestro proyecto.
     *  
     * Eliminamos el elemento de nuestra base de datos. 
     * 
     * Envíamos un mensaje flash.
     * Retornamos la vista principal.  
     */
    public function destroy(Request $request, Course $item)
    {
        // Verificar si la imagen existe antes de eliminarla
        if (file_exists(public_path($item->img))) {
            // Eliminar la imagen
            unlink(public_path($item->img));
        }
        
        $item->delete();
        session()->flash('message-success', '¡El curso fue eliminado correctamente!');
        return to_route('teacher.course.index');
    }
}
