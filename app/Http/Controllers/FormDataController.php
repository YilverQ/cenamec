<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


/*Importamos los modelos*/
use App\Models\Area;
use App\Models\Career;
use App\Models\Sector;
use App\Models\Laboral;
use App\Models\Academic;
use App\Models\Institution;
use App\Models\Academiclevel;


class FormDataController extends Controller
{
    /*Retorna los datos de un registro acádemico*/
    public function getDataAcademic(Request $request, Academic $item)
    {
        $data = ["institution"   => $item->institution,
                "academicLevel"  => $item->academicLevel,
                "career"         => $item->career,
                "area"           => $item->area,
                "sector"         => $item->sector,
                "academic_title" => $item->academic_title,
                "year_entry"     => $item->year_entry,
                "year_exit"      => $item->year_exit];
        // Construye una respuesta JSON
        return response()->json($data, 200);
    }


    /*Retorna una lista de Niveles Acádemicos*/
    public function getAcademicLevel(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $academicLevels = Academiclevel::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($academicLevels, 200);
    }


    /*Retorna una lista de Áreas de estudios*/
    public function getAreas(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $areas = Area::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($areas, 200);
    }


    /*Retorna una lista de Carreras*/
    public function getCareers(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $careers = Career::orderBy('name')->get();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($careers, 200);
    }


    /*Retorna una lista de Instituciones*/
    public function getInstitutions(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $institutions = Institution::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($institutions, 200);
    }


    /*Retorna una lista de Ámbitos de estudios*/
    public function getSectors(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $sectors = Sector::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($sectors, 200);
    }


    /*Retorna una lista de Ámbitos de estudios*/
    public function getTypeInstitution(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $sectors = Sector::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($sectors, 200);
    }



    /*Retorna los datos de un registro laboral*/
    public function getDataLaboral(Request $request, Laboral $item)
    {
        $data = ["name_intitution" => $item->name_intitution,
                "type"             => $item->type,
                "area"             => $item->area,
                "position"         => $item->position,
                "year_entry"       => $item->year_entry,
                "year_exit"        => $item->year_exit,
                "user_id"          => $item->user_id];
        // Construye una respuesta JSON
        return response()->json($data, 200);
    }
}
