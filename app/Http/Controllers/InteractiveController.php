<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Models*/
use App\Models\Module;
use App\Models\Content;
use App\Models\InteractiveEvaluation;

use App\Models\ItemEvaluation;
use App\Models\SelectionSimple;
use App\Models\TrueFalse;
use App\Models\ShortAnswer;
use App\Models\Sequence;
use App\Models\Matching;
use App\Models\ComparativeTable;
use App\Models\TableItem;


class InteractiveController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }

    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function createInteractive(Module $item)
    {
        //Cantidad de contenidos que existen para el módulo
        $contents = Content::where('modulo_id', $item->id)->get();

        //Creamos el contenido
        $content = new Content;
        $content->orden     = count($contents) + 1;
        $content->type      = "Evaluación";
        $content->modulo_id = $item->id;
        $content->save();

        //contents
        $interactive = new InteractiveEvaluation;
        $interactive->content_id = $content->id;
        $interactive->save();

        return redirect()->route('interactive', $interactive);
    }
    
    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function interactive(Request $request, InteractiveEvaluation $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        $module_id  = $item->content->module->id;
        $module = Module::where('id', $module_id)->first();

        $course = $module->course;

        $itemEvaluations = $item->itemEvaluations->sortBy('orden');
        return view('evaluations.interactive.show')
                                ->with('role', $role)
                                ->with('interactiveEvaluation', $item)
                                ->with('itemEvaluations', $itemEvaluations)
                                ->with('course', $course)
                                ->with('module', $module);
    }

    /**
     * Eliminamos una evaluación interactiva.
     */
    public function destroyInteractive(Request $request, InteractiveEvaluation $item)
    {
        $contentElement = $item->content;
        $module = $contentElement->module;
        $contents_module  = Content::where('modulo_id', '=', $module->id)->get();

        //Acomodamos el campo level.
        foreach ($contents_module as $key => $value) {
            if ($value->orden > $contentElement->orden) {
                $value->orden -= 1;
                $value->save();
            }
        }
        
        $item->delete();
        $contentElement->delete();
        session()->flash('message-success', '¡La Evaluación Interactiva fue eliminada!');
        return to_route('teacher.module.show', $module);
    }

    /**
     * Método privado para crear un Item Evaluation.
     */
    private function createItemEvaluation(Request $request, InteractiveEvaluation $item, $type)
    {
        //Cantidad de items que existen para la Evaluación Interactiva
        $itemsCant = $item->itemEvaluations;

        //Creamos un Item
        $itemEvaluation = new ItemEvaluation;
        $itemEvaluation->statement  = $request->input("statement");
        $itemEvaluation->feedback   = $request->input("feedback");
        $itemEvaluation->orden      = count($itemsCant) + 1;
        $itemEvaluation->type       = $type;
        $itemEvaluation->interactive_evaluation_id = $item->id;
        $itemEvaluation->save();

        return $itemEvaluation;
    }


    /**
     * Creamos un elemento de Selección Simple.
     */
    public function createSelectionSimple(Request $request, InteractiveEvaluation $item)
    {
        //Creamos un Item
        $itemEvaluation = $this->createItemEvaluation($request, $item, "Selección Simple");

        //Creamos un SelectionSimple
        $selectionSimple = new SelectionSimple;
        $selectionSimple->answer    = $request->input("answer");
        $selectionSimple->bad1      = $request->input("bad1");
        $selectionSimple->bad2      = $request->input("bad2");
        $selectionSimple->bad3      = $request->input("bad3");
        $selectionSimple->item_evaluation_id = $itemEvaluation->id;
        $selectionSimple->save();

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }

    /**
     * Creamos un Item de ShortAnswer.
     */
    public function createShortAnswer(Request $request, InteractiveEvaluation $item)
    {
        // Si el array está vacío, mostramos un mensaje flash y redirigimos
        $wordsArray = json_decode($request->input('wordAdd'), true);
        if (empty($wordsArray)) {
            session()->flash('message-error', 'Debe agregar al menos una palabra.');
            return redirect()->back();  
        }
        
        //Creamos un Item
        $itemEvaluation = $this->createItemEvaluation($request, $item, "Respuesta Corta");

        // Decodificar el campo 'words' para convertirlo en un array
        $wordsArray = json_decode($request->input('wordAdd'), true);
        
        // Creamos ShortAnswers para cada palabra
        foreach ($wordsArray as $word) {
            $shortAnswer = new ShortAnswer();
            $shortAnswer->word = $word;
            $shortAnswer->item_evaluation_id = $itemEvaluation->id;
            $shortAnswer->save();
        }

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }

    /**
     * Creamos un Item de Verdadero o Falso.
     */
    public function createTrueFalse(Request $request, InteractiveEvaluation $item)
    {
        //Creamos un Item
        $itemEvaluation = $this->createItemEvaluation($request, $item, "Verdadero o Falso");

        //Creamos un Item de Verdadero o Falso
        $trueFalse = new TrueFalse;
        $trueFalse->answer = $request->input("answer");
        $trueFalse->item_evaluation_id = $itemEvaluation->id;
        $trueFalse->save();

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }


    /**
     * Creamos un Item de Emparejamiento.
     */
    public function createMatching(Request $request, InteractiveEvaluation $item)
    {
        $couples = $request->input("couples"); 

        // Verificamos si la cadena 'couples' no está vacía
        if (empty($couples)) {
            session()->flash('message-error', 'No se han ingresado pares.');
            return redirect()->route('interactive', $item);
        }

        $pairs = explode('; ', $couples); 

        // Verificamos si la cadena de pares no está vacía después de dividirla
        if (count($pairs) == 0 || ($count = count(array_filter($pairs))) == 0) {
            session()->flash('message-error', 'Datos incorrectos. Por favor revisar.');
            return redirect()->route('interactive', $item);
        }

        $itemEvaluation = $this->createItemEvaluation($request, $item, "Emparejamiento");
        foreach ($pairs as $pair) {
            $elements = explode(' - ', $pair);

            // Verificamos que realmente tengamos dos elementos en la división
            if (count($elements) == 2) {
                list($elementA, $elementB) = $elements;

                $matching = new Matching;
                $matching->elementA = trim($elementA); 
                $matching->elementB = trim($elementB);
                $matching->item_evaluation_id = $itemEvaluation->id;
                $matching->save();
            } else {
                session()->flash('message-error', 'Datos incorrectos. Por favor revisar.');
                return redirect()->route('interactive', $item);
            }
        }

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }


    /**
     * Creamos un item de Secuencia.
     */
    public function createSequence(Request $request, InteractiveEvaluation $item)
    {
        // Decodificar el campo 'sequence' para convertirlo en un array
        $sequenceWords = explode(',', $request->input('sequence'));
        
        // Validar que haya al menos 2 elementos
        if (count($sequenceWords) < 2) {
            session()->flash('message-error', 'Debes agregar al menos dos elementos a la secuencia.');
            return redirect()->back()->withInput();
        }

        //Creamos un Item
        $itemEvaluation = $this->createItemEvaluation($request, $item, "Secuencia");

        
        // Creamos un item de Secuencia
        $orden_sequence = 0;
        foreach ($sequenceWords as $word) {
            $orden_sequence += 1;
            $sequence = new Sequence;
            $sequence->description      = $word;
            $sequence->orden            = $orden_sequence;
            $sequence->item_evaluation_id = $itemEvaluation->id;
            $sequence->save();
        }

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }

    /**
     * Creamos un Item de Cuadro Comparativo.
     */
    public function createComparativeTable(Request $request, InteractiveEvaluation $item)
    {
        //Creamos un Item
        $itemEvaluation = $this->createItemEvaluation($request, $item, "Cuadro Comparativo");

        //Creamos un Cuadro Comparativo
        $comparativeTable = new ComparativeTable;
        $comparativeTable->columnA = $request->input("titleColumnA");
        $comparativeTable->columnB = $request->input("titleColumnB");
        $comparativeTable->columnC = $request->input("titleColumnC");
        $comparativeTable->item_evaluation_id = $itemEvaluation->id;
        $comparativeTable->save();


        // Arreglo que asocia cada columna con sus elementos
        $columns = [
            'A' => json_decode($request->input('elementsA'), true),
            'B' => json_decode($request->input('elementsB'), true),
            'C' => json_decode($request->input('elementsC'), true),
        ];

        // Crear un TableItem para cada palabra en las columnas
        foreach ($columns as $column => $elements) {
            foreach ($elements as $word) {
                $tableItem = new TableItem;
                $tableItem->description  = $word;
                $tableItem->column       = $column;
                $tableItem->comparative_table_id = $comparativeTable->id;
                $tableItem->save();
            }
        }

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('interactive', $item);
    }


    /**
     * Eliminamos un item de Selección Simple.
     */
    public function destroyItemEvaluation(Request $request, ItemEvaluation $item)
    {
        $interactiveEvaluation = $item->interactiveEvaluation;

        // Acomodamos el campo level.
        ItemEvaluation::where('orden', '>', $item->orden)
            ->decrement('orden');

        $item->delete();
        session()->flash('message-success', '¡El Item fue eliminado!');
        return redirect()->route('interactive', $interactiveEvaluation);
    }
}
