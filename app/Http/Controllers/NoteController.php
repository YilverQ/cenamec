<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/*Importamos los modelos*/
use App\Models\Note;
use App\Models\Module;
use App\Models\Course;
use App\Models\Content;


class NoteController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }


    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function create(Request $request)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        $module_id  = $request->session()->get('module_id');
        $module = Module::where('id', $module_id)->first();

        $course = $module->course;
        
        return view('note.create')
                ->with("role", $role)
                ->with('course', $course)
                ->with('module', $module);
    }


    /**
     * Acción para crear un nuevo elemento.
     */
    public function store(Request $request)
    {
        //Obtenemos los datos básicos
        $module_id  = $request->session()->get('module_id');
        $module = Module::find($module_id);

        //Persistimos los datos.
        $content_editor = $request->input('myeditorinstance');
        if (!$content_editor) {
            session()->flash('message-error', '¡La nota educativa está vacía!');
            return back();
        }

        //Cantidad de contenidos que existen para el módulo
        $cant_content = Content::where('modulo_id', $module_id)->get();

        //Creamos el contenido
        $content = new Content;
        $content->orden     = count($cant_content) + 1;
        $content->type      = "Nota";
        $content->modulo_id = $module_id;
        $content->save();

        //Creamos la nota
        $note = new Note;
        $note->title       = $request->input('super_name');
        $note->content     = $content_editor;
        $note->content_id  = $content->id;
        $note->save();

        session()->flash('message-success', '¡La nota educativa fue creada!');
        return redirect()->route('teacher.module.show', $module);
    }


    /**
     * Retornamos un formulario que nos permite actualizar un elemento. 
     */
    public function edit(Request $request, Note $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $module = $item->module;
        $course = $module->course;
        
        return view('note.edit')
                ->with("role", $role)
                ->with('module', $module)
                ->with('course', $course)
                ->with('note', $item);
    }


     /**
     * Acción para actualizar un elemento.
     * 
     * Comprobamos si se quiere actualizar la imágen. 
     *      1. Se comprueba el campo de la imágen. 
     *      2. Se comprueba que la imágen ingresada sea correcta.
     *      3. Se guarda la imágen.
     *    
     * Se guardan los datos en bd.
     */
    public function update(Request $request, Note $item)
    {
        //Obtenemos los datos básicos
        $module_id = $item->module->id;
        $module = Module::where('id', $module_id)->first();

        //Persistimos los datos
        $item->title       = $request->input('super_name');
        $item->content     = $request->input('myeditorinstance');
        $item->save();
        

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡La nota educativa fue actualizada!');
        return to_route('teacher.module.show', $module);
    }


    /**
     * Acción para eliminar un elemento.
     * 
     * Buscamos el módulo que corresponde la nota. 
     * Buscamos todos los contenidos asociados al módulo.
     * Eliminamos el elemento de nuestra base de datos.
     * Reordenamos los elementos (Content) asociados al módulo. 
     * Envíamos un mensaje flash.
     * Retornamos la vista principal y envíamos el modulo a la vista.  
     */
    public function destroy(Request $request, Note $item)
    {
        $contentElement = $item->contentTable;
        $module = $item->module;
        $contents_module  = Content::where('modulo_id', '=', $item->module->id)->get();

        //Acomodamos el campo level.
        foreach ($contents_module as $key => $value) {
            if ($value->orden > $item->contentTable->orden) {
                $value->orden -= 1;
                $value->save();
            }
        }
        
        $item->delete();
        $contentElement->delete();
        session()->flash('message-success', '¡La nota educativa fue eliminada correctamente!');
        return to_route('teacher.module.show', $module);
    }
}
