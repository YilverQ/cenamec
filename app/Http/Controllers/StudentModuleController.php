<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Course;
use App\Models\Module;
use App\Models\Content;
use App\Models\Certificate;
use App\Models\ViewContent;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StudentModuleController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de estudiante.
     */
    public function __construct()
    {
        $this->middleware('auth.student');
    }

    /**
     * Elimimnamos todos las evaluaciones que no tengan item.
     */
    private function deleteEvaluationNull(Module $item)
    {
        // Buscamos y eliminamos contenidos relacionados con el módulo que son autoevaluaciones sin likerts
        Content::where('modulo_id', $item->id)
            ->where('type', 'Autoevaluación')
            ->doesntHave('autoevaluation.likerts')
            ->delete();

        Content::where('modulo_id', $item->id)
            ->where('type', 'Evaluación')
            ->doesntHave('interactiveEvaluation.itemEvaluations')
            ->delete();
    }

    /**
     * Obtenemos los datos de la autoevaluación por parte del estudiante.
     */
    private function handleAutoevaluation(Request $request, Student $student)
    {
        // Recoger las respuestas del formulario
        $responses = $request->except('_token'); // Excluimos el token CSRF

        // Crear las entradas en la tabla 'student_likert'
        foreach ($responses as $questionKey => $selection) {
            // Obtener el ID de la pregunta a partir de la clave
            $likert_id = substr($questionKey, 2); 
            $student->likerts()->attach($likert_id, ['selection' => $selection]);
        }
    }

    /**
     * Calculamos el porcentaje de progreso del módulo.
     */
    private function handlePercentage($student_id, $module_id)
    {
        // Obtener el módulo y los contenidos relacionados
        $module = Module::find($module_id);

        // Contamos los contenidos completados para el estudiante en este módulo
        $completedContentsCount = $module->contents()
            ->join('content_student', 'contents.id', '=', 'content_student.content_id')
            ->where('content_student.student_id', $student_id)
            ->where('content_student.state', 'Completado')
            ->count();

        // Contamos el total de contenidos en el módulo
        $totalContentsCount = $module->contents()->count();

        // Calculamos el porcentaje de completado
        $percentage = $totalContentsCount > 0 ? ($completedContentsCount / $totalContentsCount) * 100 : 0;

        // Actualizamos el porcentaje en la tabla module_student usando DB::table
        DB::table('module_student')
            ->where('module_id', $module_id)
            ->where('student_id', $student_id)
            ->update(['percentage' => round($percentage)]);
    }

    /**
     * Aprueba el módulo cuando no hay siguiente contenido.
     */
    private function approveModule(Module $module, $student_id)
    {
        // Actualizamos el estado y porcentaje del módulo en la tabla intermedia
        DB::table('module_student')
            ->where('module_id', $module->id)
            ->where('student_id', $student_id)
            ->update([
                'state' => 'finished', 
                'percentage' => 100,
                'end_date' => now(),
            ]);

        // Obtener el siguiente módulo en el curso
        $nextModule = $module->course->modules()
                             ->where('id', '>', $module->id)
                             ->orderBy('id')
                             ->first();

        // Si existe un siguiente módulo, lo activamos
        if ($nextModule) {
            $moduleStudentPivot = DB::table('module_student')
                ->where('module_id', $nextModule->id)
                ->where('student_id', $student_id)
                ->first();

            // Si no ha sido aprobado aún, lo activamos
            if ($moduleStudentPivot && $moduleStudentPivot->state == 'inactive') {
                DB::table('module_student')
                    ->where('module_id', $nextModule->id)
                    ->where('student_id', $student_id)
                    ->update(['state' => 'active', 'start_date' => now()]);
            }
        }
        else {
            DB::table('course_student')
                ->where('student_id', $student_id)
                ->where('course_id', $module->course->id)
                ->update(['dateFinished' => now()]);
        }
    }


    /**
     * Retorna una vista para estudiar.
     */
    public function study(Request $request, Module $item)
    {
        //Eliminamos las evaluaciones vacias. 
        $this->deleteEvaluationNull($item);
        
        //Buscamos los datos del estudiante.
        $student_id = $request->session()->get('student_id');
        $student = Student::find($student_id);

        // Obtener todos los contenidos del módulo ordenados
        $contents = $item->contents()->orderBy('orden')->get();

        // Obtenemos el pivot para la relación module_student. 
        $moduleStudentPivot = $student->modules()->where('module_id', $item->id)->first();
        if ($moduleStudentPivot && !$moduleStudentPivot->pivot->start_date) {
            $student->modules()->updateExistingPivot($item->id, ['start_date' => now()]);
        }

        // Obtener el primer contenido ordenado
        $firstContent = $item->contents()->orderBy('orden')->first();

        // Verificar si la relación entre el estudiante y el contenido ya existe
        if (!$student->contents->contains($firstContent)) {
            // Si no existe la relación, la creamos
            $student->contents()->attach($firstContent, ['state' => 'Activo']);
        }

        // Encontrar el índice del contenido actual
        $currentIndex = $contents->search(function($content) use ($item) {
            return $content->id == $item->id;
        });

        // Obtener el siguiente contenido
        $nextItem = $contents->get($currentIndex + 1);


        // Obtener el primer contenido activo de la relación content_student
        $activeContentStudent = $student->contents()
                    ->wherePivot('state', 'Activo')
                    ->where('modulo_id', $item->id)
                    ->first();

        // Si no hay contenido activo, obtener el primer contenido (completado o no)
        $repeatContent = null;
        if (!$activeContentStudent) {
            $repeatContent = $student->contents()
                ->where('modulo_id', $item->id)
                ->orderBy('content_student.created_at', 'asc')
                ->first();
        }

        // Obtener el estado de cada contenido para el estudiante
        $contentStates = DB::table('content_student')
            ->where('student_id', $student_id)
            ->pluck('state', 'content_id');


        // Retornamos todos los datos a la vista
        return view('studentModule.study')
            ->with('activeContentStudent', $activeContentStudent)
            ->with('repeatContent', $repeatContent)
            ->with('contentStates', $contentStates)
            ->with('module', $item)
            ->with('nextItem', $nextItem)
            ->with('contents', $contents);
    }


    /**
     * Retorna una vista para estudiar.
     */
    public function studyContent(Request $request, Content $item)
    {
        $student_id = $request->session()->get('student_id');
        $student = Student::find($student_id);

        // Obtener todos los contenidos del módulo ordenados
        $module = $item->module;
        $contents = $module->contents()->orderBy('orden')->get();
        $this->deleteEvaluationNull($module);

        // Obtener el primer contenido activo de la relación content_student
        $activeContentStudent = $student->contents()
                    ->wherePivot('state', 'Activo')
                    ->where('modulo_id', $module->id)
                    ->first();

        // Obtener el estado de cada contenido para el estudiante
        $contentStates = DB::table('content_student')
            ->where('student_id', $student_id)
            ->pluck('state', 'content_id');

        // Crear un nuevo registro en view_content
        $viewContent = $student->viewContents()->create([
            'content_id' => $item->id,
            'start_view' => now(),
        ]);
        session(['current_view_content' => $viewContent]);
        
        // Obtener el siguiente contenido directamente desde la base de datos
        $nextContent = $viewContent->content->module->contents()
            ->where('orden', '>', $viewContent->content->orden) // Buscar el siguiente por el campo 'orden'
            ->orderBy('orden', 'asc') // Asegurarse de obtener el más cercano
            ->first();


        // Retornamos todos los datos a la vista
        return view('studentModule.content')
            ->with('activeContentStudent', $activeContentStudent)
            ->with('contentStates', $contentStates)
            ->with('content', $item)
            ->with('module', $module)
            ->with('contents', $contents)
            ->with('nextItem', $nextContent)
            ->with('viewContent', $viewContent); 
    }


    /**
     * Se ha aprobado el contenido.
     */
    public function passContent(Request $request, Content $item, Content $next = null)
    {
        $student_id = $request->session()->get('student_id');
        $student = Student::find($student_id);

        if ($item->type != 'Evaluación') {
            $current_view_content = $request->session()->get('current_view_content');
            $current_view_content->update([
                'mean' => 100,
                'end_view' => now(),
                'time_view' => now()->diff($current_view_content->start_view)->format('%H:%I:%S'),
            ]);
            $current_view_content->save();
        }

        // Verificar el tipo de contenido
        if ($item->type == 'Autoevaluación') {
            $this->handleAutoevaluation($request, $student);
        }

        // Actualizamos el contenido actual
        $item->students()->updateExistingPivot($student_id, ['state' => 'Completado',]);


        // Si existe un siguiente contenido, se actualiza o se asigna
        if ($next) {
            $nextContent = $next->students()->where('student_id', $student_id)->first();
            if (!$nextContent) {
                // Si el siguiente contenido no está asociado con el estudiante, lo asociamos
                $next->students()->attach($student_id, ['state' => 'Activo']);
            }

            // Actualizamos el porcentaje del módulo
            $this->handlePercentage($student_id, $item->module->id);

            session()->flash('message-success', '¡Felicidades, has aprobado el contenido!'); 
            return redirect()->route('student.studyContent', ['item' => $next->id]);

        } else {
            // Si no existe un siguiente contenido, aprobamos el módulo
            $this->approveModule($item->module, $student_id);

            session()->flash('message-success', '¡Felicidades, has aprobado el módulo!'); 
            return redirect()->route('student.course.display', $item->module->course);
        }
    }


    /**
     * Retorna una vista para estudiar.
     */
    public function passModule(Request $request, Module $item, Content $content = null)
    {
        $student_id = $request->session()->get('student_id');
        $student = Student::find($student_id);

        $this->approveModule($item, $student_id);
        session()->flash('message-success', '¡Felicidades, has aprobado el módulo!');
        return redirect()->route('student.course.display', $item->course);
    }
}
