<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

/*Importamos los modelos*/
use App\Models\Course;
use App\Models\Module;

class SharedCourseController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * delete.sessions -> Elimina las sessiones del servidor.
     * Solamente se aplica a algunos métodos.
     */
    public function __construct()
    {
        $this->middleware('delete.sessions')
                ->only(['sharedNotUser']);
    }

    /**
     * Verificamos que el usuario esté loggeado. 
     * Si es así retorna una vista para inscribirse en el curso. 
     * En caso contrario, retorna una vista para ver el curso.  
     */
    public function sharedCourse(Request $request, int $item)
    {        
        $user_id = $request->session()->get('user_id');
        $student_id = $request->session()->get('student_id');
        $item = Course::find($item);

        if ($item == null or $item->state != 'Publicado') {
            //Página no found.
            return redirect()->back()->with('message-error', 'El curso no está disponible.');
        }

        //Agregamos un valor al contador del curso.
        $item->shared += 1;
        $item->save();

        if ($user_id == null || $student_id == null) {
            //Usuario no registrado o de tipo administrador o profesor.
            return $this->sharedNotUser($request, $item);
        }

        //Usuario registrado como estudiante.
        return redirect()->route('student.course.show', $item);
    }

    /**
     * Verificamos que el usuario esté loggeado. 
     * Si es así retorna una vista para inscribirse en el curso. 
     * En caso contrario, retorna una vista para ver el curso.  
     */
    public function sharedNotUser(Request $request, Course $item)
    {    
        //Buscamos los datos. 
        $modules = Module::where('course_id', $item->id)
                        ->orderBy('level')
                        ->get(); 

        //Retornamos todos los datos a la vista. 
        return view('login.courseView')
                ->with("course", $item)
                ->with("modules", $modules);
    }
}
