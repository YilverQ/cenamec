<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

/*Importamos los modelos*/
use App\Models\Course;

/*Exports*/
use App\Exports\StudentsByCourseExport;
use App\Exports\StudentsExport;
use App\Exports\TeachersExport;
use App\Exports\AdminsExport;
use App\Exports\DisabledUsersExport;


class ExportDataController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin')->only('usersByCourse');
        $this->middleware('auth.admin')->except('usersByCourse');
    }

    /**
     * Exportar usuarios asociado al curso
     */
    public function usersByCourse(Course $item)
    {
        $date = Carbon::parse(now())->format('d-m-Y');
        $nameFile = 'Lista de Estudiantes - ' . $item->name . ' - '. $date .'.csv';
        return Excel::download(new StudentsByCourseExport($item->id), $nameFile, \Maatwebsite\Excel\Excel::CSV);
    }


    /**
     * Exportar lista de estudiantes
     */
    public function listStudents()
    {
        $date = Carbon::parse(now())->format('d-m-Y');
        $nameFile = 'Lista de Estudiantes - ' . $date .'.csv';
        return Excel::download(new StudentsExport(), $nameFile, \Maatwebsite\Excel\Excel::CSV);
    }

    /**
     * Exportar lista de profesores
     */
    public function listTeachers()
    {
        $date = Carbon::parse(now())->format('d-m-Y');
        $nameFile = 'Lista de Profesores - ' . $date .'.csv';
        return Excel::download(new TeachersExport(), $nameFile, \Maatwebsite\Excel\Excel::CSV);
    }


    /**
     * Exportar lista de profesores
     */
    public function listAdmins()
    {
        $date = Carbon::parse(now())->format('d-m-Y');
        $nameFile = 'Lista de Administradores - ' . $date .'.csv';
        return Excel::download(new AdminsExport(), $nameFile, \Maatwebsite\Excel\Excel::CSV);
    }


    /**
     * Exportar lista de usuarios deshabilitados
     */
    public function listDisabledUsers()
    {
        $date = Carbon::parse(now())->format('d-m-Y');
        $nameFile = 'Lista de Usuarios Deshabilitados - ' . $date .'.csv';
        return Excel::download(new DisabledUsersExport(), $nameFile, \Maatwebsite\Excel\Excel::CSV);
    }
}
