<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Models*/
use App\Models\Likert;
use App\Models\Module;
use App\Models\Content;
use App\Models\Autoevaluation;

class LikertController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }

    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function createItem(Request $request, Module $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $module_id  = $item->id;
        $module = Module::where('id', $module_id)->first();
        $course = $module->course;

        return view('evaluations.likert.create')
                                ->with('role', $role)
                                ->with('course', $course)
                                ->with('module', $module);
    }

    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function store(Request $request, Module $item)
    {
        //Cantidad de contenidos que existen para el módulo
        $contents = Content::where('modulo_id', $item->id)->get();

        //Creamos el contenido
        $content = new Content;
        $content->orden     = count($contents) + 1;
        $content->type      = "Autoevaluación";
        $content->modulo_id = $item->id;
        $content->save();

        //contents
        $autoevaluation = new Autoevaluation;
        $autoevaluation->content_id = $content->id;
        $autoevaluation->save();

        //Creamos el contenido
        $likert = new Likert;
        $likert->affirmation = $request->input("affirmation");
        $likert->autoevaluation_id = $autoevaluation->id;
        $likert->save();

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('likert', $autoevaluation);
    }
    
    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function likert(Request $request, Autoevaluation $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        $module_id  = $item->content->module->id;
        $module = Module::where('id', $module_id)->first();

        $course = $module->course;

        $itemsLikerts = $item->likerts()->orderBy('created_at', 'asc')->get();

        return view('evaluations.likert.show')
                                ->with('role', $role)
                                ->with('autoevaluation', $item)
                                ->with('likerts', $itemsLikerts)
                                ->with('course', $course)
                                ->with('module', $module);
    }

    /**
     * Agregamos un item.
     */
    public function addItem(Request $request, Autoevaluation $item)
    {
        //Creamos el contenido
        $likert = new Likert;
        $likert->affirmation = $request->input("affirmation");
        $likert->autoevaluation_id = $item->id;
        $likert->save();

        session()->flash('message-success', '¡El item fue creado!');
        return redirect()->route('likert', $item);
    }

    public function update(Request $request, Autoevaluation $item)
    {
        $likert = Likert::find($request->input("editItemId"));
        $likert->affirmation = $request->input("affirmation");
        $likert->save();
        
        session()->flash('message-success', '¡El Enunciado fue editado!');
        return redirect()->route('likert', $item);
    }

    /**
     * Eliminamos un Item.
     */
    public function destroyItem(Request $request, Likert $item)
    {
        $autoevaluation = $item->autoevaluation;
        $item->delete();
        session()->flash('message-success', '¡El Enunciado fue Eliminado!');
        return redirect()->route('likert', $autoevaluation);
    }


    /**
     * Eliminamos un item.
     */
    public function destroy(Request $request, Autoevaluation $item)
    {
        $contentElement = $item->content;
        $module = $contentElement->module;
        $contents_module  = Content::where('modulo_id', '=', $module->id)->get();

        //Acomodamos el campo level.
        foreach ($contents_module as $key => $value) {
            if ($value->orden > $contentElement->orden) {
                $value->orden -= 1;
                $value->save();
            }
        }
        
        $item->delete();
        $contentElement->delete();
        session()->flash('message-success', '¡La autoevaluación fue eliminada correctamente!');
        return to_route('teacher.module.show', $module);
    }
}
