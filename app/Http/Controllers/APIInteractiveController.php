<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Importamos los modelos*/
use App\Models\InteractiveEvaluation;
use App\Models\ItemEvaluation;
use App\Models\SelectionSimple;
use App\Models\TrueFalse;
use App\Models\ShortAnswer;
use App\Models\Sequence;
use App\Models\Matching;
use App\Models\ComparativeTable;
use App\Models\TableItem;

class APIInteractiveController extends Controller
{
    /* Retorna una lista de imágenes del perfil */
    public function getItemEvaluation(ItemEvaluation $item)
    {
        // Obtener los pares asociados al ítem
        $pairs = Matching::where('item_evaluation_id', $item->id)->get();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json([
            'item' => $item,
            'pairs' => $pairs
        ], 200);
    }

}
