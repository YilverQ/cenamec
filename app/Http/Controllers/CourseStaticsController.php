<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/*Importamos los modelos*/
use App\Models\Tag;
use App\Models\Course;
use App\Models\Module;
use App\Models\Teacher;
use App\Models\Audience;
use App\Models\Certificate;
use App\Models\ViewContent;

class CourseStaticsController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }

    /**
     * Contamos la cantidad de items por tipo.
     */
    private function getCantTypeContent($modules)
    {
        // Tipos de ítems a contar
        $typesToCount = [
            'Lección Educativa' => $modules->flatMap->contents->where('type', 'Nota')->count(),
            'Autoevaluación' => $modules->flatMap->contents->where('type', 'Autoevaluación')->count(),
            'Evaluación Interactiva' => $modules->flatMap->contents->where('type', 'Evaluación')->count(),
        ];

        return $typesToCount;
    }

    /**
     * Contamos la cantidad de items por tipo.
     */
    private function getCantTypeItems($modules)
    {
        // Tipos de ítems a contar
        $typesToCount = [
            'Selección Simple',
            'Respuesta Corta',
            'Verdadero o Falso',
            'Secuencia',
            'Emparejamiento',
            'Cuadro Comparativo',
        ];

        // Inicializamos el array de conteos
        $counts = ['Escala de Likert' => $this->getCantTypeLikertItems($modules)];

        // Recorremos todos los módulos del curso
        foreach ($modules as $module) {
            // Filtramos los contenidos que tienen evaluación interactiva
            $filteredContents = $module->contents->map(function ($content) {
                return $content->interactiveEvaluation;
            })->filter();

            // Contamos los ítems por tipo para este módulo
            foreach ($typesToCount as $type) {
                if (!isset($counts[$type])) {
                    $counts[$type] = 0; // Inicializamos el contador si no existe
                }
                $counts[$type] += $filteredContents->map(function ($evaluation) use ($type) {
                    return $evaluation->itemEvaluations->where('type', $type)->count();
                })->sum();
            }
        }
        return $counts;
    }

    /**
     * Contamos la cantidad de items por escala de likert.
     */
    private function getCantTypeLikertItems($modules)
    {
        // Inicializamos el contador
        $likertCount = 0;

        // Recorremos todos los módulos del curso
        foreach ($modules as $module) {
            // Filtramos los contenidos que tienen autoevaluación
            $filteredContents = $module->contents->map(function ($content) {
                return $content->autoevaluation;
            })->filter();

            // Contamos los ítems de tipo Likert para este módulo
            $likertCount += $filteredContents->map(function ($autoevaluation) {
                return $autoevaluation->likerts->count();
            })->sum();
        }

        return $likertCount;
    }

    /**
     * Contamos la cantidad de contenidos vistos por fechas.
     */
    private function getCantViewContentFromDate(Course $course)
    {
        // Obtener los viewContents asociados al curso, filtrar por el último mes
        $lastMonth = now()->subMonth();
        $viewContents = $course->modules
            ->flatMap->contents
            ->flatMap->viewContents
            ->whereNotNull('mean')
            ->filter(function ($viewContent) use ($lastMonth) {
                return $viewContent->created_at >= $lastMonth;
            });

        // Agrupar los viewContents por fecha
        $viewData = $viewContents
            ->groupBy(function ($viewContent) {
                return $viewContent->created_at->format('d-m-Y'); // Agrupar por fecha (sin hora)
            })
            ->map(function ($group) {
                return $group->count(); 
            })
            ->sortKeysDesc(); 

        // Dividir los datos en labels (fechas) y values (cantidad de registros)
        $labels = $viewData->keys()->toArray(); 
        $values = $viewData->values()->toArray(); 
        $dateStared = $lastMonth->format('d-m-Y');
        $dateFinished = now()->format('d-m-Y');

        return [$labels, $values, $dateStared, $dateFinished];
    }

    /**
     * Distribución de género.
     */
    private function getCantGender($students)
    {
        $feminine = $students->filter(function ($student) {
            return $student->user->gender === 'Femenino'; // Ajusta según el valor exacto que uses para femenino
        })->count();

        $masculine = $students->filter(function ($student) {
            return $student->user->gender === 'Masculino'; // Ajusta según el valor exacto que uses para masculino
        })->count();

        return [$feminine, $masculine];
    }


    /**
     * Cantidad de evaluaciones interactivas respondidas según su tipo.
     */
    private function getCantTypeContentResponse(Course $course)
    {
        // Definir los tipos de contenido que se van a contar
        $typesToCount = [
            'Selección Simple',
            'Respuesta Corta',
            'Verdadero o Falso',
            'Secuencia',
            'Emparejamiento',
            'Cuadro Comparativo',
        ];

        // Obtener todas las respuestas relacionadas con los contenidos del curso
        $responses = $course->modules
                            ->flatMap->contents
                            ->flatMap->viewContents
                            ->whereNotNull('mean')
                            ->flatMap->answers
                            ->where('is_correct');

        // Inicializar un array para almacenar el conteo por tipo de contenido
        $countByType = array_fill_keys($typesToCount, 0);

        // Contar las respuestas por tipo de contenido
        foreach ($responses as $response) {
            if ($response->selectionSimple) {
                $countByType['Selección Simple']++;
            } elseif ($response->trueFalse) {
                $countByType['Verdadero o Falso']++;
            } elseif ($response->shortAnswer) {
                $countByType['Respuesta Corta']++;
            } elseif ($response->sequences->isNotEmpty()) {
                $countByType['Secuencia']++;
            } elseif ($response->matchings->isNotEmpty()) {
                $countByType['Emparejamiento']++;
            } elseif ($response->tableItems->isNotEmpty()) {
                $countByType['Cuadro Comparativo']++;
            }
        }

        return $countByType;
    }

    /**
     * Obtenemos información acerca de las lecciones educativas vistas.
     */
    private function getNotesViewedInfo($modules)
    {
        $responses = $modules
                        ->flatMap->contents
                        ->flatMap->viewContents
                        ->whereNotNull('end_view')
                        ->filter(function ($viewContent) {
                            return $viewContent->content->type === 'Nota';
                        });
                        
        // Si no hay respuestas, devolver valores predeterminados
        if ($responses->isEmpty()) {
            return [
                'average_time_seconds' => '00:00',
                'total_viewed_notes' => 0,
                'average_notes_viewed_per_user' => 0,
            ];
        }

        // Promedio de tiempo de visualización
        $totalTime = $responses->map(function ($viewContent) {
            $timeParts = explode(':', $viewContent->time_view);
            $totalSeconds = $timeParts[0] * 3600 + $timeParts[1] * 60 + $timeParts[2];
            return $totalSeconds;
        })->sum();

        $averageTime = $totalTime / $responses->count();
        $minutes = floor($averageTime / 60);
        $seconds = $averageTime % 60;
        $averageTimeFormatted = sprintf('%02d:%02d', $minutes, $seconds);

        // Cantidad de items
        $countItems = $responses->count();

        // Promedio de items vistos por usuario
        $uniqueUsers = $responses->unique('student_id')->count();
        $averageItemsPerUser = $countItems / $uniqueUsers;

        return [
            'average_time_seconds' => $averageTimeFormatted,
            'total_viewed_notes' => $countItems,
            'average_notes_viewed_per_user' => round($averageItemsPerUser, 0),
        ];
    }


    /**
     * Obtenemos información acerca de las Autoevaluaciones.
     */
    private function getAutoevaluationViewedInfo($modules)
    {
        // Obtener las autoevaluaciones completadas
        $completedAutoevaluations = $modules
            ->flatMap->contents
            ->flatMap->viewContents
            ->whereNotNull('end_view')
            ->filter(function ($viewContent) {
                return $viewContent->content->type === 'Autoevaluación';
            });

        // 1. Cantidad de autoevaluaciones completadas
        $totalCompleted = $completedAutoevaluations->count();

        $totalTime = 0;
        foreach ($completedAutoevaluations as $viewContent) {
            $timeParts = explode(':', $viewContent->time_view);
            $totalSeconds = $timeParts[0] * 3600 + $timeParts[1] * 60 + $timeParts[2];
            $totalTime += $totalSeconds; // Suma los tiempos en segundos
        }

        $averageTime = $totalCompleted > 0 ? $totalTime / $totalCompleted : 0;

        // Convertir el promedio a minutos y segundos
        $minutes = floor($averageTime / 60);
        $seconds = $averageTime % 60;
        $averageTimeFormatted = sprintf('%02d:%02d', $minutes, $seconds);

        // 3. Cantidad de Likerts respondidos
        $totalLikerts = DB::table('student_likert')
            ->whereIn('student_id', $completedAutoevaluations->pluck('student_id')->unique())
            ->count();

        // 4. Promedio de autoevaluaciones respondidas por usuario
        $uniqueUsers = $completedAutoevaluations->pluck('student_id')->unique()->count();
        $meanAutoevaluationsPerUser = $uniqueUsers > 0 ? $totalCompleted / $uniqueUsers : 0;

        // 5. Promedio de Likerts respondidos por usuario
        $averageLikertsPerUser = $uniqueUsers > 0 ? $totalLikerts / $uniqueUsers : 0;

        // Imprimir la información solicitada
        $result = [
            'total_completed_autoevaluations' => $totalCompleted,
            'average_autoevaluations_per_user' => round($meanAutoevaluationsPerUser, 0), // Redondeado a 2 decimales
            'average_time_seconds' => $averageTimeFormatted,
            'total_likerts_answered' => $totalLikerts,
            'average_likerts_answered_per_user' => round($averageLikertsPerUser, 0) // Redondeado a 2 decimales
        ];
        return $result;
    }

    /**
     * Obtenemos información acerca de las evaluaciones interactivas.
     */
    private function getEvaluationViewedInfo($modules)
    {
        // Obtener las evaluaciones interactivas completadas
        $completedEvaluations = $modules
            ->flatMap->contents
            ->flatMap->viewContents
            ->whereNotNull('end_view')
            ->filter(function ($viewContent) {
                return $viewContent->content->type === 'Evaluación';
            });

        // 1. Cantidad de Evaluaciones Interactivas completadas
        $totalCompleted = $completedEvaluations->count();

        // 2. Promedio de tiempo que tarda un estudiante en la evaluación interactiva
        $totalTime = 0;
        foreach ($completedEvaluations as $viewContent) {
            $start = \Carbon\Carbon::parse($viewContent->start_view);
            $end = \Carbon\Carbon::parse($viewContent->end_view);
            $totalTime += $end->diffInSeconds($start); // Diferencia en segundos
        }
        $averageTime = $totalCompleted > 0 ? $totalTime / $totalCompleted : 0;
        $minutes = floor($averageTime / 60);
        $seconds = $averageTime % 60;
        $averageTimeFormatted = sprintf('%02d:%02d', $minutes, $seconds);

        // 3. Cantidad de Ítems de evaluación interactiva respondidos
        // Obtener los IDs de los contenidos de las evaluaciones completadas
        $evaluationContentIds = $completedEvaluations
            ->pluck('content_id')
            ->unique()
            ->toArray();

        // Obtener la cantidad de ítems respondidos para esos contenidos
        $totalItemsAnswered = DB::table('answers') // Asume que las respuestas están en la tabla 'answers'
            ->whereIn('view_content_id', $completedEvaluations->pluck('id')->unique())
            ->count();

        // 4. Promedio de ítems respondidos por usuario
        $uniqueUsers = $completedEvaluations->pluck('student_id')->unique()->count();
        $meanItemsAnswered = $uniqueUsers > 0 ? $totalItemsAnswered / $uniqueUsers : 0;

        // 5. Promedio de evaluaciones completadas por usuario
        $meanEvaluationsPerUser = $uniqueUsers > 0 ? $totalCompleted / $uniqueUsers : 0;

        // Imprimir la información solicitada
        $result = [
            'total_completed_evaluations' => $totalCompleted,
            'average_completed_evaluations' => round($meanEvaluationsPerUser, 0), // Redondeado a 2 decimales
            'average_time_seconds' => $averageTimeFormatted,
            'total_items_answered' => $totalItemsAnswered,
            'average_items_answered' => round($meanItemsAnswered, 0), // Redondeado a 2 decimales
        ];

        return $result;
    }


    public function show(Request $request, Course $item)
    {
        // Identificamos el role del usuario.
        $role = $request->session()->get('role');

        // Buscamos los datos. 
        $teachers = $item->teachers;
        $students = $item->students;
        $modules = Module::where('course_id', $item->id)
                        ->orderBy('level')
                        ->get();

        //Cantidad de certificados emitidos
        $certificates = Certificate::where('course_id', $item->id)->get();


        /*Datos para las Estadísticas*/
        [$viewContentLabels, $viewContentValues, 
            $viewContentDateStared, $viewContentDateFinished] = $this->getCantViewContentFromDate($item);

        [$feminine, $masculine] = $this->getCantGender($students);

        $last_connections = $item->students_pivot()
            ->wherePivotNotNull('last_connection') 
            ->orderByPivot('last_connection', 'desc')
            ->take(10) 
            ->get();

        $cantTypesItems         = $this->getCantTypeItems($modules);
        $cantTypesContents      = $this->getCantTypeContent($modules);
        $cantItemsResponse      = $this->getCantTypeContentResponse($item);
        $notesViewedInfo        = $this->getNotesViewedInfo($modules);
        $autoevaluationViewedInfo = $this->getAutoevaluationViewedInfo($modules);
        $evaluationViewedInfo   = $this->getEvaluationViewedInfo($modules);

        #dd($evaluationViewedInfo);

        // Retornamos todos los datos a la vista. 
        return view('course_statics.statics')
            ->with("role", $role)
            ->with("course", $item)
            ->with("modules", $modules)
            ->with("teachers", $teachers)
            ->with("students", $students)
            ->with("feminine", $feminine)
            ->with("masculine", $masculine)
            ->with("certificates", $certificates)
            ->with("cantTypesItems", $cantTypesItems)
            ->with("notesViewedInfo", $notesViewedInfo)
            ->with("cantItemsResponse", $cantItemsResponse)
            ->with("cantTypesContents", $cantTypesContents)
            ->with("last_connections", $last_connections)
            ->with("viewContentLabels", $viewContentLabels) 
            ->with("viewContentValues", $viewContentValues)
            ->with("evaluationViewedInfo", $evaluationViewedInfo)
            ->with("viewContentDateStared", $viewContentDateStared) 
            ->with("viewContentDateFinished", $viewContentDateFinished)
            ->with("autoevaluationViewedInfo", $autoevaluationViewedInfo);
    }
}
