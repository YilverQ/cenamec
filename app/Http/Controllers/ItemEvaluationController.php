<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Models*/
use App\Models\Module;
use App\Models\Content;
use App\Models\InteractiveEvaluation;

use App\Models\ItemEvaluation;
use App\Models\SelectionSimple;
use App\Models\TrueFalse;
use App\Models\ShortAnswer;
use App\Models\Sequence;
use App\Models\Matching;
use App\Models\ComparativeTable;
use App\Models\TableItem;

class ItemEvaluationController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }

    /**
     * Formulario de un item de Selección Simple.
     */
    public function editItemEvaluation(Request $request, ItemEvaluation $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        $module_id  = $item->interactiveEvaluation->content->module->id;
        $module = Module::where('id', $module_id)->first();
        $course = $module->course;

        return view('evaluations.interactive.edit')
                                ->with('role', $role)
                                ->with('interactiveEvaluation', $item->interactiveEvaluation)
                                ->with('itemEvaluation', $item)
                                ->with('course', $course)
                                ->with('module', $module);
    }

    /**
     * Método privado para editar un Item Evaluation.
     */
    private function updateItemEvaluation(Request $request, ItemEvaluation $item)
    {
        //Creamos un Item
        $item->statement  = $request->input("statement");
        $item->feedback   = $request->input("feedback");
        $item->save();

        return $item;
    }

    /**
     * Actualizamos un item de Selección Simple.
     */
    public function updateSelectionSimple(Request $request, ItemEvaluation $item)
    {
        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Buscamos el registro existente de SelectionSimple para este item_evaluation_id
        $selectionSimple = SelectionSimple::where('item_evaluation_id', $itemEvaluation->id)->first();

        // Si existe, actualizamos los campos
        $selectionSimple->answer = $request->input('answer');
        $selectionSimple->bad1 = $request->input('bad1');
        $selectionSimple->bad2 = $request->input('bad2');
        $selectionSimple->bad3 = $request->input('bad3');
        $selectionSimple->save();
        
        session()->flash('message-success', '¡El item fue Actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }

    /**
     * Actualizamos un item de Respuesta Corta.
     */
    public function updateShortAnswer(Request $request, ItemEvaluation $item)
    {
        // Si el array está vacío, mostramos un mensaje flash y redirigimos
        $wordsArray = json_decode($request->input('wordAdd'), true);
        if (empty($wordsArray)) {
            session()->flash('message-error', 'Debe agregar al menos una palabra.');
            return redirect()->back();  
        }

        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Eliminar todas las respuestas cortas antiguas asociadas al ItemEvaluation
        ShortAnswer::where('item_evaluation_id', $itemEvaluation->id)->delete();
        
        // Crear ShortAnswers para cada palabra nueva
        foreach ($wordsArray as $word) {
            $shortAnswer = new ShortAnswer();
            $shortAnswer->word = $word;
            $shortAnswer->item_evaluation_id = $itemEvaluation->id;
            $shortAnswer->save();
        }

        session()->flash('message-success', '¡El item fue actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }

    /**
     * Actualizamos un item de Verdadero o Falso.
     */
    public function updateTrueFalse(Request $request, ItemEvaluation $item)
    {
        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Buscamos el registro existente de TrueFalse para este item_evaluation_id
        $trueFalse = TrueFalse::where('item_evaluation_id', $itemEvaluation->id)->first();

        // Si existe, actualizamos la respuesta
        $trueFalse->answer = $request->input("answer");
        $trueFalse->save();

        session()->flash('message-success', '¡El item fue Actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }


    /**
     * Actualizamos un item de Emparejamiento.
     */
    public function updateMatching(Request $request, ItemEvaluation $item)
    {
        // Verificamos si la cadena 'couples' no está vacía
        $couples = $request->input("couples");
        $pairs = explode('; ', $couples);
        if (count(array_filter($pairs)) == 0) {
            session()->flash('message-error', 'No se han ingresado pares.');
            return redirect()->route('editItemEvaluation', $item);
        }

        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Obtener los pares actuales de la base de datos
        $currentPairs = Matching::where('item_evaluation_id', $item->id)
                                    ->get(['elementA', 'elementB'])
                                    ->map(function ($pair) {
                                        return "{$pair->elementA} - {$pair->elementB}";
                                    })
                                    ->toArray();


        // Identificamos los pares a eliminar (pares que no están en los nuevos pares)
        $pairsToDelete = array_diff($currentPairs, $pairs);
        foreach ($pairsToDelete as $pairToDelete) {
            $pair = explode(' - ', $pairToDelete);
            $elementA = $pair[0];
            $elementB = $pair[1];

            // Elimina el par correspondiente de la base de datos
            Matching::where('elementA', $elementA)
                ->where('elementB', $elementB)
                ->where('item_evaluation_id', $item->id)
                ->delete();
        }

        // Creamos o actualizamos los nuevos pares
        foreach ($pairs as $pairString) {
            $elements = explode(' - ', $pairString);

            // Verificamos que realmente tengamos dos elementos en la división
            if (count($elements) === 2) {
                list($elementA, $elementB) = $elements;

                // Comprobamos si el par ya existe en la base de datos
                $matching = Matching::where('elementA', $elementA)
                                    ->where('elementB', $elementB)
                                    ->where('item_evaluation_id', $item->id)
                                    ->first();

                // Si no existe, creamos el nuevo par
                if (!$matching) {
                    $matching = new Matching;
                    $matching->elementA = $elementA;
                    $matching->elementB = $elementB;
                    $matching->item_evaluation_id = $item->id;
                    $matching->save();
                }
            } else {
                session()->flash('message-error', 'Datos incorrectos. Por favor revisar.');
                return redirect()->route('editItemEvaluation', $item);
            }
        }

        session()->flash('message-success', '¡El item fue Actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }



    /**
     * Actualizamos un item de Secuencia.
     */
    public function updateSequence(Request $request, ItemEvaluation $item)
    {
        // Decodificar el campo 'sequence' para convertirlo en un array
        $sequenceWords = explode(',', $request->input('sequence'));
        
        // Validar que haya al menos 2 elementos
        if (count($sequenceWords) < 2) {
            session()->flash('message-error', 'Debes agregar al menos dos elementos a la secuencia.');
            return redirect()->back()->withInput();
        }

        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Eliminar todas las secuencias asociadas al item
        Sequence::where('item_evaluation_id', $itemEvaluation->id)->delete();

        // Crear nuevas secuencias
        foreach ($sequenceWords as $index => $word) {
            $sequence = new Sequence;
            $sequence->description = $word;
            $sequence->orden = $index + 1;
            $sequence->item_evaluation_id = $itemEvaluation->id;
            $sequence->save();
        }

        session()->flash('message-success', '¡El item fue Actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }


    /**
     * Actualizamos un item de Cuadro Comparativo.
     */
    public function updateComparativeTable(Request $request, ItemEvaluation $item)
    {
        // Actualizamos el ItemEvaluation
        $itemEvaluation = $this->updateItemEvaluation($request, $item);

        // Buscamos el registro existente de SelectionSimple para este item_evaluation_id
        $comparativeTable = ComparativeTable::where('item_evaluation_id', $itemEvaluation->id)->first();
        $comparativeTable->columnA = $request->input("titleColumnA");
        $comparativeTable->columnB = $request->input("titleColumnB");
        $comparativeTable->columnC = $request->input("titleColumnC");
        $comparativeTable->save();

        // Eliminar los elementos existentes de TableItem para esta ComparativeTable
        TableItem::where('comparative_table_id', $comparativeTable->id)->delete();

        // Arreglo que asocia cada columna con sus elementos
        $columns = [
            'A' => json_decode($request->input('elementsA'), true),
            'B' => json_decode($request->input('elementsB'), true),
            'C' => json_decode($request->input('elementsC'), true),
        ];

        // Crear un TableItem para cada palabra en las columnas
        foreach ($columns as $column => $elements) {
            foreach ($elements as $word) {
                $tableItem = new TableItem;
                $tableItem->description  = $word;
                $tableItem->column       = $column;
                $tableItem->comparative_table_id = $comparativeTable->id;
                $tableItem->save();
            }
        }

        session()->flash('message-success', '¡El item fue Actualizado!');
        return redirect()->route('editItemEvaluation', $item);
    }
}
