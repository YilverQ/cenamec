<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/*Importamos los modelos*/
use App\Models\User;
use App\Models\Tag;
use App\Models\Course;
use App\Models\Module;
use App\Models\Content;
use App\Models\Student;
use App\Models\Audience;
use App\Models\Certificate;
use App\Models\ViewContent;

class StudentCourseController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de estudiante.
     */
    public function __construct()
    {
        $this->middleware('auth.student');
    }


    /**
     * Retornamos la vista para todos los cursos.
     */
    public function index(Request $request)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');
        $tags = Tag::all();
        $audiences = Audience::all();

        return view('studentCourse.index')
                ->with("tags", $tags)
                ->with("audiences", $audiences)
                ->with("role", $role);
    }


    /**
     * Acción para crear un nuevo elemento.
     */
    public function store(Request $request, Course $item)
    {
        $student_id = $request->session()->get('student_id');
        $student    = Student::find($student_id);  

        //Comprobamos que el usuario no tenga inscrito el curso. 
        if ($student->courses->contains('id', $item->id)) {
            session()->flash('message-success', '¡Ya estabas inscrito al curso!');
            return to_route('student.course.display', $item);
        }     

        #Agregamos el curso al estudiante. 
        #Agregamos los modulos al estudiante.
        $modules = $item->modules;
        $student->courses()->attach($item);
        $student->modules()->attach($modules);


        #Activamos el primer módulo del curso.
        // Activamos el primer módulo del curso
        $firstModule = $item->modules()->orderBy('id')->first();
        if ($firstModule) {
            $student->modules()->updateExistingPivot($firstModule->id, ['state' => 'active']);
        }

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡Te has inscrito en el curso!');
        return to_route('student.course.display', $item);
    }


    /**
     * Retornamos una vista que nos muestra un elemento.
     */
    public function show(Request $request, Course $item)
    {
        $student_id = $request->session()->get('student_id');
        $student    = Student::find($student_id);  

        //Comprobamos que el usuario no tenga inscrito el curso. 
        if ($student->courses->contains('id', $item->id)) {
            return to_route('student.course.display', $item);
        } 
        
        //Buscamos los datos. 
        $modules = Module::where('course_id', $item->id)
                        ->orderBy('level')
                        ->get(); 

        //Link del código QR. 
        $linkQr = route('shared.course', ['item' => $item]);

        //Retornamos todos los datos a la vista. 
        return view('studentCourse.show')
                ->with("course", $item)
                ->with("linkQr", $linkQr)
                ->with("modules", $modules);
    }


    /**
     * Retornamos una vista que nos muestra un elemento.
     * Buscamos los módulos que tiene el curso que viene por párametro.
     * retornamos todos los datos. 
     */
    public function display(Request $request, Course $item)
    {
        $student_id = $request->session()->get('student_id');
        $student    = Student::find($student_id);  


        //Actualizamos la última conexión
        $coursePivot = $student->courses_pivot()->where('course_id', $item->id)->first();
        $coursePivot->pivot->last_connection = now();
        $coursePivot->pivot->save();


        $modules = Module::where('course_id', $item->id)
                    ->orderBy('level')
                    ->get();

        // Llamamos al método privado para manejar la lógica de creación del certificado
        $certificate = $this->handleCertificateCreation($student, $item, $student_id);

        //Link del código QR. 
        $linkQr = route('shared.course', ['item' => $item]);

        // Obtener los contenidos relacionados con el curso
        $contentIds = Content::whereIn('modulo_id', $modules->pluck('id'))
                        ->pluck('id');
        $viewContents = ViewContent::with(['content', 'student'])
            ->where('student_id', $student_id)
            ->whereIn('content_id', $contentIds)
            ->whereNotNull('time_view')
            ->orderBy('start_view', 'desc') 
            ->get();


        //Retornamos todos los datos a la vista. 
        return view('studentCourse.display')
                ->with("course", $item)
                ->with("student", $student)
                ->with("linkQr", $linkQr)
                ->with("modules", $modules)
                ->with("viewContents", $viewContents)
                ->with("certificate", $certificate);
    }


    // Método privado para manejar la creación del certificado
    private function handleCertificateCreation($student, $item, $student_id)
    {
        // Verificamos si todos los módulos tienen el estado 'finished'
        $allFinished = $student->modules()
                            ->wherePivot('state', '!=', 'finished')
                            ->where('course_id', $item->id)
                            ->count() == 0;

        // Comprobamos si ya existe un certificado para este estudiante y curso
        $certificate = Certificate::where('student_id', $student_id)
                                    ->where('course_id', $item->id)
                                    ->first();

        // Si no existe el certificado y todos los módulos están finalizados, lo creamos
        if (!$certificate && $allFinished) {
            $certificate = new Certificate();
            $certificate->date_certificate = now(); // Fecha actual
            $certificate->course_id = $item->id;
            $certificate->student_id = $student_id;
            $certificate->save(); // Guardamos el registro en la base de datos
        }

        // Retornamos el certificado solo si todos los módulos están finalizados
        return $allFinished ? $certificate : null; 
    }
}
