<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

/*Importamos los modelos*/
use App\Models\User;
use App\Models\State;
use App\Models\Parishe;
use App\Models\Student;
use App\Models\Academic;
use App\Models\Profileimg;
use App\Models\Certificate;
use App\Models\Municipalitie;


use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class StudentController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.student -> Comprueba que el usuario tiene permiso de estudiante.
     */
    public function __construct()
    {
        $this->middleware('auth.student');
    }


    /**
     * Retornamos la vista principal del estudiante.
     */
    public function index(Request $request)
    {
        // Obtenemos algunos registros.
        $user_id = $request->session()->get('user_id');
        $user    = User::find($user_id);
        $courses = $user->student->courses_pivot;
        $student = Student::find($user->student->id);

        // Calculate course progress
        $this->calculateCourseProgress($courses, $student);

        return view('student.index')
                    ->with("courses", $courses)
                    ->with("student", $user);
    }

    private function calculateCourseProgress($courses, $student)
    {
        foreach ($courses as $course) {
            $totalModules = $course->modules->count();
            $totalPercentage = 0; // Suma total de los porcentajes de todos los módulos
            
            foreach ($course->modules as $module) {
                $moduleProgress = $student->modules_pivot()->where('module_id', $module->id)->first();
                
                if ($moduleProgress) {
                    $percentage = $moduleProgress->pivot->percentage;
                    $totalPercentage += $percentage; // Sumamos el porcentaje de cada módulo
                }
            }
            
            if ($totalModules > 0) {
                $courseProgress = ($totalPercentage / $totalModules); // Promedio de los porcentajes
                $course->progress = round($courseProgress, 2); // Redondear a dos decimales
            } else {
                $course->progress = 0; // Si no hay módulos, el progreso es 0
            }
        }
    }
}