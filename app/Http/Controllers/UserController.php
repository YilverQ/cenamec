<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

/*Importamos los modelos*/
use App\Models\User;
use App\Models\State;
use App\Models\Parishe;
use App\Models\Laboral;
use App\Models\Academic;
use App\Models\Profileimg;
use App\Models\Certificate;
use App\Models\Municipalitie;


use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class UserController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.student -> Comprueba que el usuario tiene permiso de estudiante.
     */
    public function __construct()
    {
        $this->middleware('auth.user');
    }


    /**
     * Retornamos una vista para ver el perfil.
     */
    public function profile(Request $request, User $user)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        //Buscamos todas las ubicaciones geográficas.
        $states = State::orderBy('name')->get();

        //Buscamos todas las imagenes para el perfil de usuario.
        $profileimgs = Profileimg::all();
        
        $user_id = $request->session()->get('user_id');
        $user    = User::find($user_id);

        $academics = Academic::where('user_id', $user_id)
                            ->orderBy('id', 'desc')
                            ->get();

        $laborals = Laboral::where('user_id', $user_id)
                            ->orderBy('id', 'desc')
                            ->get();

        return view('user.profile')
                    ->with("role", $role)
                    ->with("academics", $academics)
                    ->with("laborals", $laborals)
                    ->with("states", $states)
                    ->with("profileimgs", $profileimgs)
                    ->with('user', $user);
    }


    /**
     * Acción para actualizar un elemento.
     * 
     * Para actualizar el elemento se debe cumplir:
     *      1. Ingresar un correo electrónico único.
     *      2. Ingresar un número de teléfono único.
     *      3. Ingresar un número de cédula único.
     *      4. Los datos ingresados puedes ser igual al que tenían anteriormente.
     * 
     * Luego de comprobar se procede a:
     *      1. Persistir los datos que ingresó el usuario.
     *      2. Se redirecciona a la vista principal del estudiante.   
     */
    public function update(Request $request)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $request->session()->get('user_id');
        $item    = User::find($user_id);

        /*Recibimos los datos sencibles*/
        $email = strtolower($request->input('email'));
        $phone = $request->input('number_phone');

        /*Buscamos en la DB si existe alguno de los datos*/
        $is_email_valid  = User::where('email', '=', $email)->first();
        $is_phone_valid  = User::where('number_phone', '=', $phone)->first();


        //El correo ingresado en el formulario ya está en la bd.
        if (!(empty($is_email_valid->email))) {
            if ($item->email != $email) {
                #El correo ya lo tiene otra persona.
                #No actualiza el dato. 
                #Retorna un mensaje de error. 
                session()->flash('message-error', 'Error, el correo electrónico ya está en uso');
                return to_route('user.profile');
            }
        }

        //Si se encuentra un elemento es porque el número de teléfono ingresado es incorrecto.
        if (!(empty($is_phone_valid->number_phone))) {
            if ($item->number_phone != $phone) {
                #El número de teléfono ya lo tiene otra persona.
                session()->flash('message-error', 'Error, el número de teléfono ya está en uso');
                return to_route('user.profile');
            }
        }
        
        //Si no se cumple lo anterior es porque se puede actualizar los datos. 
        $item->firts_name  = ucwords(strtolower($request->input('firts_name')));
        $item->lastname    = ucwords(strtolower($request->input('lastname')));
        $item->gender      = $request->input('gender');
        $item->nacionality = $request->input('nacionality');
        $item->birthdate   = $request->input('birthdate');
        $item->identification_card = strtoupper($request->input('identification_card'));
        $item->number_phone = $request->input('number_phone');
        $item->email       = $email;
        $item->parishe_id  = $request->input('parishe');
        
        #Almacenamos los datos.
        $item->save();

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡Tus datos fueron actualizados!');
        return to_route('user.profile');
    }


    /**
     * Actualizamos la contraseña del usuario 
     */
    public function updatePassword(Request $request)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $request->session()->get('user_id');
        $item    = User::find($user_id);

        /*Si el usuario no ingresó datos, no puede actualizar la contraseña*/
        $password = $request->input('password');
        if (!$password) {
            session()->flash('message-error', '¡Tu contraseña no puede actualizarse!');
            return to_route('user.profile');
        }

        /*Encriptamos la contraseña y la guardamos*/
        $item->password = Hash::make($password);
        $item->save();

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡Tu contraseña fue actualizada!');
        return to_route('user.profile');
    }


    /**
     * Actualizamos la imagen del usuario 
     */
    public function updateImg(Request $request)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $request->session()->get('user_id');
        $item    = User::find($user_id);

        //Recibimos el nombre de la imagen. 
        $value = $request->input('picture');
        
        //Comprobamos si el usuario quiso actualizar la imagen.
        if ($value){
            $item->profileimg_id = $value;  
            $item->save();
        }

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡La imagen fue actualizada!');
        return to_route('user.profile');
    }


    /**
     * Agregamos un nuevo dato acádemico. 
     */
    public function addAcademic(Request $request)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $request->session()->get('user_id');

        session()->flash('message-error', 'Por favor completa todos los campos requeridos');
        $request->validate([
            'academicLevel' => 'required',
            'intitution' => 'required',
            'career' => 'required',
            'sector' => 'required',
            'area' => 'required'
        ]);
        session()->forget('message-error');

        /*Formateamos el datos de academic_title. Cada inicio de cada palabra es mayúscula*/
        $academic_title    = $request->input('academic_title');
        $academic_title    = ucwords($academic_title);

        /*Creamos un nuevo dato*/
        $data = new Academic;
        $data->user_id          = $user_id;
        $data->institution_id   = $request->input('intitution');
        $data->academiclevel_id = $request->input('academicLevel');
        $data->career_id        = $request->input('career');
        $data->area_id          = $request->input('area');
        $data->sector_id        = $request->input('sector');
        $data->year_entry       = $request->input('year_entry');;
        $data->year_exit        = $request->input('year_exit');
        $data->academic_title   = $academic_title;

        $data->save();

        session()->flash('message-success', '¡Se ha creado un nuevo dato acádemico!');
        return to_route('user.profile');
    }

    /**
     * Realizamos una petición para obtener los datos academicos 
     */
    public function getDataAcademic(Request $request, Academic $item)
    {
        // Construye una respuesta JSON con los datos y un código de estado personalizado
        $academic = Academic::find($item->id);
        return response()->json($academic, 200);
    }


    /**
     * Actualiza un registro acádemico 
     */
    public function updateAcademic(Request $request){
        $academic_id = $request->input('idAcademic');
        $academic = Academic::find($academic_id);

        /*Formateamos el datos de academic_title. Cada inicio de cada palabra es mayúscula*/
        $academic_title    = $request->input('academic_titleEdit');
        $academic_title    = ucwords($academic_title);

        /*Actualizamos la academia del usuario*/
        $academic->institution_id   = $request->input('intitutionEdit');
        $academic->academiclevel_id = $request->input('academicLevelEdit');
        $academic->career_id        = $request->input('careerEdit');
        $academic->area_id          = $request->input('areaEdit');
        $academic->sector_id        = $request->input('sectorEdit');
        $academic->year_entry       = $request->input('year_entryEdit');;
        $academic->year_exit        = $request->input('year_exitEdit');
        $academic->academic_title   = $academic_title;

        /*Persistimos los datos*/
        $academic->save();

        session()->flash('message-success', '¡Datos Actualizados!');
        return to_route('user.profile');
    }


    /**
     * Elimina un registro academico 
     */
    public function deleteAcademic(Request $request, Academic $item)
    {
        //Elimina el elemento y retorna un mensaje flash.
        $item->delete();
        session()->flash('message-success', '¡El dato acádemico fue eliminado correctamente!');
        return to_route('user.profile');
    }


    /**
     * Agregamos un nuevo dato acádemico. 
     */
    public function addLaboral(Request $request)
    {
        /*Buscamos el usuario de la sessión activa*/
        $user_id = $request->session()->get('user_id');

        session()->flash('message-error', 'Por favor completa todos los campos requeridos');
        $request->validate([
            'name_intitution' => 'required',
            'type_institution' => 'required',
            'area_institution' => 'required',
            'position' => 'required',
            'year_entryLaboral' => 'required'
        ]);
        session()->forget('message-error');

        /*Formateamos los datos. Cada inicio de cada palabra es mayúscula*/
        $name_intitution    = $request->input('name_intitution');
        $name_intitution    = ucwords($name_intitution);
        $position    = $request->input('position');
        $position    = ucwords($position);

        /*Creamos un nuevo registro*/
        $data = new Laboral;
        $data->user_id          = $user_id;
        $data->name_intitution  = $name_intitution;
        $data->type             = $request->input('type_institution');
        $data->area             = $request->input('area_institution');
        $data->position         = $position;
        $data->year_entry       = $request->input('year_entryLaboral');;
        $data->year_exit        = $request->input('year_exitLaboral');

        $data->save();

        session()->flash('message-success', '¡Se ha creado un nuevo dato laboral!');
        return to_route('user.profile');
    }

    /**
     * Actualiza un registro laboral 
     */
    public function updateLaboral(Request $request){
        $laboralID = $request->input('idLaboral');
        $laboral = Laboral::find($laboralID);

        /*Formateamos los datos. Cada inicio de cada palabra es mayúscula*/
        $name_intitution    = $request->input('name_institutionEdit');
        $name_intitution    = ucwords($name_intitution);
        $position    = $request->input('positionEdit');
        $position    = ucwords($position);

        /*Actualizamos la academia del usuario*/
        $laboral->name_intitution  = $name_intitution;
        $laboral->type             = $request->input('type_institutionEdit');
        $laboral->area             = $request->input('area_institutionEdit');
        $laboral->position         = $position;
        $laboral->year_entry       = $request->input('year_entryLaboralEdit');;
        $laboral->year_exit        = $request->input('year_exitLaboralEdit');

        /*Persistimos los datos*/
        $laboral->save();

        session()->flash('message-success', '¡Datos Actualizados!');
        return to_route('user.profile');
    }

    /**
     * Elimina un registro laboral 
     */
    public function deleteLaboral(Request $request, Laboral $item)
    {
        //Elimina el elemento y retorna un mensaje flash.
        $item->delete();
        session()->flash('message-success', '¡El dato laboral fue eliminado correctamente!');
        return to_route('user.profile');
    }
}
