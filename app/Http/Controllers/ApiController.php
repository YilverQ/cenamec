<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Importamos los modelos*/
use App\Models\Profileimg;


class ApiController extends Controller
{
    /*Retorna una lista de imagenes del perfil*/
    public function getPicturesProfile(Request $request)
    {
        //Buscamos todas las imagenes para el perfil de usuario.
        $pictures = Profileimg::all();

        // Construye una respuesta JSON con los datos y un código de estado personalizado
        return response()->json($pictures, 200);
    }
}
