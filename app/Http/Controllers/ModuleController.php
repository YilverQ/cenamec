<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/*Importamos los modelos*/
use App\Models\User;
use App\Models\Note;
use App\Models\Course;
use App\Models\Module;
use App\Models\Teacher;
use App\Models\Content;
use App\Models\Questionnaire;


class ModuleController extends Controller
{
    /**
     * Middlewares necesarios para comprobar los permisos
     * auth.teacher -> Comprueba que el usuario tiene permiso de profesor.
     */
    public function __construct()
    {
        $this->middleware('auth.teacher.admin');
    }


    /**
     * Retornamos un formulario que nos permite crear un nuevo elemento.
     */
    public function create(Request $request, Course $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        return view('module.create')
                        ->with("role", $role)
                        ->with('course', $item);
    }


    /**
     * Acción para crear un nuevo elemento.
     */
    public function store(Request $request, Course $item)
    {
        //buscamos los datos para el curso.
        $course_id = $item->id;
        $course = Course::where('id', $course_id)
                            ->withCount('modules')
                            ->first();

        //Persistimos los datos.
        $module = new Module;
        $module->name = $request->input('super_name');
        $module->description = $request->input('description');
        $module->level = $course->modules_count + 1;
        $module->course_id  = $course->id;
        $module->save();

        session()->flash('message-success', '¡El módulo fue creado!');
        return redirect()->route('teacher.module.show', $module);
    }


    /**
     * Retornamos una vista que nos muestra un elemento.
     */
    public function show(Request $request, Module $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        //Guardamos el ID del módulo en session.
        $request->session()->put('module_id', $item->id);
        
        //Buscamos los datos básicos
        $course = Course::find($item->course_id);

        $this->deleteEvaluationNull($item);

        //Buscamos los contenidos relacionados con el módulo
        $contents = Content::where('modulo_id', $item->id)->get();


        //Contamos cantidad de items, por tipos
        $typesToCount = [
            'Selección Simple',
            'Respuesta Corta',
            'Verdadero o Falso',
            'Secuencia',
            'Emparejamiento',
            'Cuadro Comparativo',
        ];

        $counts = [];

        $filteredContents = $item->contents->map(function($content) {
            return $content->interactiveEvaluation;
        })->filter();

        foreach ($typesToCount as $type) {
            $counts[$type] = $filteredContents->map(function($evaluation) use ($type) {
                return $evaluation->itemEvaluations->where('type', $type)->count();
            })->sum();
        }


        //Retornamos todos los datos a la vista. 
        return view('module.show')
                ->with("role", $role)
                ->with("contents", $contents)
                ->with("course", $course)
                ->with("types_items", $counts)
                ->with("module", $item);
    }


    /**
     * Retornamos un formulario que nos permite actualizar un elemento. 
     */
    public function edit(Request $request, Module $item)
    {
        //Identificamos el role del usuario.
        $role = $request->session()->get('role');

        $course = $item->course;
        return view('module.edit')
                ->with("role", $role)
                ->with('module', $item)
                ->with('course', $course);
    }


     /**
     * Acción para actualizar un elemento.
     */
    public function update(Request $request, Module $item)
    {
        //Persistimos los datos.
        $item->name = $request->input('super_name');
        $item->description = $request->input('description');
        $item->save();

        //Buscamos el curso correspondiente.
        $course = Course::find($item->course_id);

        #Retorna un mensaje flash.
        session()->flash('message-success', '¡El módulo fue actualizado!');
        return to_route('teacher.module.show', $item);
    }


    /**
     * Eliminamos un elemento de nuestra bd.
     * Buscamos el curso al cual pertenece nuestro módulo. 
     * Buscamos los módulos que tiene nuestro curso. 
     * 
     * Antes de eliminar el elemento debemos acomodar 
     * el orden de niveles de nuestro módulos.
     *  
     * Retornamos a la vista de un curso '$course'. 
     */
    public function destroy(Request $request, Module $item)
    {
        $course = Course::find($item->course_id);
        $modules  = Module::where('course_id', '=', $item->course_id)->get();
        
        //Acomodamos el level de los módulos.
        foreach ($modules as $key => $value) {
            if ($value->level > $item->level) {
                $value->level = $value->level - 1;
                $value->save();
            }
        }

        //Elimina el elemento y retorna un mensaje flash.
        $item->delete();
        session()->flash('message-success', '¡El módulo fue eliminado correctamente!');
        return to_route('teacher.course.show', $course);
    }

    /**
     * Elimimnamos todos las evaluaciones que no tengan item.
     */
    private function deleteEvaluationNull(Module $item)
    {
        // Buscamos y eliminamos contenidos relacionados con el módulo que son autoevaluaciones sin likerts
        Content::where('modulo_id', $item->id)
            ->where('type', 'Autoevaluación')
            ->doesntHave('autoevaluation.likerts')
            ->delete();

        Content::where('modulo_id', $item->id)
            ->where('type', 'Evaluación')
            ->doesntHave('interactiveEvaluation.itemEvaluations')
            ->delete();
    }
}
