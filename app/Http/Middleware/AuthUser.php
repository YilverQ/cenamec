<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $is_valid_student = $request->session()->get('is_student_valid');
        $is_valid_teacher = $request->session()->get('is_teacher_valid');
        $is_valid_admin   = $request->session()->get('is_admin_valid');
        
        if (!$is_valid_student  && !$is_valid_teacher && !$is_valid_admin) {
            session()->flash('message-error', 'Se ha cerrado sesión');
            return to_route('login.login'); 
        };

        if ($is_valid_student) {
            $request->session()->put('role', "student");
        };

        if ($is_valid_teacher) {
            $request->session()->put('role', "teacher");
        };

        if ($is_valid_admin) {
            $request->session()->put('role', "admin");
        };
        
        return $next($request);
    }
}
