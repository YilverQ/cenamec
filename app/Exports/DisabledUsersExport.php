<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class DisabledUsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Obtener los estudiantes asociados al curso con sus relaciones
        $users = User::where('disabled', true)->get();

        // Mapear los datos que deseas exportar
        return $users->map(function ($user) {
            return [
                'User-ID' => $user->id,
                'user-ID' => $user->id,
                'Cédula' => $user->identification_card,
                'Nombre' => $user->firts_name . ' ' . $user->lastname,
                'Correo Electrónico' => $user->email,
                'Teléfono' => $user->number_phone,
                'Género' => $user->gender,
                'Fecha de Nacimiento' => Carbon::parse($user->birthdate)->format('d/m/Y'),
                'Estado' => $user->parishe->municipalitie->state->name ?? 'N/A',
                'Municipio' => $user->parishe->municipalitie->name ?? 'N/A',
                'Parroquia' => $user->parishe->name ?? 'N/A',
                
                'Última Conexión' => $user->last_connection 
                    ? Carbon::parse($user->last_connection)->format('d/m/Y') 
                    : '', 

            ];
        });
    }



    /**
     * Definir los encabezados de las columnas
     */
    public function headings(): array
    {
        return [
            'User-ID',
            'user-ID',
            'Cédula',
            'Nombre',
            'Correo Electrónico',
            'Teléfono',
            'Género',
            'Fecha de Nacimiento',
            'Estado',
            'Municipio',
            'Parroquia',
            'Última Conexión',
        ];
    }
}
