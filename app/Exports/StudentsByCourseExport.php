<?php

namespace App\Exports;

use App\Models\Student;
use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class StudentsByCourseExport implements FromCollection, WithHeadings
{
    protected $courseId;

    public function __construct($courseId)
    {
        $this->courseId = $courseId;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Obtener los estudiantes asociados al curso con sus relaciones
        $students = Student::whereHas('courses', function ($query) {
            $query->where('course_id', $this->courseId);
        })->with('courses_pivot')->get();

        // Mapear los datos que deseas exportar
        return $students->map(function ($student) {
            // Obtener el módulo actual del estudiante en el curso
            $currentModule = $student->currentModule($this->courseId);
            $course = Course::find($this->courseId);

            // Obtener el pivote del curso específico
            $coursePivot = $student->courses_pivot->where('id', $this->courseId)->first();

            return [
                'Cédula' => $student->user->identification_card,
                'Nombre' => $student->user->firts_name . ' ' . $student->user->lastname,
                'Correo Electrónico' => $student->user->email,
                'Teléfono' => $student->user->number_phone,
                'Género' => $student->user->gender,
                'Fecha de Nacimiento' => Carbon::parse($student->user->birthdate)->format('d/m/Y'),
                'Estado' => $student->user->parishe->municipalitie->state->name ?? 'N/A',
                'Municipio' => $student->user->parishe->municipalitie->name ?? 'N/A',
                'Parroquia' => $student->user->parishe->name ?? 'N/A',
                'Nombre del Curso' => $course->name, // Nombre del curso
                'Fecha de Inscripción' => Carbon::parse($coursePivot->pivot->dateStart)->format('d/m/Y') ?? 'N/A', 
                'Módulo Actual' => $currentModule->level ?? 'N/A', // Nivel del módulo actual
                'Nombre del Módulo Actual' => $currentModule->name ?? 'N/A', // Nombre del módulo actual
                'Fecha de Finalización' => Carbon::parse($coursePivot->pivot->dateFinished)->format('d/m/Y') ?? 'N/A',
                'Última Conexión' => $student->user->last_connection 
                    ? Carbon::parse($student->user->last_connection)->format('d/m/Y') 
                    : '', // Formatear fecha y hora
            ];
        });
    }



    /**
     * Definir los encabezados de las columnas
     */
    public function headings(): array
    {
        return [
            'Cédula',
            'Nombre',
            'Correo Electrónico',
            'Teléfono',
            'Género',
            'Fecha de Nacimiento',
            'Estado',
            'Municipio',
            'Parroquia',
            'Nombre del Curso',
            'Fecha de Inscripción',
            'Módulo Actual',
            'Nombre del Módulo Actual',
            'Fecha de Finalización',
            'Última Conexión',
        ];
    }
}
