<?php

namespace App\Exports;

use App\Models\Teacher;
use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class TeachersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Obtener los estudiantes asociados al curso con sus relaciones
        $teachers = Teacher::all();


        // Mapear los datos que deseas exportar
        return $teachers->map(function ($teacher) {
            return [
                'User-ID' => $teacher->user->id,
                'Teacher-ID' => $teacher->id,
                'Cédula' => $teacher->user->identification_card,
                'Nombre' => $teacher->user->firts_name . ' ' . $teacher->user->lastname,
                'Correo Electrónico' => $teacher->user->email,
                'Teléfono' => $teacher->user->number_phone,
                'Género' => $teacher->user->gender,
                'Fecha de Nacimiento' => Carbon::parse($teacher->user->birthdate)->format('d/m/Y'),
                'Estado' => $teacher->user->parishe->municipalitie->state->name ?? 'N/A',
                'Municipio' => $teacher->user->parishe->municipalitie->name ?? 'N/A',
                'Parroquia' => $teacher->user->parishe->name ?? 'N/A',
                'Cursos' => $teacher->courses()->count(),
                
                'Última Conexión' => $teacher->user->last_connection 
                    ? Carbon::parse($teacher->user->last_connection)->format('d/m/Y') 
                    : '', 

            ];
        });
    }



    /**
     * Definir los encabezados de las columnas
     */
    public function headings(): array
    {
        return [
            'User-ID',
            'Teacher-ID',
            'Cédula',
            'Nombre',
            'Correo Electrónico',
            'Teléfono',
            'Género',
            'Fecha de Nacimiento',
            'Estado',
            'Municipio',
            'Parroquia',
            'Cursos',
            'Última Conexión',
        ];
    }
}
