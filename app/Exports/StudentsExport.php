<?php

namespace App\Exports;

use App\Models\Student;
use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class StudentsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Obtener los estudiantes asociados al curso con sus relaciones
        $students = Student::all();


        // Mapear los datos que deseas exportar
        return $students->map(function ($student) {
            // Obtener Cantidad de items respondidos
            $contentsWithAnswers = $student->viewContents()
                ->whereHas('answers')
                ->with('answers')
                ->get();

            $countItemsResponse = [
                'Items Respondidos' => $contentsWithAnswers->sum(function ($viewContent) {
                    return $viewContent->answers->count();
                }),
                'Items Acertados' => $contentsWithAnswers->sum(function ($viewContent) {
                    return $viewContent->answers->where('is_correct', true)->count();
                }),
                'Items Errados' => $contentsWithAnswers->sum(function ($viewContent) {
                    return $viewContent->answers->where('is_correct', false)->count();
                }),
            ];
            return [
                'User-ID' => $student->user->id,
                'Student-ID' => $student->id,
                'Cédula' => $student->user->identification_card,
                'Nombre' => $student->user->firts_name . ' ' . $student->user->lastname,
                'Correo Electrónico' => $student->user->email,
                'Teléfono' => $student->user->number_phone,
                'Género' => $student->user->gender,
                'Fecha de Nacimiento' => Carbon::parse($student->user->birthdate)->format('d/m/Y'),
                'Estado' => $student->user->parishe->municipalitie->state->name ?? 'N/A',
                'Municipio' => $student->user->parishe->municipalitie->name ?? 'N/A',
                'Parroquia' => $student->user->parishe->name ?? 'N/A',
                'Cursos Inscritos' => $student->courses()->count(),
                'Certificados Obtenidos' => $student->certificates()->count(),
                'Contenidos Visualizados' => $student->viewContents()->whereNotNull('end_view')->count(),

                //Tipos de contenidos
                'Lecciones Vistas' => $student->viewContents()
                    ->whereNotNull('end_view')
                    ->whereHas('content', function ($query) {
                        $query->where('type', 'Nota');
                    })->count(),
                'Autoevaluaciones Realizadas' => $student->viewContents()
                    ->whereNotNull('end_view')
                    ->whereHas('content', function ($query) {
                        $query->where('type', 'Autoevaluación');
                    })->count(),
                'Evaluaciones Interactivas Realizadas' => $student->viewContents()
                    ->whereNotNull('end_view')
                    ->whereHas('content', function ($query) {
                        $query->where('type', 'Evaluación');
                    })->count(),

                //Items Respondidos
                'Escalas de Likert Respondidos' => $student->likerts()->count(),
                'Items Respondidos' => $countItemsResponse['Items Respondidos'],
                'Items Acertados' => $countItemsResponse['Items Acertados'],
                'Items Errados' => $countItemsResponse['Items Errados'],

                
                'Última Conexión' => $student->user->last_connection 
                    ? Carbon::parse($student->user->last_connection)->format('d/m/Y') 
                    : '', 

            ];
        });
    }



    /**
     * Definir los encabezados de las columnas
     */
    public function headings(): array
    {
        return [
            'User-ID',
            'Student-ID',
            'Cédula',
            'Nombre',
            'Correo Electrónico',
            'Teléfono',
            'Género',
            'Fecha de Nacimiento',
            'Estado',
            'Municipio',
            'Parroquia',
            'Cursos Inscritos',
            'Certificados Obtenidos',
            'Contenidos Visualizados',
            'Lecciones Vistas',
            'Autoevaluaciones Realizadas',
            'Evaluaciones Interactivas Realizadas',
            'Escalas de Likert Respondidos',
            'Items Respondidos',
            'Items Acertados',
            'Items Errados',
            'Última Conexión',
        ];
    }
}
