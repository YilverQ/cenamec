<?php

namespace App\Exports;

use App\Models\Administrator;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class AdminsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Obtener los estudiantes asociados al curso con sus relaciones
        $admins = Administrator::all();


        // Mapear los datos que deseas exportar
        return $admins->map(function ($admin) {
            return [
                'User-ID' => $admin->user->id,
                'admin-ID' => $admin->id,
                'Cédula' => $admin->user->identification_card,
                'Nombre' => $admin->user->firts_name . ' ' . $admin->user->lastname,
                'Correo Electrónico' => $admin->user->email,
                'Teléfono' => $admin->user->number_phone,
                'Género' => $admin->user->gender,
                'Fecha de Nacimiento' => Carbon::parse($admin->user->birthdate)->format('d/m/Y'),
                'Estado' => $admin->user->parishe->municipalitie->state->name ?? 'N/A',
                'Municipio' => $admin->user->parishe->municipalitie->name ?? 'N/A',
                'Parroquia' => $admin->user->parishe->name ?? 'N/A',
                
                'Última Conexión' => $admin->user->last_connection 
                    ? Carbon::parse($admin->user->last_connection)->format('d/m/Y') 
                    : '', 

            ];
        });
    }



    /**
     * Definir los encabezados de las columnas
     */
    public function headings(): array
    {
        return [
            'User-ID',
            'admin-ID',
            'Cédula',
            'Nombre',
            'Correo Electrónico',
            'Teléfono',
            'Género',
            'Fecha de Nacimiento',
            'Estado',
            'Municipio',
            'Parroquia',
            'Última Conexión',
        ];
    }
}
