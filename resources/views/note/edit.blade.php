@extends($role.'.layout')

@section('title', 'Editar Lección Educativa')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a href="{{ route('teacher.module.show', $module) }}">
				<li class="breadcrumbs__item" 
					title="Ir al Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: {{ $note->title }}">
					<i>/</i>
					<p>{{ $note->title }}</p>
				</li>
			</a>
		</ul>
	</div>

	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<h2 class="tab__title--centered">
					Editar <strong class="color-Text">Lección Educativa</strong>
				</h2>

				<form class="form__content form__content--note form__content--autoheight" 
						id="noteForm"
						method="POST" 
						action="{{ route('teacher.note.update', $note) }}"
						enctype="multipart/form-data">
					@csrf @method('PUT')

					<h2 class="form__icon">
						<i class="fa-solid fa-note-sticky"></i>
					</h2>
					<h2 class="form__title">Datos de la Lección Educativa</h2>
					<div class="grid-one">
						<div class="form__item form__item--big">
							<label for="super_name">Título de la lección educativa:</label>
							<input class="form__input form__input form__input--big" 
									name="super_name" 
									required 
									type="text" 
									id="super_name"
									value="{{ $note->title }}" 
									placeholder="Física para principiante"
									autocomplete="off">
						</div>
					</div>

					<div class="grid-one">
						<div class="form__item form__item--big">
							<label>Contenido de la lección educativa:</label>
						</div>
					</div>
					<textarea id="myeditorinstance" name="myeditorinstance">
						{{ $note->content }}
					</textarea>


					<input class="form__send" 
						type="submit" 
						value="Actualizar">
				</form>
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<x-tiny.tinymce-config/>
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
@endsection