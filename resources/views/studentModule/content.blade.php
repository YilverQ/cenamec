@extends('student.layout')

@section('title', 'Ver Contenido')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/school.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/study.css') }}">
@endsection



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('student.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Cursos</p>
				</li>
			</a>
			<a href="{{ route('student.course.show', $module->course) }}">
				<li class="breadcrumbs__item" 
					title="Curso: {{ $module->course->name }}">
						<i>/</i>
						<p>{{ $module->course->name }}</p>
				</li>
			</a>
			<a  href="{{ route('student.module.study', $module) }}">
				<li class="breadcrumbs__item" 
					title="Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: {{ $content->type }}">
						<i>/</i>
						<p>{{ $content->orden }}. {{ $content->type }}</p>
				</li>
			</a>
		</ul>
	</div>
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<div class="gridTwo">
		    <div class="navigationContents">
		        <h2 class="navigationContents__title" id="buttonNavigation">
		            Contenido del Módulo 
		            <span class="navigationContents__hamburgerButton">
		                <i class="fa-solid fa-bars"></i>
		            </span>
		        </h2>
		        <div class="boxNavigationContents" id="boxNavigationContents">
		            <!-- Estado -->
		            <section class="navigation__section">
		            @forelse($contents as $index => $item)
					        @php
					        	$isContentViewed = $item->id == $content->id;
					            $isActive = $activeContentStudent && $activeContentStudent->id == $item->id;
					            $isCompleted = isset($contentStates[$item->id]) && $contentStates[$item->id] == 'Completado';
					            $imageSrc = '';
					            switch ($item->type) {
					                case 'Evaluación':
					                    $imageSrc = asset('img/course/interaction.png');
					                    break;
					                case 'Nota':
					                    $imageSrc = asset('img/course/nota_educativa.png');
					                    break;
					                case 'Autoevaluación':
					                    $imageSrc = asset('img/course/satisfaction-scale.png');
					                    break;
					            }
					        @endphp

					        @if($isContentViewed)
					        	<a href="{{ route('student.studyContent', $item) }}">
					                <div class="itemNavigation itemNavigation--activate">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    @if($item->type == 'Nota')
					                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
					                    @else
					                    	<p>{{ $item->type }}</p>
					                    @endif
					                </div>
					            </a>
					        @elseif($isActive)
					            <a href="{{ route('student.studyContent', $item) }}">
					                <div class="itemNavigation">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    <p>{{ $item->type }}</p>
					                </div>
					            </a>
					        @elseif($isCompleted)
					        	<a href="{{ route('student.studyContent', $item) }}">
						            <div class="itemNavigation itemNavigation--complete">
						                <div class="itemNavigation__info">
						                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
						                    <img src="{{ $imageSrc }}" class="navigation__img">
						                    @if($item->type == 'Nota')
						                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
						                    @else
						                    	<p>{{ $item->type }}</p>
						                    @endif
						                </div>
						                <i class="fa-solid fa-circle-check"></i>
						            </div>
					            </a>
					        @else
					            <div class="itemNavigation itemNavigation--block">
					                <div class="itemNavigation__info">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    @if($item->type == 'Nota')
					                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
					                    @else
					                    	<p>{{ $item->type }}</p>
					                    @endif
					                </div>
					                <i class="fa-solid fa-lock"></i> <!-- Icono de contenido bloqueado -->
					            </div>
					        @endif
					    @empty
					        <p>No hay contenidos disponibles para este módulo.</p>
					    @endforelse
					</section>
		        </div>
		    </div>

		    <article class="moduleDescription">
				@if($content->type == 'Evaluación')
					@component('components.cardsItemEvaluation', 
								["itemEvaluations" => $content->interactiveEvaluation->getShuffledItemEvaluations(), 
								 "content" => $content,
								 "viewContent" => $viewContent])
					@endcomponent


	            @elseif($content->type == 'Nota')
	            	<div class="note">
						<div class="divNote">
							<h3 class="note__title">{{ $content->note->title }}</h3>
							<div class="contentNote">
								<textarea class="contentNote__text myeditorinstance"
								         id="myeditorinstance__{{ $content->note->id }}" 
								         name="myeditorinstance"
								         style="width: 100% !important; max-width: 100%;">
								    {!! $content->note->content !!}
								</textarea>
							</div>
						</div>
					</div>
					<!-- Botones de navegación -->
					<div class= "buttonsNavigation">
						<form action="{{ route('student.passContent', ['item' => $content, 'next' => $nextItem ?? null]) }}" method="POST">
						@csrf 
				    		<button type="submit" class="buttonParts buttonParts--next">
							    Pasar Lección Educativa <i class="fas fa-arrow-right"></i>
							</button>
						</form>
					</div>

	            @elseif($content->type == 'Autoevaluación')
	            	<form action="{{ route('student.passContent', ['item' => $content, 'next' => $nextItem ?? null]) }}" method="POST">
				    @csrf

				    <div class="autoevaluationBox">
				    	<div class="autoevaluationBox__titleHidden">
				    		<h3>Autoevaluación - Escala de Likert</h3>
				    	</div>
				        <table class="autoevaluationTable">
						    <thead>
						        <tr>
						            <th class="autoevaluationTitleHead">Escala de Likert</th>
						            <th>Totalmente de acuerdo</th>
						            <th>De acuerdo</th>
						            <th>Neutral</th>
						            <th>En desacuerdo</th>
						            <th>Totalmente en desacuerdo</th>
						        </tr>
						    </thead>
						    <tbody>
						        @foreach($content->autoevaluation->likerts as $index => $item)
						            <tr class="autoevaluationCard">
						                <td class="autoevaluationQuestion">{{$item->affirmation}}</td>
						                <td>
						                    <label for="q_{{$item->id}}_5" class="hidden-label">Totalmente de acuerdo</label>
						                    <input type="radio" id="q_{{$item->id}}_5" name="q_{{$item->id}}" value="5" required>
						                </td>
						                <td>
						                    <label for="q_{{$item->id}}_4" class="hidden-label">De acuerdo</label>
						                    <input type="radio" id="q_{{$item->id}}_4" name="q_{{$item->id}}" value="4" required>
						                </td>
						                <td>
						                    <label for="q_{{$item->id}}_3" class="hidden-label">Neutral</label>
						                    <input type="radio" id="q_{{$item->id}}_3" name="q_{{$item->id}}" value="3" required>
						                </td>
						                <td>
						                    <label for="q_{{$item->id}}_2" class="hidden-label">En desacuerdo</label>
						                    <input type="radio" id="q_{{$item->id}}_2" name="q_{{$item->id}}" value="2" required>
						                </td>
						                <td>
						                    <label for="q_{{$item->id}}_1" class="hidden-label">Totalmente en desacuerdo</label>
						                    <input type="radio" id="q_{{$item->id}}_1" name="q_{{$item->id}}" value="1" required>
						                </td>
						            </tr>
						        @endforeach
						    </tbody>
						</table>

				    </div>
				    <!-- Botones de navegación -->
					<div class= "buttonsNavigation">
			    		<button type="submit" class="buttonParts buttonParts--next">
						    Enviar Autoevaluación <i class="fas fa-arrow-right"></i>
						</button>
					</div>
					</form>
	            @endif
			</article>
		</div>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
	@if($content->type == 'Nota')
		<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
		<script>
			// Función para ajustar la altura del editor según el contenido
			function adjustEditorHeight(editor) {
			    const body = editor.getDoc().body;
			    const height = body.scrollHeight;  // Calcula la altura del contenido

			    // Ajusta la altura del contenedor del editor
			    editor.getContainer().style.height = (height + 50) + 'px';
			}
		    window.onload = function() {
			    tinymce.init({
			        selector: '.myeditorinstance',
			        language: 'es',
			        readonly: true,
			        toolbar: false,
			        menubar: false,
			        statusbar: false,
			        content_css: '{{ asset('css/components/tiny.css') }}',
			        importcss_append: true,
			        license_key: 'gpl',
			        promotion: false,
			        plugins: 'autoresize',
			        autoresize_overflow_padding: 5,
			        autoresize_bottom_margin: 25,
			        min_height: 150,
			        init_instance_callback: function(editor) {
			            // Ajuste de altura al cargar el contenido
			            setTimeout(function() {
			                adjustEditorHeight(editor);
			            }, 500); 
			        }
			    });
			};
		</script>
	@endif

	@if($content->type == 'Autoevaluación')
		<script type="module" src="{{ asset('js/form/autoevaluationOptions.js') }}"></script>
	@endif

	@if($content->type == 'Evaluación')
		<script type="module" src="{{ asset('js/evaluations/globalState.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/carouselItem.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/selectionSimpleGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/selectionSimpleGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/trueOrFalseGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/sequenceDesktopGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/sequenceMovilGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/matchingGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/shortAnswersGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/comparativeTableGame.js') }}"></script>
		<script type="module" src="{{ asset('js/evaluations/sendData.js') }}"></script>
	@endif
@endsection