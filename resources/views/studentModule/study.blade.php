@extends('student.layout')


@section('title', 'Módulo del Curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/school.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/study.css') }}">
@endsection



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('student.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Cursos</p>
				</li>
			</a>
			<a href="{{ route('student.course.show', $module->course) }}">
				<li class="breadcrumbs__item" 
					title="Página Actual: {{ $module->course->name }}">
						<i>/</i>
						<p>{{ $module->course->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
		</ul>
	</div>
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<div class="gridTwo">
		    <div class="navigationContents">
		        <h2 class="navigationContents__title" id="buttonNavigation">
		            Contenido del Módulo 
		            <span class="navigationContents__hamburgerButton">
		                <i class="fa-solid fa-bars"></i>
		            </span>
		        </h2>
		        <div class="boxNavigationContents" id="boxNavigationContents">
		            <!-- Estado -->
		            <section class="navigation__section">
		            @forelse($contents as $index => $item)
					        @php
					        	$isContentViewed = null;
					            $isActive = $activeContentStudent && $activeContentStudent->id == $item->id;
					            $isCompleted = isset($contentStates[$item->id]) && $contentStates[$item->id] == 'Completado';
					            $imageSrc = '';
					            switch ($item->type) {
					                case 'Evaluación':
					                    $imageSrc = asset('img/course/interaction.png');
					                    break;
					                case 'Nota':
					                    $imageSrc = asset('img/course/nota_educativa.png');
					                    break;
					                case 'Autoevaluación':
					                    $imageSrc = asset('img/course/satisfaction-scale.png');
					                    break;
					            }
					        @endphp

					        @if($isContentViewed)
					        	<a href="{{ route('student.studyContent', $item) }}">
					                <div class="itemNavigation itemNavigation--activate">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    @if($item->type == 'Nota')
					                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
					                    @else
					                    	<p>{{ $item->type }}</p>
					                    @endif
					                </div>
					            </a>
					        @elseif($isActive)
					            <a href="{{ route('student.studyContent', $item) }}">
					                <div class="itemNavigation">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    @if($item->type == 'Nota')
					                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
					                    @else
					                    	<p>{{ $item->type }}</p>
					                    @endif
					                </div>
					            </a>
					        @elseif($isCompleted)
					        	<a href="{{ route('student.studyContent', $item) }}">
						            <div class="itemNavigation itemNavigation--complete">
						                <div class="itemNavigation__info">
						                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
						                    <img src="{{ $imageSrc }}" class="navigation__img">
						                    @if($item->type == 'Nota')
						                    	<p><b>Lección:</b> {{ $item->note->title }}</p>
						                    @else
						                    	<p>{{ $item->type }}</p>
						                    @endif
						                </div>
						                <i class="fa-solid fa-circle-check"></i>
						            </div>
					            </a>
					        @else
					            <div class="itemNavigation itemNavigation--block">
					                <div class="itemNavigation__info">
					                    <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                    <img src="{{ $imageSrc }}" class="navigation__img">
					                    <p>{{ $item->type }}</p>
					                </div>
					                <i class="fa-solid fa-lock"></i> <!-- Icono de contenido bloqueado -->
					            </div>
					        @endif
					    @empty
					        <p>No hay contenidos disponibles para este módulo.</p>
					    @endforelse
					</section>
		        </div>
		    </div>

		    <article class="moduleDescription moduleDescription--simple">
				<section class="tab">
					<img class="tab__img" 
							src="{{ asset('img/teacher/module.png') }}" 
							alt="Computadora con un curso online">

					<div class="tab__information">
						<p class="tab__subtitle tab__subtitle--bold">
							Curso: {{ $module->course->name }}
						</p>
						<h2 class="tab__title tab__title--module">
							Módulo: <strong class="color-Text">{{ $module->name }}</strong>
						</h2>
						<p>
							Descripción:
							{{ $module->description }}
						</p>
						<div class="tab__iconsDescription">
							<p class="cardModule__item cardModule__item--note">
								<i class="fa-solid fa-note-sticky"></i>
								Lecciones: {{ $module->contents->where('type', 'Nota')->count() }}
							</p>
							<p class="cardModule__item cardModule__item--autoevaluation">
								<i class="fa-solid fa-image-portrait"></i>
								Autoevaluaciones: {{ $module->contents->where('type', 'Autoevaluación')->count() }}
							</p>
							<p class="cardModule__item cardModule__item--question">
								<i class="fa-solid fa-clipboard-question"></i>
								Evaluaciones Interactivas: 
								{{ $module->contents->where('type', 'Evaluación')->count() }}
							</p>
						</div>
						@if($activeContentStudent && $activeContentStudent->orden !== null && $activeContentStudent->orden != 1)
						    <a href="{{ route('student.studyContent', $activeContentStudent) }}" 
						       class="header__loginItem header__loginItem--contrast">
						        ¡Continuar donde lo dejaste!
						    </a>
						@elseif($activeContentStudent)
						    <a href="{{ route('student.studyContent', $activeContentStudent) }}" 
						       class="header__loginItem header__loginItem--contrast">
						        ¡Empezar!
						    </a>
						@elseif($repeatContent)
						    <a href="{{ route('student.studyContent', $repeatContent) }}" 
						       class="header__loginItem header__loginItem--contrast">
						        ¡Empezar de nuevo!
						    </a>
						@else
						<div class= "buttonsNavigation">
						    <a href="{{ route('student.module.pass', $module) }}" 
						       class="buttonParts buttonParts--next">
						        No hay contenidos disponibles, avanzar al siguiente módulo
						        <i class="fas fa-arrow-right"></i>
						    </a>
						</div>
						@endif
					</div>
				</section>
			</article>
		</div>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
@endsection