@extends('student.layout')

@section('title', 'Resultado de la Evaluación Interactiva')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/components/school.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/components/study.css') }}">
@endsection
	



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('student.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Cursos</p>
				</li>
			</a>
			<a href="{{ route('student.course.show', $module->course) }}">
				<li class="breadcrumbs__item" 
					title="Curso: {{ $module->course->name }}">
						<i>/</i>
						<p>{{ $module->course->name }}</p>
				</li>
			</a>
			<a  href="{{ route('student.module.study', $module) }}">
				<li class="breadcrumbs__item" 
					title="Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Resultado de Evaluación Interactiva">
						<i>/</i>
						<p>Resultado de Evaluación Interactiva</p>
				</li>
			</a>
		</ul>
	</div>

	<!--Contenedor-->
	<main class="container container--nonePadding">
		<!--Information-->
		<div class="gridTwo">
		    <div class="navigationContents">
		        <h2 class="navigationContents__title" id="buttonNavigation">
		            Contenido del Módulo 
		            <span class="navigationContents__hamburgerButton">
		                <i class="fa-solid fa-bars"></i>
		            </span>
		        </h2>
		        <div class="boxNavigationContents" id="boxNavigationContents">
		            <!-- Estado -->
		            <section class="navigation__section">
					    @forelse($contents as $index => $item)
					        @php
					            // Determinar el estado del contenido
					            $isContentViewed = $content->id == $item->id;
					            $isActive = $activeContentStudent && $activeContentStudent->id == $item->id;
					            $isCompleted = isset($contentStates[$item->id]) && $contentStates[$item->id] == 'Completado';

					            // Determinar la imagen según el tipo de contenido
					            $imageSrc = match ($item->type) {
					                'Evaluación' => asset('img/course/interaction.png'),
					                'Nota' => asset('img/course/nota_educativa.png'),
					                'Autoevaluación' => asset('img/course/satisfaction-scale.png'),
					                default => '',
					            };

					            // Definir las clases e íconos según el estado del contenido
					            $classes = 'itemNavigation';
					            $icon = '';

					            if ($isContentViewed) {
					                $classes .= ' itemNavigation--activate';
					            } elseif ($isActive) {
					                // No se añaden clases adicionales
					            } elseif ($isCompleted) {
					                $classes .= ' itemNavigation--complete';
					                $icon = '<i class="fa-solid fa-circle-check"></i>';
					            } else {
					                $classes .= ' itemNavigation--block';
					                $icon = '<i class="fa-solid fa-lock"></i>';
					            }

					            // Determinar si el contenido es clickeable
					            $isClickable = $isContentViewed || $isActive || $isCompleted;
					        @endphp

					        @if($isClickable)
					            <a href="{{ route('student.studyContent', $item) }}">
					        @endif
					                <div class="{{ $classes }}">
					                    <div class="itemNavigation__info">
					                        <span class="itemNavigation__number">{{ $index + 1 }}.</span>
					                        <img src="{{ $imageSrc }}" class="navigation__img">
					                        @if($item->type == 'Nota')
					                            <p><b>Lección:</b> {{ $item->note->title }}</p>
					                        @else
					                            <p>{{ $item->type }}</p>
					                        @endif
					                    </div>
					                    {!! $icon !!} <!-- Mostrar el ícono correspondiente -->
					                </div>
					        @if($isClickable)
					            </a>
					       
					        @endif
					    @empty
					        <p>No hay contenidos disponibles para este módulo.</p>
					    @endforelse
					</section>
		        </div>
		    </div>

		    <article class="moduleDescription">
				<div class="boxImage" id="boxImage" data-mean="{{ $viewContent->mean }}">
					@if($viewContent->mean >= 60)
						<img src="{{ asset('img/school/success.png') }}" class="boxImage__img boxImage__img--success">
					@else
						<img src="{{ asset('img/school/cry.png') }}" class="boxImage__img">
					@endif
					<div class="boxImageInfo">
						<div class="boxImageInfo__item boxImageInfo__item--time">
							<h4 class="boxImageInfo__itemTitle">Tiempo</h4>
							<div class="boxImageInfo__description">
								<i class="fa-solid fa-stopwatch"></i>
								<p id="timeCounter">{{ str_pad(\Carbon\Carbon::createFromFormat('H:i:s', $viewContent->time_view)->minute, 2, '0', STR_PAD_LEFT) }}:{{ str_pad(\Carbon\Carbon::createFromFormat('H:i:s', $viewContent->time_view)->second, 2, '0', STR_PAD_LEFT) }}</p>
							</div>
						</div>
						@if($viewContent->mean >= 60)
							<div class="boxImageInfo__item boxImageInfo__item--porcentage">
								<h4 class="boxImageInfo__itemTitle">Desempeño</h4>
								<div class="boxImageInfo__description">
									<i class="fa-solid fa-circle-check"></i>
									<p id="percentageCounter">{{ $viewContent->mean }}%</p>
								</div>
							</div>
						@else
								<div class="boxImageInfo__item boxImageInfo__item--porcentageDanger">
									<h4 class="boxImageInfo__itemTitle">Desempeño</h4>
									<div class="boxImageInfo__description boxImageInfo__description--alert">
										<i class="fa-solid fa-circle-exclamation"></i>
										<p id="percentageCounter">{{ $viewContent->mean }}%</p>
									</div>
								</div>
						@endif
					</div>
					@if($viewContent->mean >= 60)
						<div class="boxButtonTest">
							<a href="{{ route('student.studyContent', $viewContent->content) }}" class="boxButtonTest__button boxButtonTest__button--back">
								<i class="fa-solid fa-arrow-rotate-right"></i>
								Repetir Evaluación
							</a>
						    <form 
							    action="{{ $nextItem ? route('student.passContent', [$content, $nextItem]) : route('student.passContent', $content) }}" 
							    method="POST"
							    @if(!$nextItem) id="finishModuleForm" @endif
							>
							    @csrf
							    <button type="submit" class="boxButtonTest__button boxButtonTest__button--next">
							        Continuar
									<i class="fa-solid fa-arrow-right"></i>
							    </button>
							</form>
						</div>
					@else
						<div class="boxButtonTest">
							<a href="{{ route('student.module.study', $viewContent->content->module) }}" class="boxButtonTest__button boxButtonTest__button--back">
								<i class="fa-solid fa-arrow-left"></i>
								Volver al Módulo
							</a>
							<a href="{{ route('student.studyContent', $viewContent->content) }}" class="boxButtonTest__button boxButtonTest__button--next">
								Repetir Evaluación
								<i class="fa-solid fa-arrow-rotate-right"></i>
							</a>
						</div>
					@endif
				</div>
			</article>
		</div>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
	<script type="module" src="{{ asset('js/evaluations/soundTestEvaluation.js') }}"></script>
	<script type="module" src="{{ asset('js/evaluations/animationText.js') }}"></script>
@endsection