@extends('administrator.layout')

@section('title', 'Lista de usuarios')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/navUserList.css') }}">

	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
@endsection


@section('content')
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="informationcard">
			<section class="tab">
				<img class="tab__img" 
						src="{{ asset('img/administrator/users.png') }}" 
						alt="Grupo de usuarios">

				<div class="tab__information">
					<h2 class="tab__title">
						Gestiona a otros <strong class="color-Text">usuarios</strong>
					</h2>
					<p class="tab__description">En este bloque encontrarás a todas las personas que forman parte de nuestra aplicación. Desde los administradores, encargados de gestionar y dar acceso a los demás usuarios, hasta los profesores, responsables de crear cursos y transmitir conocimientos relevantes. También encontrarás a los estudiantes, quienes tienen la responsabilidad de aprender y pueden acceder a los cursos que deseen para obtener un certificado al finalizar.</p>
					<ul class="header__bottons">
						<a href="{{ route('administrator.create') }}">
							<li class="header__loginItem header__loginItem--contrast">
								Crear un nuevo usuario
							</li>
						</a>
					</ul>
				</div>
			</section>
		</article>

		<ul class="listUserNav">
			<li class="listUserNav__item listUserNav__item--selected">
				<i class="fa-solid fa-graduation-cap"></i>
				<p class="listUserNav__text">Estudiantes</p>
			</li>
			<li class="listUserNav__item">
				<i class="fa-solid fa-chalkboard-user"></i>
				<p class="listUserNav__text">Profesores</p>
			</li>
			<li class="listUserNav__item">
				<i class="fa-solid fa-user-tie"></i>
				<p class="listUserNav__text">Administrador</p>
			</li>
			<li class="listUserNav__item">
				<i class="fa-solid fa-user-slash"></i>
				<p class="listUserNav__text">Usuarios Deshabilitados</p>
			</li>
		</ul>

		<article class="article" id="listAdmin">
			<h2 class="tab__title--centered">
				Lista de <strong class="color-Text">Administradores</strong>
			</h2>

			<div class="boxDownload">
				<a href="{{ route('listAdmins') }}" class="boxDownload__button">
					<i class="fa-solid fa-file-arrow-down"></i>
					Descargar Lista de Administradores
				</a>
			</div>
			
			<div class="containerTableUser">
			<table class="listUser" id="adminTable">
				<thead class="listUser__head">
					<tr class="listUser__trHead">
						<th class="listUser__thHead">
							Foto
						</th>
						<th class="listUser__thHead">
							Nombre
						</th>
						<th class="listUser__thHead">
							Apellido
						</th>
						<th class="listUser__thHead">
							Genero
						</th>
						<th class="listUser__thHead">
							Fecha de Nacimiento
						</th>
						<th class="listUser__thHead">
							Cédula
						</th>
						<th class="listUser__thHead">
							Teléfono
						</th>
						<th class="listUser__thHead">
							Estado
						</th>
						<th class="listUser__thHead">
							Correo<span class="visibilityFalse">i</span>electrónico
						</th>
						<th class="listUser__thHead listUser__thHead--actions">Acciones</th>
					</tr>
				</thead>
				<tbody class="listUser__head">
	        		@foreach ($administrators as $key => $item)
					<tr class="listUser__trBody">
						<td class="listUser__tdBody">
							<img class="imgTable" src="{{ $item->user->profileimg->url }}" alt="foto de perfil">
						</td>
						<td class="listUser__tdBody">{{ $item->user->firts_name }}</td>
						<td class="listUser__tdBody">{{ $item->user->lastname }}</td>
						<td class="listUser__tdBody">{{ $item->user->gender }}</td>
						<td class="listUser__tdBody">
							{{ \Carbon\Carbon::parse($item->user->birthdate)->format('d-m-Y') }}
						</td>
						<td class="listUser__tdBody">{{ $item->user->identification_card }}</td>
						<td class="listUser__tdBody">{{ $item->user->number_phone }}</td>
						<td class="listUser__tdBody">{{ $item->user->parishe->municipalitie->state->name }}</td>
						<td class="listUser__tdBody">{{ $item->user->email }}</td>
						<td class="listUser__tdBody">
							<ul class="buttonsActions">
			                    <a href="{{ route('administrator.edit', $item->user->id) }}" title="Editar" class="icon icon--edit"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
								<form 
			                    	action="{{ route('administrator.disabled', $item->user->id) }}" 
			                    	method="POST" 
			                    	class="form__delete form__delete--disabled">
			                        
			                        @csrf
			                        @method('DELETE')
			                        <button type="submit" class="icon icon--delete"><i title="Deshabilitar" class="fa-solid fa-user-slash"></i> Deshabilitar</button>                
			                    </form> 
							</ul>
						</td>
					</tr>
	        		@endforeach
				</tbody>
			</table>	        	
			</div>
		</article>

		<article class="article" id="listTeacher">
			<h2 class="tab__title--centered">
				Lista de <strong class="color-Text">Profesores</strong>
			</h2>

			<div class="boxDownload">
				<a href="{{ route('listTeachers') }}" class="boxDownload__button">
					<i class="fa-solid fa-file-arrow-down"></i>
					Descargar Lista de Profesores
				</a>
			</div>
			
			<div class="containerTableUser">
			<table class="listUser" id="teacherTable">
				<thead class="listUser__head">
					<tr class="listUser__trHead">
						<th class="listUser__thHead">
							Foto
						</th>
						<th class="listUser__thHead">
							Nombre
						</th>
						<th class="listUser__thHead">
							Apellido
						</th>
						<th class="listUser__thHead">
							Genero
						</th>
						<th class="listUser__thHead">
							Fecha de Nacimiento
						</th>
						<th class="listUser__thHead">
							Cédula
						</th>
						<th class="listUser__thHead">
							Teléfono
						</th>
						<th class="listUser__thHead">
							Estado
						</th>
						<th class="listUser__thHead">
							Correo<span class="visibilityFalse">i</span>electrónico
						</th>
						<th class="listUser__thHead listUser__thHead--actions">Acciones</th>
					</tr>
				</thead>
				<tbody class="listUser__head">
	        		@foreach ($teachers as $key => $item)
					<tr class="listUser__trBody">
						<td class="listUser__tdBody">
							<img class="imgTable" src="{{ $item->user->profileimg->url }}" alt="foto de perfil">
						</td>
						<td class="listUser__tdBody">{{ $item->user->firts_name }}</td>
						<td class="listUser__tdBody">{{ $item->user->lastname }}</td>
						<td class="listUser__tdBody">{{ $item->user->gender }}</td>
						<td class="listUser__tdBody">
							{{ \Carbon\Carbon::parse($item->user->birthdate)->format('d-m-Y') }}
						</td>
						<td class="listUser__tdBody">{{ $item->user->identification_card }}</td>
						<td class="listUser__tdBody">{{ $item->user->number_phone }}</td>
						<td class="listUser__tdBody">{{ $item->user->parishe->municipalitie->state->name }}</td>
						<td class="listUser__tdBody">{{ $item->user->email }}</td>
						<td class="listUser__tdBody">
							<ul class="buttonsActions">
			                    <a href="{{ route('administrator.edit', $item->user->id) }}" title="Editar" class="icon icon--edit"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
								<form 
			                    	action="{{ route('administrator.disabled', $item->user->id) }}" 
			                    	method="POST" 
			                    	class="form__delete form__delete--disabled">
			                        
			                        @csrf
			                        @method('DELETE')
			                        <button type="submit" class="icon icon--delete"><i title="Deshabilitar" class="fa-solid fa-user-slash"></i> Deshabilitar</button>                
			                    </form> 
							</ul>
						</td>
					</tr>
	        		@endforeach
				</tbody>
			</table>	        	
			</div>
		</article>

		<article class="article" id="listStudent">
			<h2 class="tab__title--centered">
				Lista de <strong class="color-Text">Estudiantes</strong>
			</h2>

			<div class="boxDownload">
				<a href="{{ route('listStudents') }}" class="boxDownload__button">
					<i class="fa-solid fa-file-arrow-down"></i>
					Descargar Lista de Estudiantes
				</a>
			</div>

			<div class="containerTableUser">
			<table class="listUser" id="studentTable">
				<thead class="listUser__head">
					<tr class="listUser__trHead">
						<th class="listUser__thHead">
							Foto
						</th>
						<th class="listUser__thHead">
							Nombre
						</th>
						<th class="listUser__thHead">
							Apellido
						</th>
						<th class="listUser__thHead">
							Genero
						</th>
						<th class="listUser__thHead">
							Nacimiento
						</th>
						<th class="listUser__thHead">
							Cédula
						</th>
						<th class="listUser__thHead">
							Teléfono
						</th>
						<th class="listUser__thHead">
							Estado
						</th>
						<th class="listUser__thHead">
							Correo<span class="visibilityFalse">i</span>electrónico
						</th>
						<th class="listUser__thHead listUser__thHead--actions">Acciones</th>
						
						<th class="listUser__thHead">
							Fecha de Registro
						</th>
						<th class="listUser__thHead">
							Última Conexión
						</th>
					</tr>
				</thead>
				<tbody class="listUser__head">
	        		@foreach ($students as $key => $item)
					<tr class="listUser__trBody">
						<td class="listUser__tdBody">
							<img class="imgTable" src="{{ $item->user->profileimg->url }}" alt="foto de perfil">
						</td>
						<td class="listUser__tdBody">{{ $item->user->firts_name }}</td>
						<td class="listUser__tdBody">{{ $item->user->lastname }}</td>
						<td class="listUser__tdBody">{{ $item->user->gender }}</td>
						<td class="listUser__tdBody">
							{{ \Carbon\Carbon::parse($item->user->birthdate)->format('d-m-Y') }}
						</td>
						<td class="listUser__tdBody">{{ $item->user->identification_card }}</td>
						<td class="listUser__tdBody">{{ $item->user->number_phone }}</td>
						<td class="listUser__tdBody">{{ $item->user->parishe->municipalitie->state->name }}</td>
						<td class="listUser__tdBody">{{ $item->user->email }}</td>
						<td class="listUser__tdBody">
							<ul class="buttonsActions">
			                    <a href="{{ route('administrator.edit', $item->user->id) }}" title="Editar" class="icon icon--edit"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
								<form 
			                    	action="{{ route('administrator.disabled', $item->user->id) }}" 
			                    	method="POST" 
			                    	class="form__delete form__delete--disabled">
			                        
			                        @csrf
			                        @method('DELETE')
			                        <button type="submit" class="icon icon--delete"><i title="Deshabilitar" class="fa-solid fa-user-slash"></i> Deshabilitar</button>                
			                    </form> 
							</ul>
						</td>
						
						<td class="listUser__tdBody">
							{{ \Carbon\Carbon::parse($item->user->created_at)->format('d-m-Y') }}
						</td>
						<td class="listUser__tdBody">
							{{ \Carbon\Carbon::parse($item->user->last_connection)->format('d-m-Y') }}
						</td>
						
					</tr>
	        		@endforeach
				</tbody>
			</table>	        	
			</div>
		</article>

		<article class="article" id="listUserDisabled">
			<h2 class="tab__title--centered">
				Lista de <strong class="color-Text">usuarios</strong> deshabilitados
			</h2>

			@if (count($usersDisabled) == 0)
				<h2>No hay usuarios</h2>
			@else

			<div class="boxDownload">
				<a href="{{ route('listDisabledUsers') }}" class="boxDownload__button">
					<i class="fa-solid fa-file-arrow-down"></i>
					Descargar Lista de Usuarios Deshabilitados
				</a>
			</div>
			
			<div class="containerTableUser">
			<table class="listUser" id="studentTable">
				<thead class="listUser__head">
					<tr class="listUser__trHead">
						<th class="listUser__thHead">
							Foto
						</th>
						<th class="listUser__thHead">
							Nombre
						</th>
						<th class="listUser__thHead">
							Apellido
						</th>
						<th class="listUser__thHead">
							Genero
						</th>
						<th class="listUser__thHead">
							Fecha de Nacimiento
						</th>
						<th class="listUser__thHead">
							Cédula
						</th>
						<th class="listUser__thHead">
							Teléfono
						</th>
						<th class="listUser__thHead">
							Estado
						</th>
						<th class="listUser__thHead">
							Correo<span class="visibilityFalse">i</span>electrónico
						</th>
						<th class="listUser__thHead listUser__thHead--actions">Acciones</th>
					</tr>
				</thead>
				<tbody class="listUser__head">
	        		@foreach ($usersDisabled as $key => $item)
					<tr class="listUser__trBody">
						<td class="listUser__tdBody">
							<img class="imgTable" src="{{ $item->profileimg->url }}" alt="foto de perfil">
							
						</td>
						<td class="listUser__tdBody">{{ $item->firts_name }}</td>
						<td class="listUser__tdBody">{{ $item->lastname }}</td>
						<td class="listUser__tdBody">{{ $item->gender }}</td>
						<td class="listUser__tdBody">{{ $item->birthdate }}</td>
						<td class="listUser__tdBody">{{ $item->identification_card }}</td>
						<td class="listUser__tdBody">{{ $item->number_phone }}</td>
						<td class="listUser__tdBody">{{ $item->parishe->municipalitie->state->name }}</td>
						<td class="listUser__tdBody">{{ $item->email }}</td>
						<td class="listUser__tdBody">
							<ul class="buttonsActions">
			                    <a href="{{ route('administrator.edit', $item->id) }}" title="Editar" class="icon icon--edit"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
								<form 
			                    	action="{{ route('administrator.enabled', $item) }}" 
			                    	method="POST" 
			                    	class="form__enabled">
			                        
			                        @csrf
			                        @method('PUT')
			                        <button type="submit" class="icon icon--show"><i title="Habilitar" class="fa-solid fa-user"></i> Habilitar</button>                
			                    </form> 
							</ul>
						</td>
					</tr>
	        		@endforeach
				</tbody>
			</table>	        	
			</div>
			@endif
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/administrator/navUserList.js') }}"></script>
	<script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
	<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript">
	    $(document).ready(function() {
	        // Configuración común para todas las tablas
	        const commonConfig = {
	            responsive: true, // Activar la funcionalidad responsive
	            autoWidth: false,
	            language: {
	                lengthMenu: "Mostrar _MENU_ usuarios",
	                zeroRecords: "No se encontró nada - Disculpa",
	                info: "Mostrando la página _PAGE_ de _PAGES_",
	                infoEmpty: "No hay registros disponibles",
	                infoFiltered: "(filtrado de _MAX_ registros totales)",
	                search: "Buscar:",
	                paginate: {
	                    next: "Siguiente",
	                    previous: "Anterior"
	                }
	            }
	        };

	        // Inicialización de las tablas
	        let tableAdmin = new DataTable('#adminTable', commonConfig);
	        let tableTeacher = new DataTable('#teacherTable', commonConfig);
	        let tableStudent = new DataTable('#studentTable', commonConfig);
	        let tableDisabledUser = new DataTable('#disabledUsers', commonConfig);
	    });
	</script>
@endsection