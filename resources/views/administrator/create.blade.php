@extends('administrator.layout')


@section('title', 'Crea un nuevo usuario')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection

@section('content')
<div class="breadcrumbs">
	<ul class="breadcrumbs__boxList">
		<li class="breadcrumbs__item breadcrumbs__item--base" 
			id="backButton" 
			title="Regresar a la página anterior">
			<i class="fa-solid fa-caret-left"></i>
		</li>
		<a href="{{ route('administrator.index') }}">
			<li class="breadcrumbs__item" 
				title="Ir a Mis Cursos">
					<i>/ <i class="fa-solid fa-users"></i></i>
					<p>Gestión de Usuarios</p>
			</li>
		</a>
		<a>
			<li class="breadcrumbs__item breadcrumbs__item--active" 
				title="Crear un nuevo Usuario">
					<i>/</i>
					<p>Crear un nuevo Usuario</p>
			</li>
		</a>
	</ul>
</div>

	<form class="form__content form__content--big form__content--users" 
			method="POST" 
			action="{{ route('administrator.store') }}">

		@csrf @method('POST')
		<h2 class="form__icon">
			<i class="fa-solid fa-user-plus"></i>
		</h2>
		<h2 class="form__title">Usuario nuevo</h2>

		<div class="grid-two">
			<div class="form__item">
				<label for="identification_card">Documento de Identidad:</label>
				<div class="boxNacionality">
					<select class="form__input form__input--nacionality" 
							name="nacionality" 
							id="nacionality">
						<option value="V" selected>V-</option>
						<option value="E">E-</option>
						<option value="P">P-</option>
					</select>
					<input class="form__input" 
							name="identification_card" 
							required 
							type="text" 
							id="identification_card" 
							placeholder="20111333"
							minlength="6"
							maxlength="10"
							autocomplete="off">
				</div>
			</div>
			<div class="form__item">
				<label for="name">Nombres:</label>
				<input class="form__input" 
						name="firts_name" 
						required 
						type="text" 
						id="firts_name" 
						placeholder="Vanessa"
						autocomplete="off">
			</div>
		</div>

		<div class="grid-two">
			<div class="form__item">
				<label for="lastname">Apellidos:</label>
				<input class="form__input" 
						name="lastname" 
						required 
						type="text" 
						id="lastname" 
						placeholder="Longa"
						autocomplete="off">
			</div>
			<div class="form__item">
				<label for="birthdate">Fecha de nacimiento:</label>
				<input class="form__input" 
						name="birthdate" 
						required 
						type="date" 
						id="birthdate" 
						autocomplete="off">
			</div>
		</div>

		<div class="grid-two">
			<div class="form__item">
				<label for="gender">Genero:</label>
				<div class="boxLabels">
				<label class="inputRadio">
					<input type="radio" name="gender" value="Femenino">
					<p>Femenino</p>
				</label>
				<label class="inputRadio">
					<input type="radio" name="gender" value="Masculino">
					<p>Masculino</p>
				</label>
			</div>
			</div>
			<div class="form__item">
				<label for="number_phone">Número de teléfono:</label>
				<input class="form__input" 
						name="number_phone" 
						required 
						type="text" 
						id="number_phone" 
						placeholder="04120001234"
						minlength="11"
						maxlength="11"
						autocomplete="off">
			</div>
		</div>

		<!-- Datos de Vivienda -->
		<div class="grid-two">
			<div class="form__item">
				<label for="email">Correo Eléctronico:</label>
				<input class="form__input" 
						name="email" 
						required 
						type="email" 
						id="email" 
						placeholder="vanessa@gmail.com"
						autocomplete="off">
			</div>
			<div class="form__item form__item--ubication">
				<label for="state">Estado:</label>
				<select class="form__input" 
							name="state" 
							id="state">
					<option disabled selected>Selecciona un Estado</option>
					@foreach($states as $key => $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="grid-two">
			<div class="form__item">
				<label>Municipio:</label>
				<select class="form__input form__input--disabled" 
						name="municipalitie" 
						id="municipalitie"
						required>
					<option value="" disabled selected>Selecciona un estado primero</option>
				</select>
			</div>
			<div class="form__item">
				<label>Parroquia:</label>
				<select class="form__input form__input--disabled"
						name="parishe"
						id="parishe"
						required>
					<option disabled selected>Selecciona un municipio primero</option>
				</select>
			</div>
		</div>

		<div class="grid-two">
			<div class="form__item">
				<label for="password">Contraseña:</label>
				<input class="form__input" 
						name="password" 
						required 
						type="password" 
						id="password" 
						placeholder="****"
						minlength="4"
						maxlength="20"
						autocomplete="off">
				<p class="form__eye"><i id="form_eye" class="fa-solid fa-eye"></i></p>
			</div>
		</div>

		<div class="grid-one grid-one--justify">
			<div class="form_checkbox">
				<h3 class="form__subtitle">Roles del nuevo usuario</h3>
				<div class="boxCheckbox">
					<input class="input__checkbox input__checkbox--small" 
							name="is_admin" 
							type="checkbox" 
							id="is_admin" 
							autocomplete="off">
					<label for="is_admin" class="labelTitle">
						<i class="fa-solid fa-user-tie"></i>
						<h4 class="labelTitle_text labelTitle_text--checkbox">Administrador</h4>
					</label>
				</div>

				<div class="boxCheckbox">
					<input class="input__checkbox input__checkbox--small" 
							name="is_teacher" 
							type="checkbox" 
							id="is_teacher" 
							autocomplete="off">
					<label for="is_teacher" class="labelTitle">
						<i class="fa-solid fa-person-chalkboard"></i>
						<h4 class="labelTitle_text labelTitle_text--checkbox">Profesor</h4>
					</label>
				</div>

				<div class="boxCheckbox">
					<input class="input__checkbox input__checkbox--small" 
							name="is_student" 
							type="checkbox" 
							id="is_student" 
							autocomplete="off">
					<label for="is_student" class="labelTitle">
						<i class="fa-solid fa-user-graduate"></i>
						<h4 class="labelTitle_text labelTitle_text--checkbox">Estudiante</h4>
					</label>
				</div>

			</div>
		</div>
		<input class="form__send" type="submit" value="¡Crear usuario!">
	</form>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/form/formEye.js') }}"></script>
	<script type="module" src="{{ asset('js/form/formEye.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script type="module" src="{{ asset('js/form/ubication.js') }}"></script>
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
@endsection