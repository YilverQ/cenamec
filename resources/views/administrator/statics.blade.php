@extends('administrator.layout')


@section('title', 'Estadísticas') 
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
@endsection


@section('content')
	<main class="container">
		<article class="article article--whitoutPadding">
			<section class="tab">
				<img class="tab__img" 
						src="{{ asset('img/administrator/statistics.png') }}" 
						alt="Aprende con pequeños pasos">
				
				<div class="tab__information">
					<h2 class="tab__title">Observa <strong class="color-Text">los datos</strong> obtenidos por la aplicación</h2>
					<p class="tab__description">Accede a datos detallados sobre el uso de la aplicación y optimiza la experiencia del usuario con información precisa.</p>
				</div>
			</section>
		</article>
	</main>
@endsection


@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@endsection