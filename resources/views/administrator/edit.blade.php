@extends($role.'.layout')


@section('title', 'Mis datos')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/imgOptions.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/hidden.css') }}">
@endsection


@section('content')
	<!--Contenedor-->
	<main class="container">
		<!--Formulario para cambiar la imagen de perfil-->
		@component('components_admin.formProfileImg', ["profileimgs" => $profileimgs, "user" => $user])
		@endcomponent

		<!--Formulario agregar datos academicos-->
		@component('components_admin.formAcademicAdd', ["user" => $user])
		@endcomponent

		<!--Formulario editar datos academicos-->
		@component('components_admin.formAcademicEdit', ["user" => $user])
		@endcomponent

		<!--Formulario para actualizar datos del usuario-->
		@component('components_admin.formEditProfile', ["user" => $user, "states" => $states])
		@endcomponent

		<!--Formulario agregar datos laborales-->
		@component('components_admin.formLaborAdd', ["user" => $user])
		@endcomponent

		<!--Formulario editar datos laborales-->
		@component('components_admin.formLaborEdit', ["user" => $user])
		@endcomponent


		<div class="breadcrumbs">
			<ul class="breadcrumbs__boxList">
				<li class="breadcrumbs__item breadcrumbs__item--base" 
					id="backButton" 
					title="Regresar a la página anterior">
					<i class="fa-solid fa-caret-left"></i>
				</li>
				<a href="{{ route('administrator.index') }}">
					<li class="breadcrumbs__item" 
						title="Ir a Mis Cursos">
							<i>/ <i class="fa-solid fa-users"></i></i>
							<p>Gestión de Usuarios</p>
					</li>
				</a>
				<a>
					<li class="breadcrumbs__item breadcrumbs__item--active" 
						title="Editar usuario: {{ $user->firts_name }}">
							<i>/</i>
							<p>Usuario: {{ $user->firts_name }}</p>
					</li>
				</a>
			</ul>
		</div>

		<article class="article article--whitoutPadding">
			<section class="profile">
				<div class="superContainerFixed">
					<div class="container_img">
						<div class="boxProfileImg">
							<div class="profile__container-img">
								<img class="profile__img" 
									src="{{ asset($user->profileimg->url) }}" 
									alt="Niño aprendiendo">
							</div>
							<div class="profile_img-edit" id="profileimg">
								<i class="fa-solid fa-pen" title="Editar Foto de perfil"></i>
							</div>
						</div>
						<div class="profileName">
							<h3 class="profileName__title">
								{{ $user->firts_name }}
							</h3>
							<h3 class="profileName__title">
								{{ $user->lastname }}
							</h3>
							<h3 class="profileName__character">{{ $user->profileimg->name }}</h3>
						</div>
					</div>
					<div class="buttonsNav">
						<ul class="buttonsNav__profiles">
							<li class="buttonsNav__item buttonsNav__item--contrast">
								<div class="buttonsNav__boxIcon">
									<i class="fa-solid fa-address-card"></i>
								</div>
								Datos Personales
							</li>
							<li class="buttonsNav__item">
								<div class="buttonsNav__boxIcon">
									<i class="fa-solid fa-users-line"></i>
								</div>
								Roles de usuarios
							</li>
							<li class="buttonsNav__item">
								<div class="buttonsNav__boxIcon">
									<i class="fa-solid fa-user-graduate"></i>
								</div>
								Datos Acádemicos
							</li>
							<li class="buttonsNav__item">
								<div class="buttonsNav__boxIcon">
									<i class="fa-solid fa-briefcase"></i>
								</div>
								Datos Laborales
							</li>
							<li class="buttonsNav__item">
								<div class="buttonsNav__boxIcon">
									<i class="fa-solid fa-key"></i>
								</div>
								Contraseña
							</li>
						</ul>
					</div>
				</div>

				<div class="container_profile">
					<div class="boxInformationProfile">
						<div class="boxInformationProfile__bannerTitle">
							<h3 class="boxInformationProfile__title">
								Datos Personales
							</h3>
							<div class="boxInformationProfile__xmark" id="pencilOpenForm">
								<i class="fa-solid fa-square-pen" 
									title="Editar Datos"></i>
							</div>	
						</div>
						<ul class="boxInformationProfile__details">
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-id-card"></i>
								</span> 
								<b>Documento de Identidad: </b>
								{{ $user->nacionality }}-{{ $user->identification_card }}
							</li>
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-square-phone"></i>
								</span>
								<b>Número de Teléfono: </b>
								{{ $user->number_phone }}
							</li>
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-calendar-days"></i>
								</span>
								<b>Fecha de Nacimiento: </b>
								{{ \Carbon\Carbon::parse($user->birthdate)->format('d-m-Y') }}
							</li>
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-venus-mars"></i>
								</span>
								<b>Género: </b> 
								{{ $user->gender }}
							</li>
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-envelope"></i>
								</span>
								<b>Correo Electrónico: </b> 
								{{ $user->email }}
							</li>
							<li class="boxInformationProfile__item">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-map-location-dot"></i>
								</span>
								<b>Ubicación: </b> 
								Parroquia {{ $user->parishe->name }},
								Municipio {{ $user->parishe->municipalitie->name }},
								@if($user->parishe->municipalitie->state->name == "Distrito Capital")
									{{ $user->parishe->municipalitie->state->name }},
								@else
									Edo. {{ $user->parishe->municipalitie->state->name }},
								@endif
							</li>
							
						</ul>
					</div>
				</div>

				<form action="{{ route('administrator.checkRoles', $user) }}" method="POST">
				    @csrf
				    @method('PUT') 

				    <div class="container_profile hidden">
				        <div class="boxInformationProfile">
				            <div class="boxInformationProfile__bannerTitle">
				                <h3 class="boxInformationProfile__title">
				                    Roles del usuario:
				                </h3>
				            </div>
				            <div class="form_checkbox">
				                <h3 class="form__subtitle">Roles del nuevo usuario</h3>

				                <!-- Role: Administrador -->
				                <div class="boxCheckbox">
				                    <input class="input__checkbox input__checkbox--small" 
				                           name="is_admin" 
				                           type="checkbox" 
				                           id="is_admin" 
				                           autocomplete="off"
				                           {{ $user->administrator ? 'checked' : '' }}>
				                    <label for="is_admin" class="labelTitle">
				                        <i class="fa-solid fa-user-tie"></i>
				                        <h4 class="labelTitle_text labelTitle_text--checkbox">Administrador</h4>
				                    </label>
				                </div>

				                <!-- Role: Profesor -->
				                <div class="boxCheckbox">
				                    <input class="input__checkbox input__checkbox--small" 
				                           name="is_teacher" 
				                           type="checkbox" 
				                           id="is_teacher" 
				                           autocomplete="off"
				                           {{ $user->teacher ? 'checked' : '' }}>
				                    <label for="is_teacher" class="labelTitle">
				                        <i class="fa-solid fa-person-chalkboard"></i>
				                        <h4 class="labelTitle_text labelTitle_text--checkbox">Profesor</h4>
				                    </label>
				                </div>

				                <!-- Role: Estudiante -->
				                <div class="boxCheckbox">
				                    <input class="input__checkbox input__checkbox--small" 
				                           name="is_student" 
				                           type="checkbox" 
				                           id="is_student" 
				                           autocomplete="off"
				                           {{ $user->student ? 'checked' : '' }}>
				                    <label for="is_student" class="labelTitle">
				                        <i class="fa-solid fa-user-graduate"></i>
				                        <h4 class="labelTitle_text labelTitle_text--checkbox">Estudiante</h4>
				                    </label>
				                </div>
				            </div>
				            <div class="grid-one">
				            	<input class="form__send" type="submit" value="Actualizar Roles">
							</div>
				        </div>
				    </div>
				</form>

				<div class="container_profile hidden">
					<div class="boxInformationProfile">
						<div class="boxInformationProfile__bannerTitle">
							<h3 class="boxInformationProfile__title">
								Datos Acádemicos:
							</h3>
						</div>
						<ul class="boxInformationProfile__cards">
							<li class="boxInformationProfile__addInstitution" 
								id="OpenAcademicForm">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-circle-plus"></i>
								</span> 
								<b>Agregar datos acádemicos: </b>
							</li>
							@foreach($academics as $key => $item)
								<li class="boxInformationProfile__institution">
									<span class="boxInformationProfile__iconIntitute boxInformationProfile__iconIntitute--collage">
										<i class="fa-solid fa-landmark"></i>
										<h4 class="boxInformationProfile__grade">{{ $item->academiclevel->name }}</h4>
									</span> 
									<h2 class="boxInformationProfile__titleIntitute">
										{{ $item->academic_title }}
									</h2>
									<ul class="info_intitution">
										<li>{{ $item->institution->name }}</li>
										<li>
											<span class="age-inside">
												{{ $item->year_entry }}
											</span>
											<span> - </span> 
											<span class="age-outside">
												@if($item->year_exit == null)
													En Curso
												@else
													{{ $item->year_exit }}
												@endif
											</span>
										</li>
									</ul>
									<ul class="buttons_edit">
										<li class="boxInformationProfile__xmark editAcademicData num_{{$item->id}}">
											<i class="fa-solid fa-square-pen" title="Editar datos"></i>
										</li>
										<li class="boxInformationProfile__xmark">
											<form 
						                    	action="{{ route('administrator.deleteAcademic', 
						                    				['item' => $item, 'user' => $user]) }}" 
						                    	method="POST" 
						                    	class="form__delete">
						                        
						                        @csrf
						                        @method('DELETE')
						                        <button type="submit" class="boxInformationProfile__xmark boxInformationProfile__xmark--danger"><i title="Eliminar" class="fa-solid fa-trash"></i></button>              
						                    </form> 
										</li>
									</ul>
								</li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="container_profile hidden">
					<div class="boxInformationProfile">
						<div class="boxInformationProfile__bannerTitle">
							<h3 class="boxInformationProfile__title">
								Datos Laborales: 
							</h3>	
						</div>
						<ul class="boxInformationProfile__cards">
							<li class="boxInformationProfile__addInstitution" 
								id="OpenLaboralForm">
								<span class="boxInformationProfile__boxIcon">
									<i class="fa-solid fa-circle-plus"></i>
								</span> 
								<b>Agregar datos laborales: </b>
							</li>
							@foreach($laborals as $key => $item)
								<li class="boxInformationProfile__institution">
									<span class="boxInformationProfile__iconIntitute boxInformationProfile__iconIntitute--collage">
										<i class="fa-solid fa-industry"></i>
										<h4 class="boxInformationProfile__grade">{{ $item->position }}</h4>
									</span> 
									<h2 class="boxInformationProfile__titleIntitute">
										{{ $item->name_intitution }}
									</h2>
									<ul class="info_intitution">
										<li>{{ $item->area }}</li>
										<li>
											<span class="age-inside">
												{{ $item->year_entry }}
											</span>
											<span> - </span> 
											<span class="age-outside">
												@if($item->year_exit == null)
													En Curso
												@else
													{{ $item->year_exit }}
												@endif
											</span>
										</li>
									</ul>
									<ul class="buttons_edit">
										<li class="boxInformationProfile__xmark editLaboralData num_{{$item->id}}">
											<i class="fa-solid fa-square-pen" title="Editar datos"></i>
										</li>
										<li class="boxInformationProfile__xmark">
											<form 
						                    	action="{{ route('administrator.deleteLaboral', 
						                    				['item' => $item, 'user' => $user]) }}" 
						                    	method="POST" 
						                    	class="form__delete">
						                        
						                        @csrf
						                        @method('DELETE')
						                        <button type="submit" class="boxInformationProfile__xmark boxInformationProfile__xmark--danger"><i title="Eliminar" class="fa-solid fa-trash"></i></button>              
						                    </form> 
										</li>
									</ul>
								</li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="container_profile hidden">
					<div class="boxInformationProfile">
						<form class="form__content--simple" 
						method="POST" 
						action="{{ route('administrator.password', $user) }}">
						@csrf @method('PUT')

						<h2 class="form__icon">
							<i class="fa-solid fa-key"></i>
						</h2>
						<h2 class="form__title">Contraseña</h2>
						<div class="grid-one">
							<div class="form__item">
								<label for="password">Contraseña nueva:</label>
								<input class="form__input" 
										name="password" 
										type="password" 
										id="password" 
										required 
										placeholder="****"
										minlength="4"
										maxlength="20"
										autocomplete="off">
								<p class="form__eye"><i id="form_eye" class="fa-solid fa-eye"></i></p>
							</div>
							<input class="form__send" type="submit" value="Actualizar contraseña">
						</div>
					</form>
					</div>
				</div>
			</section>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/profileImg.js') }}"></script>
	<script type="module" src="{{ asset('js/form/formEye.js') }}"></script>
	<script type="module" src="{{ asset('js/form/selectGender.js') }}"></script>

	<script type="module" src="{{ asset('js/userEvents/openDetailsProfile.js') }}"></script>
	<script type="module" src="{{ asset('js/userEvents/openEditProfile.js') }}"></script>
	<script type="module" src="{{ asset('js/userEvents/yearCheck.js') }}"></script>

	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script type="module" src="{{ asset('js/form/ubication.js') }}"></script>
	<script type="module" src="{{ asset('js/form/first_ubication.js') }}"></script>
	<script type="module" src="{{ asset('js/form/selectOptionsAcademic.js') }}"></script>
	<script type="module" src="{{ asset('js/form/selectOptionsAcademicEdit.js') }}"></script>
	<script type="module" src="{{ asset('js/form/getAcademicData.js') }}"></script>
	<script type="module" src="{{ asset('js/form/getAcademicEdit.js') }}"></script>
	<script type="module" src="{{ asset('js/form/getLaboralEdit.js') }}"></script>

	<script type="module" src="{{ asset('js/form/selectOptionsLaboral.js') }}"></script>

	<script type="module" src="{{ asset('js/messages/alertInformation.js') }}"></script>
	<script type="module" src="{{ asset('js/components/navItemProfile.js') }}"></script>
@endsection