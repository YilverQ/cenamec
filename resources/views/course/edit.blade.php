@extends($role.'.layout')


@section('title', 'Editar curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Editar Curso">
					<i>/</i>
					<p>Editar Curso</p>
				</li>
			</a>
		</ul>
	</div>
	
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<form id="formCourse" class="formCourse" 
						method="POST" 
						action="{{ route('teacher.course.update', $course) }}"
						enctype="multipart/form-data">

					@csrf @method('PUT')
					
					<div class="boxForm form__content form__content--big form__content--autoheight">
					    <div class="form__progress">
						    <div class="form__progress-bar" id="progressBar"></div>
						</div>

					    <!-- Primera Parte -->
					    <div id="part1">
					        <h2 class="form__title">Información Básica</h2>
					        <div class="grid-one">
					            <div class="form__item form__item--big">
					                <label for="super_name">Nombre del curso: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <input class="form__input form__input--big" 
					                		type="text" 
					                		value="{{ $course->name }}"
					                		name="super_name" 
					                		id="super_name" 
					                		required>
					            </div>
					            <div class="form__item form__item--big">
								    <label for="img">Imagen del curso: <span class="obligatory" title="Campo Obligatorio">*</span></label>
								    <div class="file-upload">
								        <input type="file" name="img" id="img" class="file-input">
								        <label for="img" class="file-label">Seleccionar Imagen</label>
								        <span id="file-name" class="file-name">{{ $course->img }}</span>

								        <!-- Contenedor para previsualizar la imagen -->
								        <div id="img-preview-container" class="img-preview-container">
								            <img id="img-preview" src="{{ $course->img }}" alt="Vista previa" class="img-preview">
								        </div>
								        
								        <!-- Contenedor para el mensaje de error -->
								        <p id="img-error-message" class="form__message-error" style="display: none;"></p>
								    </div>
								</div>



					            <div class="form__item form__item--big">
					                <label for="level">Estatus del Curso: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <select class="form__input form__input--big" name="status" id="status" required>
					                	<option value="{{ $course->state }}" selected>{{ $course->state }}</option>
					                    <option value="En Desarrollo">En Desarrollo</option>
					                    <option value="Publicado">Publicado</option>
					                    <option value="Archivado">Archivado</option>
					                </select>
					            </div>
					        </div>
					        <div class="buttonBoxCourse">
					            <button type="button" id="next1" class="buttonParts buttonParts--next">Siguiente</button>
					        </div>
					    </div>

					    <!-- Segunda Parte -->
					    <div id="part2" style="display:none;">
					        <h2 class="form__title">Descripción del curso</h2>
					        <div class="grid-one">
					            <div class="form__item form__item--big">
					                <label for="purpose">Propósito: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <textarea class="form__textarea form__input form__input--big" name="purpose" id="purpose" required>{{ $course->purpose }}</textarea>
					            </div>
					            <div class="form__item form__item--big">
					                <label for="general_objetive">Objetivo General: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <textarea class="form__textarea form__input form__input--big" name="general_objetive" id="general_objetive" required>{{ $course->general_objetive }}</textarea>
					            </div>
					            <div class="form__item form__item--big">
					                <label for="specific_objetive">Objetivos Especificos: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <textarea class="form__textarea form__input form__input--big" name="specific_objetive" id="specific_objetive" required>{{ $course->specific_objetive }}</textarea>
					            </div>
					            <div class="form__item form__item--big">
					                <label for="competence">Competencias: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <textarea class="form__textarea form__input form__input--big" name="competence" id="competence" required>{{ $course->competence }}</textarea>
					            </div>
					        </div>
					        <button type="button" id="prev2" class="buttonParts buttonParts--back">Regresar</button>
					        <button type="button" id="next2" class="buttonParts buttonParts--next">Siguiente</button>
					    </div>

					    <!-- Tercera Parte -->
					    <div id="part3" style="display:none;">
					        <h2 class="form__title">Profesores y Categorías</h2>
					        <div class="grid-one">
					             <div class="form__item form__item--big">
					                <label for="level">Nivel: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <select class="form__input form__input--big" name="level" id="level" required>
					                    <option value="{{ $course->level }}" selected>{{ $course->level }}</option>
								        <option value="Básico">Básico</option>
								        <option value="Intermedio">Intermedio</option>
								        <option value="Avanzado">Avanzado</option>
					                </select>
					            </div>
					            <div class="form__item form__item--big">
					                <label for="audiences">Audiencias: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <select class="form__input form__input--big" multiple="multiple" 
					                		name="audiences[]" id="audiences" required>
					                    @foreach($audiences as $key => $item)
					                        <option value="{{ $item->id }}">{{ $item->name }}</option>
					                    @endforeach
					                </select>
					            </div>

					            <div class="form__item form__item--big">
					                <label for="tags">Categorías: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <select class="form__input form__input--big" name="tags[]" id="tags" multiple="multiple" required>
					                	@foreach($tags as $key => $item)
					                        <option value="{{ $item->id }}">{{ $item->name }}</option>
					                    @endforeach
					                </select>
					            </div>
					           	<div class="form__item form__item--big">
					                <label for="teachers">Profesores asignados: <span class="obligatory" title="Campo Obligatorio">*</span></label>
					                <select class="form__input form__input--select" name="teachers[]" id="teachers" multiple required>
					                    @foreach($teachers as $key => $item)
					                        <option value="{{ $item->id }}">{{ $item->user->firts_name }} -- {{ $item->user->identification_card }}</option>
					                    @endforeach
					                </select>
					            </div>
					        </div>
					        <button type="button" id="prev3" class="buttonParts buttonParts--back">Regresar</button>
					        <div class="grid-one">
					            <input class="form__send" id="sendButtonProfile" type="submit" value="Actualizar">
					        </div>
					    </div>
				    </div>
				</form>
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
	<script type="module" src="{{ asset('js/form/partsCoursesForm.js') }}"></script>
	<script type="module" src="{{ asset('js/form/changeIMGFile.js') }}"></script>
	<script type="module" src="{{ asset('js/libsForm/formCourses.js') }}"></script>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<script>
	    $(document).ready(function() {
	        let selectedValues = null;
	        
	        // Inicia Select2 en los selectores multiselect
	        $('#audiences').select2({});
	        selectedValues = @json($course->audiences->pluck('id'));
	        $('#audiences').val(selectedValues).trigger('change');  

	        $('#teachers').select2({});
	        selectedValues = @json($course->teachers->pluck('id'));
	        $('#teachers').val(selectedValues).trigger('change');  


	        $('#tags').select2({});
	        selectedValues = @json($course->tags->pluck('id'));
	        $('#tags').val(selectedValues).trigger('change');  
	    });
	</script>
@endsection