@extends($role.'.layout')

@section('title', 'Lista de cursos')
@section('styles')
	@livewireStyles
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
@endsection



@section('content')
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article">
			<section class="tab">
				<img class="tab__img" 
						src="{{ asset('img/teacher/course.png') }}" 
						alt="Computadora con un curso online">

				<div class="tab__information">
					<h2 class="tab__title">
						Mis <strong class="color-Text">cursos</strong>
					</h2>
					<p class="tab__description">
						Un cursos es el área de estudio, estos cuentan con módulos educativos, lecciones y evaluaciones. Todo este conjuntos de herramientas pedagógicas permiten al estudiante descubrir temas nuevos, interesantes y divertidos.
					</p>
					<ul class="header__bottons">
						<a href="{{ route('teacher.course.create') }}">
							<li class="header__loginItem header__loginItem--contrast">
								Crear un curso
							</li>
						</a>
					</ul>
				</div>
			</section>
		</article>

		@livewire('course-filter')
	</main>
@endsection

@section('scripts')
	@livewireScripts
	<script type="module" src="{{ asset('js/course/openFiltered.js') }}"></script>
@endsection