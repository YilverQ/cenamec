@extends($role.'.layout')


@section('title', 'Curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/headerBackground.css') }}">
	<link href="https://cdn.datatables.net/v/dt/dt-1.13.6/datatables.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
@endsection



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<li class="breadcrumbs__item breadcrumbs__item--active" 
				title="Página Actual: {{ $course->name }}">
					<i>/</i>
					<p>{{ $course->name }}</p>
			</li>
		</ul>
	</div>

	<p class="message message--success hidden" id="linkCopy"> 
		¡Link copiado, ya puedes compartirlo!
		<i class="close-message fa-solid fa-xmark" id="xMarkClose"></i>
	</p>
	
	<!--Header-->
	<div class="containerHeader">
		<img src="{{ $course->img }}" alt="Imagen del curso {{ $course->name }}" class="headerBackground__img">
		<div class="sabana"></div>
		<header class="headerBackground">
			<div class="smallFlex">
				<div class="headerBackground__information">
					<div>
						<small class="descriptionContent">Título del curso:</small>
						<h2 class="headerBackground__title">{{ $course->name }}</h2>
					</div>
				</div>
				<div class="qrBars">
					{!! QrCode::size(250)->generate($linkQr) !!}
					<div class="shared" id="shared">
						<span class="iconShared">
							<i class="fa-solid fa-copy"></i>
						</span>
						<p class="textShared">Copiar link del curso</p>
						<span class="linkShared hidden" id="linkShared">{{ $linkQr }}</span>
					</div>
				</div>
			</div>
			<ul class="headerBackground__buttons">
				<a href="{{ route('teacher.course.edit', $course) }}">
					<li class="header__loginItem header__loginItem--contrast">
						Editar curso
					</li>
				</a>
			</ul>
		</header>
	</div>


	<div class="descriptionBanner">
		<nav class="descriptionBanner__nav">
			<p class="descriptionBanner__item descriptionBanner__item--checked">Propósito</p>
			<p class="descriptionBanner__item">Objetivos</p>
			<p class="descriptionBanner__item">Competencias a lograr</p>
			<p class="descriptionBanner__item">Profesores</p>
			<p class="descriptionBanner__item">Estudiantes</p>
			<a href="{{ route('course.statics', $course) }}">
				<p class="descriptionBanner__item">Estadísticas <i class="fa-solid fa-chart-pie"></i></p>
			</a>
			<p class="collapse__icon">
				<i class="collapse__text fa-solid fa-chevron-up"></i></i>
				<i class="collapse__text hidden fa-solid fa-chevron-down"></i></i>
			</p>
		</nav>

		<div class="descriptionInfo">
			<!-- Propósito del Curso -->
			<div class="descriptionCard">
				<h3 class="smallInformation__title">Propósito del curso</h3>
				<p class="smallInformation">{{ $course->purpose }}</p>
			</div>

			<!-- Objetivos del curso -->
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Objetivo General</h3>
				<p class="smallInformation">{{ $course->general_objetive }}</p>
				<h3 class="smallInformation__title smallInformation__title--second">Objetivos Especifícos</h3>
				<p class="smallInformation">{!! nl2br($course->specific_objetive) !!}</p>
			</div>

			<!-- Competencias a Lograr -->
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Competencias a lograr</h3>
				<p class="smallInformation">{!! nl2br($course->competence) !!}</p>
			</div>

			<!-- Lista de profesores asociado al curso -->
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Profesores asignados al curso</h3>
				<div class="containerTableUser">
					<table class="listUser" id="teacherTable">
						<thead class="listUser__head">
							<tr class="listUser__trHead">
								<th class="listUser__thHead">
									Foto
								</th>
								<th class="listUser__thHead">
									Nombre
								</th>
								<th class="listUser__thHead">
									Apellido
								</th>
								<th class="listUser__thHead">
									Cédula
								</th>
								<th class="listUser__thHead">
									Número<span class="visibilityFalse">i</span>de<span class="visibilityFalse">i</span>teléfono
								</th>
								<th class="listUser__thHead">
									Correo<span class="visibilityFalse">i</span>electrónico
								</th>
							</tr>
						</thead>
						<tbody class="listUser__head">
			        		@foreach ($teachers as $key => $item)
							<tr class="listUser__trBody">
								<td class="listUser__tdBody">
									<img class="imgTable" src="{{ $item->user->profileImg->url }}" alt="foto de perfil">
								</td>
								<td class="listUser__tdBody">{{ $item->user->firts_name }}</td>
								<td class="listUser__tdBody">{{ $item->user->lastname }}</td>
								<td class="listUser__tdBody">{{ $item->user->identification_card }}</td>
								<td class="listUser__tdBody">{{ $item->user->number_phone }}</td>
								<td class="listUser__tdBody">{{ $item->user->email }}</td>
							</tr>
			        		@endforeach
						</tbody>
					</table>
				</div>
			</div>

			<!-- Lista de estudiantes asociado al curso -->
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Estudiantes inscritos al curso</h3>
				<div class="containerTableUser">
				<div class="boxDownload">
					<a href="{{ route('usersByCourse', $course) }}" class="boxDownload__button" href="#">
						<i class="fa-solid fa-file-arrow-down"></i>
						Descargar Lista de Usuarios
					</a>
				</div>
				<table class="listUser" id="studentTable">
					<thead class="listUser__head">
						<tr class="listUser__trHead">
							<th class="listUser__thHead">
								Foto
							</th>
							<th class="listUser__thHead">
								Nombre
							</th>
							<th class="listUser__thHead">
								Apellido
							</th>
							<th class="listUser__thHead">
								Cédula
							</th>
							<th class="listUser__thHead">
								Número de teléfono
							</th>
							<th class="listUser__thHead">
								Correo electrónico
							</th>
							<th class="listUser__thHead">
								Inscripción
							</th>
							<th class="listUser__thHead">
								Última conexión
							</th>
							<th class="listUser__thHead">
								Módulo
							</th>
							<th class="listUser__thHead">
								Finalizado
							</th>
						</tr>
					</thead>
					<tbody class="listUser__head">
						@if (empty($students[0]))
						<p>No hay estudiantes inscritos al curso</p>
						@endif
		        		@foreach ($students as $key => $item)
						<tr class="listUser__trBody">
							<td class="listUser__tdBody">
								<img class="imgTable" src="{{ $item->user->profileImg->url }}" alt="foto de perfil">
							</td>
							<td class="listUser__tdBody">{{ $item->user->firts_name }}</td>
							<td class="listUser__tdBody">{{ $item->user->lastname }}</td>
							<td class="listUser__tdBody">{{ $item->user->identification_card }}</td>
							<td class="listUser__tdBody">{{ $item->user->number_phone }}</td>
							<td class="listUser__tdBody">{{ $item->user->email }}</td>
							<td class="listUser__tdBody">
							    {{ \Carbon\Carbon::parse($item->courses_pivot
							    							->where('id', $course->id)
							    							->first()->pivot->dateStart)
							    							->format('d/m/Y') }}
							</td>
							<td class="listUser__tdBody">
							    {{ \Carbon\Carbon::parse($item->courses_pivot
							    							->where('id', $course->id)
							    							->first()->pivot->last_connection)
							    							->format('d/m/Y') }}
							</td>

							<td class="listUser__tdBody">{{ $item->currentModule($course->id)->level }}° Módulo</td>
							@if($item->courses_pivot->where('id', $course->id)->first()->pivot->dateFinished)
								<td class="listUser__tdBody">{{ \Carbon\Carbon::parse($item->courses_pivot
									    							->where('id', $course->id)
									    							->first()->pivot->dateFinished)
									    							->format('d/m/Y') }}
								</td>
							@else
								<td class="listUser__tdBody">En Curso</td>
							@endif
						</tr>
		        		@endforeach
					</tbody>
				</table>
				</div>
			</div>

			<!-- Sección de Estadísticas -->
			<div class="descriptionCard hidden">
			</div>
		</div>

	</div>

	<main class="container">
		<!--Information-->
		<article class="article">
			<h2 class="tab__title--centered">
				Módulos del <strong class="color-Text">curso</strong>
			</h2>
			@if (empty($modules[0]))
				<h3>No hay módulos</h3>
			@else
				<section class="containerModules">
				@foreach ($modules as $key => $item)
					<div class="cardModule">
						<span class="cardModule__number">{{ $item->level }}</span>
						<h3 class="cardModule__title">
							{{ $item->name }}
						</h3>
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $item->contents->where('type', 'Evaluación')->count() }}
						</p>
						<ul class="list__actions list__actions--course">
							<a href="{{ route('teacher.module.show', $item) }}" title="Ver más" class="icon icon--show"><i class="fa-solid fa-gears"></i> Detalles</a>
							<form 
		                    	action="{{ route('teacher.module.destroy', $item) }}" 
		                    	method="POST" 
		                    	class="form__delete">
		                        
		                        @csrf
		                        @method('DELETE')
		                        <button type="submit" class="icon icon--delete"><i title="Eliminar" class="fa-solid fa-trash"></i> Eliminar</button>                
		                    </form> 
						</ul>
					</div>
					<div class="bar"></div>
				@endforeach
				</section>
			@endif
			<a href="{{ route('teacher.module.create', $course) }}">
				<li class="header__loginItem header__loginItem--contrast">
					Crear un módulo educativo nuevo
				</li>
			</a>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/course/descriptionBanner.js') }}"></script>
	<script type="module" src="{{ asset('js/course/copyLink.js') }}"></script>
	<script src="https://cdn.datatables.net/v/dt/jq-3.7.0/dt-1.13.6/datatables.min.js"></script>
	<script type="text/javascript">
		let table = new DataTable('#teacherTable', {
		    responsive: true,
		    autoWidth : false,
		    
		    "language": {
	            "lengthMenu": "Mostrar _MENU_ usuarios",
	            "zeroRecords": "No se encontró nada - Disculpa",
	            "info": "Mostrando la página _PAGE_ de _PAGES_",
	            "infoEmpty": "No hay registros disponibles",
	            "infoFiltered": "(filtrado de _MAX_ registros totales)",
	            "search": "Buscar:",
	            "paginate": {
	            	"next": "Siguiente",
	            	"previous": "Anterior"
	            }
	        }
		});
	</script>
	<script type="text/javascript">
		let table2 = new DataTable('#studentTable', {
		    responsive: true,
		    autoWidth : false,
		    
		    "language": {
	            "lengthMenu": "Mostrar _MENU_ usuarios",
	            "zeroRecords": "No se encontró nada - Disculpa",
	            "info": "Mostrando la página _PAGE_ de _PAGES_",
	            "infoEmpty": "No hay registros disponibles",
	            "infoFiltered": "(filtrado de _MAX_ registros totales)",
	            "search": "Buscar:",
	            "paginate": {
	            	"next": "Siguiente",
	            	"previous": "Anterior"
	            }
	        }
		});
	</script>
@endsection