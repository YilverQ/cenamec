@extends('errors.layout')


@section('title', 'Lo siento, Contenido no encontrado')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/error.css') }}">
@endsection



@section('content')
	<main class="container">
		<div class="error404Box">
			<div class="Info404">
				<h1>Oops! Página no encontrada.</h1>
				<p>Parece que has llegado a una página que no existe. ¡Lo sentimos! Por favor, regresa a la página principal o regresa a la página anterior. ¡Gracias!</p>
				<span id="backButton">
					<i class="fa-solid fa-arrow-left"></i>
					Volver
				</span>
			</div>
			<img class="img404" src="{{ asset('img/others/404.jpg') }}">
		</div>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButtonError.js') }}"></script>
@endsection