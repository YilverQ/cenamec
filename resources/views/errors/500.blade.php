@extends('errors.layout')


@section('title', 'Lo siento, Contenido no encontrado')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/error.css') }}">
@endsection



@section('content')
	<main class="container">
		<div class="error404Box">
			<div class="Info404">
				<h1>¡Ups! Algo salió mal.</h1>
				<p>Parece que hemos tenido un problema en nuestro servidor. ¡Lo sentimos! Por favor, intenta de nuevo más tarde o regresa a la página principal. ¡Gracias por tu comprensión!</p>
				<span id="backButton">
					<i class="fa-solid fa-arrow-left"></i>
					Volver
				</span>
			</div>
			<img class="img404" src="{{ asset('img/others/500.jpg') }}">
		</div>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButtonError.js') }}"></script>
@endsection