<!DOCTYPE html>
<html>
<head>
    <!--Meta-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Title-->
    <title>Certificado - {{ $course->name }} - CENAMEC</title>

    <!--Estilos-->
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/certificate/pdf.css') }}">
</head>
<body>
    <!-- Primera Página -->
    <div class="page page-one">
    	<box class="aHidden"></box>
    	<box class="aShow">a:</box>
        <div class="namePerson">
            <h2 class="namePerson__title">
                {{ $student->firts_name }}
                {{ $student->lastname }}
            </h2>
        </div>

        <p class="numberID">{{ $formattedID }}</p>

        <h2 class="courseTitle">{{ $course->name }}</h2>
        
        <p class="date-certificate">
        	{{ \Carbon\Carbon::parse($certificate->date_certificate)->format('d-m-Y') }}
        </p>
        <p class="code-certificate">
           CENAMEC-{{ $certificate->course_id }}-{{ $certificate->student_id }}-ID-{{ $certificate->id }}
        </p>
    </div>

    <!-- Segunda Página -->
    <div class="page page-two">
        <h2 class="titleCourse">{{ $course->name }}</h2>
        <p class="general_objetive">{{ $course->general_objetive }}</p>

        <div class="course-modules">
		    @foreach ($course->modules as $key => $module)
		        <div class="module">
		            <h2 class="module__title">Módulo {{$key+1}}: {{ $module->name }}</h2>
		        </div>
		    @endforeach
		</div>

        <div class="qrBars">
            <img src="data:image/png;base64, {!! base64_encode($qrImage) !!}" class="qrImagen">
        </div>
    </div>
</body>
</html>