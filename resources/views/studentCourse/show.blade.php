@extends('student.layout')


@section('title', 'Detalles del curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/headerBackground.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
	<link href="https://cdn.datatables.net/v/dt/dt-1.13.6/datatables.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">

@endsection



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('student.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Cursos</p>
				</li>
			</a>
			<li class="breadcrumbs__item breadcrumbs__item--active" 
				title="Página Actual: {{ $course->name }}">
					<i>/</i>
					<p>{{ $course->name }}</p>
			</li>
		</ul>
	</div>

	<p class="message message--success hidden" id="linkCopy"> 
		¡Link copiado, puedes compartirlo!
		<i class="close-message fa-solid fa-xmark" id="xMarkClose"></i>
	</p>

	<!--Header-->
	<div class="containerHeader">
		<img src="{{ $course->img }}" alt="Imagen del curso {{ $course->name }}" class="headerBackground__img">
		<div class="sabana"></div>
		<header class="headerBackground headerBackground--flexbox">
			<div class="BigBoxHeader">
				<div class="headerBackground__information">
					<div>
						<small class="descriptionContent">Título del curso:</small>
						<h2 class="headerBackground__title">{{ $course->name }}</h2>
					</div>
				</div>
				<form 
	            	action="{{ route('student.course.store', $course) }}" 
	            	method="POST" 
	            	class="headerBackground__buttons">
	                
	                @csrf
	                @method('POST')
	                <button type="submit" class="header__loginItem header__loginItem--contrast">
							Inscribirse
	                </button>                
	            </form>
			</div>
            <div class="qrBars">
				{!! QrCode::size(250)->generate($linkQr) !!}
				<div class="shared" id="shared">
					<span class="iconShared">
						<i class="fa-solid fa-share-nodes"></i>
					</span>
					<p class="textShared">Copiar link del curso</p>
					<span class="linkShared hidden" id="linkShared">{{ $linkQr }}</span>
				</div>
			</div>
		</header>
	</div>

	<div class="descriptionBanner">
		<nav class="descriptionBanner__nav">
			<p class="descriptionBanner__item descriptionBanner__item--checked">Propósito</p>
			<p class="descriptionBanner__item">Objetivos</p>
			<p class="descriptionBanner__item">Competencias a lograr</p>
			<p class="collapse__icon">
				<i class="collapse__text fa-solid fa-chevron-up"></i></i>
				<i class="collapse__text hidden fa-solid fa-chevron-down"></i></i>
			</p>
		</nav>
		<div class="descriptionInfo">
			<div class="descriptionCard">
				<h3 class="smallInformation__title">Propósito del curso</h3>
				<p class="smallInformation">{{ $course->purpose }}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Objetivo General</h3>
				<p class="smallInformation">{{ $course->general_objetive }}</p>
				<h3 class="smallInformation__title smallInformation__title--second">Objetivos Especifícos</h3>
				<p class="smallInformation">{!! nl2br($course->specific_objetive) !!}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Competencias a lograr</h3>
				<p class="smallInformation">{!! nl2br($course->competence) !!}</p>
			</div>
		</div>
	</div>

	<main class="container">
		<!--Information-->
		<article class="article">
			<h2 class="tab__title--centered">
				Módulos del <strong class="color-Text">curso</strong>
			</h2>
			@if (empty($modules[0]))
				<h3>No hay módulos</h3>
			@else
				<section class="containerModules">
				@foreach ($modules as $key => $item)
					<div class="cardModule cardModule--student-block">
						<span class="cardModule__number">{{ $item->level }}</span>
						<h3 class="cardModule__title">
							{{ $item->name }}
						</h3>
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $item->contents->where('type', 'Evaluación')->count() }}
						</p>
						<ul class="list__actions list__actions--course">
							<li class="icon icon--lock">
								<i class="fa-solid fa-lock iconBlockCourse"></i> 
							</li>
						</ul>
					</div>
					<div class="bar"></div>
				@endforeach
				</section>
			@endif
			<form 
            	action="{{ route('student.course.store', $course) }}" 
            	method="POST" 
            	class="bottonEnd">
                
                @csrf
                @method('POST')
                <button type="submit" class="header__loginItem header__loginItem--contrast">
                	Inscribirse
                </button>                
            </form>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/course/descriptionBanner.js') }}"></script>
	<script type="module" src="{{ asset('js/course/copyLink.js') }}"></script>
@endsection