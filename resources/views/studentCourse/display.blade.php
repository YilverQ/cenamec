@extends('student.layout')

@section('title', 'Detalles del curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/headerBackground.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
	<link href="https://cdn.datatables.net/v/dt/dt-1.13.6/datatables.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('student.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Cursos</p>
				</li>
			</a>
			<li class="breadcrumbs__item breadcrumbs__item--active" 
				title="Página Actual: {{ $course->name }}">
					<i>/</i>
					<p>{{ $course->name }}</p>
			</li>
		</ul>
	</div>
	
    <p class="message message--success hidden" id="linkCopy"> 
		¡Link copiado, ya puedes compartirlo!
		<i class="close-message fa-solid fa-xmark" id="xMarkClose"></i>
	</p>

	<!--Header-->
	<div class="containerHeader">
		<img src="{{ $course->img }}" alt="Imagen del curso {{ $course->name }}" class="headerBackground__img">
		<div class="sabana"></div>
		<header class="headerBackground headerBackground--flexbox">
			<div class="headerBackground__information">
				<div>
					<small class="descriptionContent">Título del curso:</small>
					<h2 class="headerBackground__title">{{ $course->name }}</h2>
				</div>
			</div>
			<div class="qrBars">
				{!! QrCode::size(250)->generate($linkQr) !!}
				<div class="shared" id="shared">
					<span class="iconShared">
						<i class="fa-solid fa-copy"></i>
					</span>
					<p class="textShared">Copiar link del curso</p>
					<span class="linkShared hidden" id="linkShared">{{ $linkQr }}</span>
				</div>
			</div>
		</header>
	</div>

	<div class="descriptionBanner">
		<nav class="descriptionBanner__nav">
			<p class="descriptionBanner__item descriptionBanner__item--checked">Propósito</p>
			<p class="descriptionBanner__item">Objetivos</p>
			<p class="descriptionBanner__item">Competencias a lograr</p>
			<p class="descriptionBanner__item">Desempeño Académico</p>
			<p class="collapse__icon">
				<i class="collapse__text fa-solid fa-chevron-up"></i></i>
				<i class="collapse__text hidden fa-solid fa-chevron-down"></i></i>
			</p>
		</nav>
		<div class="descriptionInfo">
			<div class="descriptionCard">
				<h3 class="smallInformation__title">Propósito del curso</h3>
				<p class="smallInformation">{{ $course->purpose }}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Objetivo General</h3>
				<p class="smallInformation">{{ $course->general_objetive }}</p>
				<h3 class="smallInformation__title smallInformation__title--second">Objetivos Especifícos</h3>
				<p class="smallInformation">{!! nl2br($course->specific_objetive) !!}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Competencias a lograr</h3>
				<p class="smallInformation">{!! nl2br($course->competence) !!}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Desempeño Académico</h3>
				<div class="containerTableUser">
					<table class="listUser" id="academyTable">
						<thead class="listUser__head">
							<tr class="listUser__trHead">
								<th class="listUser__thHead">
									Módulo
								</th>
								<th class="listUser__thHead">
									Contenido
								</th>
								<th class="listUser__thHead">
									Fecha Vista
								</th>
								<th class="listUser__thHead">
									Tiempo
								</th>
								<th class="listUser__thHead">
									Desempeño
								</th>
							</tr>
						</thead>
						<tbody class="listUser__head">
			        		@foreach ($viewContents as $key => $item)
							<tr class="listUser__trBody">
								<td class="listUser__tdBody">
									{{ $item->content->module->name }}
								</td>
								<td class="listUser__tdBody listUser__tdBody--nowrap">
									{{ $item->content->orden }}. {{ $item->content->type }}
								</td>
							    <td class="listUser__tdBody">
							    	{{ \Carbon\Carbon::parse($item->start_view)->format('d/m/Y') }}
							    </td>
							    <td class="listUser__tdBody">
							        @php
							            $time = \Carbon\Carbon::parse($item->time_view);
							            $minutes = $time->hour * 60 + $time->minute;
							            $seconds = $time->second;
							        @endphp
							        {{ sprintf('%02d:%02d', $minutes, $seconds) }}
							    </td>
							    <td class="listUser__tdBody">{{ $item->mean }}%</td>
							</tr>
			        		@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<main class="container">
		<!--Information-->
		<article class="article">
			<h2 class="tab__title--centered">
				Módulos del <strong class="color-Text">curso</strong>
			</h2>
			@if (empty($modules[0]))
				<h3>No hay módulos</h3>
			@else
				<section class="containerModules">
				@foreach ($modules as $key => $item)
					@php
						$moduleStudent = $item->students_pivot()->where('student_id', $student->id)->first()->pivot;
					@endphp
					@if ($moduleStudent->state == "active")
					<div class="cardModule">
						<span class="cardModule__number">{{ $item->level }}</span>
						<h3 class="cardModule__title">
							{{ $item->name }}
						</h3>
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $item->contents->where('type', 'Evaluación')->count() }}
						</p>
						<ul class="list__actions list__actions--course">
							<a href="{{ route('student.module.study', $item) }}" title="Ver más" class="icon icon--show"><i class="fa-solid fa-eye"></i> Estudiar</a>
						</ul>
						@if ($moduleStudent->percentage != 100)
						<span class="cardModule__percentage">
							<progress max="100" value="{{ $moduleStudent->percentage }}"></progress>
							{{ $moduleStudent->percentage }}%
						</span>
						@endif
					</div>
					<div class="bar"></div>
					
					@elseif($moduleStudent->state == "finished")
						@if ($moduleStudent->percentage == 100)
							<div class="cardModule cardModule--finished">
								<span class="cardModule__number">{{ $item->level }}</span>
								<h3 class="cardModule__title">{{ $item->name }}</h3>
								<p class="cardModule__item cardModule__item--note">
									<i class="fa-solid fa-note-sticky"></i>
									Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
								</p>
								<p class="cardModule__item cardModule__item--autoevaluation">
									<i class="fa-solid fa-image-portrait"></i>
									Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
								</p>
								<p class="cardModule__item cardModule__item--question">
									<i class="fa-solid fa-clipboard-question"></i>
									Evaluaciones Interactivas: 
									{{ $item->contents->where('type', 'Evaluación')->count() }}
								</p>
								<ul class="list__actions list__actions--course list__actions--course-white">
									<a href="{{ route('student.module.study', $item) }}" title="Ver más" class="icon icon--show icon--repeat"><i class="fa-solid fa-arrow-rotate-right"></i> Repetir</a>
								</ul>
							</div>
							<div class="bar"></div>
						@else
							<div class="cardModule">
								<span class="cardModule__number">{{ $item->level }}</span>
								<h3 class="cardModule__title">{{ $item->name }}</h3>
								<p class="cardModule__item cardModule__item--note">
									<i class="fa-solid fa-note-sticky"></i>
									Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
								</p>
								<p class="cardModule__item cardModule__item--autoevaluation">
									<i class="fa-solid fa-image-portrait"></i>
									Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
								</p>
								<p class="cardModule__item cardModule__item--question">
									<i class="fa-solid fa-clipboard-question"></i>
									Evaluaciones Interactivas: 
									{{ $item->contents->where('type', 'Evaluación')->count() }}
								</p>
								<ul class="list__actions list__actions--course list__actions--course-white">
									<a href="{{ route('student.module.study', $item) }}" title="Ver más" class="icon icon--show icon--repeat"><i class="fa-solid fa-arrow-trend-up"></i> Mejorar</a>
								</ul>
								<span class="cardModule__percentage">
									<progress max="100" value="{{ $moduleStudent->percentage }}"></progress>
									{{ $moduleStudent->percentage }}%
								</span>
							</div>
							<div class="bar"></div>
						@endif
					
					@else
					<div class="cardModule cardModule--student-block">
						<span class="cardModule__number">{{ $item->level }}</span>
						<h3 class="cardModule__title">
							{{ $item->name }}
						</h3>
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Lecciones: {{ $item->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $item->contents->where('type', 'Evaluación')->count() }}
						</p>
						<ul class="list__actions list__actions--course">
							<li class="icon icon--lock">
								<i class="fa-solid fa-lock iconBlockCourse"></i> 
							</li>
						</ul>
					</div>
					<div class="bar"></div>
					@endif

				@endforeach
				</section>
			@endif

			<div class="bottonEnd">
				@if ($certificate)
				<a href="{{ route('certificate.show', $certificate) }}">
					<li class="bottonEnd__text bottonEnd__text--contrast">
						Generar certificado
					</li>
				</a>
				@else
					<li class="bottonEnd__text bottonEnd__text--disabled">
						Generar certificado
					</li>
				@endif
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/course/descriptionBanner.js') }}"></script>
	<script type="module" src="{{ asset('js/course/copyLink.js') }}"></script>

	<script src="https://cdn.datatables.net/v/dt/jq-3.7.0/dt-1.13.6/datatables.min.js"></script>
	<script type="text/javascript">
		let table = new DataTable('#academyTable', {
		    responsive: true,
		    autoWidth : false,
		    
		    "language": {
	            "lengthMenu": "Mostrar _MENU_ usuarios",
	            "zeroRecords": "No se encontró nada - Disculpa",
	            "info": "Mostrando la página _PAGE_ de _PAGES_",
	            "infoEmpty": "No hay registros disponibles",
	            "infoFiltered": "(filtrado de _MAX_ registros totales)",
	            "search": "Buscar:",
	            "paginate": {
	            	"next": "Siguiente",
	            	"previous": "Anterior"
	            }
	        }
		});
	</script>
@endsection