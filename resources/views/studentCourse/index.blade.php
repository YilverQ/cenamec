@extends($role.'.layout')

@section('title', 'Lista de cursos')
@section('styles')
	@livewireStyles
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
@endsection



@section('content')
	<!--Contenedor-->
	<main class="container">
		@livewire('student-course')
	</main>
@endsection

@section('scripts')
	@livewireScripts
	<script type="module" src="{{ asset('js/course/openFiltered.js') }}"></script>
@endsection