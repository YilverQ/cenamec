@extends('login.layout')


@section('title', 'Olvidé mi contraseña')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection


@section('content')
	<form class="form__content--login" 
		  method="POST"
      	  id="formProfile"
		  action="{{ route('login.addNewPassword') }}">
		@csrf
		<h2 class="form__icon">
			<i class="fa-solid fa-unlock"></i>
		</h2>
		<h2 class="form__title">¡Ingresa nueva contraseña!</h2>
		<div class="form__item form__item--small">
			<label for="password">Contraseña nueva:</label>
			<input class="form__input" 
					name="password" 
					required 
					type="password" 
					id="password" 
					placeholder="****"
					minlength="4"
					maxlength="20"
					autocomplete="off">
			<p class="form__eye"><i id="form_eye" class="fa-solid fa-eye"></i></p>
		</div>
		<input id="sendButtonProfile" class="form__send" 
			type="submit" 
			value="Actualizar Contraseña">
	</form>
@endsection


@section('scripts')
	<script type="module" src="{{ asset('js/libsForm/formSignup.js') }}"></script>
	<script type="module" src="{{ asset('js/form/formEye.js') }}"></script>
@endsection