@extends('login.layout')


@section('title', 'Inicio')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/headerBackground.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
	<link href="https://cdn.datatables.net/v/dt/dt-1.13.6/datatables.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/table.css') }}">
@endsection



@section('content')
	<!--Header-->
	<div class="containerHeader">
		<img src="{{ $course->img }}" alt="Imagen del curso {{ $course->name }}" class="headerBackground__img">
		<div class="sabana"></div>
		<header class="headerBackground headerBackground--flexbox">
			<div class="BigBoxHeader">
				<div class="headerBackground__information">
					<div>
						<small class="descriptionContent">Título del curso:</small>
						<h2 class="headerBackground__title">{{ $course->name }}</h2>
					</div>
				</div>
			</div>
            <div class="infoInscription">
            	<h2 class="infoInscription__title">
	            	Participa en el curso ingresando como estudiante.
	            </h2>
				<div class="boxButtonsShared">
					<a href="{{ route('login.login') }}" class="shared">
						<span class="iconShared">
							<i class="fa-solid fa-arrow-right-to-bracket"></i>
						</span>
						<p class="textShared">Ingresar</p>
					</a>
					<a href="{{ route('login.signup') }}" class="shared shared--second">
						<span class="iconShared">
							<i class="fa-solid fa-user-plus"></i>
						</span>
						<p class="textShared">Crear una cuenta</p>
					</a>
				</div>
			</div>
		</header>
	</div>

	<div class="descriptionBanner">
		<nav class="descriptionBanner__nav">
			<p class="descriptionBanner__item descriptionBanner__item--checked">Propósito</p>
			<p class="descriptionBanner__item">Objetivos</p>
			<p class="descriptionBanner__item">Competencias a lograr</p>
			<p class="collapse__icon">
				<i class="collapse__text fa-solid fa-chevron-up"></i></i>
				<i class="collapse__text hidden fa-solid fa-chevron-down"></i></i>
			</p>
		</nav>
		<div class="descriptionInfo">
			<div class="descriptionCard">
				<h3 class="smallInformation__title">Propósito del curso</h3>
				<p class="smallInformation">{{ $course->purpose }}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Objetivo General</h3>
				<p class="smallInformation">{{ $course->general_objetive }}</p>
				<h3 class="smallInformation__title smallInformation__title--second">Objetivos Especifícos</h3>
				<p class="smallInformation">{!! nl2br($course->specific_objetive) !!}</p>
			</div>
			<div class="descriptionCard hidden">
				<h3 class="smallInformation__title">Competencias a lograr</h3>
				<p class="smallInformation">{!! nl2br($course->competence) !!}</p>
			</div>
		</div>
	</div>

	<main class="container">
		<!--Information-->
		<article class="article">
			<h2 class="tab__title--centered">
				Módulos del <strong class="color-Text">curso</strong>
			</h2>
			@if (empty($modules[0]))
				<h3>No hay módulos</h3>
			@else
				<section class="containerModules">
				@foreach ($modules as $key => $item)
					<div class="cardModule cardModule--student-block">
						<span class="cardModule__number">{{ $item->level }}</span>
						<h3 class="cardModule__title">
							{{ $item->name }}
						</h3>
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Notas educativas: {{ $item->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $item->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $item->contents->where('type', 'Evaluación')->count() }}
						</p>
						<ul class="list__actions list__actions--course">
							<li class="icon icon--lock">
								<i class="fa-solid fa-lock iconBlockCourse"></i> 
							</li>
						</ul>
					</div>
					<div class="bar"></div>
				@endforeach
				</section>
			@endif
		</article>
	</main>
	@include('login.footer')
@endsection


@section('scripts')
	<script type="module" src="{{ asset('js/course/descriptionBanner.js') }}"></script>
@endsection