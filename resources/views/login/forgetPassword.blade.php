@extends('login.layout')


@section('title', 'Olvidé mi contraseña')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection


@section('content')
	<form class="form__content--login" method="POST" action="{{ route('login.checkEmailToResetPassword') }}">
		@csrf

		<h2 class="form__icon">
			<i class="fa-solid fa-lock"></i>
		</h2>
		<h2 class="form__title">¡Recuperar contraseña!</h2>
		<div class="form__item form__item--small">
			<label for="email">Correo Eléctronico:</label>
			<input class="form__input" 
					name="email" 
					required 
					type="email" 
					id="email" 
					placeholder="usuario@gmail.com"
					autocomplete="off">
		</div>
		<div class="boxLinkText">
			<a href="{{ route('login.login') }}" class="linkText">Ingresar como usuario</a>
			<a href="{{ route('login.admin') }}" class="linkText">Administrador</a>
		</div>
		<input id="form__send" class="form__send" 
			type="submit" 
			value="Recuperar contraseña">
	</form>
@endsection


@section('scripts')
@endsection