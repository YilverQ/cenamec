@extends('login.layout')


@section('title', 'Ayuda')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection


@section('content')
	<form class="form__content" method="GET" action="{{ route('login.newPassword') }}">
		@csrf
		<h2 class="form__icon">
			<i class="fa-solid fa-circle-info"></i>
		</h2>
		<h2 class="form__title">¡Ponte en contacto con el personal para recuperar la contraseña!</h2>
		
		<div class="contactSupport">
			<p>Correo Electrónico: yilver0906@gmail.com</p>
			<p>Número de teléfono: 04160140472</p>
			<a target="_blank" href="https://wa.link/87no11" class="WhatssapButton">Whatsapp</a>
		</div>
		<div class="boxLinkText">
			<a href="{{ route('login.login') }}" class="linkText">Ingresar como usuario</a>
			<a href="{{ route('login.admin') }}" class="linkText">Administrador</a>
		</div>
		
	</form>
@endsection


@section('scripts')
@endsection