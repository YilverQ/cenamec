<!-- Footer -->
	<footer class="footer">
		<div class="endTitle">
			<img class="endTitle__img" 
				src="{{ asset('img/logo.png') }}" 
				alt="Imagen principal, wallpaper de un laboratorio">
			<h3 class="endTitle__title">Aprende, Diviertete y Disfruta con</h3>
			<h3 class="endTitle__title">Científicos en Acción</h3>
		</div>
	</footer>