@extends('login.layout')


@section('title', 'Registro')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection

@section('content')
<form class="form__content form__content--big form__content--signup" 
      method="POST" 
      id="formProfile"
      action="{{ route('login.newStudent') }}">
    	@csrf @method('POST')

    <h2 class="form__icon">
        <i class="fa-solid fa-user-plus"></i>
    </h2>
    <h2 class="form__title">Estudiante nuevo</h2>

    <!-- Primera Parte -->
    <div class="boxPartForm" id="part1">
        <div class="grid-two">
            <div class="form__item">
                <label for="identification_card">Documento de identidad: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <div class="boxNacionality">
                    <select class="form__input form__input--nacionality" 
                            name="nacionality" 
                            id="nacionality">
                        <option value="V" selected>V-</option>
                        <option value="E">E-</option>
                    </select>
                    <input class="form__input" 
                           name="identification_card" 
                           required 
                           type="text" 
                           id="identification_card" 
                           placeholder="Ingresa tu documento de identidad"
                           minlength="7"
                           maxlength="8"
                           autocomplete="off">
                </div>
            </div>
            <div class="form__item">
                <label for="firts_name">Nombres: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input" 
                       name="firts_name" 
                       required 
                       type="text" 
                       id="firts_name" 
                       placeholder="Ingresa tu nombre"
                       autocomplete="off">
            </div>
        </div>

        <div class="grid-two">
            <div class="form__item">
                <label for="lastname">Apellidos: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input" 
                       name="lastname" 
                       required 
                       type="text" 
                       id="lastname" 
                       placeholder="Ingresa tu apellido"
                       autocomplete="off">
            </div>
            <div class="form__item">
                <label for="birthdate">Fecha de nacimiento: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input" 
                       name="birthdate" 
                       required 
                       type="date" 
                       id="birthdate" 
                       autocomplete="off">
            </div>
            
            
        </div>

        <div class="grid-two">
            <div class="form__item">
                <label for="gender">Género: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <div class="boxLabels">
                    <label class="inputRadio">
                        <input type="radio" name="gender" value="Femenino" checked>
                        <p>Femenino</p>
                    </label>
                    <label class="inputRadio">
                        <input type="radio" name="gender" value="Masculino">
                        <p>Masculino</p>
                    </label>
                </div>
            </div>
            <div class="form__item">
                <label for="number_phone">Número de teléfono: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input" 
                       name="number_phone" 
                       required 
                       type="text" 
                       id="number_phone" 
                       placeholder="Ingresa tu número de teléfono"
                       minlength="11"
                       maxlength="11"
                       autocomplete="off">
            </div>
        </div>

        <!-- Botón para avanzar a la segunda parte -->
        <button type="button" id="nextPart1" class="buttonParts buttonParts--next">Siguiente</button>
    </div>

    <!-- Segunda Parte -->
    <div class="boxPartForm" id="part2" style="display:none;">
        <!-- Datos de Vivienda -->
        <div class="grid-two">
        	<div class="form__item">
                <label for="email">Correo electrónico: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <input class="form__input"
                       name="email"
                       required
                       type="email"
                       id="email"
                       placeholder="Ingresa tu correo electrónico"
                       autocomplete="off">
            </div>

            <!-- Estado -->
            <div class="form__item form__item--ubication">
                <label for="state">Estado: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <select class="form__input"
                        name="state"
                        id="state">
                    <option disabled selected>Selecciona un estado</option >
                    @foreach($states as $key=> $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option >
                    @endforeach
                </select >
            </div>
        </div>

        <!-- Municipios y Parroquias -->
        <div class="grid-two">
            <div class="form__item">
                <label>Municipio: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <select class="form__input form__input--disabled"
                        name="municipalitie"
                        id="municipalitie"
                        required >
                    <option value="" disabled selected>Selecciona un estado primero</option >
                </select >
            </div>
            <div class="form__item">
                <label>Parroquia: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <select class="form__input form__input--disabled"
                        name="parishe"
                        id="parishe"
                        required >
                    <option disabled selected>Selecciona un municipio primero</option >
                </select >
            </div>
        </div>
        <!-- Correo y Contraseña -->
        <div class="grid-two">
            
            <div class="form__item">
                <label for="password">Contraseña: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <input class="form__input"
                       name="password"
                       required
                       type="password"
                       id="password"
                       placeholder="****"
                       minlength="4"
                       maxlength="20"
                       autocomplete="off">
                <p class="form__eye"><i id="form_eye"class="fa-solid fa-eye"></i ></p >
            </div>
            <div class="form__item">
                <label for="password">Repetir la contraseña: <span class="obligatory" title="Campo Obligatorio">*</span></label >
                <input class="form__input"
                       name="re-password"
                       required
                       type="password"
                       id="re-password"
                       placeholder="****"
                       minlength="4"
                       maxlength="20"
                       autocomplete="off">
                <p class="form__eye"><i id="re-form_eye"class="fa-solid fa-eye"></i ></p >
            </div>
        </div>

        <!-- Botones para regresar y enviar el formulario -->
        <div class="grid-one">
        	<input class="form__send" id="sendButtonProfile" type="submit" value="¡Vamos a estudiar!">
        </div>

        <button type="button" id="prevPart2" class="buttonParts buttonParts--back">Regresar</button >
    </div>
</form>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/form/formEye.js') }}"></script>
	<script type="module" src="{{ asset('js/form/selectGender.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script type="module" src="{{ asset('js/form/ubication.js') }}"></script>

	<script type="module" src="{{ asset('js/libsForm/formSignup.js') }}"></script>
@endsection