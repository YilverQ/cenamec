@extends($role.'.layout')


@section('title', 'Curso')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/statics.css') }}">
@endsection


@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: {{ $course->name }}">
						<i>/</i>
						<p>Estadísticas</p>
				</li>
			</a>
		</ul>
	</div>

	<main class="container">
		<div class="article">
			<h3 class="article__title">Estadísticas del curso: {{$course->name}}</h3>

			<div class="boxHeaderStatics">
				<div class="boxheaderStatics__card">
					<p class="boxheaderStatics__title">Veces Compartido</p>
					<h4 class="boxheaderStatics__number">{{ $course->shared }}</h4>
					<p class="boxheaderStatics__smallInfo">Veces que se compartió el curso</p>
				</div>
				<div class="boxheaderStatics__card">
					<p class="boxheaderStatics__title">Módulos Educativos</p>
					<h4 class="boxheaderStatics__number">{{ count($modules) }}</h4>
					<p class="boxheaderStatics__smallInfo">Cantidad de módulos educativos</p>
				</div>
				<div class="boxheaderStatics__card">
					<p class="boxheaderStatics__title">Contenidos</p>
					<h4 class="boxheaderStatics__number">{{ $modules->flatMap->contents->count() }}</h4>
					<p class="boxheaderStatics__smallInfo">Cantidad de contenidos educativos</p>
				</div>
				<div class="boxheaderStatics__card">
					<p class="boxheaderStatics__title">Estudiantes</p>
					<h4 class="boxheaderStatics__number">{{ count($students) }}</h4>
					<p class="boxheaderStatics__smallInfo">Cantidad de usuarios inscritos</p>
				</div>
				<div class="boxheaderStatics__card">
					<p class="boxheaderStatics__title">Certificados</p>
					<h4 class="boxheaderStatics__number">{{ count($certificates) }}</h4>
					<p class="boxheaderStatics__smallInfo">Cantidad certificados emitidos</p>
				</div>
			</div>

			<div class="boxCharts">
				<div class="leftBoxCharts">
					<div class="itemChart">
						<h5 class="itemChart__superTitle">Contendios Educativos Vistos</h5>
						<p class="itemChart__title">
							Fecha Consultada: {{$viewContentDateStared}} - {{$viewContentDateFinished}} 
						</p>
				        <canvas id="viewContentLineChart"></canvas>
				    </div>

				    <div class="gridItemCharts gridItemCharts--medimCharts">
						<div class="itemChart itemChart--pieChart itemChart--small">
							<h5 class="itemChart__superTitle">Distribución de Contenidos</h5>
						    <canvas id="contentPolarChart"></canvas>
						    <div class="itemChart__info">
							  	<p class="itemLabel">
							  		Autoevaluación: 
							  		{{$cantTypesContents["Autoevaluación"]}}
							  	</p>
							  	<p class="itemLabel">
							  		Lección Educativa: 
							  		{{$cantTypesContents["Lección Educativa"]}}
							  	</p>
							  	<p class="itemLabel">
							  		Evaluación Interactiva: 
							  		{{$cantTypesContents["Evaluación Interactiva"]}}
							  	</p>
							</div>
						</div>

					    <div class="itemChart itemChart--mediumCharts itemChart--medium">
							<h5 class="itemChart__superTitle">Cantidad de Items por Tipo</h5>
						    <canvas id="typeBarChart"></canvas>
						</div>
				    </div>

				    <div class="gridItemCharts gridItemCharts--cards">
						<div class="itemChart itemChart--small">
							<h5 class="itemChart__superTitle">Lecciones</h5>
							<div class="boxDescriptionItems">
								<div class="boxDescriptionItems__item boxDescriptionItems__item--lecciones">
									<div class="smallSeparation">
										<i class="fa-solid fa-book"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Lecciones Vistas</p>
											<p class="boxDescriptionItems__description">Cantidad total de lecciones vistas por todos los usuarios.</p>
										</div>
									</div>
									<div class="numberItem numberItem--lecciones">
										<h4 class="numberItem__info">{{ $notesViewedInfo['total_viewed_notes'] }}</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--lecciones">
									<div class="smallSeparation">
										<i class="fa-solid fa-percent"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Promedio</p>
											<p class="boxDescriptionItems__description">Promedio de lecciones vistas por usuario.</p>
										</div>
									</div>
									<div class="numberItem numberItem--lecciones">
										<h4 class="numberItem__info">{{ $notesViewedInfo['average_notes_viewed_per_user'] }}</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--lecciones">
									<div class="smallSeparation">
										<i class="fa-solid fa-stopwatch"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Tiempo Promedio</p>
											<p class="boxDescriptionItems__description">Tiempo promedio que tarda cada usuario viendo una lección.</p>
										</div>
									</div>
									<div class="numberItem numberItem--lecciones">
										<h4 class="numberItem__info">{{ $notesViewedInfo['average_time_seconds'] }}</h4>
									</div>
								</div>
							</div>
						</div>

						<div class="itemChart itemChart--small">
							<h5 class="itemChart__superTitle">Autoevaluación</h5>
							<div class="boxDescriptionItems">
								<div class="boxDescriptionItems__item boxDescriptionItems__item--autoevaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-circle-check"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Autoevaluaciones Completadas</p>
											<p class="boxDescriptionItems__description">Cantidad total de autoevaluaciones completadas.</p>
										</div>
									</div>
									<div class="numberItem">
										<h4 class="numberItem__info">
											{{ $autoevaluationViewedInfo['total_completed_autoevaluations'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--autoevaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-percent"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Promedio</p>
											<p class="boxDescriptionItems__description">Promedio de autoevaluaciones completadas por usuario.</p>
										</div>
									</div>
									<div class="numberItem">
										<h4 class="numberItem__info">
											{{ $autoevaluationViewedInfo['average_autoevaluations_per_user'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--autoevaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-stopwatch"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Tiempo Promedio</p>
											<p class="boxDescriptionItems__description">Tiempo promedio que tarda cada usuario realizando una autoevaluación.</p>
										</div>
									</div>
									<div class="numberItem">
										<h4 class="numberItem__info">
											{{ $autoevaluationViewedInfo['average_time_seconds'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--autoevaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-list-check"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Escala de Likert</p>
											<p class="boxDescriptionItems__description">Cantidad total de escalas de likerts hechas por todos los usuarios.</p>
										</div>
									</div>
									<div class="numberItem">
										<h4 class="numberItem__info">
											{{ $autoevaluationViewedInfo['total_likerts_answered'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--autoevaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-user-check"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Escala de Likert por Usuario</p>
											<p class="boxDescriptionItems__description">Promedio de escala de likert hecha por usuario.</p>
										</div>
									</div>
									<div class="numberItem">
										<h4 class="numberItem__info">
											{{ $autoevaluationViewedInfo['average_likerts_answered_per_user'] }}
										</h4>
									</div>
								</div>
							</div>
						</div>

						<div class="itemChart itemChart--small">
							<h5 class="itemChart__superTitle">Evaluaciones Interactivas</h5>
							<div class="boxDescriptionItems">
								<div class="boxDescriptionItems__item boxDescriptionItems__item--evaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-clipboard-question"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Evaluaciones Interactivas Completas</p>
											<p class="boxDescriptionItems__description">Cantidad total de evaluaciones interactivas completadas por todos los usuarios.</p>
										</div>
									</div>
									<div class="numberItem numberItem--evaluation">
										<h4 class="numberItem__info">
											{{ $evaluationViewedInfo['total_completed_evaluations'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--evaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-percent"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Promedio</p>
											<p class="boxDescriptionItems__description">Promedio de evaluaciones interactivas completas por usuario.</p>
										</div>
									</div>
									<div class="numberItem numberItem--evaluation">
										<h4 class="numberItem__info">
											{{ $evaluationViewedInfo['average_completed_evaluations'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--evaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-stopwatch"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Tiempo Promedio</p>
											<p class="boxDescriptionItems__description">Tiempo promedio que tarda cada usuario realizando una evaluación interactiva.</p>
										</div>
									</div>
									<div class="numberItem numberItem--evaluation">
										<h4 class="numberItem__info">
											{{ $evaluationViewedInfo['average_time_seconds'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--evaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-file-circle-check"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Items de Evaluación Interactiva</p>
											<p class="boxDescriptionItems__description">Cantidad total de items respondidos por todos los usuarios.</p>
										</div>
									</div>
									<div class="numberItem numberItem--evaluation">
										<h4 class="numberItem__info">
											{{ $evaluationViewedInfo['total_items_answered'] }}
										</h4>
									</div>
								</div>
								<div class="boxDescriptionItems__item boxDescriptionItems__item--evaluation">
									<div class="smallSeparation">
										<i class="fa-solid fa-spell-check"></i>
										<div class="boxDescriptionItems__info">
											<p class="boxDescriptionItems__subtitle">Promedio de Items Respondido</p>
											<p class="boxDescriptionItems__description">Promedio de items de evaluaciones interactivas respondido por usuario.</p>
										</div>
									</div>
									<div class="numberItem numberItem--evaluation">
										<h4 class="numberItem__info">
											{{ $evaluationViewedInfo['average_items_answered'] }}
										</h4>
									</div>
								</div>
							</div>
						</div>
				    </div>
				</div>
				
				<div class="rightBoxCharts">
					<div class="itemChart">
						<h5 class="itemChart__superTitle">Distribución de Género</h5>
					  <canvas id="statusGender"></canvas>
					  <div class="itemChart__info">
					  	<p class="itemLabel">Femenino: <span class="itemLabel__number"> {{$feminine}} </span></p>
					  	<p class="itemLabel">Másculino: <span class="itemLabel__number">{{$masculine}}</span></p>
					  </div>
					</div>

					<div class="itemChart">
						<h5 class="itemChart__superTitle">Últimas conexiones</h5>
						<ul class="listUsers">
							@foreach ($last_connections as $key => $student)
								<li class="listUsers__item">
									<div class="smallListUserBox">
										<img class="listUsers__img" src="{{ $student->user->profileimg->url }}" alt="foto de perfil">
										<div class="listUsers__itemInfo">
											<p class="listUsers__title">
												{{ $student->user->firts_name }}
												{{ $student->user->lastname }}
											</p>
											<p class="listUsers__identificationCard">
												{{ $student->user->identification_card }}
											</p>
										</div>
									</div>
									<div class="listUsers__itemDate">
										<p class="listUsers__date">
											{{ \Carbon\Carbon::parse($student->pivot->last_connection)->format('d-m-Y') }}
										</p>
										<p class="listUsers__ago">
										    {{ \Carbon\Carbon::parse($student->pivot->last_connection)->diffForHumans() }}
										</p>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

	<script>
        // Datos pasados desde el controlador
        const viewContentLabels = @json($viewContentLabels); // Fechas
        const viewContentValues = @json($viewContentValues); // Cantidad de registros

        // Configuración del gráfico
		const config = {
		    type: 'line', // Tipo de gráfico (línea)
		    data: {
		        labels: viewContentLabels, // Fechas en el eje X
		        datasets: [{
		            label: 'Contenidos Educativos Vistos', 
		            data: viewContentValues, // Cantidad de registros en el eje Y
		            backgroundColor: 'rgba(75, 192, 192, 0.2)', // Color de fondo
		            borderColor: 'rgba(75, 192, 192, 1)', // Color de la línea
		            borderWidth: 2, // Ancho de la línea
		            fill: true, // Asegúrate de que esté activado el relleno
		            tension: 0.1 // Puedes ajustar la tensión para suavizar la línea
		        }]
		    },
		    options: {
		        responsive: true, // Hacer el gráfico responsivo
		        scales: {
		            y: {
		                beginAtZero: true, // Comenzar el eje Y desde 0
		                title: {
		                    display: true,
		                    text: 'Cantidad de Registros' // Título del eje Y
		                }
		            },
		            x: {
		                title: {
		                    display: true,
		                    text: 'Fecha' // Título del eje X
		                }
		            }
		        }
		    }
		};


        // Renderizar el gráfico
        const ctx = document.getElementById('viewContentLineChart').getContext('2d');
        new Chart(ctx, config);
    </script>

    <script>
	    // Datos para el gráfico de pie
	    const genderLabels = ["Femenino", "Másculino"];
	    const genderValues = [{{$feminine}}, {{$masculine}}]; 

	    // Configuración del gráfico de pie
	    const genderConfig = {
	        type: 'pie', // Tipo de gráfico (tarta)
	        data: {
	            labels: genderLabels, // Etiquetas para los géneros
	            datasets: [{
	                label: 'Distribución por Género',
	                data: genderValues, // Cantidad de registros para cada género
	                backgroundColor: [
	                    'rgba(255, 99, 132, 0.5)', // Rojo
	                    'rgba(54, 162, 235, 0.5)', // Azul
	                    // Agrega más colores si tienes más géneros
	                ],
	                borderColor: [
	                    'rgba(255, 99, 132, 1)', // Rojo
	                    'rgba(54, 162, 235, 1)', // Azul
	                    // Agrega más colores si tienes más géneros
	                ],
	                borderWidth: 1
	            }]
	        },
	        options: {
	            responsive: true, // Hacer el gráfico responsivo
	        }
	    };

	    // Renderizar el gráfico de pie
	    const genderCtx = document.getElementById('statusGender').getContext('2d');
	    new Chart(genderCtx, genderConfig);
	</script>

	<script>
        // Datos para el gráfico de barras
        const typeLabels = @json(array_keys($cantTypesItems)); // Etiquetas para los tipos
        const typeValues = @json(array_values($cantTypesItems)); // Cantidad de cada tipo

        // Configuración del gráfico de barras
		const typeConfig = {
		    type: 'bar', // Tipo de gráfico (barras)
		    data: {
		        labels: typeLabels, // Etiquetas para los tipos
		        datasets: [{
		            label: 'Cantidad de Items por Tipo',
		            data: typeValues, // Cantidad de cada tipo
		            backgroundColor: [
		                '#e4e5e7dd', // Gris
		                'rgba(255, 99, 132, 0.5)', // Rojo
		                'rgba(54, 162, 235, 0.5)', // Azul
		                'rgba(255, 206, 86, 0.5)', // Amarillo
		                'rgba(75, 192, 192, 0.5)', // Verde azulado
		                'rgba(153, 102, 255, 0.5)', // Púrpura
		                'rgba(255, 159, 64, 0.5)', // Naranja

		            ],
		            borderColor: [
		                '#c9cbcf', // Gris
		                'rgba(255, 99, 132, 1)', // Rojo
		                'rgba(54, 162, 235, 1)', // Azul
		                'rgba(255, 206, 86, 1)', // Amarillo
		                'rgba(75, 192, 192, 1)', // Verde azulado
		                'rgba(153, 102, 255, 1)', // Púrpura
		                'rgba(255, 159, 64, 1)', // Naranja
		            ],
		            borderWidth: 1,
		            barThickness: 20, // Ajusta el grosor de las barras
		        }]
		    },
		    options: {
		        responsive: true, // Hacer el gráfico responsivo
		        indexAxis: 'y', // Cambia el eje principal a vertical
		        scales: {
		            x: {
		                beginAtZero: true, // Comenzar el eje X desde 0
		                title: {
		                    display: true,
		                    text: 'Cantidad de Items' // Título del eje X
		                }
		            },
		            y: {
		                title: {
		                    display: true,
		                    text: 'Tipo de Item' // Título del eje Y
		                }
		            }
		        }
		    }
		};


        // Renderizar el gráfico de barras
        const typeCtx = document.getElementById('typeBarChart').getContext('2d');
        new Chart(typeCtx, typeConfig);
    </script>

    <script>
	    // Datos para el gráfico de Polar Area
	    const contentLabels = @json(array_keys($cantTypesContents)); // Etiquetas para los tipos de contenido
	    const contentValues = @json(array_values($cantTypesContents)); // Cantidad de cada tipo de contenido

	    // Configuración del gráfico de Polar Area
	    const contentConfig = {
	        type: 'doughnut', // Tipo de gráfico (Polar Area)
	        data: {
	            labels: contentLabels, // Etiquetas para los tipos de contenido
	            datasets: [{
	                label: 'Distribución de Contenidos',
	                data: contentValues, // Cantidad de cada tipo de contenido
	                backgroundColor: [
	                    'rgba(75, 192, 192, 0.5)', // Verde azulado
	                    'rgba(54, 162, 235, 0.5)', // Azul
	                    'rgba(153, 102, 255, 0.5)', // Púrpura
	                ],
	                borderColor: [
	                    'rgba(75, 192, 192, 1)', // Verde azulado
	                    'rgba(54, 162, 235, 1)', // Azul
	                    'rgba(153, 102, 255, 1)', // Púrpura
	                ],
	                borderWidth: 1
	            }]
	        },
	        options: {
	            responsive: true, // Hacer el gráfico responsivo
	        }
	    };

	    // Renderizar el gráfico de Polar Area
	    const contentCtx = document.getElementById('contentPolarChart').getContext('2d');
	    new Chart(contentCtx, contentConfig);
	</script>
@endsection