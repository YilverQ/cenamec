@extends('teacher.layout')


@section('title', 'Crea un cuestionario')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
@endsection

@section('content')
	<button id="backButton" class="backButton" title="Regresar a la página anterior">
		<i class="fa-solid fa-angles-left"></i>
		Regresar a la página anterior
	</button>
	
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<h2 class="tab__title--centered">
					Crear un nuevo <strong class="color-Text">cuestionario</strong>
				</h2>
				<form class="form__content form__content--big" 
						method="POST" 
						action="{{ route('teacher.question.store') }}"
						enctype="multipart/form-data">

					@csrf @method('POST')
					<h2 class="form__icon">
						<i class="fa-solid fa-clipboard-question"></i>
					</h2>
					<h2 class="form__title form__title__course">
						<strong class="color-Text">
							Módulo: 
						</strong>
							{{ $module->name }}
					</h2>
					<h2 class="form__title">
						Datos para la pregunta: {{ count($module->questionnaires)+1 }}
					</h2>
					
					<div class="grid-one">
						<div class="form__item form__item--big">
							<label for="ask">Pregunta:</label>
							<input class="form__input form__input form__input--big"
									name="ask" 
									required 
									type="text" 
									id="ask" 
									placeholder="¿Cúal es un área de la ciencia naturales?"
									autocomplete="off">
						</div>
					</div>

					<div class="grid-two">
						<div class="form__item">
							<label for="answer" class="label-correct">Respuesta correcta:</label>
							<input class="form__input form__input--input" 
									name="answer" 
									required 
									type="text" 
									id="answer" 
									placeholder="Física"
									autocomplete="off">
						</div>
						<div class="form__item">
							<label for="bad1">1° Respuesta incorrecta:</label>
							<input class="form__input form__input--input" 
									name="bad1" 
									required 
									type="text" 
									id="bad1" 
									placeholder="Artes plásticas"
									autocomplete="off">
						</div>
					</div>

					<div class="grid-two">
						<div class="form__item">
							<label for="bad2">2° Respuesta incorrecta:</label>
							<input class="form__input form__input--input" 
									name="bad2" 
									required 
									type="text" 
									id="bad2" 
									placeholder="Educación física"
									autocomplete="off">
						</div>
						<div class="form__item">
							<label for="bad3">3° Respuesta incorrecta:</label>
							<input class="form__input form__input--input" 
									name="bad3" 
									required 
									type="text" 
									id="bad3" 
									placeholder="Economía"
									autocomplete="off">
						</div>
					</div>
					
					<input class="form__send" 
						type="submit" 
						value="Crear">
				</form>
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
@endsection