@extends($role.'.layout')

@section('title', 'Crea un nuevo módulo educativo')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Crear Cursos">
					<i>/</i>
					<p>Crear Módulo</p>
				</li>
			</a>
		</ul>
	</div>

	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<h2 class="tab__title--centered">
					Crear un nuevo <strong class="color-Text">módulo educativo</strong>
				</h2>
				<form class="form__content form__content--big form__content--autoheight" 
						method="POST" 
						action="{{ route('teacher.module.store', $course) }}"
						enctype="multipart/form-data">

					@csrf @method('POST')
					<h2 class="form__icon">
						<i class="fa-solid fa-book-open-reader"></i>
					</h2>
					<h2 class="form__title">Datos para el módulo nuevo</h2>
					<div class="grid-one">
						<div class="form__item form__item--big">
							<label for="super_name">Nombre: <span class="obligatory" title="Campo Obligatorio">*</span></label>
							<input class="form__input form__input form__input--big" 
									name="super_name" 
									required 
									type="text" 
									id="super_name" 
									placeholder="Física para principiante"
									autocomplete="off">
						</div>
					</div>
					<div class="grid-one">
						<div class="form__item form__item--big">
							<label for="description">Descripción: <span class="obligatory" title="Campo Obligatorio">*</span></label>
							<textarea class="form__textarea form__input form__input--big"
										name="description"
										id="description"
										placeholder="Consolidar el desarrollo formativo del “Diplomado en Ciencia y Calidad Educativa en el Sub - Sistema de Educación Básica”..." 
										required="true"
										rows="7"></textarea>
						</div>
					</div>	
						
					<input class="form__send" 
						type="submit" 
						value="Crear">
				</form>
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
@endsection