@extends($role.'.layout')


@section('title', 'Módulo educativo')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/hidden.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
@endsection



@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: {{ $module->name }}">
					<i>/</i>
					<p>{{ $module->name }}</p>
				</li>
			</a>
		</ul>
	</div>

<section class="containerFormFloat containerFormFloat--small containerFormFloat--hidden" id="boxFormFloat">
	<article class="formFloat">
		<div class="form__content form__content--big form__content--autoheight">
			<p class="formFloat__button" id="xmarkCloseForm">
				<i class="fa-solid fa-xmark"></i>
				Quitar
			</p>

			<h2 class="form__icon">
				<i class="fa-solid fa-file-circle-plus"></i>
			</h2>
			<h2 class="form__title" id="addElement">Agregar un Nuevo Contenido</h2>
			<div class="menuNewContent">
				<div class="menuNewContent__division">
					<h4 class="menuNewContent__subtitle">Contenidos Educativos</h4>
					<div class="boxContentItems">
						<a href="{{ route('teacher.note.create') }}" class="menuNewContent__item">
							<img src="{{ asset('img/course/nota_educativa.png') }}" class="menuNewContent__img">
							<p class="menuNewContent__titleItem">Lección Educativa</p>
							<i class="fa-solid fa-angle-right"></i>
						</a>
					</div>
				</div>
				<div class="menuNewContent__division">
					<h4 class="menuNewContent__subtitle">Evaluaciones</h4>
					<div class="boxContentItems">
						<a href="{{ route('likert.createItem', $module) }}" class="menuNewContent__item menuNewContent__item--second">
							<img src="{{ asset('img/course/satisfaction-scale.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Autoevaluación</p>
							<i class="fa-solid fa-angle-right"></i>
						</a>

						<a href="{{ route('createInteractive', $module) }}" class="menuNewContent__item menuNewContent__item--second">
							<img src="{{ asset('img/course/interaction.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Evaluación Interactiva</p>
							<i class="fa-solid fa-angle-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>

	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article">
			<section class="tab">
				<img class="tab__img" 
						src="{{ asset('img/teacher/module.png') }}" 
						alt="Computadora con un curso online">

				<div class="tab__information">
					<p class="tab__subtitle tab__subtitle--bold">
						Curso: {{ $course->name }}
					</p>
					<h2 class="tab__title tab__title--module">
						Módulo: <strong class="color-Text">{{ $module->name }}</strong>
					</h2>
					<p>
						{{ $module->description }}
					</p>
					<a href="{{ route('teacher.module.edit', $module) }}"
					   class="header__loginItem header__loginItem--contrast">
						Editar Información
					</a>

					<div class="tab__iconsDescription">
						<p class="cardModule__item cardModule__item--note">
							<i class="fa-solid fa-note-sticky"></i>
							Lecciones: {{ $module->contents->where('type', 'Nota')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--autoevaluation">
							<i class="fa-solid fa-image-portrait"></i>
							Autoevaluaciones: {{ $module->contents->where('type', 'Autoevaluación')->count() }}
						</p>
						<p class="cardModule__item cardModule__item--question" id="showCantItems">
							<i class="fa-solid fa-clipboard-question"></i>
							Evaluaciones Interactivas: 
							{{ $module->contents->where('type', 'Evaluación')->count() }}
							<i class="fa-solid fa-caret-down" id="caretIcon"></i>
						</p>
					</div>
					
					<div class="tab__iconsDescription tab__iconsDescription--flex hidden" id="evaluationsDescription">
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/online-test.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Selección Simple: {{ $types_items["Selección Simple"] }}
						</p>
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/text-box.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Respuesta Corta: {{ $types_items["Respuesta Corta"] }}
						</p>
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/vote.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Verdadero o Falso: {{ $types_items["Verdadero o Falso"] }}
						</p>
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/prioritize.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Secuencia: {{ $types_items["Secuencia"] }}
						</p>
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/compare.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Emparejamiento: {{ $types_items["Emparejamiento"] }}
						</p>
						<p class="cardModule__item">
							<img src="{{ asset('img/course/Evaluations/frequency.png') }}" alt="Evaluación interactiva" class="tab__iconsDescription__img">
							Cuadro Comparativo: {{ $types_items["Cuadro Comparativo"] }}
						</p>
					</div>
				</div>
			</section>
		</article>

		<!--Contenidos-->
		<article class="article notesSection">
			<h2 class="tab__title--centered">
				Contenidos del Módulo
			</h2>
			<section class="containerModules">

			@if (empty($contents[0]))
				<h3>No hay Contenidos</h3>
			@else
			<div class="cardNoteContent">
			@foreach ($contents as $key => $content)
				@if ($content->type == "Nota")
					@component('components.Contents.note', ["note" => $content->note, 
															"itemNumber" => $content->orden])
					@endcomponent
				@elseif ($content->type == "Autoevaluación")
					@component('components.Contents.autoevaluation', 
						["autoevaluation" => $content->autoevaluation, 
						"itemNumber" => $content->orden])
					@endcomponent
				@elseif ($content->type == "Evaluación")
					@component('components.Contents.interactive', 
						["interactive" => $content->interactiveEvaluation, 
						"itemNumber" => $content->orden])
					@endcomponent
				@endif
			@endforeach
			</div>
			@endif
			<button class="header__loginItem header__loginItem--contrast" id="windowsCreateContent">
				Crear contenido 
			</button>
			</section>

		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/module/expandContent.js') }}"></script>
	<script type="module" src="{{ asset('js/module/showCantItems.js') }}"></script>
	<script type="module" src="{{ asset('js/course/buttonEllipsis.js') }}"></script>
	<script type="module" src="{{ asset('js/course/addContent_WindowsAlert.js') }}"></script>
@endsection