@extends($role.'.layout')

@section('title', 'Editar Item')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/formInteractive.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a href="{{ route('teacher.module.show', $module) }}">
				<li class="breadcrumbs__item" 
					title="Ir al Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a href="{{ route('interactive', $interactiveEvaluation) }}">
				<li class="breadcrumbs__item" 
					title="Ir al Evaluación Interactiva">
						<i>/</i>
						<p>Evaluación Interactiva</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Editar Item">
					<i>/</i>
					<p>Editar Item: {{ $itemEvaluation->type }}</p>
				</li>
			</a>
		</ul>
	</div>

	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!-- Emparejamiento -->
			@if ($itemEvaluation->type == "Emparejamiento")
		    <form method="POST" action="{{ route('updateMatching', $itemEvaluation) }}" class="form__content form__content--big form__content--autoheight">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">    
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       value="{{$itemEvaluation->statement}}" 
		                       type="text" 
		                       id="statement" 
		                       placeholder="Selecciona un Área de la Ciencias Naturales"
		                       autocomplete="off">
		            </div>
		        </div>

		        <div class="boxPair">
		            <div id="pairContainer">
		            	<!-- Aquí se agregarán dinámicamente los pares -->
		            </div>
		            <div class="grid-two boxPairContainerForms">
		                <input type="hidden" id="couples" name="couples" value="">
		                <div class="form__item">
		                    <label for="elementoA">Elemento-A: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                    <input class="form__input" 
		                           name="elementoA" 
		                           type="text" 
		                           id="elementoA" 
		                           placeholder="Introduce el Elemento A"
		                           autocomplete="off">
		                </div>
		                <i class="fas fa-plus-circle add-pair-icon" 
		                   id="addPairButton" 
		                   title="Agregar Pareja"></i>

		                <div class="form__item">
		                    <label for="elementoB">Elemento-B: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                    <input class="form__input" 
		                           name="elementoB" 
		                           type="text" 
		                           id="elementoB" 
		                           placeholder="Introduce el Elemento B"
		                           autocomplete="off">
		                </div>
		            </div>
		        </div>
		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big" 
		                          name="feedback" 
		                          required 
		                          maxlength="255"
		                          value=""
		                          id="feedback" 
		                          placeholder="Proporciona retroalimentación sobre la pareja de elementos"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>

		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>


			<!-- Secuencia -->
			@elseif ($itemEvaluation->type == "Secuencia")
		    <form method="POST" action="{{ route('updateSequence', $itemEvaluation) }}" class="form__content form__content--big form__content--autoheight">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       type="text" 
		                       value="{{$itemEvaluation->statement}}"
		                       id="statement" 
		                       placeholder="Selecciona un Área de la Ciencias Naturales"
		                       autocomplete="off">
		            </div>
		        </div>

		        <!-- Campo oculto para almacenar las etiquetas -->
		        <div class="grid-one">
		            <input type="hidden" id="sequence" name="sequence" value="">
		            <div class="form__item">
		                <label>Orden de los Elementos:</label>
		                <div class="form__input--bigWord form__input--big form__input--tags" id="tagInputContainer">
		                    <p class="infoSequenceItem infoSequenceItem--alert hidden">Debes agregar al menos dos elementos</p>

		                    <!-- Para mostrar las etiquetas -->
		                    <div id="sequenceList" data-sequences='@json($itemEvaluation->sequences)'></div>

		                    <div class="boxInputSmall boxInputSmall--CompleteWidth">
		                        <input class="form__input form__input--CompleteWidth" 
		                               name="sequenceItem" 
		                               type="text" 
		                               id="sequenceItem" 
		                               placeholder="Fuego"
		                               autocomplete="off">
		                        <i class="fas fa-plus-circle add-word-icon" 
		                                id="addSequenceButton" 
		                                title="Agregar Elemento"></i>
		                    </div>
		                </div>
		            </div>
		        </div>

		        
		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big" 
		                          name="feedback" 
		                          required 
		                          maxlength="255"
		                          id="feedback" 
		                          placeholder="Proporciona retroalimentación"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>
		        <!-- Additional fields here -->
		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>


			<!-- Cuadro Comparativo -->
			@elseif ($itemEvaluation->type == "Cuadro Comparativo")
		    <form method="POST" action="{{ route('updateComparativeTable', $itemEvaluation) }}" class="form__content form__content--big form__content--autoheight">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       type="text"
		                       value="{{$itemEvaluation->statement}}" 
		                       id="statement" 
		                       placeholder="Selecciona un Área de la Ciencias Naturales"
		                       autocomplete="off">
		            </div>
		        </div>
		        
		        <div class="grid-one">
				    <div class="form__item form__item--big">
				        <div class="boxTableCompare">
				            <div class="boxTableCompare__table">
				                <!-- Columna A -->
				                <div class="tableColumn">
				                    <label class="tableColumn__label">Título para la Columna A</label>
				                    <input class="form__input tableColumn__title tableColumn__title--input" 
				                           name="titleColumnA" 
				                           required 
				                           type="text" 
				                           id="titleColumnA" 
				                           placeholder="Elementos A"
				                           autocomplete="off"
				                           value="{{ $itemEvaluation->comparativeTable->columnA }}">
				                    <div class="tableColumn__body" id="tagListA" 
				                         data-existing-words='{!! json_encode($itemEvaluation->comparativeTable->tableItems->where('column', 'A')->pluck('description')) !!}'>
				                        <!-- Las etiquetas se agregarán aquí -->
				                    </div>
				                    <div class="tableColumn__boxByInput">
				                        <input class="form__input form__input--tableCompare" 
				                               id="inputA"
				                               type="text" 
				                               placeholder="Elemento"
				                               autocomplete="off">
				                        <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
				                    </div>
				                </div>

				                <!-- Columna B -->
				                <div class="tableColumn">
				                    <label class="tableColumn__label">Título para la Columna B</label>
				                    <input class="form__input tableColumn__title tableColumn__title--input" 
				                           name="titleColumnB" 
				                           required 
				                           type="text" 
				                           id="titleColumnB" 
				                           placeholder="Elementos B"
				                           autocomplete="off"
				                           value="{{ $itemEvaluation->comparativeTable->columnB }}">
				                    <div class="tableColumn__body" id="tagListB" 
				                         data-existing-words='{!! json_encode($itemEvaluation->comparativeTable->tableItems->where('column', 'B')->pluck('description')) !!}'>
				                        <!-- Las etiquetas se agregarán aquí -->
				                    </div>
				                    <div class="tableColumn__boxByInput">
				                        <input class="form__input" 
				                               id="inputB"
				                               type="text" 
				                               placeholder="Elemento"
				                               autocomplete="off">
				                        <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
				                    </div>
				                </div>

				                <!-- Columna C -->
				                <div class="tableColumn">
				                    <label class="tableColumn__label">Título para la Columna C</label>
				                    <input class="form__input tableColumn__title tableColumn__title--input" 
				                           name="titleColumnC" 
				                           required 
				                           type="text" 
				                           id="titleColumnC" 
				                           placeholder="Elementos C"
				                           autocomplete="off"
				                           value="{{ $itemEvaluation->comparativeTable->columnC }}">
				                    <div class="tableColumn__body" id="tagListC" 
				                         data-existing-words='{!! json_encode($itemEvaluation->comparativeTable->tableItems->where('column', 'C')->pluck('description')) !!}'>
				                        <!-- Las etiquetas se agregarán aquí -->
				                    </div>
				                    <div class="tableColumn__boxByInput">
				                        <input class="form__input" 
				                               id="inputC"
				                               type="text" 
				                               placeholder="Elemento"
				                               autocomplete="off">
				                        <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>




		        <!-- Campo oculto para almacenar los elementos -->
		        <input type='hidden' id='elementsA' name='elementsA' value=''>
		        <input type='hidden' id='elementsB' name='elementsB' value=''>
		        <input type='hidden' id='elementsC' name='elementsC' value=''>

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big"
		                          name="feedback"
		                          required
		                          maxlength="255"
		                          id="feedback"
		                          placeholder="Proporciona retroalimentación sobre la pareja de elementos"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>

		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>


			<!-- Respuesta Corta -->
			@elseif ($itemEvaluation->type == "Respuesta Corta")
		    <form class="form__content form__content--big form__content--autoheight" 
		        action="{{ route('updateShortAnswer', $itemEvaluation) }}" 
		        method="POST">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       type="text" 
		                       id="statement" 
		                       value="{{$itemEvaluation->statement}}"
		                       placeholder="Escribe un Elemento de la Naturaleza"
		                       autocomplete="off">
		            </div>
		        </div>
		        
		        <!-- Campo oculto para almacenar las etiquetas -->
		        <div class="grid-one">
		            <input type="hidden" id="wordAdd" name="wordAdd" value="">
		            <div class="form__item form__item--big">
		                <label>Palabras Aceptadas: </label>
		                <div class="form__input--bigWord form__input--big form__input--tags" id="tagInputContainer">

		                    <!-- Para mostrar las etiquetas -->
		                    <div id="tagList" data-existing-words='{!! json_encode($itemEvaluation->shortAnswers->pluck('word')) !!}'></div>
		                    
		                    <div class="boxInputSmall">
		                        <input class="form__input" 
		                               name="word" 
		                               type="text" 
		                               id="word" 
		                               placeholder="Fuego"
		                               autocomplete="off">
		                        <i class="fas fa-plus-circle add-word-icon add-word-icon--small" 
		                                id="addWordButton" 
		                                title="Agregar Palabra"></i>
		                    </div>
		                </div>
		            </div>
		        </div>


		        
		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big" 
		                          name="feedback" 
		                          required 
		                          maxlength="255"
		                          id="feedback" 
		                          placeholder="Proporciona retroalimentación"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>
		        <!-- Additional fields here -->
		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>

			<!-- Verdadero o Falso -->
			@elseif ($itemEvaluation->type == "Verdadero o Falso")
		    <form class="form__content form__content--big form__content--autoheight" 
		        action="{{ route('updateTrueFalse', $itemEvaluation) }}" 
		        method="POST">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       type="text" 
		                       id="statement"
		                       value="{{$itemEvaluation->statement}}" 
		                       placeholder="Selecciona las parejas de colores"
		                       autocomplete="off">
		            </div>
		        </div>
		        <div class="grid-one">
				    <div class="form__item form__item--big">
				        <label>Selecciona la Respuesta Correcta</label>
				        <ul class="trueOrFalse__content">
				            <li class="trueOrFalse__item trueOrFalse__item--form" data-value="1">
				                <input 
				                    type="radio" 
				                    id="verdadero" 
				                    name="answer" 
				                    value="1" 
				                    required
				                    {{ $itemEvaluation->trueOrFalse->answer == 1 ? 'checked' : '' }}>
				                <label for="verdadero">Verdadero</label>
				            </li>
				            <li class="trueOrFalse__item trueOrFalse__item--form" data-value="0">
				                <input 
				                    type="radio" 
				                    id="falso" 
				                    name="answer" 
				                    value="0" 
				                    required
				                    {{ $itemEvaluation->trueOrFalse->answer == 0 ? 'checked' : '' }}>
				                <label for="falso">Falso</label>
				            </li>
				        </ul>
				    </div>
				</div>


		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big" 
		                          name="feedback" 
		                          required 
		                          maxlength="255"
		                          id="feedback" 
		                          placeholder="Proporciona una retroalimentación"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>
		        <!-- Additional fields here -->
		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>


			<!-- Selección Simple -->
			@elseif ($itemEvaluation->type == "Selección Simple")
		    <form method="POST" action="{{ route('updateSelectionSimple', $itemEvaluation) }}" class="form__content form__content--big form__content--autoheight">
		        @csrf @method('PUT')

		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--big" 
		                       name="statement" 
		                       required 
		                       type="text" 
		                       id="statement" 
		                       value="{{$itemEvaluation->statement}}"
		                       placeholder="Selecciona un Área de la Ciencias Naturales"
		                       autocomplete="off">
		            </div>
		        </div>
		        <div class="grid-two">
		            <div class="form__item">
		                <label for="answer" class="label-correct">Respuesta correcta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--input" 
		                		name="answer"
		                		value="{{ $itemEvaluation->selectionSimple->answer }}"
		                		required
		                		type="text" 
		                		id="answer" 
		                		placeholder="Física" 
		                		autocomplete="off">
		            </div>
		            <div class="form__item">
		                <label for="bad1">1° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--input" 
		                		name="bad1" 
		                		value="{{ $itemEvaluation->selectionSimple->bad1 }}" 
		                		required
		                		type="text" 
		                		id="bad1" 
		                		placeholder="Artes plásticas" 
		                		autocomplete="off">
		            </div>
		        </div>
		        <div class="grid-two">
		            <div class="form__item">
		                <label for="bad2">2° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--input" 
		                		name="bad2" 
		                		value="{{ $itemEvaluation->selectionSimple->bad2 }}"
		                		required
		                		type="text" 
		                		id="bad2" 
		                		placeholder="Educación física" 
		                		autocomplete="off">
		            </div>
		            <div class="form__item">
		                <label for="bad3">3° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <input class="form__input form__input--input" 
		                		name="bad3"
		                		value="{{ $itemEvaluation->selectionSimple->bad3 }}" 
		                		required
		                		type="text" 
		                		id="bad3" 
		                		placeholder="Economía" 
		                		autocomplete="off">
		            </div>
		        </div>

		        
		        <div class="grid-one">
		            <div class="form__item form__item--big">
		                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
		                <textarea class="form__input form__input--big" 
		                          name="feedback" 
		                          required 
		                          maxlength="255"
		                          id="feedback" 
		                          placeholder="Proporciona una retroalimentación"
		                          autocomplete="off">{{$itemEvaluation->feedback}}</textarea>
		            </div>
		        </div>
		        <!-- Additional fields here -->
		        <div class="grid-one">
		            <input class="form__send" type="submit" value="Actualizar Item">
		        </div>
		    </form>
		    @endif
		</article>
	</main>	
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>


	@if ($itemEvaluation->type == "Emparejamiento")
	<script>
	    // Asegúrate de pasar los datos como un JSON válido
	    const existingPairs = {!! json_encode($itemEvaluation->matchings->map(function($matching) {
	        return [
	            'elementA' => $matching->elementA,
	            'elementB' => $matching->elementB
	        ];
	    })) !!};
	</script>
	<script type="module" src="{{ asset('js/form/addElementMatching.js') }}"></script>


	@elseif ($itemEvaluation->type == "Secuencia")
	<script type="module" src="{{ asset('js/form/addElementBySequence.js') }}"></script>

	@elseif ($itemEvaluation->type == "Cuadro Comparativo")
	<script type="module" src="{{ asset('js/form/addElementComparativeTable.js') }}"></script>

	@elseif ($itemEvaluation->type == "Respuesta Corta")
	<script type="module" src="{{ asset('js/form/addWordForm.js') }}"></script>

	@elseif ($itemEvaluation->type == "Verdadero o Falso")
	<script type="module" src="{{ asset('js/form/booleanInputTrueFalse.js') }}"></script>

	@elseif ($itemEvaluation->type == "Selección Simple")

    @endif
@endsection