@extends($role.'.layout')

@section('title', 'Configurar una Evaluación Interactiva')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/formInteractive.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a href="{{ route('teacher.module.show', $module) }}">
				<li class="breadcrumbs__item" 
					title="Ir al Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Evaluación Interactiva">
					<i>/</i>
					<p>Evaluación Interactiva</p>
				</li>
			</a>
		</ul>
	</div>

<section class="containerFormFloat containerFormFloat--small containerFormFloat--hidden" id="boxFormFloat">
	<article class="formFloat">
		<div class="form__content form__content--big form__content--bigHeightScroll form__content--interactive">
			<p class="formFloat__button" id="xmarkCloseForm">
				<i class="fa-solid fa-xmark"></i>
				Quitar
			</p>

			<h2 class="form__icon" id="formIcon">
			    <i class="fa-solid fa-file-circle-plus"></i>
			</h2>
			<h2 class="form__title" id="addElement">Agregar un Nuevo Item</h2>
			<div class="menuNewContent">
				<div class="menuNewContent__division">
					<h4 class="menuNewContent__subtitle">Batería de Evaluaciones</h4>
					<div class="boxContentItems">
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormMatching">
							<img src="{{ asset('img/course/Evaluations/compare.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Emparejamiento</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormSecuence">
							<img src="{{ asset('img/course/Evaluations/prioritize.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Secuencia	</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormComparativeTable">
							<img src="{{ asset('img/course/Evaluations/frequency.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Cuadro Comparativo</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormShortAnswer">
							<img src="{{ asset('img/course/Evaluations/text-box.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Respuesta Corta</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormTrueFalse">
							<img src="{{ asset('img/course/Evaluations/vote.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Verdadero o Falso</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
						<div class="menuNewContent__item menuNewContent__item--third" id="openFormSelectionSimple">
							<img src="{{ asset('img/course/Evaluations/online-test.png') }}" class="menuNewContent__img menuNewContent__img--second">
							<p class="menuNewContent__titleItem">Selección Simple</p>
							<i class="fa-solid fa-angle-right"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="boxEvaluationForms hidden">
				@component('components.evaluationForms.formCreate', 
							["interactiveEvaluation"=> $interactiveEvaluation])
				@endcomponent
			</div>
		</div>
	</article>
</section>
	
	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<h2 class="tab__title--centered">
					Evaluación <strong class="color-Text"> Interactiva</strong>
				</h2>
				<div class="boxEvaluationTeach">
					<div class="boxEvaluationTeach__description">
					    <h2>Configuración de la Evaluación Interactiva</h2>
					    <p class="boxEvaluationTeach__text">Por favor, ingrese los items que los estudiantes utilizarán para participar en la evaluación interactiva. Esto les permitirá poner a prueba sus conocimientos adquiridos de manera efectiva y fomentar un aprendizaje activo.</p>
					</div>

					<div class="buttonAddItem" title="Agregar elemento nuevo" id="windowsCreateContent">
						<i class="fa-solid fa-circle-plus"></i>
						<p>Crear item</p>
					</div>

					<div class="boxInteractive">
						@foreach ($itemEvaluations as $key => $itemEvaluation)
							@component('components.cardsItemInteractive', 
										["itemEvaluation" => $itemEvaluation])
							@endcomponent
						@endforeach
					</div>
				</div>
			</div>
		</article>
	</main>
@endsection
@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
	<script type="module" src="{{ asset('js/course/feedbackDisplay.js') }}"></script>
	<script type="module" src="{{ asset('js/course/interactiveDetailsBox.js') }}"></script>
	<script type="module" src="{{ asset('js/course/addContent_WindowsAlert.js') }}"></script>
	<script type="module" src="{{ asset('js/form/evaluationFormCreate.js') }}"></script>
	<script type="module" src="{{ asset('js/form/booleanInputTrueFalse.js') }}"></script>
	<script type="module" src="{{ asset('js/form/addWordForm.js') }}"></script>
	<script type="module" src="{{ asset('js/form/addElementBySequence.js') }}"></script>
	<script type="module" src="{{ asset('js/form/addElementMatching.js') }}"></script>
	<script type="module" src="{{ asset('js/form/addElementComparativeTable.js') }}"></script>
@endsection