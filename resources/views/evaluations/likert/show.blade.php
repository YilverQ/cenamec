@extends($role.'.layout')

@section('title', 'Escala de Likert')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/form.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/components/course.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/administrator/list.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		<ul class="breadcrumbs__boxList">
			<li class="breadcrumbs__item breadcrumbs__item--base" 
				id="backButton" 
				title="Regresar a la página anterior">
				<i class="fa-solid fa-caret-left"></i>
			</li>
			<a href="{{ route('teacher.course.index') }}">
				<li class="breadcrumbs__item" 
					title="Ir a Mis Cursos">
						<i>/ <i class="fa-solid fa-book-open-reader"></i></i>
						<p>Mis Cursos</p>
				</li>
			</a>
			<a href="{{ route('teacher.course.show', $course) }}">
				<li class="breadcrumbs__item" 
					title="Ir al curso: {{ $course->name }}">
						<i>/</i>
						<p>{{ $course->name }}</p>
				</li>
			</a>
			<a href="{{ route('teacher.module.show', $module) }}">
				<li class="breadcrumbs__item" 
					title="Ir al Módulo: {{ $module->name }}">
						<i>/</i>
						<p>{{ $module->name }}</p>
				</li>
			</a>
			<a>
				<li class="breadcrumbs__item breadcrumbs__item--active" 
					title="Página Actual: Autoevaluación">
					<i>/</i>
					<p>Autoevaluación</p>
				</li>
			</a>
		</ul>
	</div>

<section class="containerFormFloat containerFormFloat--small containerFormFloat--hidden" id="boxFormFloat">
	<article class="formFloat">
		<form class="form__content form__content--likert form__content--autoheight" 
				method="POST" 
				action="{{ route('likert.addItem', $autoevaluation) }}">
				@csrf @method('POST')

				<p class="formFloat__button" id="xmarkCloseForm">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-clipboard-question"></i>
				</h2>
				<h2 class="form__title" id="addElement">Agregar item</h2>
				<div class="grid-one">
					<div class="form__item form__item--big">
						<label for="affirmation">Afirmación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input form__input--big" 
										name="affirmation" 
										required 
										type="text" 
										id="affirmation" 
										placeholder="Me gustó..."
										autocomplete="off">
					</div>
				</div>
				<input class="form__send" type="submit" value="Agregar item">
		</form>
	</article>
</section>

<section class="containerFormFloat containerFormFloat--small containerFormFloat--hidden" id="boxEditFormFloat">
    <article class="formFloat">
        <form class="form__content form__content--likert form__content--autoheight" 
              method="POST" 
              action="{{ route('likert.updateItem', $autoevaluation) }}">
            @csrf @method('PUT')

            <p class="formFloat__button" id="xmarkCloseEditForm">
                <i class="fa-solid fa-xmark"></i>
                Quitar
            </p>

            <h2 class="form__icon">
                <i class="fa-solid fa-clipboard-question"></i>
            </h2>
            <h2 class="form__title" id="editElement">Editar item</h2>
            <div class="grid-one">
                <div class="form__item form__item--big">
                    <label for="editAffirmation">Afirmación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                    <input class="form__input form__input--big" 
                           name="affirmation" 
                           required 
                           type="text" 
                           id="editAffirmation"
                           placeholder="Me gustó..."
                           autocomplete="off">
                </div>
            </div>
            <input type="hidden" name="editItemId" id="editItemId">
            <input class="form__send" type="submit" value="Actualizar item">
        </form>
    </article>
</section>


	<!--Contenedor-->
	<main class="container">
		<!--Information-->
		<article class="article" id="profile">
			<!--Information-->
			<div class="form">
				<h2 class="tab__title--centered">
					Autoevaluación:<strong class="color-Text"> Escala de Likert</strong>
				</h2>
				<div class="boxEvaluationTeach">
					<div class="boxEvaluationTeach__description">
					    <h2>Configuración de la Autoevaluación</h2>
					    <p class="boxEvaluationTeach__text">Las autoevaluaciones se basan en una escala de Likert, que permite medir el grado de acuerdo o desacuerdo de los estudiantes con respecto a cada afirmación. La escala incluye cinco opciones: <b>"Totalmente en desacuerdo", "En desacuerdo", "Neutral", "De acuerdo" y "Totalmente de acuerdo"</b>. Los estudiantes seleccionarán la opción que mejor represente su percepción sobre cada afirmación, proporcionando una retroalimentación clara y precisa sobre su experiencia en el curso.</p>
					</div>

					<div class="buttonAddItem" title="Crear elemento nuevo" id="buttonOpenForm">
						<i class="fa-solid fa-circle-plus"></i>
						<p>Crear item</p>
					</div>
					<div class="boxTableLikert">
						<table class="tableLikert">
						    <thead>
						        <tr>
						            <th>Afirmaciones</th>
						            <th>Totalmente de acuerdo</th>
						            <th>De acuerdo</th>
						            <th>Neutral</th>
						            <th>En desacuerdo</th>
						            <th>Totalmente en desacuerdo</th>
						            <th>Acciones</th>
						        </tr>
						    </thead>
						    <tbody>
						    	@foreach ($likerts as $key => $item)
									<tr>
							            <td>{{ $item->affirmation }}</td>
							            <td><input type="radio" name="q1" value="1" disabled></td>
							            <td><input type="radio" name="q1" value="2" disabled></td>
							            <td><input type="radio" name="q1" value="3" disabled></td>
							            <td><input type="radio" name="q1" value="4" disabled></td>
							            <td><input type="radio" name="q1" value="5" disabled></td>
							            <td class="actionsTable">
							            	<ul class="actionsTable__ul">
							            		<li class="actionsTable__item editItem" data-id="{{ $item->id }}">
								                    <i class="fa-solid fa-pen-to-square"></i>
								                    <p>Editar</p>
								                </li>
								                <form 
							                    	action="{{ route('likert.destroyItem', $item) }}" 
							                    	method="POST" 
							                    	class="form__delete">
							                        
							                        @csrf
							                        @method('DELETE')
							                        <button type="submit" class="actionsTable__item actionsTable__item--deleted">
								            			<i class="fa-solid fa-trash"></i>
								            			<p>Eliminar</p>
							                        </button>                
							                    </form> 
							            	</ul>
							            </td>
							        </tr>
								@endforeach
						    </tbody>
						</table>
					</div>
					
				</div>
			</div>
		</article>
	</main>
@endsection

@section('scripts')
	<script type="module" src="{{ asset('js/components/backButton.js') }}"></script>
	<script type="module" src="{{ asset('js/form/formEvaluation.js') }}"></script>
	<script type="module" src="{{ asset('js/form/editFormLikert.js') }}"></script>
@endsection