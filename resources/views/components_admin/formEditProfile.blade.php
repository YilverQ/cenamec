<section class="containerFormFloat containerFormFloat--small hidden" id="containerFormFloat">
	<article class="formFloat">
		<form class="form__content form__content--big" 
				method="POST" 
				id="formProfile" 
				action="{{ route('administrator.update', $user) }}">
				@csrf @method('PUT')

				<p class="formFloat__button" id="formFloat__button">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-chalkboard-user"></i>
				</h2>
				<h2 class="form__title">Datos Personales</h2>
				<div class="grid-two">
					<div class="form__item">
						<label for="firts_name">Nombres:</label>
						<input class="form__input" 
								name="firts_name" 
								required 
								type="text" 
								id="firts_name" 
								value="{{ $user->firts_name }}"
								placeholder="Vanessa"
								autocomplete="off">
					</div>
					<div class="form__item">
						<label for="lastname">Apellidos:</label>
						<input class="form__input" 
								name="lastname" 
								required 
								type="text" 
								id="lastname" 
								value="{{ $user->lastname }}" 
								placeholder="Longa"
								autocomplete="off">
					</div>
				</div>

					<div class="grid-two">
						<div class="form__item">
							<label for="gender">Genero:</label>
							<div class="boxLabels">
								@if($user->gender == "Femenino")
								<label class="inputRadio inputRadio--checked">
									<input type="radio" name="gender" value="Femenino" checked>
									<p>Femenino</p>
								</label>
								<label class="inputRadio">
									<input type="radio" name="gender" value="Masculino">
									<p>Masculino</p>
								</label>
								@else
								<label class="inputRadio">
									<input type="radio" name="gender" value="Femenino">
									<p>Femenino</p>
								</label>
								<label class="inputRadio inputRadio--checked">
									<input type="radio" name="gender" value="Masculino" checked>
									<p>Masculino</p>
								</label>
								@endif
							</div>
						</div>
						<div class="form__item">
							<label for="birthdate">Fecha de Nacimiento:</label>
							<input class="form__input" 
									name="birthdate" 
									required 
									value="{{ $user->birthdate }}" 
									type="date" 
									id="birthdate" 
									autocomplete="off">
						</div>
					</div>

					<div class="grid-two">
						<div class="form__item">
							<label for="identification_card">Documento de Identidad:</label>
							<div class="boxNacionality">
								<select class="form__input form__input--nacionality" 
										name="nacionality" 
										id="nacionality">
									<option value="{{ $user->nacionality }}" selected>{{ $user->nacionality }}</option>
									<option value="V">V-</option>
									<option value="E">E-</option>
								</select>
								<input class="form__input" 
										name="identification_card" 
										required 
										value="{{ $user->identification_card }}" 
										type="text" 
										id="identification_card" 
										placeholder="20111333"
										minlength="6"
										maxlength="10"
										autocomplete="off">
							</div>
						</div>
						<div class="form__item">
							<label for="number_phone">Número de teléfono:</label>
							<input class="form__input" 
									name="number_phone" 
									required 
									type="text" 
									id="number_phone"
									value="{{ $user->number_phone }}" 
									placeholder="04120001234"
									minlength="11"
									maxlength="11"
									autocomplete="off">
						</div>
					</div>

				<!-- Datos de Vivienda -->
				<div class="grid-two">
					<div class="form__item form__item--ubication">
						<label for="state">Estado:</label>
						<select class="form__input" 
									name="state" 
									id="state">
							<option value="{{ $user->parishe->municipalitie->state->id }}" disabled selected>
								{{ $user->parishe->municipalitie->state->name }}
							</option>
							@foreach($states as $key => $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form__item">
						<label>Municipio:</label>
						<select class="form__input" 
								name="municipalitie" 
								id="municipalitie"
								required>
							<option value="{{ $user->parishe->municipalitie->id }}" selected>
								{{ $user->parishe->municipalitie->name }}
							</option>
						</select>
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item">
						<label>Parroquia:</label>
						<select class="form__input"
								name="parishe"
								id="parishe"
								required>
							<option value="{{ $user->parishe->id }}" selected>
								{{ $user->parishe->name }}
							</option>
						</select>
					</div>
					<div class="form__item">
						<label for="email">Correo Eléctronico:</label>
						<input class="form__input" 
								name="email" 
								required 
								value="{{ $user->email }}" 
								type="email" 
								id="email" 
								placeholder="vanessa@gmail.com"
								autocomplete="off">
					</div>
				</div>
				<input class="form__send" type="submit" value="Actualizar mis datos">
		</form>
			
	</article>
</section>