<section class="containerFormFloat containerFormFloat--small hidden" id="containerFormFloat--AcademicEdit">
	<article class="formFloat">
		<form class="form__content form__content--big" 
				method="POST" 
				action="{{ route('administrator.updateAcademic', $user) }}">
				@csrf @method('PUT')
				<input type="hidden" name="idAcademic" id="idAcademic" value="">

				<p class="formFloat__button" id="formFloat__button-academicEdit">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-user-graduate"></i>
				</h2>
				<h2 class="form__title">Editar datos acádemicos</h2>
				<div class="grid-two">
					<div class="form__item form__item--ubication">
						<label for="academicLevelEdit">Nivel acádemico:</label>
						<select class="form__input" 
									name="academicLevelEdit" 
									id="academicLevelEdit">
						</select>
					</div>
					<div class="form__item select-search select-search--Edit">
						<label>Institución:</label>
				    	<input id="hiddenIntitutionEdit" type="hidden" name="intitutionEdit">
						<div class="select-btn">
					    	<span class="spanSelectEdit">Seleccionar una institutición</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item select-search select-search--Edit">
						<label>Carrera:</label>
				    	<input id="hiddenCarrerEdit" type="hidden" name="careerEdit">
						<div class="select-btn">
					    	<span class="spanSelectEdit">Seleccionar la carrera</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
					<div class="form__item">
						<label for="sectorEdit">Ámbito:</label>
						<select class="form__input"
								name="sectorEdit"
								id="sectorEdit"
								required>
						</select>
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item">
						<label for="areaEdit">Área:</label>
						<select class="form__input"
								name="areaEdit"
								id="areaEdit"
								required>
						</select>
					</div>
					<div class="form__item">
						<label for="academic_title">Título obtenido:</label>
						<input class="form__input" 
										name="academic_titleEdit" 
										required 
										type="text" 
										id="academic_titleEdit" 
										placeholder="Informática para la Gestión Social"
										autocomplete="off">
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item">
						<label>Año de ingreso:</label>
						<input class="form__input" 
										name="year_entryEdit" 
										required 
										type="number" 
										id="year_entryEdit" 
										placeholder="2018"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
					<div class="form__item">
						<label>Año de egreso (Opcional):</label>
						<input class="form__input" 
										name="year_exitEdit" 
										type="number" 
										id="year_exitEdit" 
										placeholder="{{date('Y')}}"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
				</div>
				<input class="form__send" type="submit" value="Editar datos">
		</form>
			
	</article>
</section>