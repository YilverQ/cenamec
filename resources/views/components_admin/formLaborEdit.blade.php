<section class="containerFormFloat containerFormFloat--small hidden" id="containerFormFloat--LaboralEdit">
	<article class="formFloat">
		<form class="form__content form__content--big" 
				method="POST" 
				action="{{ route('administrator.updateLaboral', $user) }}">
				@csrf @method('PUT')
				<input type="hidden" name="idLaboral" id="idLaboral" value="">

				<p class="formFloat__button" id="formFloat__button-laboralEdit">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-briefcase"></i>
				</h2>
				<h2 class="form__title">Editar datos laborales</h2>

				<div class="grid-two">
					<div class="form__item">
						<label for="name_institutionEdit">Nombre de la institución:</label>
						<input class="form__input" 
								name="name_institutionEdit" 
								required 
								type="text" 
								id="name_institutionEdit" 
								placeholder="Centro Nacional para el Mejoramiento de la Enseñanza de la Ciencia"
								autocomplete="off">
					</div>
					<div class="form__item select-search-laboral">
						<label>Tipo de institución:</label>
				    	<input id="hiddenTypeIntitutionEdit" type="hidden" name="type_institutionEdit">
						<div class="select-btn">
					    	<span class="spanSelect" id="typeEdit">Seleccionar un tipo</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
				</div>

				<div class="grid-two">
					<div class="form__item select-search-laboral">
						<label>Área laboral:</label>
				    	<input id="hiddenAreaIntitutionEdit" type="hidden" name="area_institutionEdit">
						<div class="select-btn">
					    	<span class="spanSelect" id="areaLaboralEdit">Seleccionar un área</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
					<div class="form__item">
						<label for="positionEdit">Cargo dentro de la institución:</label>
						<input class="form__input" 
								name="positionEdit" 
								required 
								type="text" 
								id="positionEdit" 
								placeholder="Investigador"
								autocomplete="off">
					</div>
				</div>

				<div class="grid-two">
					<div class="form__item">
						<label>Año de ingreso:</label>
						<input class="form__input" 
										name="year_entryLaboralEdit" 
										required 
										type="number" 
										id="year_entryLaboralEdit" 
										placeholder="2018"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
					<div class="form__item">
						<label>Año de egreso (Opcional):</label>
						<input class="form__input" 
										name="year_exitLaboralEdit" 
										type="number" 
										id="year_exitLaboralEdit" 
										placeholder="{{date('Y')}}"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
				</div>
				
				<input class="form__send" type="submit" value="Editar datos">
		</form>
			
	</article>
</section>