<section class="containerFormFloat containerFormFloat--small hidden" id="containerFormFloat--AcademicAdd">
	<article class="formFloat">
		<form class="form__content form__content--big" 
				method="POST" 
				action="{{ route('user.addAcademic') }}">
				@csrf @method('POST')

				<p class="formFloat__button" id="formFloat__button-academic">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-user-graduate"></i>
				</h2>
				<h2 class="form__title">Agregar datos acádemicos</h2>
				<div class="grid-two">
					<div class="form__item form__item--ubication">
						<label for="academicLevel">Nivel acádemico: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<select class="form__input" 
									name="academicLevel" 
									id="academicLevel">
						</select>
					</div>
					<div class="form__item select-search">
						<label>Institución: <span class="obligatory" title="Campo Obligatorio">*</span></label>
				    	<input id="hiddenIntitution" type="hidden" name="intitution">
						<div class="select-btn">
					    	<span class="spanSelect">Seleccionar una institutición</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item select-search">
						<label>Carrera: <span class="obligatory" title="Campo Obligatorio">*</span></label>
				    	<input id="hiddenCarrer" type="hidden" name="career">
						<div class="select-btn">
					    	<span class="spanSelect">Seleccionar la carrera</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
					<div class="form__item">
						<label for="sector">Ámbito: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<select class="form__input"
								name="sector"
								id="sector"
								required>
						</select>
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item">
						<label for="area">Área: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<select class="form__input"
								name="area"
								id="area"
								required>
						</select>
					</div>
					<div class="form__item">
						<label for="academic_title">Título obtenido: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input" 
										name="academic_title" 
										required 
										type="text" 
										id="academic_title"
										pattern="^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$"
										oninput="setCustomValidity('')"
										oninvalid="setCustomValidity('Por favor, ingresa solo letras y espacios.')" 
										placeholder="Informática para la Gestión Social"
										autocomplete="off">
					</div>
				</div>
				<div class="grid-two">
					<div class="form__item">
						<label>Año de ingreso: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input" 
										name="year_entry" 
										required 
										type="number" 
										id="year_entry" 
										placeholder="2018"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
					<div class="form__item">
						<label>Año de egreso (Opcional):</label>
						<input class="form__input" 
										name="year_exit" 
										type="number" 
										id="year_exit" 
										placeholder="{{date('Y')}}"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
				</div>
				<input class="form__send" type="submit" value="Agregar datos">
		</form>
			
	</article>
</section>