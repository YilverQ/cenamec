<div id="containerImgOptions" class="containerImgOptions containerImgOptions--hidden">
	<div class="imgOptions">
		<p class="imgOptions__closeButton">
			<i class="fa-solid fa-xmark"></i>
			Quitar
		</p>
		<form class="imgOptions__form"
				method="POST" 
				action="{{ route('user.img') }}">
				@csrf @method('PUT')

			<div class="grid-one grid-one--justify">
				<div class="form_checkbox">
					<h3 class="form__subtitle">Selecciona una foto de perfil</h3>
					
					@foreach($profileimgs as $key => $item)
					<div class="boxCheckbox">
						<input class="input__checkbox" 
								name="picture" 
								type="radio"
								value="{{ $item->id }}" 
								id="{{ $item->name }}" 
								autocomplete="off">
						<label for="{{ $item->name }}" class="labelTitle">
							<img class="imgOptions__img" 
								src="{{ $item->url }}" 
								alt="Imagen de perfil">
							<h4 class="labelTitle_text">{{ $item->name }}</h4>
						</label>
					</div>
					@endforeach

				</div>
			</div>
			<div class="buttonFixed">
				<input class="form__send" type="submit" value="¡Actualizar Foto!">
			</div>
		</form>
	</div>
</div>