<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script>
    const example_image_upload_handler = (blobInfo, progress) => new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true; 
        xhr.open('POST', '/upload-images');

        var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        xhr.setRequestHeader('X-CSRF-TOKEN', token);

        xhr.upload.onprogress = (e) => {
            progress(e.loaded / e.total * 100);
        };

        xhr.onload = () => {
            if (xhr.status === 403) {
                reject({ message: 'HTTP Error: ' + xhr.status, remove: true });
                return;
            }

            if (xhr.status < 200 || xhr.status >= 300) {
                reject('HTTP Error: ' + xhr.status);
                return;
            }

            const json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location !== 'string') {
                reject('Invalid JSON: ' + xhr.responseText);
                return;
            }

            resolve(json.location);
        };

        xhr.onerror = () => {
            reject('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
        };

        const formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
    });

    tinymce.init({
        selector: 'textarea#myeditorinstance',
        language: 'es',
        plugins: 'code table lists image media link preview fullscreen',
        toolbar: 'undo redo | blocks | bold italic | alignleft aligncenter alignright alignjustify | indent outdent | bullist numlist | table | image media link | code preview | fullscreen',
        height: 300,
        min_height: 300,
        resize: false,
        automatic_uploads: true,
        images_upload_credentials: true,
        media_live_embeds: true,
        relative_urls: false,
        remove_script_host: false,
        convert_urls: false,
        statusbar: false,
        images_upload_handler: example_image_upload_handler,
        license_key: 'gpl',
        promotion: false
    });
</script>
