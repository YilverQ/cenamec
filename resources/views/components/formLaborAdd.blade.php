<section class="containerFormFloat containerFormFloat--small hidden" id="containerFormFloat--LaboralAdd">
	<article class="formFloat">
		<form class="form__content form__content--big" 
				method="POST" 
				action="{{ route('user.addLaboral') }}">
				@csrf @method('POST')

				<p class="formFloat__button" id="formFloat__button-laboral">
					<i class="fa-solid fa-xmark"></i>
					Quitar
				</p>

				<h2 class="form__icon">
					<i class="fa-solid fa-briefcase"></i>
				</h2>
				<h2 class="form__title">Agregar datos laborales</h2>

				<div class="grid-two">
					<div class="form__item">
						<label for="name_intitution">Nombre de la institución: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input" 
								name="name_intitution" 
								required 
								type="text" 
								id="name_intitution" 
								pattern="^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$"
								oninput="setCustomValidity('')"
								oninvalid="setCustomValidity('Por favor, ingresa solo letras y espacios.')" 
								placeholder="Centro Nacional para el Mejoramiento de la Enseñanza de la Ciencia"
								autocomplete="off">
					</div>
					<div class="form__item select-search-laboral">
						<label>Tipo de institución: <span class="obligatory" title="Campo Obligatorio">*</span></label>
				    	<input id="hiddenTypeIntitution" type="hidden" name="type_institution">
						<div class="select-btn">
					    	<span class="spanSelect">Seleccionar un tipo</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
				</div>

				<div class="grid-two">
					<div class="form__item select-search-laboral">
						<label>Área laboral: <span class="obligatory" title="Campo Obligatorio">*</span></label>
				    	<input id="hiddenAreaIntitution" type="hidden" name="area_institution">
						<div class="select-btn">
					    	<span class="spanSelect">Seleccionar un área</span>
					    	<i class="fa-solid fa-angle-down"></i>
					    </div>
					    <div class="content">
					    	<div class="search">
					        	<i class="fa-solid fa-magnifying-glass"></i>
					        	<input  spellcheck="false" 
					        			type="text"
					        			autocomplete="off" 
					        			class="searchInput" 
					        			placeholder="Search">
					    	</div>
					    	<ul class="options"></ul>
					    </div>
					</div>
					<div class="form__item">
						<label for="position">Cargo dentro de la institución: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input" 
								name="position" 
								required 
								pattern="^[A-Za-zÁÉÍÓÚáéíóúÑñ0-9\s]+$"
								oninput="setCustomValidity('')"
								oninvalid="setCustomValidity('Por favor, ingresa solo letras, números y espacios.')"
								type="text" 
								id="position" 
								placeholder="Investigador"
								autocomplete="off">
					</div>
				</div>

				<div class="grid-two">
					<div class="form__item">
						<label>Año de ingreso: <span class="obligatory" title="Campo Obligatorio">*</span></label>
						<input class="form__input" 
										name="year_entryLaboral" 
										required 
										type="number" 
										id="year_entryLaboral" 
										placeholder="2018"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
					<div class="form__item">
						<label>Año de egreso (Opcional):</label>
						<input class="form__input" 
										name="year_exitLaboral" 
										type="number" 
										id="year_exitLaboral" 
										placeholder="{{date('Y')}}"
										autocomplete="off"
										min="1920"
										max="{{date('Y')}}">
					</div>
				</div>
				
				<input class="form__send" type="submit" value="Agregar datos">
		</form>
			
	</article>
</section>