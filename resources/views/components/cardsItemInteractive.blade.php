@if ($itemEvaluation->type == "Verdadero o Falso")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/vote.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Verdadero o Falso</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{ $itemEvaluation->orden }}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>

		<div class="interactiveDetailsBox hidden">
			<h4 class="titleEvaluation">{{ $itemEvaluation->statement }}</h4>
			<div class="trueOrFalseBox">
				<ul class="trueOrFalse__content">
					@if($itemEvaluation->trueOrFalse->answer == true)
						<li class="trueOrFalse__item trueOrFalse__item--correct">Verdadero</li>
						<li class="trueOrFalse__item">Falso</li>
					@else($itemEvaluation->trueOrFalse->answer == true)
						<li class="trueOrFalse__item">Verdadero</li>
						<li class="trueOrFalse__item trueOrFalse__item--correct">Falso</li>
					@endif
				</ul>
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{ $itemEvaluation->feedback }}
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>
				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>

@elseif ($itemEvaluation->type == "Selección Simple")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/online-test.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Selección Simple</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{$itemEvaluation->orden}}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>

		<div class="interactiveDetailsBox hidden">
			<h4 class="titleEvaluation">{{$itemEvaluation->statement}}</h4>
			<div class="cardQuestion__contentAnswer">
				<p class="cardQuestion__answer cardQuestion__answer--true">{{$itemEvaluation->selectionSimple->answer}}</p>
				<p class="cardQuestion__answer">{{$itemEvaluation->selectionSimple->bad1}}</p>
				<p class="cardQuestion__answer">{{$itemEvaluation->selectionSimple->bad2}}</p>
				<p class="cardQuestion__answer">{{$itemEvaluation->selectionSimple->bad3}}</p>
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{$itemEvaluation->feedback}}
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>
				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>

@elseif ($itemEvaluation->type == "Respuesta Corta")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/text-box.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Respuesta Corta</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{$itemEvaluation->orden}}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>

		<div class="interactiveDetailsBox hidden">
			<h4 class="titleEvaluation">{{$itemEvaluation->statement}}</h4>
			<div class="shortAnswer__box">
				<p class="shortAnswer__text">Palabras Aceptadas:</p>
				<ul class="shortAnswer__boxItems">
					@foreach ($itemEvaluation->shortAnswers as $key => $word)
						<li class="shortAnswer__item">
							{{ $word->word }}
						</li>	
					@endforeach
				</ul>
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{$itemEvaluation->feedback}}
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>
				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>

@elseif ($itemEvaluation->type == "Cuadro Comparativo")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/frequency.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Cuadro Comparativo</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{$itemEvaluation->orden}}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>

		<div class="interactiveDetailsBox hidden">
			<div class="boxTableCompare">
				<p>{{$itemEvaluation->statement}}</p>
				<div class="boxTableCompare__table">
					<div class="tableColumn">
						<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnA}}</h3>
						<div class="tableColumn__body">
							@foreach ($itemEvaluation->comparativeTable->tableItems as $key => $word)
								@if($word->column == "A")
								<div class="tableColumn__item">
									<p class="tableColumn__text">
										{{ $word->description }}
									</p>
								</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tableColumn">
						<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnB}}</h3>
						<div class="tableColumn__body">
							@foreach ($itemEvaluation->comparativeTable->tableItems as $key => $word)
								@if($word->column == "B")
								<div class="tableColumn__item">
									<p class="tableColumn__text">
										{{ $word->description }}
									</p>
								</div>
								@endif
							@endforeach
						</div>
					</div>
					<div class="tableColumn">
						<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnC}}</h3>
						<div class="tableColumn__body">
							@foreach ($itemEvaluation->comparativeTable->tableItems as $key => $word)
								@if($word->column == "C")
								<div class="tableColumn__item">
									<p class="tableColumn__text">
										{{ $word->description }}
									</p>
								</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{$itemEvaluation->feedback}}
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>
				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>

@elseif ($itemEvaluation->type == "Secuencia")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/prioritize.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Secuencia</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{$itemEvaluation->orden}}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>

		<div class="interactiveDetailsBox hidden">
			<div class="boxSequence">
				<p class="boxCompare__enunciado">{{$itemEvaluation->statement}}</p>
				@foreach ($itemEvaluation->sequences as $key => $sequence)
					<div class="boxSequence__element">
						<p class="boxSequence__textNumber">{{$sequence->orden}}</p>
						<p class="boxSequence__textElement">{{$sequence->description}}</p>
						<i class="fa-solid fa-grip-vertical"></i>
					</div>
				@endforeach
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{$itemEvaluation->feedback}}	
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>
				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>

@elseif ($itemEvaluation->type == "Emparejamiento")
	<div class="interactiveTest">
		<div class="interactiveTest__card">
			<div class="interactiveTest__logo">
				<img src="{{ asset('img/course/Evaluations/compare.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
				<h3 class="interactiveTest__title">Emparejamiento</h3>
			</div>
			<div class="interactiveTest__itemNum">
				<p>Item {{$itemEvaluation->orden}}</p>
				<i class="interactiveTest__i fa-solid fa-caret-down"></i>
			</div>
		</div>
		
		<div class="interactiveDetailsBox hidden">
			<p class="boxCompare__enunciado">{{ $itemEvaluation->statement }}</p>
			<div class="boxCompare">
			    <div class="boxCompare__column boxCompare__column--left">
			    	<p>Columna A</p>
			    	@foreach ($itemEvaluation->matchings as $key => $pair)
						<div class="boxCompare__item">
				            <p class="boxCompare__titleItem">{{ $pair->elementA }}</p>
				        </div>
					@endforeach
			    </div>

			    <div class="boxCompare__column boxCompare__column--right">
			    	<p>Columna B</p>
			        @foreach ($itemEvaluation->matchings as $key => $pair)
						<div class="boxCompare__item">
				            <p class="boxCompare__titleItem">{{ $pair->elementB }}</p>
				        </div>
					@endforeach
			    </div>
			</div>

			<div class="boxFeedback">
				<div class="boxFeedBackHeader">
					<div class="boxFeedback__boxTitle">
						<i class="boxFeedback__icon fa-solid fa-circle-info"></i>
						<p class="boxFeedback__title">Retroalimentación:</p>
					</div>
					<i class="fa-solid fa-caret-down"></i>
				</div>

				<p class="boxFeedback__info hidden">
					{{$itemEvaluation->feedback}}	
				</p>
			</div>

			<div class="buttonBox">
				<a href="{{ route('editItemEvaluation', $itemEvaluation) }}">
					<div class="buttonAdd buttonAdd--edit">
						<i class="fa-solid fa-pen-to-square"></i>
						<p>Editar</p>
					</div>
				</a>

				<form action="{{ route('destroyItemEvaluation', $itemEvaluation) }}" 
                    method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="buttonAdd buttonAdd--delete">
                        <i class="fa-solid fa-trash"></i>
						<p>Eliminar</p>
                    </button> 
                </form>
			</div>
		</div>
	</div>
@endif