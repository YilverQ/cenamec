<section class="containerFormFloat containerFormFloat--small containerFormFloat--hidden" id="editBoxWindows">
    <article class="formFloat">
        <div class="form__content form__content--big">
            <p class="formFloat__button" id="xmarkCloseEditBoxWindows">
                <i class="fa-solid fa-xmark"></i>
                Quitar
            </p>

            <h2 class="form__icon">
                <img class="iconLogoForm" 
                    src="{{ asset('img/course/Evaluations/compare.png') }}">
            </h2>
            <h2 class="form__title">Editar Item de Emparejamiento</h2>
            <div class="boxEvaluationForms">
                <form method="POST" action="{{ route('editMatching', 1) }}" 
                      class="formInteractiveBox" 
                      id="formEditMatching">
                    @csrf @method('PUT')

                    <div class="grid-one">
                        <div class="form__item form__item--big">    
                            <label for="statement">Enunciado:</label>
                            <input class="form__input form__input--big" 
                                   name="statement" 
                                   required 
                                   type="text" 
                                   id="statement" 
                                   placeholder="Selecciona un Área de la Ciencias Naturales"
                                   autocomplete="off">
                        </div>
                    </div>

                    <div class="boxPair">
                        <div id="pairContainer--edit">
                            <!-- Aquí se agregarán dinámicamente los pares -->
                        </div>
                        <div class="grid-two boxPairContainerForms">
                            <input type="hidden" id="couples" name="couples" value="">
                            <div class="form__item">
                                <label for="elementoA">Elemento-A:</label>
                                <input class="form__input" 
                                       name="elementoA" 
                                       type="text" 
                                       id="elementoA" 
                                       placeholder="Introduce el Elemento A"
                                       autocomplete="off">
                            </div>
                            <i class="fas fa-plus-circle add-pair-icon" 
                               id="addPairButton" 
                               title="Agregar Pareja"></i>

                            <div class="form__item">
                                <label for="elementoB">Elemento-B:</label>
                                <input class="form__input" 
                                       name="elementoB" 
                                       type="text" 
                                       id="elementoB" 
                                       placeholder="Introduce el Elemento B"
                                       autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="grid-one">
                        <div class="form__item form__item--big">
                            <label for="feedback">Retroalimentación:</label>
                            <textarea class="form__input form__input--big" 
                                      name="feedback" 
                                      required 
                                      maxlength="255"
                                      id="feedback" 
                                      placeholder="Proporciona retroalimentación sobre la pareja de elementos"
                                      autocomplete="off"></textarea>
                        </div>
                    </div>

                    <div class="grid-one">
                        <input class="form__send" type="submit" value="Editar Item">
                    </div>
                </form>
            </div>
        </div>
    </article>
</section>