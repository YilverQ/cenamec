<button class="btnRegresar" id="btnRegresar">
    <i class="fa-solid fa-angle-left"></i>
    <p>Regresar</p>
</button>

<div class="hidden" id="formMatching">
    <form method="POST" action="{{ route('createMatching', $interactiveEvaluation) }}" class="formInteractiveBox">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">    
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Selecciona un Área de la Ciencias Naturales"
                       autocomplete="off">
            </div>
        </div>

        <div class="boxPair">
            <div id="pairContainer">
                <!-- Aquí se agregarán dinámicamente los pares -->
            </div>
            <div class="grid-two boxPairContainerForms">
                <input type="hidden" id="couples" name="couples" value="">
                <div class="form__item">
                    <label for="elementoA">Elemento-A: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                    <input class="form__input" 
                           name="elementoA" 
                           type="text" 
                           id="elementoA" 
                           placeholder="Introduce el Elemento A"
                           autocomplete="off">
                </div>
                <i class="fas fa-plus-circle add-pair-icon" 
                   id="addPairButton" 
                   title="Agregar Pareja"></i>

                <div class="form__item">
                    <label for="elementoB">Elemento-B: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                    <input class="form__input" 
                           name="elementoB" 
                           type="text" 
                           id="elementoB" 
                           placeholder="Introduce el Elemento B"
                           autocomplete="off">
                </div>
            </div>
        </div>

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big" 
                          name="feedback" 
                          required 
                          maxlength="255" 
                          id="feedback" 
                          placeholder="Proporciona retroalimentación sobre la pareja de elementos"
                          autocomplete="off"></textarea>
            </div>
        </div>

        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>

<div class="hidden" id="formSecuence">
    <form method="POST" action="{{ route('createSequence', $interactiveEvaluation) }}" class="formInteractiveBox">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Selecciona un Área de la Ciencias Naturales"
                       autocomplete="off">
            </div>
        </div>

        <!-- Campo oculto para almacenar las etiquetas -->
        <div class="grid-one">
            <input type="hidden" id="sequence" name="sequence" value="">
            <div class="form__item">
                <label>Orden de los Elementos:</label>
                <div class="form__input--bigWord form__input--big form__input--tags" id="tagInputContainer">
                    <p class="infoSequenceItem infoSequenceItem--alert hidden">Debes agregar al menos dos elementos</p>
                    <div id="sequenceList"></div> <!-- Para mostrar las etiquetas -->
                    <div class="boxInputSmall boxInputSmall--CompleteWidth">
                        <input class="form__input form__input--CompleteWidth" 
                               name="sequenceItem" 
                               type="text" 
                               id="sequenceItem" 
                               placeholder="Fuego"
                               autocomplete="off">
                        <i class="fas fa-plus-circle add-word-icon" 
                                id="addSequenceButton" 
                                title="Agregar Elemento"></i>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big" 
                          name="feedback" 
                          required 
                          maxlength="255"
                          id="feedback" 
                          placeholder="Proporciona retroalimentación"
                          autocomplete="off"></textarea>
            </div>
        </div>
        <!-- Additional fields here -->
        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>

<div class="hidden" id="formComparativeTable">
    <form method="POST" action="{{ route('createComparativeTable', $interactiveEvaluation) }}" class="formInteractiveBox">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Selecciona un Área de la Ciencias Naturales"
                       autocomplete="off">
            </div>
        </div>
        
        <div class="grid-one">
            <div class="form__item form__item--big">
                <div class="boxTableCompare">
                    <div class="boxTableCompare__table">
                        <!-- Columna A -->
                        <div class="tableColumn">
                            <label class="tableColumn__label">Título para la Columna A</label>
                            <input class="form__input tableColumn__title tableColumn__title--input" 
                                   name="titleColumnA" 
                                   required 
                                   type="text" 
                                   id="titleColumnA" 
                                   placeholder="Elementos A"
                                   autocomplete="off">
                            <div class="tableColumn__body" id="tagListA">
                                <!-- Etiquetas se agregarán aquí -->
                            </div>
                            <div class="tableColumn__boxByInput">
                                <input class="form__input form__input--tableCompare" 
                                       id="inputA"
                                       type="text" 
                                       placeholder="Elemento"
                                       autocomplete="off">
                                <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
                            </div>
                        </div>

                        <!-- Columna B -->
                        <div class="tableColumn">
                            <label class="tableColumn__label">Título para la Columna B</label>
                            <input class="form__input tableColumn__title tableColumn__title--input" 
                                   name="titleColumnB" 
                                   required 
                                   type="text" 
                                   id="titleColumnB" 
                                   placeholder="Elementos B"
                                   autocomplete="off">
                            <div class="tableColumn__body" id="tagListB">
                                <!-- Etiquetas se agregarán aquí -->
                            </div>
                            <div class="tableColumn__boxByInput">
                                <input class="form__input" 
                                       id="inputB"
                                       type="text" 
                                       placeholder="Elemento"
                                       autocomplete="off">
                                <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
                            </div>
                        </div>

                        <!-- Columna C -->
                        <div class="tableColumn">
                            <label class="tableColumn__label">Título para la Columna C</label>
                            <input class="form__input tableColumn__title tableColumn__title--input" 
                                   name="titleColumnC" 
                                   required 
                                   type="text" 
                                   id="titleColumnC" 
                                   placeholder="Elementos C"
                                   autocomplete="off">
                            <div class="tableColumn__body" id="tagListC">
                                <!-- Etiquetas se agregarán aquí -->
                            </div>
                            <div class="tableColumn__boxByInput">
                                <input class="form__input" 
                                       id="inputC"
                                       type="text" 
                                       placeholder="Elemento"
                                       autocomplete="off">
                                <i class="fas fa-plus-circle add-element-icon" title="Agregar Elemento"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Campo oculto para almacenar los elementos -->
        <input type='hidden' id='elementsA' name='elementsA' value=''>
        <input type='hidden' id='elementsB' name='elementsB' value=''>
        <input type='hidden' id='elementsC' name='elementsC' value=''>

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big"
                          name="feedback"
                          required
                          maxlength="255"
                          id="feedback"
                          placeholder="Proporciona retroalimentación sobre la pareja de elementos"
                          autocomplete="off"></textarea>
            </div>
        </div>

        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>

<div class="hidden" id="formShortAnswer">
    <form class="formInteractiveBox" 
        action="{{ route('createShortAnswer', $interactiveEvaluation) }}" 
        method="POST">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Escribe un Elemento de la Naturaleza"
                       autocomplete="off">
            </div>
        </div>
        
        <!-- Campo oculto para almacenar las etiquetas -->
        <div class="grid-one">
            <input type="hidden" id="wordAdd" name="wordAdd" value="">
            <div class="form__item form__item--big">
                <label>Palabras Aceptadas: </label>
                <div class="form__input--bigWord form__input--big form__input--tags" id="tagInputContainer">
                    <div id="tagList"></div> <!-- Para mostrar las etiquetas -->
                    <div class="boxInputSmall">
                        <input class="form__input" 
                               name="word" 
                               type="text" 
                               id="word" 
                               placeholder="Fuego"
                               autocomplete="off">
                        <i class="fas fa-plus-circle add-word-icon add-word-icon--small" 
                                id="addWordButton" 
                                title="Agregar Palabra"></i>
                    </div>
                </div>
            </div>
        </div>


        
        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big" 
                          name="feedback" 
                          required 
                          maxlength="255"
                          id="feedback" 
                          placeholder="Proporciona retroalimentación"
                          autocomplete="off"></textarea>
            </div>
        </div>
        <!-- Additional fields here -->
        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>

<div class="hidden" id="formTrueFalse">
    <form class="formInteractiveBox" 
        action="{{ route('createTrueFalse', $interactiveEvaluation) }}" 
        method="POST">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Selecciona las parejas de colores"
                       autocomplete="off">
            </div>
        </div>
        <div class="grid-one">
            <div class="form__item form__item--big">
                <label>Selecciona la Respuesta Correcta</label>
                <ul class="trueOrFalse__content">
                    <li class="trueOrFalse__item trueOrFalse__item--form" data-value="1">
                        <input type="radio" id="verdadero" name="answer" value="1" required>
                        <label for="verdadero">Verdadero</label>
                    </li>
                    <li class="trueOrFalse__item trueOrFalse__item--form" data-value="0">
                        <input type="radio" id="falso" name="answer" value="0" required>
                        <label for="falso">Falso</label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big" 
                          name="feedback" 
                          required 
                          maxlength="255"
                          id="feedback" 
                          placeholder="Proporciona una retroalimentación"
                          autocomplete="off"></textarea>
            </div>
        </div>
        <!-- Additional fields here -->
        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>

<div class="hidden" id="formSelectionSimple">
    <form method="POST" action="{{ route('createSelectionSimple', $interactiveEvaluation) }}" class="formInteractiveBox">
        @csrf @method('POST')

        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="statement">Enunciado: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--big" 
                       name="statement" 
                       required 
                       type="text" 
                       id="statement" 
                       placeholder="Selecciona un Área de la Ciencias Naturales"
                       autocomplete="off">
            </div>
        </div>
        <div class="grid-two">
            <div class="form__item">
                <label for="answer" class="label-correct">Respuesta correcta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--input" name="answer" required="" type="text" id="answer" placeholder="Física" autocomplete="off">
            </div>
            <div class="form__item">
                <label for="bad1">1° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--input" name="bad1" required="" type="text" id="bad1" placeholder="Artes plásticas" autocomplete="off">
            </div>
        </div>
        <div class="grid-two">
            <div class="form__item">
                <label for="bad2">2° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--input" name="bad2" required="" type="text" id="bad2" placeholder="Educación física" autocomplete="off">
            </div>
            <div class="form__item">
                <label for="bad3">3° Respuesta incorrecta: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <input class="form__input form__input--input" name="bad3" required="" type="text" id="bad3" placeholder="Economía" autocomplete="off">
            </div>
        </div>

        
        <div class="grid-one">
            <div class="form__item form__item--big">
                <label for="feedback">Retroalimentación: <span class="obligatory" title="Campo Obligatorio">*</span></label>
                <textarea class="form__input form__input--big" 
                          name="feedback" 
                          required 
                          maxlength="255"
                          id="feedback" 
                          placeholder="Proporciona una retroalimentación"
                          autocomplete="off"></textarea>
            </div>
        </div>
        <!-- Additional fields here -->
        <div class="grid-one">
            <input class="form__send" type="submit" value="Agregar Item">
        </div>
    </form>
</div>