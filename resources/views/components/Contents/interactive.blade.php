<div class="boxItemCardNote">
	<div class="buttonRightCuestion">
		<div class="cardNote">
			<img class="cardNote__img" src="{{ asset('img/course/test.png') }}">
			<a href="{{ route('interactive', $interactive) }}">
				<div class="cardNote__content cardNote__content--interactive">
					<h3 class="cardNote__number">{{ $itemNumber }}°</h3>
					<h3 class="cardNote__title">Evaluación Interactiva</h3>
				</div>
			</a>
			<p class="cardNote__description">
				<i class="fa-solid fa-clipboard-question"></i>
				{{ count($interactive->itemEvaluations) }}
				Items
			</p>
		</div>

		<div class="buttonRight">
			<i class="fa-solid fa-ellipsis"></i>
		</div>
		<div class="menuNoteCard hidden">
		    <ul class="menuNoteCard__box">
		    	<a href="{{ route('interactive', $interactive) }}">
			        <li class="menuNoteCard__item">
			            <i class="fa-solid fa-pen-to-square"></i>
			            <p>Editar</p>
			        </li>
		    	</a>
		        <form action="{{ route('destroyInteractive', $interactive) }}" 
		        	method="POST" 
		        	class="form__delete">
		        	@csrf
                    @method('DELETE')
			        <button type="submit">
				        <li class="menuNoteCard__item menuNoteCard__item--deleted">
				            <i class="fa-solid fa-trash"></i>
				            <p>Eliminar</p>
				        </li>
			        </button> 
		        </form>
		    </ul>
		</div>
	</div>
</div>