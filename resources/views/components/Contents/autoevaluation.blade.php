<div class="boxItemCardNote">
	<div class="buttonRightCuestion">
		<div class="cardNote">
			<img class="cardNote__img" src="{{ asset('img/course/autoevaluation.png') }}">
			<a href="{{ route('likert', $autoevaluation) }}">
				<div class="cardNote__content cardNote__content--autoevaluation">
					<h3 class="cardNote__number">{{ $itemNumber }}°</h3>
					<h3 class="cardNote__title">Autoevaluación</h3>
				</div>
			</a>
			<p class="cardNote__description">
				<i class="fa-solid fa-clipboard-question"></i>
				{{ count($autoevaluation->likerts) }}
				items
			</p>
		</div>

		<div class="buttonRight">
			<i class="fa-solid fa-ellipsis"></i>
		</div>

		<div class="menuNoteCard hidden">
		    <ul class="menuNoteCard__box">
		    	<a href="{{ route('likert', $autoevaluation) }}">
			        <li class="menuNoteCard__item">
			            <i class="fa-solid fa-pen-to-square"></i>
			            <p>Editar</p>
			        </li>
		    	</a>
		        <form action="{{ route('likert.destroy', $autoevaluation) }}" 
		        	method="POST" 
		        	class="form__delete">
		        	@csrf
                    @method('DELETE')
			        <button type="submit">
				        <li class="menuNoteCard__item menuNoteCard__item--deleted">
				            <i class="fa-solid fa-trash"></i>
				            <p>Eliminar</p>
				        </li>
			        </button> 
		        </form>
		    </ul>
		</div>
	</div>
</div>