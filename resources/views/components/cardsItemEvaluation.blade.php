<!-- Barra de navegación -->
<div class="carousel-navigation">
    @foreach($itemEvaluations as $index => $itemEvaluation)
        <button class="nav-dot" data-index="{{ $index }}" aria-label="Ir al ítem {{ $index + 1 }}"></button>
    @endforeach
</div>

<div class="boxCardsItemEvaluation boxCardsItemEvaluation--carousel">
    <div class="carousel">
		@foreach($itemEvaluations as $index => $itemEvaluation)
    	<div class="carousel-item" data-index="{{ $index }}">
		@if ($itemEvaluation->type == "Verdadero o Falso")
		    <div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}">
		        <div class="interactiveTest__card">
		            <div class="interactiveTest__logo">
		                <img src="{{ asset('img/course/Evaluations/vote.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
		                <h3 class="interactiveTest__title">Verdadero o Falso</h3>
		            </div>
		            <div class="interactiveTest__itemNum interactiveTest__itemNum--number">
		                <p>Item {{ $index + 1 }}</p>
		            </div>
		        </div>

		        <div class="interactiveDetailsBox">
		            <h4 class="titleEvaluation">{{ $itemEvaluation->statement }}</h4>
		            <div class="trueOrFalseBox">
					    <ul class="trueOrFalse__content" data-correct="{{ $itemEvaluation->trueOrFalse->answer == 1 ? 'true' : 'false' }}">
					        <li class="trueOrFalse__item" data-selection="1">Verdadero</li>
					        <li class="trueOrFalse__item" data-selection="0">Falso</li>
					    </ul>
					</div>
					<div class="hiddenRetro active">
						<i class="fa-solid fa-circle-exclamation"></i>            	
		            	<p class="hiddenRetro__text">
		            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
		            		{{ $itemEvaluation->feedback}}
		            	</p>
		            </div>
		        </div>
		    </div>

		@elseif ($itemEvaluation->type == "Selección Simple")
		    <div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}">
		        <div class="interactiveTest__card">
		            <div class="interactiveTest__logo">
		                <img src="{{ asset('img/course/Evaluations/online-test.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
		                <h3 class="interactiveTest__title">Selección Simple</h3>
		            </div>
		            <div class="interactiveTest__itemNum interactiveTest__itemNum--number">
		                <p>Item {{$index + 1 }}</p>
		            </div>
		        </div>

		        <div class="interactiveDetailsBox">
		            <h4 class="titleEvaluation">{{$itemEvaluation->statement}}</h4>
		            <div class="cardQuestion__contentAnswer" id="options-{{$itemEvaluation->id}}">
		                <p class="cardQuestion__answer" data-answer-id="correct">{{ $itemEvaluation->selectionSimple->answer }}</p>
		                <p class="cardQuestion__answer" data-answer-id="bad1">{{ $itemEvaluation->selectionSimple->bad1 }}</p>
		                <p class="cardQuestion__answer" data-answer-id="bad2">{{ $itemEvaluation->selectionSimple->bad2 }}</p>
		                <p class="cardQuestion__answer" data-answer-id="bad3">{{ $itemEvaluation->selectionSimple->bad3 }}</p>
		            </div>
		            <div class="hiddenRetro active">
						<i class="fa-solid fa-circle-exclamation"></i>            	
		            	<p class="hiddenRetro__text">
		            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
		            		{{ $itemEvaluation->feedback}}
		            	</p>
		            </div>
		        </div>
		    </div>

		@elseif ($itemEvaluation->type == "Respuesta Corta")
			<div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}">
				<div class="interactiveTest__card">
					<div class="interactiveTest__logo">
						<img src="{{ asset('img/course/Evaluations/text-box.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
						<h3 class="interactiveTest__title">Respuesta Corta</h3>
					</div>
					<div class="interactiveTest__itemNum interactiveTest__itemNum--number interactiveTest__itemNum interactiveTest__itemNum--number--number">
						<p>Item {{$index + 1}}</p>
					</div>
				</div>
				<div class="interactiveDetailsBox" data-accept-words="{{ json_encode($itemEvaluation->shortAnswersAcceptedWords()) }}">
					<h4 class="titleEvaluation">{{$itemEvaluation->statement}}</h4>
					
					<div class="shortAnswer__response">
					    <label>Respuesta</label>
					    <div class="input-container">
						    <input class="superInputText" type="text" placeholder="Escribe una Palabra..." autocomplete="off">
					    	<i class="fa-solid fa-circle-check icon-input icon-correct icon-hidden"></i>
					    	<i class="fa-solid fa-circle-xmark icon-input icon-incorrect icon-hidden"></i>
						</div>
						<div class="box-check-word-button">
						    <button class="check-word-button">Verificar Palabra</button> 
						</div>
					</div>

					<div class="hiddenRetro active">
						<i class="fa-solid fa-circle-exclamation"></i>            	
		            	<p class="hiddenRetro__text">
		            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
		            		{{ $itemEvaluation->feedback}}
		            	</p>
		            </div>
				</div>
			</div>

		@elseif ($itemEvaluation->type == "Cuadro Comparativo")
			<div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}">
				<div class="interactiveTest__card">
					<div class="interactiveTest__logo">
						<img src="{{ asset('img/course/Evaluations/frequency.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
						<h3 class="interactiveTest__title">Cuadro Comparativo</h3>
					</div>
					<div class="interactiveTest__itemNum interactiveTest__itemNum--number">
						<p>Item {{$index + 1}}</p>
					</div>
				</div>

				<div class="interactiveDetailsBox">
					<div class="boxTableCompare">
						<p>{{$itemEvaluation->statement}}</p>
						<div class="boxTableCompare__table">
							<div class="tableColumn" data-itemsID-A="{{ json_encode($itemEvaluation->comparativeTable->getItemsByColumnAsArray("A")) }}">
								<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnA}}</h3>
								<div class="tableColumn__body">
									
								</div>
							</div>
							<div class="tableColumn" data-itemsID-B="{{ json_encode($itemEvaluation->comparativeTable->getItemsByColumnAsArray("B")) }}">
								<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnB}}</h3>
								<div class="tableColumn__body">
									
								</div>
							</div>
							<div class="tableColumn" data-itemsID-C="{{ json_encode($itemEvaluation->comparativeTable->getItemsByColumnAsArray("C")) }}">
								<h3 class="tableColumn__title">{{$itemEvaluation->comparativeTable->columnC}}</h3>
								<div class="tableColumn__body">
									
								</div>
							</div>
						</div>
						<div class="tableOptionsList">
							@foreach ($itemEvaluation->comparativeTable->tableItems->shuffle() as $key => $word)
							    <div class="tableOptionsList__item" data-itemId="{{ $word->id }}">
							        <p class="tableOptionsList__text">
							            {{ $word->description }}
							        </p>
							        <i class="fa-solid fa-grip-vertical"></i>
							    </div>
							@endforeach
						</div>
						<div class="hiddenRetro active">
							<i class="fa-solid fa-circle-exclamation"></i>            	
			            	<p class="hiddenRetro__text">
			            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
			            		{{ $itemEvaluation->feedback}}
			            	</p>
			            </div>
			            <div class="box-check-tableCompare-button">
						    <button class="check-tableCompare-button hidden">Verificar Tabla</button> 
						</div>
					</div>
				</div>
			</div>

		@elseif ($itemEvaluation->type == "Secuencia")
			<div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}"> 
	            <div class="interactiveTest__card">
	                <div class="interactiveTest__logo">
	                    <img src="{{ asset('img/course/Evaluations/prioritize.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
	                    <h3 class="interactiveTest__title">Secuencia</h3>
	                </div>
	                <div class="interactiveTest__itemNum interactiveTest__itemNum--number">
	                    <p>Item {{$index + 1}}</p>
	                </div>
	            </div>

	            <div class="interactiveDetailsBox">
				    <div class="boxSequence" 
				         data-sequence-correct="{{ json_encode($itemEvaluation->correctSequenceId()) }}">
				        <p class="boxCompare__enunciado">{{$itemEvaluation->statement}}</p>
				        @foreach ($itemEvaluation->sequences->shuffle() as $key => $sequence)
				            <div class="boxSequence__element" data-sequence-id={{$sequence->id}}>
				                <p class="boxSequence__textNumber">{{ $key + 1 }}</p> 
				                <p class="boxSequence__textElement">{{$sequence->description}}</p>
				                <i class="fa-solid fa-grip-vertical"></i>
				            </div>
				        @endforeach
				    </div>
				    <div class="hiddenRetro active">
						<i class="fa-solid fa-circle-exclamation"></i>            	
		            	<p class="hiddenRetro__text">
		            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
		            		{{ $itemEvaluation->feedback}}
		            	</p>
		            </div>
		            <div class="box-check-order-button">
				    	<button class="check-order-button">Verificar Orden</button> 
		            </div>
				</div>
        	</div>

		@elseif ($itemEvaluation->type == "Emparejamiento")
			<div class="interactiveTest" data-evaluation-id="{{ $itemEvaluation->id }}">
				<div class="interactiveTest__card">
					<div class="interactiveTest__logo">
						<img src="{{ asset('img/course/Evaluations/compare.png') }}" alt="Evaluación interactiva" class="interactiveTest__img">
						<h3 class="interactiveTest__title">Emparejamiento</h3>
					</div>
					<div class="interactiveTest__itemNum interactiveTest__itemNum--number">
						<p>Item {{$index + 1}}</p>
					</div>
				</div>
				<div class="interactiveDetailsBox">
					<p class="boxCompare__enunciado">{{$itemEvaluation->statement}}</p>
					<div class="boxCompare">
					    <div class="boxCompare__column boxCompare__column--left">
					    	<p>Columna A</p>
					    	@php
		                        // Desordenar elementos de la columna A
		                        $shuffledA = $itemEvaluation->matchings->pluck('elementA')->shuffle();
		                    @endphp
					    	@foreach ($shuffledA as $elementA)
								<div class="boxCompare__item" 
									 data-element="{{ $elementA }}" 
									 data-matching="{{ $itemEvaluation->matchings->where('elementA', $elementA)->first()->elementB }}" data-matching-id="{{ $itemEvaluation->matchings->where('elementA', $elementA)->first()->id }}">

						            <p class="boxCompare__titleItem">{{ $elementA }}</p>
						        </div>
							@endforeach
					    </div>

					    <div class="boxCompare__column boxCompare__column--right">
					    	<p>Columna B</p>
					        @php
		                        // Desordenar elementos de la columna B
		                        $shuffledB = $itemEvaluation->matchings->pluck('elementB')->shuffle();
		                    @endphp
					    	@foreach ($shuffledB as $elementB)
								<div class="boxCompare__item" 
									 data-element="{{ $elementB }}" 
									 data-matching="{{ $itemEvaluation->matchings->where('elementB', $elementB)->first()->elementA }}"
									 data-matching-id="{{ $itemEvaluation->matchings->where('elementB', $elementB)->first()->id }}">
						            <p class="boxCompare__titleItem">{{ $elementB }}</p>
						        </div>
							@endforeach
					    </div>
					</div>

				    <div class="hiddenRetro active">
						<i class="fa-solid fa-circle-exclamation"></i>            	
		            	<p class="hiddenRetro__text">
		            		<span class="hiddenRetro__retro">Retroalimentación:</span> 
		            		{{ $itemEvaluation->feedback}}
		            	</p>
		            </div>
		            <div class="box-check-pairing-button">
					    <button class="check-pairing-button hidden">Verificar Selección</button> 
					</div>
				</div>
			</div>
		@endif
    	</div>
		@endforeach
    </div>
</div>

<!-- Botones de navegación -->
<div class="boxNavigationCarousel">
    <button class="carousel-button prev hidden" aria-label="Anterior">&#10094;</button>
    <button class="carousel-button next" aria-label="Siguiente">Siguiente Item &#10095;</button>
</div>

<div class="buttonsNavigation hidden" id="ButtonSendForm">
    <form action="{{ route('student.test', $viewContent) }}" method="POST" id="sendButton">
        @csrf
        <input type="hidden" name="evaluationState" id="evaluationStateField">
        <button type="submit" class="buttonParts buttonParts--next">
            Terminar evaluación <i class="fas fa-arrow-right"></i>
        </button>
    </form>
</div>