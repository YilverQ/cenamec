<div class="boxGridTwo">
    <div class="filteredContainer">
        <h2 class="filteredContainer__title" id="buttonFiltered">
            Filtrar por 
            <span class="filteredContainer__filterButton">
                <i class="fa-solid fa-filter"></i>
            </span>
        </h2>

        <!-- Nuevo buscador -->
        <div class="searchBox">
            <input type="text" 
                   class="searchBox__input" 
                   placeholder="Buscar cursos..." 
                   wire:change="$refresh"
                   wire:model="search">
            <span class="searchBox__icon">
                <i class="fa-solid fa-search"></i>
            </span>
        </div>

        <div class="boxFilteredTags" id="boxFilteredTags">
            <!-- Estado -->
            <section class="filtered__section">
                <h3 class="filtered__sectionTitle">Estado del Curso</h3>
                <ul class="filtered__sectionBox">
                    @foreach(['Inscrito', 'No Inscrito'] as $tag)
                        <li class="filtered__sectionItem">
                            <input type="checkbox" 
                                    id="estado-{{ strtolower(str_replace('/', '-', $tag)) }}" 
                                    wire:model="courseStatus" 
                                    wire:change="$refresh" 
                                    value="{{ $tag }}" 
                                    name="courseStatus">
                            <label for="estado-{{ strtolower(str_replace('/', '-', $tag)) }}">{{ $tag }}</label>
                        </li>
                    @endforeach
                </ul>
            </section>


            <!-- Audiencia -->
            <section class="filtered__section">
                <h3 class="filtered__sectionTitle">Audiencia</h3>
                <ul class="filtered__sectionBox">
                    @foreach($audiences_list as $audience)
                        <li class="filtered__sectionItem">
                            <input type="checkbox" 
                                    id="audiencia-{{ $audience->id }}" 
                                    wire:model="audiences" 
                                    wire:change="$refresh" 
                                    value="{{ $audience->id }}" 
                                    name="audiences">
                            <label for="audiencia-{{ $audience->id }}">{{ $audience->name }}</label>
                        </li>
                    @endforeach
                </ul>
            </section>

            <!-- Categoría -->
            <section class="filtered__section">
                <h3 class="filtered__sectionTitle">Categoría</h3>
                <ul class="filtered__sectionBox">
                    @foreach($tags_list as $tag)
                        <li class="filtered__sectionItem">
                            <input type="checkbox" 
                                    id="categorie-{{ $tag->id }}" 
                                    wire:model="categories" 
                                    wire:change="$refresh" 
                                    value="{{ $tag->id }}" 
                                    name="categories">
                            <label for="categorie-{{ $tag->id }}">{{ $tag->name }}</label>
                        </li>
                    @endforeach
                </ul>
            </section>


            <!-- Nivel -->
            <section class="filtered__section">
                <h3 class="filtered__sectionTitle">Nivel</h3>
                <ul class="filtered__sectionBox">
                    @foreach(['Básico', 'Intermedio', 'Avanzado'] as $tag)
                        <li class="filtered__sectionItem">
                            <input type="checkbox" 
                                    id="nivel-{{ strtolower(str_replace('/', '-', $tag)) }}" 
                                    wire:model="levels" 
                                    wire:change="$refresh" 
                                    value="{{ $tag }}" 
                                    name="levels">
                            <label for="nivel-{{ strtolower(str_replace('/', '-', $tag)) }}">{{ $tag }}</label>
                        </li>
                    @endforeach
                </ul>
            </section>
        </div>
    </div>


    <article class="article article--courses" id="scrollCourses">
        <h2 class="article__title">Lista de Cursos</h2>
        @if($courses->isEmpty())
            <div class="notFindCourse">
                <i class="fa-solid fa-file-circle-xmark"></i>
                <p>No hay cursos disponibles con los filtros seleccionados.</p>
            </div>
        @else
            <div class="containerCourses">
                @foreach ($courses as $item)
                <div class="cardCourse">
                    <img class="cardCourse__img" src="{{ $item->img }}" alt="{{ $item->name }}">
                    <h2 class="cardCourse__title">{{ $item->name }}</h2>
                    <p class="cardCourse__info"><i class="fa-solid fa-book"></i> {{ $item->modules_count }} módulos</p>
                    
                    <!-- Indicador de inscripción -->
                    @if($item->isEnrolled)
                        <span class="enrollment-status enrolled">
                            </i> Inscrito
                        </span>
                        <ul class="list__actions list__actions--cardCourse">
                            <li>
                                <a href="{{ route('student.course.show', $item) }}" title="Ver más" class="icon icon--show">
                                    <i class="fa-solid fa-right-to-bracket"></i>
                                    Ir al curso
                                </a>
                            </li>
                        </ul>
                    @else
                        <span class="enrollment-status not-enrolled">
                            No Inscrito
                        </span>
                        <ul class="list__actions list__actions--cardCourse">
                            <li>
                                <a href="{{ route('student.course.show', $item) }}" title="Ver más" class="icon icon--show">
                                    <i class="fa-solid fa-magnifying-glass-arrow-right"></i>
                                    Explorar curso
                                </a>
                            </li>
                        </ul>
                    @endif
                </div>
                @endforeach
            </div>

            <div>
                @if(!$courses->onFirstPage() || $courses->hasMorePages())
                    <div class="pagination-container">
                        @if(!$courses->onFirstPage())
                            <button wire:click="previousPage" 
                                    class="pagination-button" 
                                    onclick="scrollToCourses()">
                                    Anterior
                            </button>
                        @endif

                        @if($courses->hasMorePages())
                            <button wire:click="nextPage" 
                                    class="pagination-button" 
                                    onclick="scrollToCourses()">
                                    Siguiente
                            </button>
                        @endif
                    </div>
                @endif
            </div>


        @endif
    </article>
</div>


<script>
    function scrollToCourses() {
        const element = document.getElementById('scrollCourses');
        if (element) {
            // Calculate the position of the element
            const elementPosition = element.getBoundingClientRect().top + window.scrollY;
            // Calculate the offset
            const offsetPosition = elementPosition - 60; // 60px offset

            // Scroll to the calculated position
            window.scrollTo({
                top: offsetPosition,
                behavior: 'smooth' // Smooth scrolling
            });
        }
    }
</script>
