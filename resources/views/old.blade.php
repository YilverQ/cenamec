<div class="descriptionCard hidden">
    <h3 class="smallInformation__title">Contenidos Vistos Por Estudiantes</h3>
    <div class="boxContainerDescriptionCard">
        <div class="containerDescriptionCard">
            <div class="containerBoxData">
                <p class="boxData">
                    Cantidad de Lecciones Vistas por Estudiantes:
                    <span class="dataNumber">
                        {{ $modules->flatMap->contents->where('type', 'Nota')->count() }}
                    </span>
                </p>
                <p class="boxData">
                    Cantidad de Autoevaluaciones Vistas por Estudiantes:
                    <span class="dataNumber">
                        {{ $modules->flatMap->contents->where('type', 'Autoevaluación')->count(); }}
                    </span>
                </p>
                <p class="boxData">
                    Cantidad de Evaluaciones Interactivas Vistas por Estudiantes:
                    <span class="dataNumber">
                        {{ $modules->flatMap->contents->where('type', 'Evaluación')->count(); }}
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>