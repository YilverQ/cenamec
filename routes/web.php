<?php

use Illuminate\Support\Facades\Route;

/*Importamos los controladores*/
use App\Http\Controllers\ApiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\LikertController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\FormDataController;
use App\Http\Controllers\ExportDataController;
use App\Http\Controllers\InteractiveController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\AdminStaticController;
use App\Http\Controllers\SharedCourseController;
use App\Http\Controllers\UbicationAPIController;
use App\Http\Controllers\AdministratorController;
use App\Http\Controllers\StudentCourseController;
use App\Http\Controllers\StudentModuleController;
use App\Http\Controllers\CourseStaticsController;
use App\Http\Controllers\ItemEvaluationController;
use App\Http\Controllers\APIInteractiveController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login.home');
});


/** Login
 * HTTP     URI           Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get   /home            index           View      Retorna la vista principal. 
 * get   /login           login           View      Formulario para ingresar al sistema. 
 * post  /login           auth            Action    Combrueba los datos del usuario y redirecciona. 
 * post  /login/student   checkStudent    Action    Combrueba los datos del usuario y redirecciona. 
 * post  /login/teacher   checkTeacher    Action    Combrueba los datos del usuario y redirecciona. 
 * get   /signup          signup          View      Formulario para crear un nuevo estudiante. 
 * post  /signup/student  store           Action    Crea un nuevo estudiante.
 * get   /logout          logout          Action    Cerramos la sección. 
 * get   /admin           admin           View      Formulario para ingresar al sistema como admin. 
 * post  /admin           checkAdmin      Action    Combrueba los datos del usuario y redirecciona. 
 *
 * get   /forget          forgotPassword  View      Formulario para enviar un correo al usuario. 
 * post  /forget          sendEmailLink   Action    Envía al usuario un link para resetear la clave. 
 * post  /forget          sendEmailLink   Action    Envía al usuario un link para resetear la clave. 
 * get   /new/password/{item}    newPassword     View      Formulario para ingresar nueva clave. 
 * post  /new/password/{item}    newPassword     Action    Resetea la clave del usuario. 
*/
Route::controller(LoginController::class)->group(function () {
    Route::get( '/home',    'home')->name('login.home');
    Route::get( '/login',   'login')->name('login.login');
    Route::post('/login',   'auth')->name('login.auth');
    Route::post('/login/student', 'checkStudent')->name('login.checkStudent');
    Route::post('/login/teacher', 'checkTeacher')->name('login.checkTeacher');
    Route::get( '/signup',  'signup')->name('login.signup');
    Route::post('/signup/student', 'store')->name('login.newStudent');
    Route::get( '/logout',  'logout')->name('login.logout');
    Route::get( '/admin',   'admin')->name('login.admin');
    Route::post('/admin',   'checkAdmin')->name('login.checkAdmin');
    
    Route::get('/forget/password', 'forgetPassword')->name('login.forgetPassword');
    Route::post('/forget/password', 'checkEmailToResetPassword')->name('login.checkEmailToResetPassword');
    Route::get('/new/password', 'newPassword')->name('login.newPassword');
    Route::post('/new/password', 'addNewPassword')->name('login.addNewPassword');
});


/** Páginas Para compartir un Curso.
 * HTTP    URI                          Method         Reponse  Description
 * -----------------------------------------------------------------------
 * get     /page/404/student   sharedCourse     View   Comparte un curso a través de un link.
*/
Route::controller(SharedCourseController::class)->group(function () {
    Route::get('/shared/link/course/{item}', 'sharedCourse')->name('shared.course');
    Route::get('/shared/user/course/{item}', 'sharedUser')->name('shared.userCourse');
    Route::get('/shared/course/{item}', 'sharedNotUser')->name('shared.notUserCourse');
});


/** Usuario.
 * HTTP     URI                  Method         Reponse     Description
 * -----------------------------------------------------------------------
 * get   /user/profile           profile         View        Retorna el perfil del usuario. 
 * put   /user/profile           update          Action      Actualiza un elemento. 
 * put   /user/profile/img       updateImg       Action      Actualiza la foto de perfil.
 * put   /user/profile/password  updatePassword  Action      Actualiza la contraseña.
 * post   /user/academic         addAcademic     Action      Agrega un dato academico.
 * delete /user/academic/{item}  deleteAcademic  Action      Elimina un dato académico.
*/
Route::controller(UserController::class)->group(function () {
    Route::get('/user/profile', 'profile')->name('user.profile');
    Route::put('/user/profile', 'update')->name('user.update');
    Route::put('/user/profile/img', 'updateImg')->name('user.img');
    Route::put('/user/profile/password', 'updatePassword')->name('user.password');
    Route::post('/user/academic', 'addAcademic')->name('user.addAcademic');
    Route::put('/user/academic', 'updateAcademic')->name('user.updateAcademic');
    Route::delete('/user/academic/{item}', 'deleteAcademic')->name('user.deleteAcademic');
    Route::post('/user/laboral', 'addLaboral')->name('user.addLaboral');
    Route::put('/user/laboral', 'updateLaboral')->name('user.updateLaboral');
    Route::delete('/user/laboral/{item}', 'deleteLaboral')->name('user.deleteLaboral');
});


/** Administrador
 * HTTP     URI                Method       Reponse  Description
 * -----------------------------------------------------------------------
 * get     /admin              home         View     Retorna la vista principal. 
 * get     /admin              index        View     Retorna todos los elementos. 
 * get     /admin/create       create       View     Formulario para crear un nuevo elemento. 
 * post    /admin              store        Action   Crea un nuevo elemento. 
 * get     /admin/{item}       show         View     Vista de un elemento. 
 * get     /admin/{item}/edit  edit         View     Vista para editar un elemento. 
 * put     /admin/{item}       update       Action   Actualiza un elemento. 
 * put     /admin/{item}/img   updateImg    Action   Actualiza un elemento. 
 * delete  /admin/{item}       delete       Action   Elimina un elemento.
 * put     /admin/profile/password  updatePassword  Action  Actualiza la contraseña del usuario. 
*/
Route::controller(AdministratorController::class)->group(function () {
    Route::get( '/administrator', 'home')->name('administrator.home');
    Route::get( '/administrator/user', 'index')->name('administrator.index');
    Route::get( '/administrator/create', 'create')->name('administrator.create');
    Route::post('/administrator/user', 'store')->name('administrator.store');
    Route::get( '/administrator/user/{item}/edit', 'edit')->name('administrator.edit');
    Route::put( '/administrator/user/{item}', 'update')->name('administrator.update');
    Route::delete('/administrator/user/{item}', 'disabled')->name('administrator.disabled');
    Route::put('/administrator/user/{item}/enabled', 'enabled')->name('administrator.enabled');
    
    Route::post('/administrator/academic/{item}', 'addAcademic')->name('administrator.addAcademic');
    Route::put('/administrator/academic/{item}', 'updateAcademic')->name('administrator.updateAcademic');
    Route::delete('/administrator/academic/del/{item}/by-user/{user}', 'deleteAcademic')->name('administrator.deleteAcademic');

    Route::post('/administrator/laboral/{item}', 'addLaboral')->name('administrator.addLaboral');
    Route::put('/administrator/laboral/{item}', 'updateLaboral')->name('administrator.updateLaboral');
    Route::delete('/administrator/laboral/{item}/by-user/{user}', 'deleteLaboral')->name('administrator.deleteLaboral');

    Route::put( '/administrator/user/{item}/img', 'updateImg')->name('administrator.img');
    Route::put( '/administrator/profile/{item}/password', 'updatePassword')->name('administrator.password');

    Route::put( '/administrator/update/roles/{item}', 'checkRoles')->name('administrator.checkRoles');
});


/** Estadísticas del Administrador
 * HTTP     URI            Method          Reponse    Description
 * -----------------------------------------------------------------------
 * get   /teacher          index           View       Retorna la vista principal del profesor. 
*/
Route::controller(AdminStaticController::class)->group(function () {
    Route::get( '/admin/statics/', 'adminStatics')->name('adminStatics');
});


/** Profesor
 * HTTP     URI            Method          Reponse    Description
 * -----------------------------------------------------------------------
 * get   /teacher          index           View       Retorna la vista principal del profesor. 
*/
Route::controller(TeacherController::class)->group(function () {
    Route::get( '/teacher', 'index')->name('teacher.index');
});


/** Curso
 * HTTP    URI                          Method     Reponse  Description
 * -----------------------------------------------------------------------
 * get     /course              index      View     Retorna todos los elementos. 
 * get     /course/create       create     View     Formulario para crear un nuevo elemento. 
 * post    /course/create       store      Action   Crea un nuevo elemento. 
 * get     /course/{item}       show       View     Vista con información del elemento 
 * get     /course/{item}/edit  edit       Action   Formulario para actualizar. 
 * put     /course/{item}       update     Action   Actualiza un elemento. 
 * delete  /course/{item}       delete     Action   Elimina un elemento.
 * get     /shared/course/{item}        shared     Action   Comparte un curso a través de un link.
*/
Route::controller(CourseController::class)->group(function () {
    Route::get( '/courses/', 'index')->name('teacher.course.index');
    Route::get( '/courses/create', 'create')->name('teacher.course.create');
    Route::post('/courses/create', 'store')->name('teacher.course.store');
    Route::get( '/course/{item}', 'show')->name('teacher.course.show');
    Route::get( '/course/{item}/edit', 'edit')->name('teacher.course.edit');
    Route::put( '/course/{item}', 'update')->name('teacher.course.update');
    Route::delete('/course/{item}', 'destroy')->name('teacher.course.destroy');
    Route::get('/shared/user/course/{item}', 'sharedUser')->name('sharedUser.course');
    Route::get('/shared/course/{item}', 'sharedNotUser')->name('sharedNotUser.course');
});


/** Módulo
 * HTTP     URI                 Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get     /module/create       create     View      Formulario para crear un nuevo elemento. 
 * post    /module              store      Action    Crea un nuevo elemento. 
 * get     /module/{item}       show       View      Vista con información del elemento 
 * get     /module/{item}/edit  edit       Action    Formulario para actualizar. 
 * put     /module/{item}       update     Action    Actualiza un elemento. 
 * delete  /module/check        delete     Action    Elimina un elemento.
*/
Route::controller(ModuleController::class)->group(function () {
    Route::get( '/module/create/{item}', 'create')->name('teacher.module.create');
    Route::post('/module/{item}', 'store')->name('teacher.module.store');
    Route::get( '/module/{item}', 'show')->name('teacher.module.show');
    Route::get( '/module/{item}/edit', 'edit')->name('teacher.module.edit');
    Route::put( '/module/{item}', 'update')->name('teacher.module.update');
    Route::delete('/module/{item}', 'destroy')->name('teacher.module.destroy');
});


/** Notas
 * HTTP     URI                       Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get     /note/create       create     View      Formulario para crear un nuevo elemento. 
 * post    /note              store      Action    Crea un nuevo elemento. 
 * get     /note/{item}/edit  edit       Action    Formulario para actualizar. 
 * put     /note/{item}       update     Action    Actualiza un elemento. 
 * delete  /note/check        delete     Action    Elimina un elemento.
*/
Route::controller(NoteController::class)->group(function () {
    Route::get( '/note/create', 'create')->name('teacher.note.create');
    Route::post('/note', 'store')->name('teacher.note.store');
    Route::get( '/note/{item}/edit', 'edit')->name('teacher.note.edit');
    Route::put( '/note/{item}', 'update')->name('teacher.note.update');
    Route::delete('/note/{item}', 'destroy')->name('teacher.note.destroy');
});


/** Profesor - Estadísticas del Curso
 * HTTP     URI                    Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get   /course/{item}/statics    show       View      Retorna una vista de las estadísticas. 
*/
Route::controller(CourseStaticsController::class)->group(function () {
    Route::get('/course/{item}/statics', 'show')->name('course.statics');

});




/** Estudiante
 * HTTP     URI       Method          Reponse     Description
 * -----------------------------------------------------------------------
 * get   /student     index           View        Retorna vista principal. 
*/
Route::controller(StudentController::class)->group(function () {
    Route::get( '/student', 'index')->name('student.index');
});


/** Estudiante - Cursos
 * HTTP     URI                    Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get   /student/course/          index      View      Retorna todos los elementos. 
 * post  /student/course/{item}/   store      Action    Agregarmos un curso al estudiante. 
 * get   /student/course/{item}/   show       View      vista de un elemento para inscribir. 
 * get   /student/course/{item}/   display    View      vista de un elemento inscrito. 
*/
Route::controller(StudentCourseController::class)->group(function () {
    Route::get('/student/course/', 'index')->name('student.course.index');
    Route::post('/student/course/{item}', 'store')->name('student.course.store');
    Route::get('/student/course/{item}', 'show')->name('student.course.show');
    Route::get('/student/my/course/{item}', 'display')->name('student.course.display');
});


/** Estudiante - Module
 * HTTP     URI                    Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get   /student/module/{item}    index      View      Retorna una vista para estudiar. 
*/
Route::controller(StudentModuleController::class)->group(function () {
    Route::get('/study/module/{item}', 'study')->name('student.module.study');
    Route::get('/study/content/{item}', 'studyContent')->name('student.studyContent');
    Route::get('/pass/module/{item}', 'passModule')->name('student.module.pass');
    Route::post('/pass/content/{item}/next/{next?}', 'passContent')->name('student.passContent');
});


/** Estudiante - Test
 * HTTP     URI                    Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get   /study/test/    index      View      Retorna una vista para estudiar. 
*/
Route::controller(TestController::class)->group(function () {
    Route::post('/study/test/{item}', 'test')->name('student.test');
});


/** Certificado
 * HTTP     URI                    Method     Reponse   Description
 * -----------------------------------------------------------------------
 * get   /certificate/{item}       show       View      Mostramos un elemento. 
 * post  /certificate              store      Action    Crea un nuevo elemento. 
*/
Route::controller(CertificateController::class)->group(function () {
    Route::get( '/certificate/{item}', 'show')->name('certificate.show');
});


/** UbicationAPI
 * HTTP     URI                          Method             Reponse   Description
 * -----------------------------------------------------------------------
 * get      /api/municipalities/{item}   getMunicipalities  JSON      Retorna una lista de municipios. 
 * get      /api/parishes/{item}         getParishes        JSON      Retorna una lista de parroquias. 
*/
Route::controller(UbicationAPIController::class)->group(function () {
    Route::get('/api/municipalities/{item}', 'getMunicipalities')->name('api.municipalities');
    Route::get('/api/parishes/{item}', 'getParishes')->name('api.parishes');
});


/** ApiController
 * HTTP     URI                     Method                Reponse   Description
 * -----------------------------------------------------------------------
 * get      /api/get/pictures       getPicturesProfile    JSON      Retorna una lista de imagenes. 
*/
Route::controller(ApiController::class)->group(function () {
    Route::get('/api/get/pictures', 'getPicturesProfile')->name('api.getPicturesProfile');
});


/** Curso compartido
 * HTTP    URI                          Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /shared/link/course/{item}   sharedCourse   Action    Comparte un curso a través de un link.
 * get     /shared/course/{item}        sharedNotUser  View      Comparte un curso a través de un link.
*/
Route::controller(SharedCourseController::class)->group(function () {
    Route::get('/shared/link/course/{item}', 'sharedCourse')->name('shared.course');
    Route::get('/shared/course/{item}', 'sharedNotUser')->name('shared.notUserCourse');
});


/** FormDataController
 * HTTP     URI                      Method                Reponse   Description
 * -----------------------------------------------------------------------
 * get      /api/get/academic/level  getAcademicLevel      JSON      Retorna una lista JSON. 
 * get      /api/get/areas           getAreas              JSON      Retorna una lista JSON. 
 * get      /api/get/careers         getCareers            JSON      Retorna una lista JSON. 
 * get      /api/get/institutions    getInstitutions       JSON      Retorna una lista JSON. 
 * get      /api/get/sectors         getSectors            JSON      Retorna una lista JSON. 
*/
Route::controller(FormDataController::class)->group(function () {
    Route::get('/api/get/academic/level', 'getAcademicLevel')->name('data.getAcademicLevel');
    Route::get('/api/get/areas', 'getAreas')->name('data.getAreas');
    Route::get('/api/get/careers', 'getCareers')->name('data.getCareer');
    Route::get('/api/get/institutions', 'getInstitutions')->name('data.getInstitution');
    Route::get('/api/get/sectors', 'getSectors')->name('data.getSectors');
    Route::get('/api/get/dataAcademic/{item}','getDataAcademic')->name('data.getDataAcademic');
    Route::get('/api/get/dataLaboral/{item}','getDataLaboral')->name('data.getDataLaboral');
});


/** ImageController
 * HTTP     URI               Method                Reponse   Description
 * -----------------------------------------------------------------------
 * post      /upload-images   upload                JSON      Retorna JSON. 
*/
Route::controller(ImageController::class)->group(function () {
    Route::post('/upload-images', 'upload')->name('imagen.upload');
});


/** Escala de Likert - Autoevaluación
 * HTTP    URI       Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /likert   create         View        Vista para crear un elemento.
*/
Route::controller(LikertController::class)->group(function () {
    Route::get('/likert/{item}', 'likert')->name('likert');
    Route::get('/likert/template/module/{item}', 'createItem')->name('likert.createItem');
    Route::post('/likert/create/by/module{item}', 'store')->name('likert.create');
    Route::post('/likert/add/by/autoevaluation/{item}', 'addItem')->name('likert.addItem');
    Route::put( '/likert/update/{item}', 'update')->name('likert.updateItem');
    Route::delete('/likert/item/{item}', 'destroyItem')->name('likert.destroyItem');
    Route::delete('/likert/{item}', 'destroy')->name('likert.destroy');
});


/** Evaluaciones Interactivas
 * HTTP    URI                      Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /interactive     create         View      Vista para crear un elemento.
*/
Route::controller(InteractiveController::class)->group(function () {
    Route::get('/interactive/{item}', 'interactive')->name('interactive');
    Route::get('/interactive/create/by/module/{item}', 'createInteractive')->name('createInteractive');
    Route::delete('/interactive/{item}', 'destroyInteractive')->name('destroyInteractive');
    
    // Creamos los items
    Route::post('/interactive/{item}/create/selection-simple/', 'createSelectionSimple')->name('createSelectionSimple');
    Route::post('/interactive/{item}/create/short-answer/', 'createShortAnswer')->name('createShortAnswer');
    Route::post('/interactive/{item}/create/trueFalse/', 'createTrueFalse')->name('createTrueFalse');
    Route::post('/interactive/{item}/create/sequence/', 'createSequence')->name('createSequence');
    Route::post('/interactive/{item}/create/matching/', 'createMatching')->name('createMatching');
    Route::post('/interactive/{item}/create/comparative-table/','createComparativeTable')->name('createComparativeTable');

    // Eliminamos los items
    Route::delete('/interactive/itemEvaluation/{item}', 'destroyItemEvaluation')->name('destroyItemEvaluation');
});

/** ItemEvaluation
 * HTTP    URI                      Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /interactive     create         View      Vista para crear un elemento.
*/
Route::controller(ItemEvaluationController::class)->group(function () {
    // Actualizamos los items
    Route::get('/edit/itemEvaluation/{item}', 'editItemEvaluation')->name('editItemEvaluation');
    Route::put('/update/selection-simple/{item}', 'updateSelectionSimple')->name('updateSelectionSimple');
    Route::put('/update/short-answer/{item}', 'updateShortAnswer')->name('updateShortAnswer');
    Route::put('/update/trueFalse/{item}', 'updateTrueFalse')->name('updateTrueFalse');
    Route::put('/update/sequence/{item}', 'updateSequence')->name('updateSequence');
    Route::put('/update/matching/{item}', 'updateMatching')->name('updateMatching');
    Route::put('/update/comparative-table/{item}', 'updateComparativeTable')->name('updateComparativeTable');
});



/** API Evaluaciones Interactivas
 * HTTP    URI                      Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /interactive     create         View      Vista para crear un elemento.
*/
Route::controller(APIInteractiveController::class)->group(function () {
    Route::get('/api/get/itemEvaluation/{item}','getItemEvaluation')->name('getItemEvaluation');
});



/** Exportar Datas
 * HTTP    URI                      Method         Reponse   Description
 * -----------------------------------------------------------------------
 * get     /interactive     create         View      Vista para crear un elemento.
*/
Route::controller(ExportDataController::class)->group(function () {
    Route::get('/export/users/by/course/{item}','usersByCourse')->name('usersByCourse');
    Route::get('/export/list/students','listStudents')->name('listStudents');
    Route::get('/export/list/students','listStudents')->name('listStudents');
    Route::get('/export/list/teachers','listTeachers')->name('listTeachers');
    Route::get('/export/list/admins','listAdmins')->name('listAdmins');
    Route::get('/export/list/users/disabled','listDisabledUsers')->name('listDisabledUsers');
});