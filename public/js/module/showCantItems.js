// Agregar evento de clic
document.getElementById('showCantItems').addEventListener('click', function() {
    // Obtener el div que se mostrará u ocultará
    var evaluationsDiv = document.getElementById('evaluationsDescription');
    
    // Obtener el icono del caret
    var caretIcon = document.getElementById('caretIcon');
    
    // Mostrar u ocultar el div
    if (evaluationsDiv.classList.contains('hidden')) {
        evaluationsDiv.classList.remove('hidden');
        caretIcon.classList.add('rotate');
    } else {
        evaluationsDiv.classList.add('hidden');
        caretIcon.classList.remove('rotate');
    }
});
