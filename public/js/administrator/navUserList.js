document.addEventListener("DOMContentLoaded", function () {
    // Obtener los elementos del menú y las secciones
    const menuItems = document.querySelectorAll(".listUserNav__item");
    const sections = {
        "Estudiantes": document.getElementById("listStudent"),
        "Administrador": document.getElementById("listAdmin"),
        "Profesores": document.getElementById("listTeacher"),
        "Usuarios Deshabilitados": document.getElementById("listUserDisabled")
    };

    // Función para manejar el clic en el menú
    menuItems.forEach(item => {
        item.addEventListener("click", function () {
            // Remover la clase 'selected' de todos los elementos
            menuItems.forEach(el => el.classList.remove("listUserNav__item--selected"));

            // Agregar la clase 'selected' al elemento clickeado
            this.classList.add("listUserNav__item--selected");

            // Ocultar todas las secciones
            Object.values(sections).forEach(section => section.style.display = "none");

            // Obtener el texto del menú y mostrar la sección correspondiente
            const selectedText = this.querySelector(".listUserNav__text").textContent.trim();
            if (sections[selectedText]) {
                sections[selectedText].style.display = "block";
            }
        });
    });

    // Mostrar solo la sección de "Estudiantes" al cargar la página
    Object.values(sections).forEach(section => section.style.display = "none");
    sections["Estudiantes"].style.display = "block"; // Cambiado a "Estudiantes"
});