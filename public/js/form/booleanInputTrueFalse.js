const itemsBooleanTrueFalse = document.querySelectorAll('.trueOrFalse__item');

itemsBooleanTrueFalse.forEach(item => {
    item.addEventListener('click', () => {
        // Obtener el valor del atributo data-value
        const value = item.getAttribute('data-value');
        const input = document.getElementById(value);

        // Habilitar el input si está deshabilitado
        if (input) {
            input.disabled = false; // Habilita el input
            input.checked = true; // Marca el input como seleccionado
        }
    });
});
