//Selects que están en el html.
let openIconForms       = document.querySelectorAll(".editAcademicData");
let classText, number   = "";
let idAcademic          = document.getElementById('idAcademic');
let selectAcademicLevel = document.getElementById('academicLevelEdit');
let selectSector        = document.getElementById('sectorEdit');
let selectArea          = document.getElementById('areaEdit');

let selectInstitution   = document.querySelectorAll('.spanSelectEdit')[0];
let selectCareer        = document.querySelectorAll('.spanSelectEdit')[1];

let data    = null;
let count   = 0;

openIconForms.forEach((icon)=> {
    icon.addEventListener("click", function() {
        resetSelects();
        classText = icon.classList[2];
        number = classText.match(/\d+/)[0];
        getDataAcademic(number);
        idAcademic.value = number;

        getAcademicLevels();
        getSectors();
        getAreas();

        getInstitutions();
        getCareers();
    });
})


function resetSelects() {
    selectAcademicLevel.innerHTML = "";
    selectSector.innerHTML = "";
    selectArea.innerHTML = "";
}

function getDataAcademic(number) {
    axios.get('/api/get/dataAcademic/'+number)
    .then(function (response) {
            data = response.data;
            createInputs(data);
    })
    .catch(function (error) {
        console.log(error);
    });
}


function createInputs(data) {
    let academic_titleEdit = document.getElementById('academic_titleEdit');
    let year_entryEdit = document.getElementById('year_entryEdit');
    let year_exitEdit = document.getElementById('year_exitEdit');

    let option = document.createElement("OPTION");
        option.value = data.academicLevel.id;
        option.innerHTML = data.academicLevel.name;
        selectAcademicLevel.appendChild(option);


    option = document.getElementById("hiddenIntitutionEdit");
        option.value = data.institution.id;
        selectInstitution.innerText = data.institution.name;

    option = document.getElementById("hiddenCarrerEdit");
        option.value = data.career.id;
        selectCareer.innerText = data.career.name;


    option = document.createElement("OPTION");
        option.value = data.sector.id;
        option.innerHTML = data.sector.name;
        selectSector.appendChild(option);


    option = document.createElement("OPTION");
        option.value = data.area.id;
        option.innerHTML = data.area.name;
        selectArea.appendChild(option);

        academic_titleEdit.value = data.academic_title; 
        year_entryEdit.value = data.year_entry;
        year_exitEdit.value = data.year_exit;
}

function getAcademicLevels() {
    axios.get('/api/get/academic/level')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;
  
        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            option.classList.add("optionAcademic")
            selectAcademicLevel.appendChild(option);
        });
    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getInstitutions() {
    axios.get('/api/get/institutions')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getCareers() {
    axios.get('/api/get/careers')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;
     
        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getSectors() {
    axios.get('/api/get/sectors')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectSector.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getAreas() {
    axios.get('/api/get/areas')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectArea.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}