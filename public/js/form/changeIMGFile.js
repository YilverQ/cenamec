// Evento al enviar el formulario
const sendButton = document.getElementById('sendButtonProfile');
const fileInput = document.getElementById('img');
const fileNameDisplay = document.getElementById('file-name');
const imgPreviewContainer = document.getElementById('img-preview-container');
const imgPreview = document.getElementById('img-preview');

sendButton.addEventListener('click', (e) => {
    // Verificar si no se ha seleccionado un archivo
    if (fileNameDisplay.textContent === "No se ha seleccionado ningún archivo") {
        e.preventDefault(); // Evitar el envío del formulario
        alert("Por favor, seleccione una imagen.");
        return; // Salir de la función para no ejecutar el resto
    }

    // Validar otros campos del formulario
    if (!validateForm()) {
        e.preventDefault(); // Evitar el envío del formulario si hay errores
        alert("Por favor, corrige los campos resaltados en rojo.");
    }
});

// JavaScript para mostrar el preview de la imagen seleccionada
fileInput.addEventListener('change', function () {
    // Mostrar el nombre del archivo
    if (fileInput.files.length > 0) {
        fileNameDisplay.textContent = fileInput.files[0].name;
    } else {
        fileNameDisplay.textContent = "No se ha seleccionado ningún archivo";
    }

    // Verificar si hay un archivo
    if (fileInput.files && fileInput.files[0]) {
        // Crear un objeto de URL para la previsualización
        const reader = new FileReader();

        reader.onload = function (e) {
            // Mostrar la imagen en el contenedor
            imgPreviewContainer.style.display = 'block';
            imgPreview.src = e.target.result;
        };

        reader.readAsDataURL(fileInput.files[0]);
    } else {
        imgPreviewContainer.style.display = 'none'; // Ocultar el contenedor si no se selecciona archivo
    }
});
