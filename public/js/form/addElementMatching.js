const usedElements = new Set(); // Set to track used elements
const couplesArray = []; // Array to track pairs

if (typeof existingPairs !== 'undefined') {
    existingPairs.forEach(pair => {
        addPairElement(pair.elementA, pair.elementB);
    });
}

// Lógica para agregar un nuevo par de elementos
document.getElementById('addPairButton').addEventListener('click', addPair);
['elementoA', 'elementoB'].forEach(id => {
    document.getElementById(id).addEventListener('keypress', function(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            addPair();
        }
    });
});

function addPair() {
    const elementoAInput = document.getElementById('elementoA');
    const elementoBInput = document.getElementById('elementoB');
    
    const elementoAValue = elementoAInput.value.trim();
    const elementoBValue = elementoBInput.value.trim();

    if (elementoAValue === '' || elementoBValue === '') {
        alert("Por favor, completa ambos campos antes de agregar.");
        return;
    }

    if (usedElements.has(elementoAValue) || usedElements.has(elementoBValue)) {
        alert("Uno o ambos elementos ya han sido usados. Por favor, introduce elementos únicos.");
        return;
    }

    addPairElement(elementoAValue, elementoBValue, true);
    elementoAInput.value = '';
    elementoBInput.value = '';
    elementoAInput.focus();
}

function addPairElement(elementA, elementB) {
    const pairContainer = document.getElementById('pairContainer');
    const pairDiv = document.createElement('div');
    pairDiv.className = 'pair-item';
    
    const newPair = `<div class="boxText"><span class="elementoA">${elementA}</span> - <span class="elementoB">${elementB}</span></div>`;
    pairDiv.innerHTML = newPair;

    const removeButton = document.createElement('i');
    removeButton.className = 'fas fa-times';
    removeButton.title = 'Eliminar Pareja';

    removeButton.addEventListener('click', function() {
        pairContainer.removeChild(pairDiv);
        usedElements.delete(elementA);
        usedElements.delete(elementB);
        
        const index = couplesArray.findIndex(pair => pair === `${elementA} - ${elementB}`);
        if (index > -1) {
            couplesArray.splice(index, 1);
            updateCouplesInput();
        }
        scrollToBottom();
    });

    pairDiv.appendChild(removeButton);
    pairContainer.appendChild(pairDiv);

    // Solo agrega a couplesArray si no estaba en existingPairs
    if (!usedElements.has(elementA) && !usedElements.has(elementB)) {
        couplesArray.push(`${elementA} - ${elementB}`);
        updateCouplesInput();
    }

    usedElements.add(elementA);
    usedElements.add(elementB);
}


function updateCouplesInput() {
    document.getElementById('couples').value = couplesArray.join('; ');
}

function scrollToBottom() {
    const pairContainer = document.getElementById('pairContainer');
    pairContainer.scrollTop = pairContainer.scrollHeight;
}
