let inputRadios = document.querySelectorAll(".inputRadio");

inputRadios.forEach( (input) => {
	input.addEventListener("click", function(){
		disabledInputs();
		input.classList.toggle("inputRadio--checked")
	});
});

function disabledInputs() {
	inputRadios.forEach( (input) => {
		input.classList.remove("inputRadio--checked");
	});
}