//Selects que están en el html.
let selectState = document.getElementById("state");
let selectMunicipalitie = document.getElementById('municipalitie');
let selectParishe = document.getElementById('parishe');
let data = null;

//Modificamos los Municipios.
selectState.addEventListener("change", function() {
  	axios.get('/api/municipalities/' + selectState.value)
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        selectMunicipalitie.classList.remove('form__input--disabled');
        data = response.data;

        //Eliminamos primero los elementos
        while (selectMunicipalitie.firstChild) {
		    selectMunicipalitie.removeChild(selectMunicipalitie.firstChild);
		}
        
        //Agregamos los elementos que vienen en la API.
		let option = document.createElement("OPTION");
        option.disabled = true;
        option.selected = true;
		option.value = null;
		option.innerHTML = "Escoge una opción";
		selectMunicipalitie.appendChild(option);
        data.forEach( (item) => {
			option = document.createElement("OPTION");
        	option.selected = false;
        	option.disabled = false;
			option.value = item.id;
			option.innerHTML = item.name;
			selectMunicipalitie.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
});


//Modificamos las parroquias.
selectMunicipalitie.addEventListener("change", function() {
  	axios.get('/api/parishes/' + selectMunicipalitie.value)
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        selectParishe.classList.remove('form__input--disabled');
        data = response.data;

        //Eliminamos primero los elementos
        while (selectParishe.firstChild) {
		    selectParishe.removeChild(selectParishe.firstChild);
		}
        
        //Agregamos los elementos que vienen en la API.
        let option = document.createElement("OPTION");
        option.disabled = true;
        option.selected = true;
		option.value = null;
		option.innerHTML = "Escoge una opción";
		selectParishe.appendChild(option);
        data.forEach( (item) => {
			option = document.createElement("OPTION");
        	option.selected = false;
        	option.disabled = false;
			option.value = item.id;
			option.innerHTML = item.name;
			selectParishe.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
});