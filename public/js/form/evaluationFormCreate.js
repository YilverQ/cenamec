document.querySelectorAll('.menuNewContent__item--third').forEach(item => {
    item.addEventListener('click', function() {
        // Hide the menu
        document.querySelector('.menuNewContent').classList.add('hidden');
        
        // Show the forms container
        const boxEvaluationForms = document.querySelector('.boxEvaluationForms');
        boxEvaluationForms.classList.remove('hidden');
        
        // Hide all forms (divs)
        const forms = boxEvaluationForms.querySelectorAll('div[id^="form"]'); 
        forms.forEach(form => form.classList.add('hidden'));
        
        // Show the corresponding form
        const formId = this.id.replace('openForm', 'form');
        const targetForm = document.getElementById(formId);
        
        if (targetForm) {
            targetForm.classList.remove('hidden');
        }

        // Update the icon and title based on the selected item
        const iconImg = this.querySelector('img').src; // Get the image source from clicked item
        const titleText = this.querySelector('.menuNewContent__titleItem').textContent; // Get the title text
        
        // Set new icon and title
        document.getElementById('formIcon').innerHTML = `<img src="${iconImg}" class="iconLogoForm">`;
        document.getElementById('addElement').textContent = "Agregar Item de " + titleText;
    });
});


// Al hacer clic en el botón "Regresar"
document.getElementById('btnRegresar').addEventListener('click', function() {
    // Ocultar el contenedor de formularios
    document.querySelector('.boxEvaluationForms').classList.add('hidden');
    
    // Mostrar nuevamente el menú
    document.querySelector('.menuNewContent').classList.remove('hidden');
    
    // Restablecer el ícono y el título a su estado original
    document.getElementById('formIcon').innerHTML = `<i class="fa-solid fa-file-circle-plus"></i>`; // Ícono original
    document.getElementById('addElement').textContent = 'Agregar un Nuevo Item'; // Título original
});
