function updateProgressBar(part) {
    const progressBar = document.getElementById('progressBar');
    let progress = 0;

    if (part === 'part1') progress = 33;
    if (part === 'part2') progress = 66;
    if (part === 'part3') progress = 100;

    progressBar.style.width = progress + '%';
}

document.getElementById('next1').addEventListener('click', function() {
    document.getElementById('part1').style.display = 'none';
    document.getElementById('part2').style.display = 'block';
    updateProgressBar('part2');
});

document.getElementById('prev2').addEventListener('click', function() {
    document.getElementById('part2').style.display = 'none';
    document.getElementById('part1').style.display = 'block';
    updateProgressBar('part1');
});

document.getElementById('next2').addEventListener('click', function() {
    document.getElementById('part2').style.display = 'none';
    document.getElementById('part3').style.display = 'block';
    updateProgressBar('part3');
});

document.getElementById('prev3').addEventListener('click', function() {
    document.getElementById('part3').style.display = 'none';
    document.getElementById('part2').style.display = 'block';
    updateProgressBar('part2');
});

document.addEventListener('DOMContentLoaded', function() {
    updateProgressBar('part1');  // Establece el progreso en 33% al cargar
});