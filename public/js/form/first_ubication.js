//Selects que están en el html.
let selectState = document.getElementById("state");
let selectMunicipalitie = document.getElementById('municipalitie');
let selectParishe = document.getElementById('parishe');
let data = null;


//Modificamos los Municipios.
function getMunicipalities() {
    axios.get('/api/municipalities/' + selectState.value)
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectMunicipalitie.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getParishes() {
    axios.get('/api/parishes/' + selectMunicipalitie.value)
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectParishe.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}


getMunicipalities();
getParishes();