// Función para configurar un selector
function setupSelectSearch(wrapperIndex, apiUrl, hiddenInputId) {
    const wrapper = document.querySelectorAll(".select-search")[wrapperIndex],
        selectBtn = wrapper.querySelector(".select-btn"),
        searchInp = wrapper.querySelector(".searchInput"),
        spanSelect = wrapper.querySelector(".spanSelect"),
        options = wrapper.querySelector(".options");
    
    const inputHidden = document.getElementById(hiddenInputId);
    let data = null;

    function getAcademicData() {
        axios.get(apiUrl)
            .then(function (response) {
                data = response.data;
                let selectOptions = data.map(item => item.name);
                let selectIDs = data.map(item => item.id);

                // Función específica para este selector
                function updateName(selectedLi) {
                    searchInp.value = "";
                    addCountry(selectedLi.innerText);
                    wrapper.classList.remove("active");
                    spanSelect.innerText = selectedLi.innerText;
                    let position = selectOptions.indexOf(selectedLi.innerText);
                    inputHidden.value = selectIDs[position];
                }

                function addCountry(selectedCountry) {
                    options.innerHTML = "";
                    selectOptions.forEach(elementOption => {
                        let isSelected = elementOption == selectedCountry ? "selected" : "";
                        let li = document.createElement("li");
                        li.className = isSelected;
                        li.innerText = elementOption;
                        li.addEventListener("click", function() {
                            updateName(this); // Llama a updateName en el contexto adecuado
                        });
                        options.appendChild(li);
                    });
                }
                addCountry();

                searchInp.addEventListener("keyup", () => {
                    let arr = [];
                    let searchWord = searchInp.value.toLowerCase();
                    arr = selectOptions.filter(data => {
                        return data.toLowerCase().includes(searchWord);
                    }).map(data => {
                        let isSelected = data == selectBtn.firstElementChild.innerText ? "selected" : "";
                        return `<li class="${isSelected}">${data}</li>`; // Generar solo el HTML
                    }).join("");
                    options.innerHTML = arr ? arr : `<p style="margin-top: 10px; color:#d93e4c;">Elemento no encontrado</p>`;

                    // Agregar el evento click a los nuevos elementos generados
                    options.querySelectorAll("li").forEach(li => {
                        li.addEventListener("click", function() {
                            updateName(this); // Llama a updateName en el contexto adecuado
                        });
                    });
                });

                selectBtn.addEventListener("click", () => wrapper.classList.toggle("active"));
                // Cerrar el wrapper si se hace clic fuera de él
                document.addEventListener("click", (event) => {
                    if (!wrapper.contains(event.target) && !selectBtn.contains(event.target)) {
                        wrapper.classList.remove("active");
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getAcademicData();
}

// Configurar los selectores para instituciones y carreras
setupSelectSearch(0, '/api/get/institutions', "hiddenIntitution");
setupSelectSearch(1, '/api/get/careers', "hiddenCarrer");