// Obtener elementos del DOM
const buttonOpenForm = document.getElementById("buttonOpenForm");
const boxFormFloat = document.getElementById("boxFormFloat");
const xmarkCloseForm = document.getElementById("xmarkCloseForm")

buttonOpenForm.addEventListener('click', () =>{
    boxFormFloat.classList.toggle("containerFormFloat--hidden");
});

xmarkCloseForm.addEventListener('click', () =>{
    boxFormFloat.classList.toggle("containerFormFloat--hidden");
});