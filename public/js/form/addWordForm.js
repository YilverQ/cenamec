document.addEventListener('DOMContentLoaded', function() {
    // Obtener las palabras existentes desde el atributo 'data-existing-words'
    const tagListDiv = document.getElementById('tagList');
    const existingWords = JSON.parse(tagListDiv.getAttribute('data-existing-words')) || [];

    // Si hay palabras existentes, agregar esas al array 'words'
    const words = [...existingWords];  // Iniciar 'words' con las palabras existentes

    // Llamamos a la función que renderiza las palabras ya existentes
    renderTags();

    // Evento de agregar palabra con el botón
    document.getElementById('addWordButton').addEventListener('click', addWord);

    // Evento de agregar palabra al presionar "Enter"
    document.getElementById('word').addEventListener('keypress', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault(); // Evitar el envío del formulario
            addWord();
        }
    });

    // Función para mostrar las palabras como etiquetas
    function renderTags() {
        const tagList = document.getElementById('tagList');
        tagList.innerHTML = '';  // Limpiar las etiquetas anteriores

        words.forEach(word => {
            const newTag = document.createElement('span');
            newTag.className = 'tag';
            newTag.textContent = word;

            const removeButton = document.createElement('span');
            removeButton.className = 'tag-remove';
            removeButton.textContent = '×';
            removeButton.onclick = function() {
                tagList.removeChild(newTag);
                words.splice(words.indexOf(word), 1); // Eliminar palabra del array
                updateWordsField();  // Sincronizar el campo oculto
            };

            newTag.appendChild(removeButton);
            tagList.appendChild(newTag);
        });

        updateWordsField();  // Actualizar el campo oculto con el array de palabras
    }

    // Función para actualizar el campo oculto con el array de palabras
    function updateWordsField() {
        document.getElementById('wordAdd').value = JSON.stringify(words);
    }

    // Función para agregar una nueva palabra
    function addWord() {
        const input = document.getElementById('word');
        const value = removeAccents(input.value.trim().toUpperCase()); // Eliminar acentos y pasar a mayúsculas

        if (value) {
            if (!words.includes(value)) {  // Verificar si la palabra no está duplicada
                words.push(value);  // Agregar al array
                renderTags();  // Renderizar nuevamente las etiquetas
                input.value = '';  // Limpiar el campo de entrada
            } else {
                alert('Esta palabra ya ha sido añadida.');
            }
        } else {
            alert('Por favor, escribe una palabra.');
        }
    }

    // Función para quitar los acentos de las palabras
    function removeAccents(str) {
        const accents = 'áéíóúÁÉÍÓÚñÑ';
        const noAccents = 'aeiouAEIOUnN';
        let newStr = '';

        for (let i = 0; i < str.length; i++) {
            const index = accents.indexOf(str[i]);
            newStr += index !== -1 ? noAccents[index] : str[i];
        }

        return newStr;
    }
});
