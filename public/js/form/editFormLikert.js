// Obtener elementos del DOM para el formulario de edición
const editItemButtons = document.querySelectorAll('.editItem');
const boxEditFormFloat = document.getElementById("boxEditFormFloat");
const xmarkCloseEditForm = document.getElementById("xmarkCloseEditForm");
const editAffirmationInput = document.getElementById("editAffirmation");
const editItemIdInput = document.getElementById("editItemId");

editItemButtons.forEach(button => {
    button.addEventListener('click', (event) => {
        const itemId = event.currentTarget.getAttribute('data-id');
        const affirmationText = event.currentTarget.closest('tr').querySelector('td').innerText;

        // Llenar los campos del formulario con los datos del elemento
        editAffirmationInput.value = affirmationText;
        editItemIdInput.value = itemId;

        // Mostrar el formulario de edición
        boxEditFormFloat.classList.remove("containerFormFloat--hidden");
    });
});

xmarkCloseEditForm.addEventListener('click', () => {
    boxEditFormFloat.classList.toggle("containerFormFloat--hidden");
});
