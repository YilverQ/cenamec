// Obtener los elementos del DOM
const eyePassword = document.getElementById('form_eye'); // Icono del ojo para la contraseña
const eyeRePassword = document.getElementById('re-form_eye'); // Icono del ojo para repetir contraseña
const passwordForm = document.getElementById("password"); // Campo de contraseña
const rePasswordForm = document.getElementById("re-password"); // Campo de repetir contraseña

// Función para alternar la visibilidad de la contraseña
function togglePasswordVisibility(inputField, eyeIcon) {
    eyeIcon.classList.toggle("fa-eye");
    eyeIcon.classList.toggle("fa-eye-slash");
    inputField.type = (inputField.type === "password") ? "text" : "password";
}

// Evento para el icono del ojo de la contraseña
eyePassword.addEventListener("click", () => {
    togglePasswordVisibility(passwordForm, eyePassword);
});

// Evento para el icono del ojo de repetir contraseña
eyeRePassword.addEventListener("click", () => {
    togglePasswordVisibility(rePasswordForm, eyeRePassword);
});