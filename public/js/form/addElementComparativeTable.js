const elementsA = []; // Array para almacenar los elementos de la columna A
const elementsB = []; // Array para almacenar los elementos de la columna B
const elementsC = []; // Array para almacenar los elementos de la columna C

function updateElementsField() {
    document.getElementById('elementsA').value = JSON.stringify(elementsA);
    document.getElementById('elementsB').value = JSON.stringify(elementsB);
    document.getElementById('elementsC').value = JSON.stringify(elementsC);
}

// Función para agregar un nuevo elemento a la columna especificada
function addElement(column) {
    const input = document.getElementById(`input${column}`);
    const value = input.value.trim();

    if (value) {
        let elementsArray;
        const tagList = document.getElementById(`tagList${column}`);

        // Determinar el array correspondiente
        if (column === 'A') {
            elementsArray = elementsA;
        } else if (column === 'B') {
            elementsArray = elementsB;
        } else {
            elementsArray = elementsC;
        }

        // Verificar si el elemento ya existe en cualquier columna
        if (!elementsA.includes(value) && !elementsB.includes(value) && !elementsC.includes(value)) {
            if (!elementsArray.includes(value)) {
                elementsArray.push(value); // Agregar el elemento al array
                updateElementsField(); // Actualizar los campos ocultos

                // Crear y mostrar la etiqueta
                const newTag = document.createElement('span');
                newTag.className = 'tag';
                newTag.textContent = value;

                const removeButton = document.createElement('span');
                removeButton.className = 'tag-remove';
                removeButton.textContent = '×';
                removeButton.onclick = function() {
                    tagList.removeChild(newTag);
                    elementsArray.splice(elementsArray.indexOf(value), 1); // Eliminar el elemento del array
                    updateElementsField(); // Actualizar los campos ocultos
                    scrollToBottom(column); // Asegurarse de que el scroll esté al final después de eliminar
                };

                newTag.appendChild(removeButton);
                tagList.appendChild(newTag);

                input.value = ''; // Limpiar el input

                // Desplazar el scroll hacia abajo
                scrollToBottom(column);
            } else {
                alert('Este elemento ya ha sido añadido a esta columna.'); // Mensaje si el elemento ya existe en la misma columna
            }
        } else {
            alert('Este elemento ya ha sido añadido a una columna.'); // Mensaje si el elemento ya existe en otra columna
        }
    } else {
        alert('Por favor, escribe un elemento.'); // Mensaje si el input está vacío
    }
}

// Función para crear etiquetas desde valores preexistentes
function createTag(column, value) {
    const tagList = document.getElementById(`tagList${column}`);
    
    const newTag = document.createElement('span');
    newTag.className = 'tag';
    newTag.textContent = value;

    const removeButton = document.createElement('span');
    removeButton.className = 'tag-remove';
    removeButton.textContent = '×';
    removeButton.onclick = function() {
        tagList.removeChild(newTag);
        let elementsArray;
        if (column === 'A') {
            elementsArray = elementsA;
        } else if (column === 'B') {
            elementsArray = elementsB;
        } else {
            elementsArray = elementsC;
        }

        elementsArray.splice(elementsArray.indexOf(value), 1);
        updateElementsField();
        scrollToBottom(column);
    };

    newTag.appendChild(removeButton);
    tagList.appendChild(newTag);
}

// Función para inicializar los valores preexistentes en los arrays y mostrarlos como etiquetas
function initializeTags() {
    // Obtener los valores preexistentes de los atributos data-existing-words
    const existingA = JSON.parse(document.getElementById('tagListA').getAttribute('data-existing-words')) || [];
    const existingB = JSON.parse(document.getElementById('tagListB').getAttribute('data-existing-words')) || [];
    const existingC = JSON.parse(document.getElementById('tagListC').getAttribute('data-existing-words')) || [];

    // Agregar los valores preexistentes a los arrays
    elementsA.push(...existingA);
    elementsB.push(...existingB);
    elementsC.push(...existingC);

    // Crear las etiquetas para los elementos preexistentes
    existingA.forEach(value => createTag('A', value));
    existingB.forEach(value => createTag('B', value));
    existingC.forEach(value => createTag('C', value));

    // Actualizar los campos ocultos con los elementos preexistentes
    updateElementsField();
}

// Agregar eventos después de que el DOM esté completamente cargado
document.addEventListener('DOMContentLoaded', function() {
    // Inicializar las etiquetas y los valores preexistentes
    initializeTags();

    // Agregar eventos para los íconos de agregar elementos
    document.querySelectorAll('.add-element-icon').forEach(icon => {
        icon.addEventListener('click', function() {
            const columnId = this.closest('.tableColumn').querySelector('input.tableColumn__title').name;
            const columnLetter = columnId.replace('titleColumn', ''); // Extraer A, B o C
            addElement(columnLetter);
        });
    });

    // Agregar eventos para permitir agregar elementos al presionar Enter
    ['A', 'B', 'C'].forEach(column => {
        const inputField = document.getElementById(`input${column}`);
        inputField.addEventListener('keypress', function(event) {
            if (event.key === 'Enter') {
                event.preventDefault(); // Evitar el envío del formulario
                addElement(column);
            }
        });
    });

    handleTabKey();
});

// Función para manejar el evento TAB
function handleTabKey() {
    const titles = [
        document.getElementById('titleColumnA'),
        document.getElementById('titleColumnB'),
        document.getElementById('titleColumnC')
    ];
    
    const inputs = [
        document.getElementById('inputA'),
        document.getElementById('inputB'),
        document.getElementById('inputC')
    ];

    titles.forEach((title, index) => {
        title.addEventListener('keydown', function(event) {
            if (event.key === 'Tab') {
                event.preventDefault();

                if (event.shiftKey) { 
                    if (index === 0) { 
                        inputs[inputs.length - 1].focus(); 
                    } else { 
                        titles[index - 1].focus(); 
                    }
                } else { 
                    if (index === titles.length - 1) { 
                        inputs[0].focus(); 
                    } else { 
                        titles[index + 1].focus(); 
                    }
                }
            }
        });
    });

    inputs.forEach((input, index) => {
        input.addEventListener('keydown', function(event) {
            if (event.key === 'Tab') {
                event.preventDefault();

                if (event.shiftKey) { 
                    if (index === 0) { 
                        titles[titles.length - 1].focus(); 
                    } else { 
                        inputs[index - 1].focus(); 
                    }
                } else { 
                    if (index === inputs.length - 1) { 
                        titles[0].focus(); 
                    } else { 
                        inputs[index + 1].focus(); 
                    }
                }
            }
        });
    });
}

function scrollToBottom(column) {
    const tagList = document.getElementById(`tagList${column}`);
    tagList.scrollTop = tagList.scrollHeight; // Mover el scroll al final
}
