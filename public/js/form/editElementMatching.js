document.addEventListener('DOMContentLoaded', function() {
    // Agregar evento al botón de cerrar
    document.getElementById('xmarkCloseEditBoxWindows').addEventListener('click', hideEditBox);

    // Agregar evento a los botones "Editar" (suponiendo que ya tienes este código)
    const editButtons = document.querySelectorAll('.buttonAdd--edit');

    editButtons.forEach(button => {
        button.addEventListener('click', function() {
            const itemId = this.getAttribute('data-item-id'); // Obtener el ID del atributo data
            loadItemData(itemId); // Llamar a la función con el ID
            showEditBox(); // Mostrar la ventana de edición
        });
    });
});

// Función para mostrar la ventana de edición
function showEditBox() {
    const editBox = document.getElementById('editBoxWindows');
    editBox.classList.remove('containerFormFloat--hidden'); // Muestra la ventana
}

// Función para ocultar la ventana de edición
function hideEditBox() {
    const editBox = document.getElementById('editBoxWindows');
    editBox.classList.add('containerFormFloat--hidden'); // Oculta la ventana
}

// Función para cargar los datos en el formulario de edición (ya proporcionada anteriormente)
// Función para cargar los datos en el formulario de edición (ya proporcionada anteriormente)
function loadItemData(itemId) {
    axios.get(`/api/get/itemEvaluation/${itemId}`)
        .then(function (response) {
            const itemData = response.data.item; // Datos del ítem
            const pairsData = response.data.pairs; // Datos de los pares

            const formEditMatching = document.getElementById("formEditMatching");
            formEditMatching.querySelector('#statement').value = itemData.statement;
            formEditMatching.querySelector('#feedback').value = itemData.feedback;

            // Limpiar contenedor de pares antes de agregar nuevos
            const pairContainer = document.getElementById("pairContainer--edit");
            pairContainer.innerHTML = ''; // Limpiar contenido previo

            pairsData.forEach(pair => {
                // Crear un nuevo contenedor para el par
                const pairDiv = document.createElement('div');
                pairDiv.className = 'pair-item'; // Clase para el par

                // Crear un contenedor para el texto
                const textContainer = document.createElement('div');
                textContainer.className = 'boxText'; // Clase para el contenedor de texto

                // Agregar los elementos A y B al contenedor de texto
                textContainer.innerHTML = `
                    <span class="elementoA">${pair.elementA}</span> - 
                    <span class="elementoB">${pair.elementB}</span>
                `;

                // Crear el botón de eliminar
                const removeButton = document.createElement('i');
                removeButton.className = 'fas fa-times remove-button';
                removeButton.title = 'Eliminar Pareja';
                
                // Agregar evento al botón de eliminar
                removeButton.addEventListener('click', function() {
                    pairDiv.remove(); // Elimina el par del DOM
                    // Aquí podrías agregar lógica adicional para eliminarlo del servidor si es necesario
                    console.log(`Eliminado: ${pair.elementA} - ${pair.elementB}`);
                });

                // Agregar el contenedor de texto y el botón al par
                pairDiv.appendChild(textContainer);
                pairDiv.appendChild(removeButton);

                // Agregar el par al contenedor principal
                pairContainer.appendChild(pairDiv);
            });
        })
        .catch(function (error) {
            console.error("Error al cargar los datos del ítem:", error);
        });
}
