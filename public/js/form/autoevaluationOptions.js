document.addEventListener('DOMContentLoaded', function () {
    const tableCells = document.querySelectorAll('.autoevaluationCard td');

    tableCells.forEach(cell => {
        cell.addEventListener('click', function () {
            const input = this.querySelector('input[type="radio"]');
            if (input) {
                input.checked = true;
            }
        });
    });
});
