//Selects que están en el html.
const openAcademicForm = document.getElementById("OpenAcademicForm");
let selectAcademicLevel = document.getElementById('academicLevel');
let selectSector = document.getElementById('sector');
let selectArea = document.getElementById('area');
let data = null;
let count = 0

openAcademicForm.addEventListener("click", ()=>{
    if (count == 0) {
        getAcademicLevels();
        getSectors();
        getAreas();

        getInstitutions();
        getCareers();
    }
    count ++;
})


function getAcademicLevels() {
    axios.get('/api/get/academic/level')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;
        
        let optionDisabled = document.createElement("OPTION");
            optionDisabled.selected = true;
            optionDisabled.disabled = true;
            optionDisabled.innerHTML = "Seleccionar el nivel acádemico";
            selectAcademicLevel.appendChild(optionDisabled);

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            option.classList.add("optionAcademic")
            selectAcademicLevel.appendChild(option);
        });
    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getInstitutions() {
    axios.get('/api/get/institutions')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        let optionDisabled = document.createElement("OPTION");
            optionDisabled.selected = true;
            optionDisabled.disabled = true;
            optionDisabled.innerHTML = "Seleccionar la institutición";
        

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getCareers() {
    axios.get('/api/get/careers')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;
        
        let optionDisabled = document.createElement("OPTION");
            optionDisabled.selected = true;
            optionDisabled.disabled = true;
            optionDisabled.innerHTML = "Seleccionar la carrera";

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getSectors() {
    axios.get('/api/get/sectors')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        let optionDisabled = document.createElement("OPTION");
            optionDisabled.selected = true;
            optionDisabled.disabled = true;
            optionDisabled.innerHTML = "Seleccionar el ámbito";
            selectSector.appendChild(optionDisabled);
        

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectSector.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}

function getAreas() {
    axios.get('/api/get/areas')
    .then(function (response) {
        // Recibes los datos de los municipios en response.data
        data = response.data;

        let optionDisabled = document.createElement("OPTION");
            optionDisabled.selected = true;
            optionDisabled.disabled = true;
            optionDisabled.innerHTML = "Seleccionar el área";
            selectArea.appendChild(optionDisabled);
        

        //Agregamos los elementos que vienen en la API.
        data.forEach( (item) => {
            let option = document.createElement("OPTION");
            option.selected = false;
            option.disabled = false;
            option.value = item.id;
            option.innerHTML = item.name;
            selectArea.appendChild(option);
        });

    })
    .catch(function (error) {
        // Manejo de errores
        console.log(error);
    });
}