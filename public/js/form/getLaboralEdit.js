//Selects que están en el html.
let openIconForms       = document.querySelectorAll(".editLaboralData");
let classText, number   = "";
let idLaboral          = document.getElementById('idLaboral');

openIconForms.forEach((icon)=> {
    icon.addEventListener("click", function() {
        classText = icon.classList[2];
        number = classText.match(/\d+/)[0];
        getDataLaboral(number);
        idLaboral.value = number;
    });
})

//Selects que están en el html.
function getDataLaboral(number) {
    axios.get('/api/get/dataLaboral/'+number)
    .then(function (response) {
            let data = response.data;
            change_values_default(data);
    })
    .catch(function (error) {
        console.log(error);
    });
}


// Función para cambiar los valores de los inputs
function change_values_default(data) {
    // Cambiar los valores de los inputs
    let inputIDs = {
        'name_intitution': document.getElementById('name_institutionEdit'),
        'position'       : document.getElementById('positionEdit'),
        'year_entry'     : document.getElementById('year_entryLaboralEdit'),
        'year_exit'      : document.getElementById('year_exitLaboralEdit'),
        'type'      : document.getElementById('hiddenTypeIntitutionEdit'),
        'area'      : document.getElementById('hiddenAreaIntitutionEdit')
    }
    Object.entries(inputIDs).forEach(([key, input]) => {
        input.value = data[key];
    });

    // Cambiar los valores de los selects
    let labelSelectIDs = {
        'type'      : document.getElementById('typeEdit'),
        'area'      : document.getElementById('areaLaboralEdit')
    }
    Object.entries(labelSelectIDs).forEach(([key, label]) => {
        label.innerHTML = data[key];
    });
}
