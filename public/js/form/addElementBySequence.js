document.addEventListener('DOMContentLoaded', function () {
    const sequenceList = document.getElementById('sequenceList');
    const sequenceField = document.getElementById('sequence');
    const addSequenceButton = document.getElementById('addSequenceButton');
    const sequenceItemInput = document.getElementById('sequenceItem');
    const infoAlert = document.querySelector('.infoSequenceItem');

    // Obtener secuencias desde el atributo data
    const sequencesData = sequenceList.dataset.sequences;
    const sequences = sequencesData ? JSON.parse(sequencesData) : [];

    // Cargar secuencias existentes al cargar la página
    if (sequences.length > 0) {
        sequences.forEach((sequence, index) => {
            addSequenceItem(sequence.description, index + 1);
        });
        updateHiddenField();
        validateSequenceCount();
    }

    // Función para agregar un elemento a la lista
    function addSequenceItem(value, index = null) {
        const itemValue = (typeof value === 'string') ? value.trim() : sequenceItemInput.value.trim();
        
        // Verificar duplicados
        const existingItems = Array.from(sequenceList.getElementsByClassName('sequence-item'));
        const isDuplicate = existingItems.some(item => item.textContent.replace(/^\d+\.\s*/, '').trim() === itemValue);

        if (itemValue && !isDuplicate) {
            const sequenceItem = document.createElement('p');
            sequenceItem.className = 'sequence-item';
            
            const currentCount = index || sequenceList.children.length + 1;
            sequenceItem.innerHTML = `<span class="item-sequence">${currentCount}. </span>${itemValue}`;

            const removeButton = document.createElement('i');
            removeButton.className = 'fas fa-times remove-sequence-icon';
            removeButton.setAttribute('title', 'Quitar Elemento');
            removeButton.onclick = function () {
                sequenceList.removeChild(sequenceItem);
                updateHiddenField();
                validateSequenceCount();
                updateOrder();
                scrollToBottom();
            };

            sequenceItem.appendChild(removeButton);
            sequenceList.appendChild(sequenceItem);

            if (!value) {
                sequenceItemInput.value = '';
            }
            
            updateHiddenField();
            validateSequenceCount();
            scrollToBottom();
        } else if (isDuplicate) {
            alert("Este elemento ya existe en la lista.");
        }
    }

    // Evento para agregar elemento al hacer clic
    addSequenceButton.addEventListener('click', addSequenceItem);

    // Evento para agregar al presionar "Enter"
    sequenceItemInput.addEventListener('keypress', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            addSequenceItem();
        }
    });

    // Actualizar campo oculto con los valores
    function updateHiddenField() {
        const items = Array.from(sequenceList.getElementsByClassName('sequence-item'));
        const itemsArray = items.map(item => item.textContent.replace(/^\d+\.\s*/, '').trim());
        sequenceField.value = itemsArray.join(',');
    }

    // Validar cantidad mínima de elementos
    function validateSequenceCount() {
        const itemCount = sequenceList.children.length;
        itemCount < 2 ? infoAlert.classList.remove('hidden') : infoAlert.classList.add('hidden');
    }

    // Actualizar el orden de la lista
    function updateOrder() {
        const items = Array.from(sequenceList.children);
        items.forEach((item, index) => {
            const orderSpan = item.querySelector('.item-sequence');
            orderSpan.textContent = `${index + 1}. `;
        });
    }

    // Desplazar scroll al final
    function scrollToBottom() {
        sequenceList.scrollTop = sequenceList.scrollHeight;
    }
});
