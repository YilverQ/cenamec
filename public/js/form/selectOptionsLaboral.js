function setupSelectSearch(wrapperIndex, areas, hiddenInputId) {
    const wrapper = document.querySelectorAll(".select-search-laboral")[wrapperIndex],
        selectBtn = wrapper.querySelector(".select-btn"),
        searchInp = wrapper.querySelector(".searchInput"),
        spanSelect = wrapper.querySelector(".spanSelect"),
        options = wrapper.querySelector(".options");
    
    const inputHidden = document.getElementById(hiddenInputId);
    let data = areas; // Use the provided areas directly

    function updateName(selectedLi) {
        searchInp.value = "";
        addCountry(selectedLi.innerText);
        wrapper.classList.remove("active");
        spanSelect.innerText = selectedLi.innerText;
        inputHidden.value = selectedLi.innerText;
    }

    function addCountry(selectedCountry) {
        options.innerHTML = "";
        data.forEach(elementOption => {
            let isSelected = elementOption === selectedCountry ? "selected" : "";
            let li = document.createElement("li");
            li.className = isSelected;
            li.innerText = elementOption;
            li.addEventListener("click", function() {
                updateName(this);
            });
            options.appendChild(li);
        });
    }
    addCountry(); // Initialize with all options

    searchInp.addEventListener("keyup", () => {
        let arr = [];
        let searchWord = searchInp.value.toLowerCase();
        arr = data.filter(item => {
            return item.toLowerCase().includes(searchWord);
        }).map(item => {
            let isSelected = item === selectBtn.firstElementChild.innerText ? "selected" : "";
            return `<li class="${isSelected}">${item}</li>`;
        }).join("");
        options.innerHTML = arr ? arr : `<p style="margin-top: 10px; color:#d93e4c;">Elemento no encontrado</p>`;

        options.querySelectorAll("li").forEach(li => {
            li.addEventListener("click", function() {
                updateName(this);
            });
        });
    });

    selectBtn.addEventListener("click", () => wrapper.classList.toggle("active"));

    document.addEventListener("click", (event) => {
        if (!wrapper.contains(event.target) && !selectBtn.contains(event.target)) {
            wrapper.classList.remove("active");
        }
    });
}

// Configurar los selectores para áreas y tipos de empresas
const areas = [
    'Tecnología', 'Investigación', 'Educación', 'Salud', 'Ingeniería',
    'Administración', 'Ciencias Sociales', 'Arte y Diseño', 'Marketing',
    'Finanzas', 'Recursos Humanos', 'Turismo', 'Construcción',
    'Logística', 'Medio Ambiente', 'Comunicación', 'Desarrollo Sostenible',
    'Ciberseguridad', 'Energías Renovables', 'Emprendimiento'
];

const tipos_empresas = [
    'Gobierno', 'Empresa Pública', 'Empresa Privada', 'Empresa Mixta',
    'Educación Universitaria', 'Organización no Gubernamental', 'Poder Popular',
    'Emprendimiento'
];
areas.sort();
tipos_empresas.sort();

// Initialize the selectors with the predefined arrays
setupSelectSearch(0, tipos_empresas, "hiddenTypeIntitution");
setupSelectSearch(1, areas, "hiddenAreaIntitution");
setupSelectSearch(2, tipos_empresas, "hiddenTypeIntitutionEdit");
setupSelectSearch(3, areas, "hiddenAreaIntitutionEdit");