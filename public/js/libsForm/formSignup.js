// Mensajes de error
const messagesInputs = {
    firts_name: 'Debes agregar un nombre real.',
    lastname: 'Debes agregar un apellido real.',
    gender: 'Debes escoger una opción.',
    birthdate:  'Debes escoger una fecha de nacimiento válida.',
    identification_card: 'La Cédula de Identidad es incorrecta.',
    number_phone: 'Debes agregar un número de teléfono válido. Ejemplo: 04160001010',
    email: 'Debes agregar un correo electrónico válido.',
    password: 'Debes agregar una contraseña entre 4 y 20 caracteres alfanumericos y signos especiales: .+*=#$%&?',
    re_password: 'La contraseña no coinciden.',
    state: 'Debes escoger una opción.',
    municipalitie: 'Debes escoger una opción.',
    parishe: 'Debes escoger una opción.',
};

// Expresiones regulares
const regexInputs = {
    firts_name: /^[a-zA-ZÀ-ÿ]+( [a-zA-ZÀ-ÿ]+)*$/,
    lastname: /^[a-zA-ZÀ-ÿ]+( [a-zA-ZÀ-ÿ]+)*$/,
    gender: /^(Masculino|Femenino)$/,
    birthdate: /^(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/,
    identification_card: /^\d{6,8}$/,
    number_phone: /^((0416)|(0212)|(0426)|(0412)|(0414)|(0424))\d{7}$/,
    email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    password: /^[a-zA-Z0-9_.+*=#$%&?]{4,20}$/,
};

// Función para validar cada input
function validateInput(input) {
    const name = input.name;
    console.log(name)
    const value = input.value.trim();
    
    // Comprobar si el campo está vacío
    if (!value) {
        showError(input, "Este campo es obligatorio.");
        return false;
    }

    // Validar con la expresión regular correspondiente
    const regex = regexInputs[name];
    
    if (regex && !regex.test(value)) {
        showError(input, messagesInputs[name]);
        return false;
    }

    // Verificar coincidencia de contraseñas
    if (name === "re-password") {
        const passwordInput = document.querySelector('input[name="password"]');
        if (passwordInput.value !== value) {
            showError(input, messagesInputs.re_password);
            return false;
        }
    }

    // Validar fecha de nacimiento
    if (name === "birthdate") {
        if (!validateBirthdate(value)) {
            showError(input, messagesInputs.birthdate);
            return false;
        }
    }

    // Si pasa todas las validaciones
    clearError(input);
    return true;
}

// Función para validar la fecha de nacimiento
function validateBirthdate(birthdate) {
    const today = new Date();
    const birthDateObj = new Date(birthdate);
  
    // Calcular la edad
    let age = today.getFullYear() - birthDateObj.getFullYear();
    
    const monthDiff = today.getMonth() - birthDateObj.getMonth();
  
    // Ajustar si no ha cumplido años este año
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDateObj.getDate())) {
        age--;
    }

    // Verificar si es menor de edad
    return age >= 5 && age <= 80; // Retorna true si es mayor o igual a 18 años
}

// Mostrar mensaje de error
function showError(input, message) {
    input.classList.add("form__input--errorField");
    input.classList.remove("form__input--successField");

    let errorMessage = input.parentNode.querySelector(".form__message-error");
    
    // Crear el mensaje de error si no existe
    if (!errorMessage) {
        errorMessage = document.createElement("P");
        errorMessage.classList.add("form__message-error");
        input.parentNode.appendChild(errorMessage);
    }
    
    errorMessage.innerHTML = message;
}

// Limpiar mensaje de error
function clearError(input) {
    input.classList.remove("form__input--errorField");
    
    const errorMessage = input.parentNode.querySelector(".form__message-error");
    
    if (errorMessage) {
        errorMessage.remove();
    }
}

// Validar todos los inputs del formulario
function validateForm() {
    const formToValidate = document.getElementById("formProfile");
    const inputs = formToValidate.querySelectorAll(".form__input");
    let isValid = true;
    
    inputs.forEach(input => {
        if (!validateInput(input)) {
            isValid = false; // Si algún campo no es válido, cambiar a falso
        }
    });



    return isValid;
}

// Evento al enviar el formulario
//sendButtonProfile
const sendButton = document.getElementById('sendButtonProfile');
sendButton.addEventListener('click', (e) => {
    if (!validateForm()) {
        e.preventDefault(); // Evitar el envío del formulario si hay errores
        alert("Por favor, corrige los campos resaltados en rojo.");
    }
});


/*Formulario de varias partes*/
document.addEventListener('DOMContentLoaded', function() {
    // Verificar si la primera parte del formulario existe
    const part1 = document.getElementById('part1');
    
    if (part1) {
        // Manejar el evento de clic para el botón "Siguiente" en la primera parte
        document.getElementById('nextPart1').addEventListener('click', function(e) {
            const part1Inputs = document.querySelectorAll("#part1 .form__input");
            let isValid = true;

            // Validar solo los inputs de la primera parte
            part1Inputs.forEach(input => {
                if (!validateInput(input)) {
                    isValid = false; // Si algún campo no es válido, cambiar a falso
                }
            });

            console.log('Is valid?', isValid); // Depuración

            if (isValid) {
                // Si todos los campos son válidos, ocultar la primera parte y mostrar la segunda parte
                document.getElementById('part1').style.display = 'none';
                document.getElementById('part2').style.display = 'block';
            } else {
                // Si algún campo no es válido, prevenir el avance
                e.preventDefault(); // Prevenir el avance
                alert("Por favor, corrige los campos resaltados en rojo.");
            }
        });
    }

    // Manejar el evento de clic para el botón "Regresar" en la segunda parte
    const prevPart2Button = document.getElementById('prevPart2');
    if (prevPart2Button) {
        prevPart2Button.addEventListener('click', function() {
            // Ocultar la segunda parte y mostrar la primera parte
            document.getElementById('part2').style.display = 'none';
            document.getElementById('part1').style.display = 'block';
        });
    }
});
