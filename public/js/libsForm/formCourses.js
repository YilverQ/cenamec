// Mensajes de error
const messagesInputs = {
    super_name: 'Debes agregar un nombre para el curso.',
    img: 'Debes seleccionar una imagen para el curso.',
    status: 'Debes seleccionar un estado para el curso.',
    level: 'Debes seleccionar un nivel para el curso.',
    purpose: 'Debes agregar un propósito.',
    general_objetive: 'Debes agregar un objetivo general.',
    competence: 'Debes agregar competencias.',
    teachers: 'Debes seleccionar al menos un profesor.',
    target_audience: 'Debes seleccionar al menos una audiencia.',
    tag: 'Debes seleccionar al menos una categoría.',
};

// Expresiones regulares
const regexInputs = {
    super_name: /^[a-zA-ZÀ-ÿ0-9\s,.'!¿?()&-]*$/
    img: /.+/,  // La validación para la imagen ahora será solo si está vacía o no
    status: /^(En Desarrollo|Publicado|Archivado)$/,
    level: /^(Básico|Intermedio|Avanzado)$/,
    purpose: /.+/,
    general_objetive: /.+/,
    competence: /.+/,
    teachers: /.+/,
    target_audience: /.+/,
    tag: /.+/,
};

// Función para validar cada input
function validateInput(input) {
    const name = input.name;
    const value = input.value.trim();

    // Si el campo no es tipo 'file', validamos si está vacío
    if (input.type !== 'file') {
        if (!value) {
            showError(input, "Este campo es obligatorio.");
            return false;
        }

        // Validar con la expresión regular correspondiente
        const regex = regexInputs[name];
        if (regex && !regex.test(value)) {
            showError(input, messagesInputs[name]);
            return false;
        }
    } else { // Para el campo de tipo 'file', verificamos si se seleccionó un archivo
        if (!input.files.length) {
            showError(input, messagesInputs.img);
            return false;
        }
    }

    // Si pasa todas las validaciones
    clearError(input);
    return true;
}

// Mostrar mensaje de error
function showError(input, message) {
    input.classList.add("form__input--errorField");
    input.classList.remove("form__input--successField");

    let errorMessage = input.parentNode.querySelector(".form__message-error");

    // Si no existe el mensaje de error, lo creamos
    if (!errorMessage) {
        errorMessage = document.createElement("P");
        errorMessage.classList.add("form__message-error");
        input.parentNode.appendChild(errorMessage);
    }

    errorMessage.innerHTML = message;
    errorMessage.style.display = 'block'; // Asegúrate de que el mensaje se muestre
}

// Limpiar mensaje de error
function clearError(input) {
    input.classList.remove("form__input--errorField");

    const errorMessage = input.parentNode.querySelector(".form__message-error");

    if (errorMessage) {
        errorMessage.style.display = 'none'; // Esconde el mensaje de error
    }
}

// Validar todos los inputs del formulario
function validateForm() {
    const formToValidate = document.getElementById("formCourse");
    const inputs = formToValidate.querySelectorAll(".form__input");
    let isValid = true;

    inputs.forEach(input => {
        if (!validateInput(input)) {
            isValid = false; // Si algún campo no es válido, cambiar a falso
        }
    });

    return isValid;
}

// Evento al enviar el formulario
const sendButton = document.getElementById('sendButtonProfile');
sendButton.addEventListener('click', (e) => {
    if (!validateForm()) {
        e.preventDefault(); // Evitar el envío del formulario si hay errores
        alert("Por favor, corrige los campos resaltados en rojo.");
    }
});
