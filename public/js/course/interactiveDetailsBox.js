// interactiveToggle.js
const itemNums = document.querySelectorAll('.interactiveTest__itemNum');

itemNums.forEach(itemNum => {
    itemNum.addEventListener('click', function() {
        // Obtener el contenedor de detalles
        const detailsBox = this.closest('.interactiveTest').querySelector('.interactiveDetailsBox');
        const icon = this.querySelector('.interactiveTest__i');

        // Alternar la clase hidden en el contenido
        detailsBox.classList.toggle('hidden');

        // Rotar el ícono utilizando style.transform
        if (icon.style.transform === 'rotate(180deg)') {
            icon.style.transform = 'rotate(0deg)';
        } else {
            icon.style.transform = 'rotate(180deg)';
        }

        // Añadir transición suave
        icon.style.transition = 'transform 0.3s ease'; // Animación suave
    });
});
