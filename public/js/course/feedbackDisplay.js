const boxFeedBackHeaders = document.querySelectorAll('.boxFeedBackHeader');

boxFeedBackHeaders.forEach(header => {
    const caretIcon = header.querySelector('.fa-caret-down'); // Selecciona el ícono dentro del header
    const boxFeedbackInfo = header.nextElementSibling; // Encuentra el siguiente elemento (el párrafo)

    header.addEventListener('click', function() {
        // Alterna la visibilidad del contenido
        boxFeedbackInfo.classList.toggle('hidden'); 

        // Verifica si el ícono ya está rotado
        if (caretIcon.style.transform === 'rotate(180deg)') {
            caretIcon.style.transform = 'rotate(0deg)'; // Restaura a 0 grados
        } else {
            caretIcon.style.transform = 'rotate(180deg)'; // Rota a 180 grados
        }

        // Añade una transición suave
        caretIcon.style.transition = 'transform 0.3s ease'; 
    });
});