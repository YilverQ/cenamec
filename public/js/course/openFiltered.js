const filterButton = document.getElementById('buttonFiltered');
const filterBox = document.getElementById('boxFilteredTags');
const tamWidth = 911;

filterBox.classList.remove('hidden'); // Mostrar por defecto

filterButton.addEventListener('click', function() {
    if (window.innerWidth < tamWidth) {
        // Alternar la clase 'hidden'
        filterBox.classList.toggle('hidden');
    }
});

window.addEventListener('resize', function() {
    if (window.innerWidth >= tamWidth) {
        filterBox.classList.remove('hidden'); 
    } else {
        filterBox.classList.add('hidden'); 
    }
});

function checkWindowSize() {
    if (window.innerWidth < tamWidth) {
        filterBox.classList.add('hidden'); // Ocultar si es menor
    } else {
        filterBox.classList.remove('hidden'); // Mostrar si es mayor o igual
    }
}

checkWindowSize();