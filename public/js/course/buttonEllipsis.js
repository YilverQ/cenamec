// Selecciona todos los botones de opciones
const buttons = document.querySelectorAll('.buttonRight');

// Agrega un evento de clic a cada botón
buttons.forEach(button => {
    button.addEventListener('click', function(event) {
        // Prevenir la propagación del clic
        event.stopPropagation();

        // Obtener el menú correspondiente y el icono
        let menuNoteCard = button.closest('.buttonRightCuestion').querySelector('.menuNoteCard');
        let icon = button.querySelector('i');

        // Alternar clases del icono y visibilidad del menú
        icon.classList.toggle("fa-xmark");
        icon.classList.toggle("fa-ellipsis");
        menuNoteCard.classList.toggle('hidden');

        // Actualizar el estado del atributo aria-expanded
        const isExpanded = menuNoteCard.classList.contains('hidden') ? 'false' : 'true';
        button.setAttribute('aria-expanded', isExpanded);
    });
});

// Evento para cerrar el menú y restaurar el icono al hacer clic fuera
document.addEventListener('click', function(event) {
    buttons.forEach(button => {
        let menuNoteCard = button.closest('.buttonRightCuestion').querySelector('.menuNoteCard');
        let icon = button.querySelector('i');

        // Verificar si el clic fue fuera del botón y del menú
        if (!button.contains(event.target) && !menuNoteCard.contains(event.target)) {
            // Cerrar el menú y restaurar el icono
            menuNoteCard.classList.add('hidden');
            icon.classList.remove("fa-xmark");
            icon.classList.add("fa-ellipsis");

            // Restaurar el estado del atributo aria-expanded
            button.setAttribute('aria-expanded', 'false');
        }
    });
});
buttons.forEach(button =>{
    let resetIcon = button.querySelector('i');
    resetIcon.classList.remove("fa-xmark")
    resetIcon.classList.add("fa-ellipsis")
});