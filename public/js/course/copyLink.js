const buttonsShared = document.getElementById("shared");
const linkCopy = document.getElementById("linkCopy");
const xMarkClose = document.getElementById("xMarkClose");

buttonsShared.addEventListener("click", function(){
       let linkShared = document.getElementById("linkShared");
       let linkText = linkShared.innerText;
       
       let inputHidden = document.createElement("INPUT");
       inputHidden.style.position = 'fixed';
       inputHidden.opacity = 0;
       inputHidden.value = linkText;
       document.body.appendChild(inputHidden);
       inputHidden.select();
       document.execCommand('copy');
       document.body.removeChild(inputHidden);

       linkCopy.classList.remove("hidden");
       setTimeout(function() {
        linkCopy.classList.add('hidden');
    }, 5000);
});

xMarkClose.addEventListener("click", function(){
       linkCopy.classList.add("hidden");
});