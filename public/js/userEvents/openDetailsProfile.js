// Obtener elementos del DOM
const userDataFormAcademic = document.getElementById('OpenAcademicForm');
const userDataFormLaboral = document.getElementById('OpenLaboralForm');

// Formularios
const forms = {
    academicAdd : document.getElementById("containerFormFloat--AcademicAdd"),
    academicEdit: document.getElementById("containerFormFloat--AcademicEdit"),
    laboralAdd  : document.getElementById("containerFormFloat--LaboralAdd"),
    laboralEdit : document.getElementById("containerFormFloat--LaboralEdit")
};

// Marcas de X
const closeButtons = {
    academicAdd : document.getElementById("formFloat__button-academic"),
    academicEdit: document.getElementById("formFloat__button-academicEdit"),
    laboralAdd  : document.getElementById("formFloat__button-laboral"),
    laboralEdit : document.getElementById("formFloat__button-laboralEdit")
};

// Función para mostrar el formulario
function showForm(form) {
    form.classList.remove("hidden");
}

// Función para ocultar el formulario
function hideForm(form) {
    form.classList.add("hidden");
}

// Función para manejar la apertura de formularios
function setupFormToggle(triggerElement, formToShow, closeButton) {
    triggerElement.addEventListener("click", function() {
        showForm(formToShow);
    });

    closeButton.addEventListener("click", function() {
        hideForm(formToShow);
    });
}

// Configurar eventos para formularios académicos
setupFormToggle(userDataFormAcademic, forms.academicAdd, closeButtons.academicAdd);

// Configurar eventos para formularios de edición académica
const userDataFormAcademicEdit = document.querySelectorAll('.editAcademicData');
userDataFormAcademicEdit.forEach((item) => {
    item.addEventListener("click", function() {
        showForm(forms.academicEdit);
    });
});
closeButtons.academicEdit.addEventListener("click", function() {
    hideForm(forms.academicEdit);
});

// Configurar eventos para formularios laborales
setupFormToggle(userDataFormLaboral, forms.laboralAdd, closeButtons.laboralAdd);


// Configurar eventos para formularios de edición laboral
const userDataFormLaboralEdit = document.querySelectorAll('.editLaboralData');
userDataFormLaboralEdit.forEach((item) => {
    item.addEventListener("click", function() {
        showForm(forms.laboralEdit);
    });
});
closeButtons.laboralEdit.addEventListener("click", function() {
    hideForm(forms.laboralEdit);
});