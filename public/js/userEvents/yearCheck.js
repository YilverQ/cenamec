// Función para actualizar la fecha mínima
function actualizarFechaMin(yearEntryId, yearExitId) {
    const yearEntry = document.getElementById(yearEntryId);
    const yearExit = document.getElementById(yearExitId);

    // Agregar el evento de entrada
    yearEntry.addEventListener("input", function() {
        const yearMin = yearEntry.value;
        yearExit.setAttribute("min", yearMin);
    });

    // Agregar el evento de entrada
    yearExit.addEventListener("input", function() {
        const yearMin = yearEntry.value;
        yearExit.setAttribute("min", yearMin);
    });
}

// Configuración para entradas académicas
actualizarFechaMin("year_entry", "year_exit");
actualizarFechaMin("year_entryEdit", "year_exitEdit");

// Configuración para entradas laborales
actualizarFechaMin("year_entryLaboral", "year_exitLaboral");
actualizarFechaMin("year_entryLaboralEdit", "year_exitLaboralEdit");
