document.addEventListener('DOMContentLoaded', function () {
    const carousel = document.querySelector('.carousel');
    const items = document.querySelectorAll('.carousel-item');
    const prevButton = document.querySelector('.carousel-button.prev');
    const nextButton = document.querySelector('.carousel-button.next');
    const navDots = document.querySelectorAll('.nav-dot');
    const buttonSendForm = document.getElementById("ButtonSendForm"); // Referencia al botón ButtonSendForm
    let currentIndex = 0;


    function scrollToTop() {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }


    // Función para verificar si el ítem actual está completado
    function isItemCompleted(index) {
        const currentItem = items[index]; // Obtener el ítem actual
        const evaluationId = currentItem.querySelector('.interactiveTest').getAttribute('data-evaluation-id'); // Obtener el ID único del ítem
        return window.evaluationState && window.evaluationState[evaluationId] && window.evaluationState[evaluationId].answered;
    }

    // Función para mostrar el ítem actual con animación
    function showItem(index, animationDirection) {
        items.forEach((item, i) => {
            item.classList.remove('active', 'moverDerecha', 'moverIzquierda', 'moverArriba'); // Limpiar clases anteriores
            if (i === index) {
                item.classList.add('active'); // Mostrar el elemento activo
                // Aplicar la animación correspondiente
                if (animationDirection === 'right') {
                    item.style.animation = 'moverDerecha 0.5s ease-out'; // Animación hacia la derecha
                } else if (animationDirection === 'left') {
                    item.style.animation = 'moverIzquierda 0.5s ease-out'; // Animación hacia la izquierda
                } else if (animationDirection === 'up') {
                    item.style.animation = 'moverArriba 0.5s ease-out'; // Animación hacia arriba
                }
            }
        });

        navDots.forEach((dot, i) => {
            dot.classList.toggle('active', i === index);
        });

        // Mostrar u ocultar botones según la posición actual
        if (currentIndex === 0) {
            prevButton.classList.add('hidden');
        } else {
            prevButton.classList.remove('hidden');
        }

        if (currentIndex === items.length - 1) {
            nextButton.classList.add('hidden');
            buttonSendForm.classList.remove('hidden'); // Mostrar el botón ButtonSendForm
        } else {
            nextButton.classList.remove('hidden');
            buttonSendForm.classList.add('hidden'); // Ocultar el botón ButtonSendForm
        }
    }

    // Mostrar el primer ítem sin animación
    showItem(currentIndex, '');

    // Evento para el botón "Siguiente" del carrusel
    nextButton.addEventListener('click', () => {
        if (currentIndex < items.length - 1) {
            // Verificar si el ítem actual está completado
            if (!isItemCompleted(currentIndex)) {
                const moveItem = items[currentIndex]; // Obtener el ítem actual
                console.log(moveItem);

                // Reiniciar la animación directamente en el estilo
                moveItem.style.animation = 'none'; // Eliminar cualquier animación previa
                void moveItem.offsetWidth; // Forzar un reflow para reiniciar la animación
                moveItem.style.animation = 'shake 0.5s ease-in-out'; // Aplicar la animación de "temblor"

                // Reproducir sonido de alerta
                const alertSound = new Audio('/audios/alert.wav');
                alertSound.play();

                // Eliminar la animación después de que termine
                moveItem.addEventListener('animationend', () => {
                    moveItem.style.animation = ''; // Limpiar la animación
                }, { once: true });

                return; // Detener la navegación
            }
            // Llamar a scrollToTop antes de mostrar el nuevo ítem
            scrollToTop();
        
            currentIndex++;
            showItem(currentIndex, 'right'); // Animación hacia la derecha
        }
    });

    // Evento para el botón "Anterior" del carrusel
    prevButton.addEventListener('click', () => {
        if (currentIndex > 0) {
            currentIndex--;
            // Llamar a scrollToTop antes de mostrar el ítem anterior
            scrollToTop();
            showItem(currentIndex, 'left'); // Animación hacia la izquierda
        }
    });

    // Evento para el botón de envío final
    buttonSendForm.addEventListener('click', (event) => {
        const lastIndex = items.length - 1; // Índice del último ítem

        // Verificar si el último ítem está completado
        if (!isItemCompleted(lastIndex)) {
            event.preventDefault(); // Evitar el envío del formulario

            const lastItem = items[lastIndex]; // Obtener el último ítem
            console.log("Último ítem no completado:", lastItem);

            // Aplicar animación de "temblor" al último ítem
            lastItem.style.animation = 'none'; // Reiniciar cualquier animación previa
            void lastItem.offsetWidth; // Forzar un reflow para reiniciar la animación
            lastItem.style.animation = 'shake 0.5s ease-in-out'; // Aplicar la animación de "temblor"

            // Reproducir sonido de alerta
            const alertSound = new Audio('/audios/alert.wav');
            alertSound.play();

            // Eliminar la animación después de que termine
            lastItem.addEventListener('animationend', () => {
                lastItem.style.animation = ''; // Limpiar la animación
            }, { once: true });

            return; // Detener cualquier acción adicional
        }

        // Si el último ítem está completado, permitir el envío del formulario
        console.log('Último ítem completado. Enviando formulario...');
    });
});
