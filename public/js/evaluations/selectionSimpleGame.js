document.addEventListener('DOMContentLoaded', function() {
    const optionContainers = document.querySelectorAll('[id^="options-"]');

    function shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }

    optionContainers.forEach(container => {
        const options = shuffleArray(Array.from(container.querySelectorAll('.cardQuestion__answer')));

        container.innerHTML = '';
        options.forEach(option => {
            container.appendChild(option);

            option.addEventListener('click', function() {
                const selectionSimpleId = container.id.split('-')[1]; // Obtener el ID de la evaluación
                const selection = option.textContent; // Obtener la selección del estudiante
                const correct = option.dataset.answerId === 'correct'; // Determinar si es correcta

                // Almacenar la selección en el objeto
                window.evaluationState[selectionSimpleId] = {
                    answered: true,
                    type: "Selección Simple",
                    selection: selection,
                    correct: correct,
                };
                console.log(window.evaluationState); 

                if (correct) {
                    option.classList.add('cardQuestion__answer--true');
                    option.setAttribute("style", "background: #28a745;");
                    const successSound = new Audio('/audios/correct.mp3');
                    successSound.play();
                } else {
                    option.classList.add('cardQuestion__answer--bad');
                    option.setAttribute("style", "background: #e75480;");
                    const errorSound = new Audio('/audios/error.wav');
                    errorSound.play();
                    
                    const feedbackContainer = container.parentElement.querySelector('.hiddenRetro');
                    if (feedbackContainer) {
                        feedbackContainer.classList.remove('active'); 
                    }
                }

                // Desactivar eventos de clic en las opciones
                options.forEach(opt => {
                    opt.style.pointerEvents = 'none';
                });
            });

        });
    });
});