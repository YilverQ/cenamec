document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.check-word-button').forEach(button => {
        button.addEventListener('click', function() {
            handleCheckWord(this);
        });
    });

    // Agregar evento para el campo de entrada
    document.querySelectorAll('.superInputText').forEach(input => {
        input.addEventListener('keydown', function(event) {
            if (event.key === 'Enter') {
                event.preventDefault(); // Evitar el comportamiento por defecto del Enter
                const button = this.closest('.interactiveTest').querySelector('.check-word-button');
                handleCheckWord(button); // Llamar a la función para verificar la palabra
            }
        });
    });

    // Función para manejar la verificación de la palabra
    function handleCheckWord(button) {
        const parent = button.closest('.interactiveTest');
        const evaluationId = parent.getAttribute('data-evaluation-id');
        const userResponse = parent.querySelector('.superInputText').value.trim();
        const acceptedWords = JSON.parse(parent.querySelector('.interactiveDetailsBox').getAttribute('data-accept-words')) || [];

        // Función para normalizar la respuesta (mayúsculas y sin acentos)
        const normalizeString = (str) => {
            return str
                .toUpperCase() // Convertir a mayúsculas
                .normalize("NFD") // Normalizar para eliminar acentos
                .replace(/[\u0300-\u036f]/g, ""); // Eliminar diacríticos
        };

        // Normalizar la respuesta del usuario
        const normalizedResponse = normalizeString(userResponse);

        // Verificar si la respuesta del usuario es correcta
        const isCorrect = acceptedWords.map(normalizeString).includes(normalizedResponse);

        // Almacenar la selección en el objeto
        window.evaluationState[evaluationId] = {
            answered: true,
            type: "Respuesta Corta",
            selection: normalizedResponse,
            correct: isCorrect,
        };
        console.log(window.evaluationState); 


        // Mostrar retroalimentación al usuario
        const feedbackBox = parent.querySelector('.hiddenRetro');
        
        if (!isCorrect) {
            feedbackBox.classList.remove('active'); // Ocultar si es incorrecto
        } 

        // Deshabilitar el campo de entrada y ocultar el botón
        const inputField = parent.querySelector('.superInputText');
        inputField.disabled = true; // Deshabilitar el input
        inputField.classList.add('inactive'); // Agregar clase para aplicar estilos

        button.classList.add('inactive'); // Agregar clase para ocultar el botón

        // Manejar los íconos de retroalimentación
        const correctIcon = parent.querySelector('.icon-correct');
        const incorrectIcon = parent.querySelector('.icon-incorrect');

        if (isCorrect) {
            correctIcon.classList.remove('icon-hidden'); // Mostrar ícono correcto
            incorrectIcon.classList.add('icon-hidden'); // Ocultar ícono incorrecto
            const successSound = new Audio('/audios/correct.mp3');
            successSound.play();
        } else {
            correctIcon.classList.add('icon-hidden'); // Ocultar ícono correcto
            incorrectIcon.classList.remove('icon-hidden'); // Mostrar ícono incorrecto
            const errorSound = new Audio('/audios/error.wav');
            errorSound.play();
        }
    }
});
