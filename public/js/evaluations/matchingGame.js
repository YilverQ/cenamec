document.addEventListener('DOMContentLoaded', function () {
    const studentSelections = {};
    const interactiveTests = document.querySelectorAll('.interactiveTest');

    interactiveTests.forEach(test => {
        const items = test.querySelectorAll('.boxCompare__item');
        const checkButton = test.querySelector('.check-pairing-button'); // Botón de verificación
        const feedbackContainer = test.querySelector('.hiddenRetro');
        let willSeefeedbackContainer = false;
        let selectedItems = [];
        const evaluationId = test.dataset.evaluationId;
        let totalPairs = items.length / 2;
        let selectedPairs = 0;

        items.forEach(item => {
            item.addEventListener('click', function () {
                this.classList.toggle('selected');

                if (this.classList.contains('selected')) {
                    if (selectedItems.length === 1) {
                        const firstSelected = selectedItems[0];

                        if (firstSelected.parentElement === this.parentElement) {
                            firstSelected.classList.remove('selected');
                            selectedItems = selectedItems.filter(selected => selected !== firstSelected);
                            selectedItems.push(this);
                        } else {
                            selectedItems.push(this);
                        }
                    } else {
                        selectedItems.push(this);
                    }
                } else {
                    selectedItems = selectedItems.filter(selected => selected !== this);
                }

                if (selectedItems.length === 2) {
                    const firstElement = selectedItems[0];
                    const secondElement = selectedItems[1];

                    const firstMatching = firstElement.dataset.matching;
                    const secondMatching = secondElement.dataset.matching;

                    let isCorrect;

                    firstElement.classList.add('previous-selected');
                    secondElement.classList.add('previous-selected');

                    if (firstMatching === secondElement.dataset.element && secondMatching === firstElement.dataset.element) {
                        isCorrect = true; 
                    } else {
                        isCorrect = false; 
                    }

                    selectedPairs++;
                    if (isCorrect) {
                        firstElement.classList.add('correct');
                        secondElement.classList.add('correct');
                        const pairSound = new Audio('/audios/pair.mp3');
                        pairSound.play();
                    } else {
                        willSeefeedbackContainer = true;
                        firstElement.classList.add('incorrect');
                        secondElement.classList.add('incorrect');
                        const pairSound = new Audio('/audios/pair.mp3');
                        pairSound.play();
                    }

                    const firstId = firstElement.dataset.matchingId;
                    const secondId = secondElement.dataset.matchingId;

                    if (!studentSelections[evaluationId]) {
                        studentSelections[evaluationId] = [];
                    }
                    
                    studentSelections[evaluationId].push({
                        selection: [firstElement.dataset.element, secondElement.dataset.element],
                        ids: [firstId, secondId], 
                        correct: isCorrect,
                    });
                    //console.log(studentSelections);  

                    // Reiniciar la selección después de verificar
                    selectedItems = [];

                    // Mostrar el botón si se han seleccionado todos los pares
                    if (checkButton && selectedPairs === totalPairs) { // Verificar si checkButton existe
                        checkButton.classList.remove("hidden");
                    }
                }
            });
        });

        // Función para manejar el clic en el botón "Verificar Selección"
        if (checkButton) { // Verificar si checkButton existe
            checkButton.addEventListener('click', function () {
                checkButton.classList.add('inactive');
                if (willSeefeedbackContainer) {
                    feedbackContainer.classList.remove('active');
                    const errorSound = new Audio('/audios/error_sequence.mp3');
                    errorSound.play();
                }
                else{
                    const successSound = new Audio('/audios/correct.mp3');
                    successSound.play();
                }

                // Almacenar la selección en el objeto
                window.evaluationState[evaluationId] = {
                    answered: true,
                    type: "Emparejamiento",
                    selection: studentSelections[evaluationId],
                    correct: !willSeefeedbackContainer,
                    itemEvaluation_id: evaluationId,
                };
                console.log(window.evaluationState); 

                items.forEach(item => {
                    item.classList.remove('previous-selected');
                });
            });
        }
    });
});
