document.addEventListener('DOMContentLoaded', function () {
    // Función para incrementar el porcentaje
    function animatePercentage(targetElement, targetValue, duration) {
        let startValue = 0;
        let increment = Math.ceil(targetValue / (duration / 100)); // Incremento por cada 100ms
        let startTime = null;

        function animate(time) {
            if (!startTime) startTime = time;
            let progress = Math.min((time - startTime) / duration, 1);
            let currentValue = Math.min(startValue + Math.floor(progress * targetValue), targetValue);
            targetElement.innerText = currentValue + '%';

            if (progress < 1) {
                requestAnimationFrame(animate);
            }
        }

        requestAnimationFrame(animate);
    }

    // Función para incrementar el tiempo
    function animateTime(targetElement, targetMinutes, targetSeconds, duration) {
        let totalSeconds = targetMinutes * 60 + targetSeconds;
        let increment = Math.ceil(totalSeconds / (duration / 100)); // Incremento por cada 100ms
        let startTime = null;

        function animate(time) {
            if (!startTime) startTime = time;
            let elapsed = Math.min((time - startTime) / duration, 1);
            let currentSeconds = Math.min(Math.floor(elapsed * totalSeconds), totalSeconds);

            let minutes = Math.floor(currentSeconds / 60);
            let seconds = currentSeconds % 60;

            targetElement.innerText = 
                String(minutes).padStart(2, '0') + ':' + 
                String(seconds).padStart(2, '0');

            if (elapsed < 1) {
                requestAnimationFrame(animate);
            }
        }

        requestAnimationFrame(animate);
    }

    // Llama a las funciones con los valores deseados
    const percentageCounter = document.getElementById('percentageCounter');
    const timeCounter = document.getElementById('timeCounter');

    // Cambia estos valores según tus necesidades
    const finalPercentage = parseInt(percentageCounter.innerText);
    const finalMinutes = parseInt(timeCounter.innerText.split(':')[0]);
    const finalSeconds = parseInt(timeCounter.innerText.split(':')[1]);

    // Duración de la animación en milisegundos
    const duration = 500; // Duración total de la animación

    animatePercentage(percentageCounter, finalPercentage, duration);
    animateTime(timeCounter, finalMinutes, finalSeconds, duration);
});
