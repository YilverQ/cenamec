document.addEventListener('DOMContentLoaded', function () {
    // Asegúrate de que el formulario esté disponible en el DOM
    const form = document.getElementById('sendButton');
    const evaluationStateField = document.getElementById('evaluationStateField');
    
    // Cuando el formulario se va a enviar, asignamos el objeto al campo oculto
    form.addEventListener('submit', function (event) {
        event.preventDefault(); 
        evaluationStateField.value = JSON.stringify(window.evaluationState);
        
        // Una vez asignado el valor, envía el formulario
        form.submit();
    });
});
