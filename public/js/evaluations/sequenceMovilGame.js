document.addEventListener('DOMContentLoaded', function () {
    const boxSequences = document.querySelectorAll('.boxSequence');

    boxSequences.forEach(boxSequence => {
        const evaluationId = boxSequence.closest('.interactiveTest').dataset.evaluationId;
        const sequenceElements = boxSequence.querySelectorAll('.boxSequence__element');

        sequenceElements.forEach(element => {
            // Eventos para móviles
            setupMobileDragAndDrop(element, boxSequence);
        });
    });

    function setupMobileDragAndDrop(element, boxSequence) {
        let initialTouch = null;

        element.addEventListener('touchstart', function (e) {
            initialTouch = e.touches[0];
            e.preventDefault();
            setTimeout(() => {
                element.classList.add('touchMove'); // Hacer invisible el elemento mientras se arrastra
            }, 0);
        });

        element.addEventListener('touchmove', function (e) {
            if (!initialTouch) return;

            const touch = e.touches[0];
            const targetElement = document.elementFromPoint(touch.clientX, touch.clientY);

            if (targetElement && targetElement.closest('.boxSequence__element')) {
                const draggedElementId = element.dataset.sequenceId;
                const targetElementId = targetElement.dataset.sequenceId;

                // Intercambiar posiciones
                if (draggedElementId !== targetElementId) {
                    swapElements(element, targetElement, boxSequence);
                    updatePositions(boxSequence);
                }
            }
        });

        element.addEventListener('touchend', function () {
            element.classList.remove('touchMove');
            resetReplaceClasses(boxSequence);
            initialTouch = null;
        });
    }

    function swapElements(draggedElement, targetElement, boxSequence) {
        const draggedNextSibling = draggedElement.nextSibling;
        const targetNextSibling = targetElement.nextSibling;

        boxSequence.insertBefore(draggedElement, targetNextSibling);
        boxSequence.insertBefore(targetElement, draggedNextSibling);
    }

    function resetReplaceClasses(boxSequence) {
        const sequenceElements = boxSequence.querySelectorAll('.boxSequence__element');
        sequenceElements.forEach(el => el.classList.remove('replace'));
    }

    function updatePositions(boxSequence) {
        const updatedElements = boxSequence.querySelectorAll('.boxSequence__element'); 
        updatedElements.forEach((el, index) => {
            const textNumber = el.querySelector('.boxSequence__textNumber');
            textNumber.textContent = index + 1; 
        });
    }
});
