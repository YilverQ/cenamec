document.addEventListener('DOMContentLoaded', function () {
    const studentSelections = {}; // Objeto para almacenar las respuestas del participante
    const evaluations = document.querySelectorAll('.interactiveTest');

    evaluations.forEach(evaluation => {
        const optionsList = evaluation.querySelector('.tableOptionsList');
        const verifyButton = evaluation.querySelector('.check-tableCompare-button');
        const columns = evaluation.querySelectorAll('.tableColumn');
        const feedback = evaluation.querySelector('.hiddenRetro');

        let draggedItem = null; // Elemento que se está moviendo
        let initialTouch = null; // Posición inicial del toque

        function checkAllItemsDropped() {
            const remainingItems = optionsList.querySelectorAll('.tableOptionsList__item').length;

            if (remainingItems === 0) {
                verifyButton.classList.remove('hidden'); // Mostrar el botón si no quedan elementos
                optionsList.classList.add('tableOptionsList--hidden'); // Ocultar la lista de opciones
            }
        }

        function allowDrop(ev) {
            ev.preventDefault();
            const targetColumn = ev.target.closest('.tableColumn__body');
            if (targetColumn) {
                targetColumn.classList.add('highlight');
            }
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.dataset.itemid);
        }

        function drop(ev) {
            ev.preventDefault();
            const data = ev.dataTransfer.getData("text");
            const draggedElement = evaluation.querySelector(`[data-itemid="${data}"]`);
            const targetColumn = ev.target.closest('.tableColumn__body');

            if (targetColumn) {
                targetColumn.appendChild(draggedElement);
                checkAllItemsDropped();
            }

            removeHighlight(ev);
        }

        function removeHighlight(ev) {
            const targetColumn = ev.target.closest('.tableColumn__body');
            if (targetColumn) {
                targetColumn.classList.remove('highlight');
            }
        }

        // Función para manejar el inicio del toque
        function touchStart(ev) {
            initialTouch = ev.touches[0]; // Guardar la posición inicial del toque
            draggedItem = ev.target.closest('.tableOptionsList__item'); // Seleccionar el elemento tocado
            if (draggedItem) {
                draggedItem.classList.add('dragging'); // Aplicar estilo visual durante el movimiento
            }
        }

        // Función para manejar el movimiento del toque
        // Función para manejar el movimiento del toque
        function touchMove(ev) {
            if (!draggedItem) return;
            ev.preventDefault();

            const touch = ev.touches[0];
            const targetElement = document.elementFromPoint(touch.clientX, touch.clientY); // Obtener el elemento bajo el toque
            const targetColumn = targetElement.closest('.tableColumn__body');

            // Quitar el resaltado de todas las columnas primero
            columns.forEach(column => {
                column.querySelector('.tableColumn__body').classList.remove('highlight');
            });

            // Resaltar la columna de destino si existe
            if (targetColumn) {
                targetColumn.classList.add('highlight');
            }
        }

        // Función para manejar el final del toque
        function touchEnd(ev) {
            if (!draggedItem) return;

            const touch = ev.changedTouches[0];
            const targetElement = document.elementFromPoint(touch.clientX, touch.clientY); // Obtener el elemento bajo el toque
            const targetColumn = targetElement.closest('.tableColumn__body');

            if (targetColumn) {
                targetColumn.appendChild(draggedItem); // Mover el elemento a la columna de destino
                checkAllItemsDropped();
            }

            // Restablecer el estado
            draggedItem.classList.remove('dragging');
            draggedItem = null;
            initialTouch = null;

            // Quitar el resaltado de todas las columnas
            columns.forEach(column => {
                column.querySelector('.tableColumn__body').classList.remove('highlight');
            });
        }

        // Función para manejar el final del toque
        function touchEnd(ev) {
            if (!draggedItem) return;

            const touch = ev.changedTouches[0];
            const targetElement = document.elementFromPoint(touch.clientX, touch.clientY); // Obtener el elemento bajo el toque
            const targetColumn = targetElement.closest('.tableColumn__body');

            if (targetColumn) {
                targetColumn.appendChild(draggedItem); // Mover el elemento a la columna de destino
                checkAllItemsDropped();
            }

            // Restablecer el estado
            draggedItem.classList.remove('dragging');
            draggedItem = null;
            initialTouch = null;

            // Quitar el resaltado de todas las columnas
            columns.forEach(column => {
                column.querySelector('.tableColumn__body').classList.remove('highlight');
            });
        }

        // Función para registrar las selecciones al hacer clic en el botón
        function registerSelections() {
            const evaluationId = evaluation.dataset.evaluationId;

            studentSelections[evaluationId] = {
                columnA_correct: JSON.parse(columns[0].dataset.itemsidA),
                columnA_selection: Array.from(columns[0].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnA_boolean: [], // Para respuestas correctas
                columnA_selection_boolean: [], // Para las selecciones del estudiante

                columnB_correct: JSON.parse(columns[1].dataset.itemsidB),
                columnB_selection: Array.from(columns[1].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnB_boolean: [], // Para respuestas correctas
                columnB_selection_boolean: [], // Para las selecciones del estudiante

                columnC_correct: JSON.parse(columns[2].dataset.itemsidC),
                columnC_selection: Array.from(columns[2].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnC_boolean: [], // Para respuestas correctas
                columnC_selection_boolean: [], // Para las selecciones del estudiante

                correct: false, // Inicializar como incorrecto
                percentage: 0 // Inicializar porcentaje
            };

            // Almacenar la selección en el objeto
            window.evaluationState[evaluationId] = {
                answered: true,
                type: "Cuadro Comparativo",
                selection: studentSelections[evaluationId],
                correct: studentSelections[evaluationId].correct,
            };

            // Generar los booleanos para cada columna
            generateBooleanArray(evaluationId, 'columnA');
            generateBooleanArray(evaluationId, 'columnB');
            generateBooleanArray(evaluationId, 'columnC');

            // Calcular resultados
            calculateResults(evaluationId, feedback);

            // Pintar los elementos correctos e incorrectos
            highlightAnswers(evaluationId);

            // Ocultar el botón después de verificar
            verifyButton.classList.add('hidden');

            // Deshabilitar arrastre después de la verificación
            disableDragAndDrop();
        }

        function disableDragAndDrop() {
            columns.forEach(column => {
                column.querySelectorAll('.tableOptionsList__item').forEach(item => {
                    item.setAttribute('draggable', false); // Deshabilitar arrastre
                    item.removeEventListener('touchstart', touchStart);
                    item.removeEventListener('touchmove', touchMove);
                    item.removeEventListener('touchend', touchEnd);
                });
            });
        }

        // Función para generar los booleanos de una columna específica
        function generateBooleanArray(evaluationId, column) {
            const correctAnswers = studentSelections[evaluationId][`${column}_correct`];
            const userSelection = studentSelections[evaluationId][`${column}_selection`];

            // Limpiar los booleanos existentes
            studentSelections[evaluationId][`${column}_boolean`] = [];
            studentSelections[evaluationId][`${column}_selection_boolean`] = [];

            // Comparar cada respuesta correcta con las selecciones del usuario
            correctAnswers.forEach(correctAnswer => {
                studentSelections[evaluationId][`${column}_boolean`].push(userSelection.includes(correctAnswer.toString()));
            });

            // Comparar cada selección del usuario con las respuestas correctas
            userSelection.forEach(selectedItem => {
                studentSelections[evaluationId][`${column}_selection_boolean`].push(correctAnswers.includes(parseInt(selectedItem)));
            });
        }

        // Función para calcular los resultados
        function calculateResults(evaluationId, feedback) {
            const correctAnswersCount = [
                ...studentSelections[evaluationId].columnA_boolean,
                ...studentSelections[evaluationId].columnB_boolean,
                ...studentSelections[evaluationId].columnC_boolean
            ].filter(isCorrect => isCorrect).length; // Contar respuestas correctas

            const totalAnswersCount = studentSelections[evaluationId].columnA_correct.length +
                                      studentSelections[evaluationId].columnB_correct.length +
                                      studentSelections[evaluationId].columnC_correct.length;

            studentSelections[evaluationId].correct = correctAnswersCount === totalAnswersCount; // Determinar si todas son correctas

            // Mostrar una retroalimentación y agregar sonidos
            if (studentSelections[evaluationId].correct) {
                const successSound = new Audio('/audios/correct.mp3');
                successSound.play();
            } else {
                const errorSound = new Audio('/audios/error.wav');
                errorSound.play();
                feedback.classList.remove('active'); // Eliminar la clase 'active'
            }

            // Calcular porcentaje de aciertos y redondear a dos decimales
            studentSelections[evaluationId].percentage = totalAnswersCount > 0 ? 
              parseFloat(((correctAnswersCount / totalAnswersCount) * 100).toFixed(2)) : 0; 
        }

        // Función para pintar elementos correctos e incorrectos
        function highlightAnswers(evaluationId) {
            columns.forEach((column, index) => {
                const correctAnswers = studentSelections[evaluationId][`column${String.fromCharCode(65 + index)}_correct`];
                const userSelection = studentSelections[evaluationId][`column${String.fromCharCode(65 + index)}_selection`];

                column.querySelectorAll('.tableOptionsList__item').forEach(item => {
                    if (correctAnswers.includes(parseInt(item.dataset.itemid))) {
                        item.classList.add('tableOptionsList__item--correct'); // Estilo para respuestas correctas
                    } else {
                        item.classList.add('tableOptionsList__item--incorrect'); // Estilo para respuestas incorrectas
                    }
                });
            });
        }

        // Función para manejar el clic en el botón "Verificar Selección"
        if (verifyButton) { // Verificar si checkButton existe
            verifyButton.addEventListener('click', registerSelections);
            // Configurar eventos de arrastrar y soltar para desktop
            columns.forEach(column => {
                column.addEventListener('dragover', allowDrop);
                column.addEventListener('drop', drop);
                column.addEventListener('dragleave', removeHighlight);
            });

            optionsList.querySelectorAll('.tableOptionsList__item').forEach(item => {
                item.setAttribute('draggable', true);
                item.addEventListener('dragstart', drag);

                // Configurar eventos táctiles para móviles
                item.addEventListener('touchstart', touchStart);
                item.addEventListener('touchmove', touchMove);
                item.addEventListener('touchend', touchEnd);
            });
        }
    });
});