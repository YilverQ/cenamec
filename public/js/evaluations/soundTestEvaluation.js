document.addEventListener('DOMContentLoaded', function () {
    const boxImage = document.getElementById('boxImage');
    const meanValue = boxImage.getAttribute('data-mean');
    const completeSound = new Audio('/audios/complete.mp3');
    const failSound = new Audio('/audios/fail.mp3');

    if (meanValue >= 60) {
        completeSound.play();
    } else {
        failSound.play();
    }
});
