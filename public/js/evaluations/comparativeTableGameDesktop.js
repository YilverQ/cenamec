document.addEventListener('DOMContentLoaded', function () {
    const studentSelections = {}; // Objeto para almacenar las respuestas del participante

    const evaluations = document.querySelectorAll('.interactiveTest');

    evaluations.forEach(evaluation => {
        const optionsList = evaluation.querySelector('.tableOptionsList');
        const verifyButton = evaluation.querySelector('.check-tableCompare-button');
        const columns = evaluation.querySelectorAll('.tableColumn');
        const feedback = evaluation.querySelector('.hiddenRetro');

        function checkAllItemsDropped() {
            const remainingItems = optionsList.querySelectorAll('.tableOptionsList__item').length;

            if (remainingItems === 0) {
                verifyButton.classList.remove('hidden'); // Mostrar el botón si no quedan elementos
                optionsList.classList.add('tableOptionsList--hidden'); // Ocultar la lista de opciones
            }
        }

        function allowDrop(ev) {
            ev.preventDefault();
            const targetColumn = ev.target.closest('.tableColumn__body');
            if (targetColumn) {
                targetColumn.classList.add('highlight');
            }
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.dataset.itemid);
        }

        function drop(ev) {
            ev.preventDefault();
            const data = ev.dataTransfer.getData("text");
            const draggedElement = evaluation.querySelector(`[data-itemid="${data}"]`);
            const targetColumn = ev.target.closest('.tableColumn__body');

            if (targetColumn) {
                targetColumn.appendChild(draggedElement);
                checkAllItemsDropped();
            }

            removeHighlight(ev);
        }

        function removeHighlight(ev) {
            const targetColumn = ev.target.closest('.tableColumn__body');
            if (targetColumn) {
                targetColumn.classList.remove('highlight');
            }
        }

        // Función para registrar las selecciones al hacer clic en el botón
        function registerSelections() {
            const evaluationId = evaluation.dataset.evaluationId;

            studentSelections[evaluationId] = {
                columnA_correct: JSON.parse(columns[0].dataset.itemsidA),
                columnA_selection: Array.from(columns[0].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnA_boolean: [],

                columnB_correct: JSON.parse(columns[1].dataset.itemsidB),
                columnB_selection: Array.from(columns[1].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnB_boolean: [],

                columnC_correct: JSON.parse(columns[2].dataset.itemsidC),
                columnC_selection: Array.from(columns[2].querySelectorAll('.tableColumn__body > .tableOptionsList__item')).map(item => item.dataset.itemid),
                columnC_boolean: [],
                
                correct: false, // Inicializar como incorrecto
                percentage: 0 // Inicializar porcentaje
            };

            // Generar los booleanos para cada columna
            generateBooleanArray(evaluationId, 'columnA');
            generateBooleanArray(evaluationId, 'columnB');
            generateBooleanArray(evaluationId, 'columnC');

            // Calcular resultados
            calculateResults(evaluationId, feedback);

            // Pintar los elementos correctos e incorrectos
            highlightAnswers(evaluationId);

            // Ocultar el botón después de verificar
            verifyButton.classList.add('hidden'); 
            
            console.log(studentSelections); // Para verificar los datos registrados
            
            // Deshabilitar arrastre después de la verificación
            disableDragAndDrop();
        }

        function disableDragAndDrop() {
            columns.forEach(column => {
                column.querySelectorAll('.tableOptionsList__item').forEach(item => {
                    item.setAttribute('draggable', false); // Deshabilitar arrastre
                });
            });
        }

        // Función para generar los booleanos de una columna específica
        function generateBooleanArray(evaluationId, column) {
            const correctAnswers = studentSelections[evaluationId][`${column}_correct`];
            const userSelection = studentSelections[evaluationId][`${column}_selection`];

            // Limpiar el booleano existente
            studentSelections[evaluationId][`${column}_boolean`] = [];

            // Comparar cada respuesta correcta con las selecciones del usuario
            correctAnswers.forEach(correctAnswer => {
                studentSelections[evaluationId][`${column}_boolean`].push(userSelection.includes(correctAnswer.toString()));
            });
        }

        // Función para calcular los resultados
        function calculateResults(evaluationId, feedback) {
            const correctAnswersCount = [
                ...studentSelections[evaluationId].columnA_boolean,
                ...studentSelections[evaluationId].columnB_boolean,
                ...studentSelections[evaluationId].columnC_boolean
            ].filter(isCorrect => isCorrect).length; // Contar respuestas correctas

            const totalAnswersCount = studentSelections[evaluationId].columnA_correct.length +
                                      studentSelections[evaluationId].columnB_correct.length +
                                      studentSelections[evaluationId].columnC_correct.length;

            studentSelections[evaluationId].correct = correctAnswersCount === totalAnswersCount; // Determinar si todas son correctas

            //Mostramos una retroalimentación y agregamos sonidos
            if (studentSelections[evaluationId].correct) {
                const successSound = new Audio('/audios/correct.mp3');
                successSound.play();
            }
            else{
                const errorSound = new Audio('/audios/error.wav');
                errorSound.play();
                feedback.classList.remove('active'); // Eliminar la clase 'active'
            }



            // Calcular porcentaje de aciertos y redondear a dos decimales
            studentSelections[evaluationId].percentage = totalAnswersCount > 0 ? 
              parseFloat(((correctAnswersCount / totalAnswersCount) * 100).toFixed(2)) : 0; 
        }

        // Función para pintar elementos correctos e incorrectos
        function highlightAnswers(evaluationId) {
            columns.forEach((column, index) => {
                const correctAnswers = studentSelections[evaluationId][`column${String.fromCharCode(65 + index)}_correct`];
                const userSelection = studentSelections[evaluationId][`column${String.fromCharCode(65 + index)}_selection`];

                column.querySelectorAll('.tableOptionsList__item').forEach(item => {
                    if (correctAnswers.includes(parseInt(item.dataset.itemid))) {
                        item.classList.add('tableOptionsList__item--correct'); // Estilo para respuestas correctas
                    } else {
                        item.classList.add('tableOptionsList__item--incorrect'); // Estilo para respuestas incorrectas
                    }
                });
            });
        }

        verifyButton.addEventListener('click', registerSelections);

        columns.forEach(column => {
            column.addEventListener('dragover', allowDrop);
            column.addEventListener('drop', drop);
            column.addEventListener('dragleave', removeHighlight);
        });

        optionsList.querySelectorAll('.tableOptionsList__item').forEach(item => {
            item.setAttribute('draggable', true);
            item.addEventListener('dragstart', drag);
        });
    });
});
