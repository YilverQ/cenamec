document.addEventListener('DOMContentLoaded', function () {
    const trueFalseContainers = document.querySelectorAll('.trueOrFalseBox'); // Seleccionar todos los contenedores de Verdadero o Falso

    trueFalseContainers.forEach(container => {
        const evaluationId = container.closest('.interactiveTest').dataset.evaluationId; // Obtener el ID de la evaluación
        const correctAnswer = container.querySelector('.trueOrFalse__content').dataset.correct === 'true'; // Obtener la respuesta correcta del atributo data-correct
        const options = container.querySelectorAll('.trueOrFalse__item'); // Opciones "Verdadero" y "Falso"

        options.forEach(option => {
            option.addEventListener('click', function () {
                const selection = option.textContent.trim(); // Obtener la selección del estudiante
                const isCorrect = (option.dataset.selection === '1') === correctAnswer; // Determinar si es correcta

                // Almacenar la selección en el objeto
                window.evaluationState[evaluationId] = {
                    answered: true,
                    type: "Verdadero o Falso",
                    selection: selection,
                    correct: isCorrect,
                };
                console.log(window.evaluationState); 

                // Proveer retroalimentación visual
                if (isCorrect) {
                    option.classList.add('trueOrFalse__item--true'); // Cambiar estilo a verde si es correcta
                    option.setAttribute('aria-label', 'Respuesta correcta');
                    const successSound = new Audio('/audios/correct.mp3');
                    successSound.play();
                } else {
                    option.classList.add('trueOrFalse__item--false'); // Cambiar estilo a rojo si es incorrecta
                    option.setAttribute('aria-label', 'Respuesta incorrecta');
                    const errorSound = new Audio('/audios/error.wav');
                    errorSound.play();

                    // Mostrar retroalimentación adicional si existe
                    const feedbackContainer = container.closest('.interactiveDetailsBox').querySelector('.hiddenRetro');
                    if (feedbackContainer) {
                        feedbackContainer.classList.remove('active'); 
                    }
                }

                // Deshabilitar las opciones después de seleccionar una respuesta
                options.forEach(opt => {
                    opt.style.pointerEvents = 'none';
                    opt.classList.add('trueOrFalse__item--disabled');
                });
            });
        });
    });
});