document.addEventListener('DOMContentLoaded', function () {
    const boxSequences = document.querySelectorAll('.boxSequence'); // Seleccionar todos los contenedores de secuencia

    boxSequences.forEach(boxSequence => {
        // Reiniciar la animación al cargar el contenido
        boxSequence.classList.remove('animate'); // Asegúrate de tener una clase para reiniciar si es necesario
        void boxSequence.offsetWidth; // Forzar reflow para reiniciar la animación
        boxSequence.classList.add('animate');

        const evaluationId = boxSequence.closest('.interactiveTest').dataset.evaluationId; // Obtener el ID de la evaluación
        const sequenceElements = boxSequence.querySelectorAll('.boxSequence__element'); // Seleccionar solo los elementos de secuencia dentro del contenedor actual

        sequenceElements.forEach(element => {
            element.setAttribute('draggable', true); // Hacer que cada elemento sea arrastrable

            element.addEventListener('dragstart', function (e) {
                e.dataTransfer.setData('text/plain', element.dataset.sequenceId); // Almacenar el ID de la secuencia arrastrada
                setTimeout(() => {
                    element.classList.add('invisible'); // Hacer invisible el elemento mientras se arrastra
                }, 0);
            });

            element.addEventListener('dragend', function () {
                element.classList.remove('invisible'); // Restaurar la visibilidad al soltar
                updatePositions(boxSequence); // Actualizar las posiciones al finalizar el arrastre solo para este contenedor

                // Remover la clase 'replace' de todos los elementos al final del drag
                sequenceElements.forEach(el => el.classList.remove('replace'));
            });
        });

        boxSequence.addEventListener('dragover', function (e) {
            e.preventDefault(); // Prevenir el comportamiento por defecto para permitir el drop
        });

        boxSequence.addEventListener('dragenter', function (e) {
            const targetElement = e.target.closest('.boxSequence__element');
            if (targetElement) {
                targetElement.classList.add('replace'); // Añadir clase 'replace' al elemento objetivo
            }
        });

        boxSequence.addEventListener('dragleave', function (e) {
            const targetElement = e.target.closest('.boxSequence__element');
            if (targetElement) {
                targetElement.classList.remove('replace'); // Remover clase 'replace' cuando se sale del área
            }
        });

        boxSequence.addEventListener('drop', function (e) {
            e.preventDefault(); // Prevenir el comportamiento por defecto

            const draggedElementId = e.dataTransfer.getData('text/plain'); // Obtener el ID del elemento arrastrado
            const draggedElement = document.querySelector(`.boxSequence__element[data-sequence-id="${draggedElementId}"]`); // Seleccionar el elemento arrastrado

            const targetElement = e.target.closest('.boxSequence__element'); // Obtener el elemento objetivo

            if (targetElement && draggedElementId !== targetElement.dataset.sequenceId) {
                // Intercambiar posiciones entre elementos arrastrado y objetivo
                const draggedNextSibling = draggedElement.nextSibling;
                const targetNextSibling = targetElement.nextSibling;

                boxSequence.insertBefore(draggedElement, targetNextSibling); // Mover arrastrado a la posición del objetivo
                boxSequence.insertBefore(targetElement, draggedNextSibling); // Mover objetivo a la posición del arrastrado

                updatePositions(boxSequence); // Actualizar las posiciones después de mover solo para este contenedor
            }

            // Remover la clase 'replace' de todos los elementos al final del drop
            sequenceElements.forEach(el => el.classList.remove('replace'));
        });

        // Lógica para verificar si el orden es correcto
        const checkOrderButton = boxSequence.parentNode.querySelector('.check-order-button');
        if (checkOrderButton) { 
            checkOrderButton.addEventListener('click', function () {
                const correctOrder = JSON.parse(boxSequence.dataset.sequenceCorrect); 
                const currentOrder = Array.from(boxSequence.querySelectorAll('.boxSequence__element')).map(el => parseInt(el.dataset.sequenceId)); 
                const sequenceElements = Array.from(boxSequence.querySelectorAll('.boxSequence__element')); 
                const isCorrect = JSON.stringify(correctOrder) === JSON.stringify(currentOrder); 

                // Crear un arreglo para almacenar los resultados de cada elemento
                let elementResults = [];

                sequenceElements.forEach((el, index) => {
                    const currentElementId = parseInt(el.dataset.sequenceId);  
                    const correctElementId = correctOrder[index];  

                    // Solo pintar los elementos que están en la posición incorrecta
                    if (currentElementId !== correctElementId) {
                        el.classList.add('trueOrFalse__item--false'); 
                        elementResults.push(false); // Guardar false si el elemento está mal colocado
                    } else {
                        el.classList.remove('trueOrFalse__item--false'); 
                        el.classList.add('trueOrFalse__item--true'); 
                        elementResults.push(true); // Guardar true si el elemento está bien colocado
                    }
                });

                
                window.evaluationState[evaluationId] = {
                    answered: true,
                    type: "Secuencia",
                    selection: [
                        currentOrder,  
                        elementResults
                    ] ,
                    correct: isCorrect, 
                };
                console.log(window.evaluationState);

                if (!isCorrect) {
                    const feedbackContainer = boxSequence.parentNode.querySelector('.hiddenRetro');
                    if (feedbackContainer) {
                       feedbackContainer.classList.remove('active'); 
                   }
                   const errorSound = new Audio('/audios/error_sequence.mp3');
                   errorSound.play();
                } else{
                   const successSound = new Audio('/audios/correct.mp3');
                   successSound.play();
                }

                sequenceElements.forEach(el => {
                    el.style.pointerEvents = 'none';
                    el.setAttribute('draggable', false);
                });

                checkOrderButton.parentNode.classList.add('inactive');
            });
        }
    });

    function updatePositions(boxSequence) {
        const updatedElements = boxSequence.querySelectorAll('.boxSequence__element'); // Solo seleccionar elementos dentro del contenedor actual
        updatedElements.forEach((el, index) => {
            const textNumber = el.querySelector('.boxSequence__textNumber');
            textNumber.textContent = index + 1; // Actualizar la posición basada en el índice actual
        });
    }
});
