let windowsTitle = document.querySelector(".windows__title--closed");
let windowsAnswer = document.querySelector(".header__loginItem--closed");
const buttonLogout = document.querySelector(".nav__item--closed"); 
const question = "¿Quieres salir de la aplicación?";
const anwser = "Si, salir";

let xmark = document.querySelector('.closeText');
let cancelButton = document.querySelector('.windows__cancel');
let cancelOptions = [cancelButton, xmark];


//Se cancela la acción. Se cierra la ventana emergente.
cancelOptions.forEach( (cancelBtn) => {
	cancelBtn.addEventListener('click', () =>{
		windowsTitle.textContent = "¿Quieres eliminar el elemento?";
	 	windowsAnswer.textContent = "Si, eliminar";
	})
});

buttonLogout.addEventListener("click", ()=>{
	 windowsTitle.textContent = question;
	 windowsAnswer.textContent = anwser;
});