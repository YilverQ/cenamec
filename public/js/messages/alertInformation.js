const alert_xmark = document.getElementById('alert__xmark');
const alertInformation = document.querySelector(".alertInformation");

//Cuando hacemos click en el elemento, ocultamos el mensaje
alert_xmark.addEventListener('click', function (){
	alertInformation.classList.add('hidden');
});