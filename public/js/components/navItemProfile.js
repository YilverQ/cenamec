/*Variables*/
let navItems = document.querySelectorAll(".buttonsNav__item");
let boxContainerProfile = document.querySelectorAll(".container_profile");

navItems.forEach( (item, key) =>{
	item.addEventListener('click', ()=>{
		uncheckItems();
		checkItem(key);
	});
});

function uncheckItems() {
	navItems.forEach( (item, key) =>{
		item.classList.remove("buttonsNav__item--contrast");
		boxContainerProfile[key].classList.add("hidden");
	})
}

function checkItem(key){
	navItems[key].classList.add("buttonsNav__item--contrast");
	boxContainerProfile[key].classList.remove("hidden");
}