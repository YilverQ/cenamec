let loadingWindows = document.getElementById('loading');
loadingWindows.classList.add('show');

// Al cargar la página, asegúrate de que el cargador esté oculto
window.addEventListener('load', function() {
    const loading = document.getElementById('loading');
    loading.classList.remove('show'); // Asegúrate de que el cargador esté oculto
});