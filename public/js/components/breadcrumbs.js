window.addEventListener('load', function() {
    const breadcrumbs = document.querySelector('.breadcrumbs__boxList');
    
    if (breadcrumbs) { // Verifica si el elemento existe
        const isMobile = window.innerWidth <= 700;

        if (isMobile) {
            breadcrumbs.scrollLeft = breadcrumbs.scrollWidth; // Desplaza el scroll al final
        }
    }
});