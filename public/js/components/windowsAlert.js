let xmark = document.querySelector('.closeText');
let cancelButton = document.querySelector('.windows__cancel');
let sheetWindows = document.querySelector('.sheetWindows');
let deleteButton = document.querySelector('.windows__confirm');
let cancelOptions = [cancelButton, xmark];
let formsDelete = document.querySelectorAll(".form__delete");
let modalTitle = document.querySelector('.windows__title'); 
let modalConfirmText = document.querySelector('.header__loginItem--contrast'); 

cancelOptions.forEach((cancelBtn) => {
    cancelBtn.addEventListener('click', () => {
        sheetWindows.classList.toggle('sheetWindows--hidden');
    });
});

formsDelete.forEach((formDelete) => {
    formDelete.addEventListener('click', (e) => {
        e.preventDefault();

        if (formDelete.classList.contains('form__delete--disabled')) {
            modalTitle.textContent = '¿Quieres deshabilitar el elemento?'; 
            modalConfirmText.textContent = 'Si, deshabilitar'; 
        } else {
            modalTitle.textContent = '¿Quieres eliminar el elemento?'; 
            modalConfirmText.textContent = 'Si, eliminar'; 
        }

        sheetWindows.classList.toggle('sheetWindows--hidden'); 

        deleteButton.addEventListener('click', () => {
            formDelete.submit(); 
        });
    });
});
