// script.js
document.querySelectorAll('a').forEach(link => {
    link.addEventListener('click', function(event) {
        event.preventDefault(); // Evita que el enlace se siga inmediatamente

        const loading = document.getElementById('loading');
        loading.classList.add('show'); // Muestra el cargador

        // Redirige después de un pequeño retraso para que el efecto sea visible
        setTimeout(() => {
            window.location.href = this.href; // Navega a la nueva página
            
        }, 50);

        setTimeout(() => {
            loading.classList.remove('show'); // Asegúrate de que el cargador esté oculto
        }, 1000);

    });
});

// Al cargar la página, asegúrate de que el cargador esté oculto
window.addEventListener('load', function() {
    const loading = document.getElementById('loading');
    loading.classList.remove('show'); // Asegúrate de que el cargador esté oculto
});
