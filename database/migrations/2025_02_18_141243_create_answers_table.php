<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->id();
            $table->boolean("is_correct")->default(false);
            $table->float('percentage')->nullable();
            
            // Foreign keys
            $table->unsignedBigInteger('view_content_id');
            $table->foreign('view_content_id')
                ->references('id')
                ->on('view_content')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('item_evaluation_id');
            $table->foreign('item_evaluation_id')
                            ->references('id')
                            ->on('item_evaluations')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answers');
    }
};
