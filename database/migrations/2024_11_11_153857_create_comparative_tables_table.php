<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comparative_tables', function (Blueprint $table) {
            $table->id();
            $table->string("columnA");
            $table->string("columnB");
            $table->string("columnC");
            $table->unsignedBigInteger('item_evaluation_id');
            $table->foreign('item_evaluation_id')
                            ->references('id')
                            ->on('item_evaluations')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comparative_tables');
    }
};
