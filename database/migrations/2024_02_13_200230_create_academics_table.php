<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('academics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                            ->references('id')
                            ->on('users')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedBigInteger('institution_id');
            $table->foreign('institution_id')
                            ->references('id')
                            ->on('institutions')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedBigInteger('academiclevel_id');
            $table->foreign('academiclevel_id')
                            ->references('id')
                            ->on('academiclevels')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedBigInteger('career_id');
            $table->foreign('career_id')
                            ->references('id')
                            ->on('careers')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedBigInteger('area_id');
            $table->foreign('sector_id')
                            ->references('id')
                            ->on('sectors')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedBigInteger('sector_id');
            $table->foreign('area_id')
                            ->references('id')
                            ->on('areas')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->year("year_entry");
            $table->year("year_exit")->nullable();
            $table->string("academic_title");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('academics');
    }
};
