<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answer_true_false', function (Blueprint $table) {
            $table->id();
            $table->string("selection");
            $table->boolean("is_correct");

            $table->unsignedBigInteger('true_false_id');
            $table->foreign('true_false_id')
                            ->references('id')
                            ->on('true_falses')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            
            // Foreign keys
            $table->unsignedBigInteger('answer_id');
            $table->foreign('answer_id')
                ->references('id')
                ->on('answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answer_true_false');
    }
};
