<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('likerts', function (Blueprint $table) {
            $table->id();
            $table->string("affirmation");
            $table->unsignedBigInteger('autoevaluation_id');
            $table->foreign('autoevaluation_id')
                            ->references('id')
                            ->on('autoevaluations')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('likerts');
    }
};
