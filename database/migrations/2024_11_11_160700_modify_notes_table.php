<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('notes', function (Blueprint $table) {
            // Eliminar las claves foráneas
            $table->dropForeign(['teacher_id']);
            $table->dropForeign(['module_id']);
            
            //Eliminar las columnas
            $table->dropColumn('teacher_id'); 
            $table->dropColumn('module_id'); 
            $table->dropColumn('level'); 

            // Agregar la clave foránea para curso_id
            $table->unsignedBigInteger('content_id')->nullable();
            $table->foreign('content_id')
                            ->references('id')
                            ->on('contents')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('notes', function (Blueprint $table) {
            // Creamos el campo de teacher_id
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->foreign('teacher_id')
                  ->references('id')
                  ->on('teachers')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            // Creamos el campo de module_id
            $table->unsignedBigInteger('module_id')->nullable();
            $table->foreign('module_id')
                            ->references('id')
                            ->on('modules')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            // Creamos nuevamente el campo level
            $table->integer("level")->nullable();

            // Eliminamos la clave foránea
            $table->dropForeign(['content_id']);

            // Luego eliminamos la columna
            $table->dropColumn('content_id');
        });
    }
};
