<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answer_matching', function (Blueprint $table) {
            $table->id();
            $table->string("elementA");
            $table->string("elementB");
            $table->boolean("is_correct");
            
            // Foreign keys
            $table->unsignedBigInteger('answer_id');
            $table->foreign('answer_id')
                ->references('id')
                ->on('answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('matching_id');
            $table->foreign('matching_id')
                            ->references('id')
                            ->on('matchings')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answer_matching');
    }
};
