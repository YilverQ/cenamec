<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('content_student', function (Blueprint $table) {
            $table->id();
            $table->string("state")->default("Visto");
            $table->date("viewed_at")->default(now());
            $table->time("completed_time")->nullable();
            
            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')
                            ->references('id')
                            ->on('students')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->unsignedBigInteger('content_id');
            $table->foreign('content_id')
                            ->references('id')
                            ->on('contents')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('content_student');
    }
};
