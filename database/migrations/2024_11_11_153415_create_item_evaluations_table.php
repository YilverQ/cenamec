<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item_evaluations', function (Blueprint $table) {
            $table->id();
            $table->string("statement");
            $table->string("feedback");
            $table->integer('orden');
            
            //Tipo ENUM
            $table->enum('type', ['Verdadero o Falso', 
                                  'Selección Simple', 
                                  'Respuesta Corta',
                                  'Cuadro Comparativo',
                                  'Secuencia',
                                  'Emparejamiento']);
            
            $table->unsignedBigInteger('interactive_evaluation_id');
            $table->foreign('interactive_evaluation_id')
                            ->references('id')
                            ->on('interactive_evaluations')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item_evaluations');
    }
};
