<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answer_selection_simple', function (Blueprint $table) {
            $table->id();
            $table->string("selection");
            $table->boolean("is_correct");

            // Foreign keys
            $table->unsignedBigInteger('answer_id');
            $table->foreign('answer_id')
                ->references('id')
                ->on('answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('selection_simple_id');
            $table->foreign('selection_simple_id')
                            ->references('id')
                            ->on('selection_simples')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
                            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answer_selection_simple');
    }
};
