<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('courses', function (Blueprint $table) {
            // Agregar nuevo campo que será el reemplazo del modificado.
            $table->enum('state', ['En Desarrollo', 'Publicado', 'Archivado'])
                  ->default('En Desarrollo'); 

            // Cambiar el campo 'disabled' por 'Estado_del_Curso'
            $table->dropColumn('disabled'); // Eliminar el campo 'disabled'

            // Agregar nuevos campos
            $table->string('tag')->nullable(); 
            $table->string('target_audience')->nullable(); 
            $table->string('level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('courses', function (Blueprint $table) {
            // Volver a agregar el campo 'disabled'
            $table->boolean('disabled')->default(false);

            // Eliminar el nuevo campo 'state'
            $table->dropColumn('state');

            // Eliminar los nuevos campos añadidos
            $table->dropColumn(['tag', 'target_audience', 'level']);
        });
    }
};
