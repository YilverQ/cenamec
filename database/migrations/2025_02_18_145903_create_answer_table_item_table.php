<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answer_table_item', function (Blueprint $table) {
            $table->id();
            $table->string("column");
            $table->boolean("is_correct");
            
            // Foreign keys
            $table->unsignedBigInteger('answer_id');
            $table->foreign('answer_id')
                ->references('id')
                ->on('answers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('table_item_id');
            $table->foreign('table_item_id')
                            ->references('id')
                            ->on('table_items')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answer_table_item');
    }
};
