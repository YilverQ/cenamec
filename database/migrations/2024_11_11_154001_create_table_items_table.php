<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_items', function (Blueprint $table) {
            $table->id();
            $table->string("description");
            $table->string("column");
            $table->unsignedBigInteger('comparative_table_id');
            $table->foreign('comparative_table_id')
                            ->references('id')
                            ->on('comparative_tables')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_items');
    }
};
