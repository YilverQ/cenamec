<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Obtener los duplicados
        $duplicates = DB::table('certificates')
            ->select('course_id', 'student_id', DB::raw('MIN(id) as min_id'))
            ->groupBy('course_id', 'student_id')
            ->havingRaw('COUNT(*) > 1')
            ->get();

        // Eliminar los duplicados
        foreach ($duplicates as $duplicate) {
            DB::table('certificates')
                ->where('course_id', $duplicate->course_id)
                ->where('student_id', $duplicate->student_id)
                ->where('id', '!=', $duplicate->min_id) // Mantener solo el registro con el ID mínimo
                ->delete();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('certificates', function (Blueprint $table) {
            //No se puede revertir esta acción
        });
    }
};
