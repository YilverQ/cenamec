<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Teacher;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Teacher::create(['user_id' => 1]); //Yilver
        Teacher::create(['user_id' => 2]); //Vanessa
        Teacher::create(['user_id' => 8]); //Barbara
        Teacher::create(['user_id' => 9]); //Pedro
        Teacher::create(['user_id' => 10]); //Jessica
        Teacher::create(['user_id' => 11]); //Orlando
    }
}
