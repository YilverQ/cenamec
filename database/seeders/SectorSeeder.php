<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Sector;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Sector::create(['name' => 'Ambiental']);
        Sector::create(['name' => 'Cultural']);
        Sector::create(['name' => 'Económico']);
        Sector::create(['name' => 'Educativo']);
        Sector::create(['name' => 'Geográfico']);
        Sector::create(['name' => 'Militar']);
        Sector::create(['name' => 'Político']);
        Sector::create(['name' => 'Salud']);
        Sector::create(['name' => 'Social']);
        Sector::create(['name' => 'Tecnológico']);

    }
}
