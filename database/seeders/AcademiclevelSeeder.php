<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Academiclevel;

class AcademiclevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*Niveles Acádemicos*/ 
        Academiclevel::create(['name' => 'Pregrado']);
        Academiclevel::create(['name' => 'Técnico Superior Universitario']);
        Academiclevel::create(['name' => 'Ingeniería']);
        Academiclevel::create(['name' => 'Licenciatura']);
        Academiclevel::create(['name' => 'Especialización']);
        Academiclevel::create(['name' => 'Maestría']);
        Academiclevel::create(['name' => 'Doctorado']);
    }
}
