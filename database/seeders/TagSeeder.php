<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('tags')->insert([
            ['name' => 'Matemáticas', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Ciencias', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Literatura', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Ciencias Naturales', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Física', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Química', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Biología', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Tecnología', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Ingeniería', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Robótica', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Programación', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Ciencias de la Computación', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Inteligencia Artificial', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Ciencias de la Tierra', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], 
            ['name' => 'Astronomía', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()], 
            ['name' => 'Biotecnología', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Artes', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Humanidades', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Salud', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Psicología', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ]);
    }
}
