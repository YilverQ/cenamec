<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * Los seeder deben ejecutarse según el orden de las migraciones.
     * $this->call(ModelSeeder::class);
     */
    public function run(): void
    {
        $this->call([
            AudienceSeeder::class,
            TagSeeder::class,
        ]);
       
    }
}
