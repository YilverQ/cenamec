<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create(['firts_name' => ucwords(strtolower('Yilver')),
            'lastname'      => ucwords(strtolower('Quevedo')),
            'gender'        => 'Masculino',
            'birthdate'     => '09/06/2001',
            'nacionality' => 'V',
            'identification_card' => '28333459',
            'number_phone'  => '04160140472',
            'email'         => strtolower('yilver0906@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '49',
            'parishe_id'    => '670'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Vanessa')),
            'lastname'      => ucwords(strtolower('Longa')),
            'gender'        => 'Femenino',
            'birthdate'     => '09/06/2001',
            'nacionality' => 'V',
            'identification_card' => '27914751',
            'number_phone'  => '04127603410',
            'email'         => strtolower('vanessa.longa06@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '46',
            'parishe_id'    => '639'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Biagni')),
            'lastname'      => ucwords(strtolower('Abano')),
            'gender'        => 'Femenino',
            'birthdate'     => '31/08/2001',
            'nacionality' => 'V',
            'identification_card' => '30496800',
            'number_phone'  => '04123744425',
            'email'         => strtolower('biagniabano1@gmail.com'),
            'password'      => Hash::make('usuario123'),
            'profileimg_id' => '46',
            'parishe_id'    => '667'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Melanie')),
            'lastname'      => ucwords(strtolower('Rodríguez')),
            'gender'        => 'Femenino',
            'birthdate'     => '19/10/2000',
            'nacionality' => 'V',
            'identification_card' => '29743133',
            'number_phone'  => '04127008380',
            'email'         => strtolower('alexandrapetra2000@gmail.com'),
            'password'      => Hash::make('usuario123'),
            'profileimg_id' => '46',
            'parishe_id'    => '349'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Juan')),
            'lastname'      => ucwords(strtolower('Castillo')),
            'gender'        => 'Masculino',
            'birthdate'     => '27/10/1997',
            'nacionality' => 'V',
            'identification_card' => '26268311',
            'number_phone'  => '04147111664',
            'email'         => strtolower('juan.daniel2661@gmail.com'),
            'password'      => Hash::make('usuario123'),
            'profileimg_id' => '49',
            'parishe_id'    => '670'
        ]);


        User::create(['firts_name' => ucwords(strtolower('Yefferson')),
            'lastname'      => ucwords(strtolower('Quevedo')),
            'gender'        => 'Masculino',
            'birthdate'     => '23/12/1997',
            'nacionality' => 'V',
            'identification_card' => '26334043',
            'number_phone'  => '04123083565',
            'email'         => strtolower('yeffersonq5@gmail.com'),
            'password'      => Hash::make('usuario123'),
            'profileimg_id' => '49',
            'parishe_id'    => '669'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Victoria')),
            'lastname'      => ucwords(strtolower('Rodríguez')),
            'gender'        => 'Femenino',
            'birthdate'     => '07/09/2001',
            'nacionality' => 'V',
            'identification_card' => '29611002',
            'number_phone'  => '04265200250',
            'email'         => strtolower('victoriavalentinarodriguez08@gmail.com'),
            'password'      => Hash::make('usuario123'),
            'profileimg_id' => '46',
            'parishe_id'    => '337'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Barbara')),
            'lastname'      => ucwords(strtolower('Sanz')),
            'gender'        => 'Femenino',
            'birthdate'     => '31/01/2001',
            'nacionality' => 'V',
            'identification_card' => '29973247',
            'number_phone'  => '04124713945',
            'email'         => strtolower('barbarassilvaw@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '46',
            'parishe_id'    => '667'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Pedro Gabriel')),
            'lastname'      => ucwords(strtolower('Velásquez Méndez')),
            'gender'        => 'Masculino',
            'birthdate'     => '13/02/2001',
            'nacionality' => 'V',
            'identification_card' => '31146547',
            'number_phone'  => '04242794276',
            'email'         => strtolower('gvmpeter@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '46',
            'parishe_id'    => '334'
        ]);

        User::create(['firts_name' => ucwords(strtolower('Jessica')),
            'lastname'      => ucwords(strtolower('Pantoja')),
            'gender'        => 'Femenino',
            'birthdate'     => '05/06/2001',
            'nacionality' => 'V',
            'identification_card' => '27914797',
            'number_phone'  => '04123873985',
            'email'         => strtolower('jessicapf676@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '46',
            'parishe_id'    => '347'
        ]);


        User::create(['firts_name' => ucwords(strtolower('Orlando')),
            'lastname'      => ucwords(strtolower('Cáceres')),
            'gender'        => 'Masculino',
            'birthdate'     => '29/03/2003',
            'nacionality' => 'V',
            'identification_card' => '29861028',
            'number_phone'  => '04242083041',
            'email'         => strtolower('orlandwcc@gmail.com'),
            'password'      => Hash::make('#cenamec#'),
            'profileimg_id' => '49',
            'parishe_id'    => '639'
        ]);
    }
}
