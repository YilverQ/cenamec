<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Area;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Area::create(['name' => 'Artes Y Humanidades']);
        Area::create(['name' => 'Ciencias Agrícolas']);
        Area::create(['name' => 'Ciencias Del Deporte']);
        Area::create(['name' => 'Ciencias De La Educación']);
        Area::create(['name' => 'Ciencias Médicas Y De La Salud']);
        Area::create(['name' => 'Ciencias Naturales']);
        Area::create(['name' => 'Ciencias Sociales']);
        Area::create(['name' => 'Ciencias Y Artes Militares']);
        Area::create(['name' => 'Ingeniería, Arquitectura Y Tecnología']);
    }
}
