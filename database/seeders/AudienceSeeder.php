<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AudienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
         DB::table('audiences')->insert([
            ['name' => 'Niños/Niñas', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Adolescentes', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Adultos', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Profesores', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Padres de Familia', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Primer Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Segundo Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Tercer Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Cuarto Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Quinto Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Sexto Grado', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '1° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '2° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '3° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '4° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '5° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => '6° Año de Bachillerato', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Estudiantes Universitarios', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
